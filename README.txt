This is the source for the Sahara web application.  It is based on Pylons a Python based web application framework.

It relies on many components some of which are internal to ARTstor, Inc.

It uses the PostgreSQL database for storing data, and SOLR for providing searching into that data.

Image upload handling is handled by an ARTstor internal image service.  Replacing that service would not be diffcult.

Look up and linking of vocabularies is also handled via an ARTstor internal service.  Those services would be more challenging to replicate.


Installation and Setup
======================

Install ``sahara`` using easy_install::

    easy_install sahara

Make a config file as follows::

    paster make-config sahara config.ini

Tweak the config file as appropriate and then setup the application::

    paster setup-app config.ini

Then you are ready to go.
