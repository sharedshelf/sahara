"""Routes configuration

The more specific and detailed routes should be defined first so they
may take precedent over the more generic routes. For more information
refer to the routes manual at http://routes.groovie.org/docs/
"""
from pylons import config
from routes import Mapper

def make_map():
    """Create, configure and return the routes Mapper"""
    map = Mapper(directory=config['pylons.paths']['controllers'],
                 always_scan=config['debug'])
    map.minimization = False
    
    map.connect('home', '/',
                controller='home',
                action='home')
    map.connect('terms', '/terms',
                controller='home',
                action='terms')
    map.connect('welcome', '/welcome',
                controller='home',
                action='welcome')
    map.connect('privacy', '/privacy.html')
    map.connect('fullterms', '/fullterms.html')
    map.connect('register', '/register',
                controller='login',
                action='register')
    map.connect('registered', '/registered',
                controller='login',
                action='registered')

    map.connect('totals', '/collection/totals',
                controller='home',
                action='object_totals')
    
    # The ErrorController route (handles 404/500 error pages); it should
    # likely stay at the top, ensuring it can always be resolved
    map.connect('/error/{action}', controller='error')
    map.connect('/error/{action}/{id}', controller='error')

    #
    # User authentication
    #
    map.connect('login', '/login',
                controller='login',
                action='login')
    map.connect('logout', '/logout',
                controller='login',
                action='logout')

    #
    # User maintenance
    #
    map.connect('profile_update', '/profile',
                controller='user',
                action='update',
                conditions=dict(method='POST'))
    map.connect('profile', '/profile',
                controller='user',
                action='index')
    map.connect('password', '/profile/passwd',
                controller='user',
                action='password',
                conditions=dict(method='POST'))
    map.connect('priveleged', '/priveleged-users',
                controller='user',
                action='priveleged_users')
    #
    # ANA lookup
    #
    map.connect('ana', '/ana/creator',
                controller="ana", 
                action="creator_map")

    #
    # Role maintenance
    #
    map.connect('roles', '/roles',
                controller='role',
                action='index')
    map.connect('role_update', '/roles/{role_id}/update',
                controller='role',
                action='update',
                conditions=dict(method='POST'))
    map.connect('role_create', '/roles/create',
                controller='role',
                action='create',
                conditions=dict(method='POST'))
    map.connect('role_delete', '/roles/{role_id}/delete',
                controller='role',
                action='delete',
                conditions=dict(method='POST'))
    
    #
    # View Type maintenance
    #
    map.connect('view_types', '/view_types',
                controller='view_type',
                action='index')
    map.connect('view_type_update', '/view_types/{view_type_id}/update',
                controller='view_type',
                action='update',
                conditions=dict(method='POST'))
    map.connect('view_type_create', '/view_types/create',
                controller='view_type',
                action='create',
                conditions=dict(method='POST'))
    map.connect('view_type_delete', '/view_types/{view_type_id}/delete',
                controller='view_type',
                action='delete',
                conditions=dict(method='POST'))
    
    #
    # Attribution maintenance
    #
    map.connect('attributions', '/attributions',
                controller='attribution',
                action='index')
    map.connect('attribution_update', '/attributions/{attribution_id}/update',
                controller='attribution',
                action='update',
                conditions=dict(method='POST'))
    map.connect('attribution_create', '/attributions/create',
                controller='attribution',
                action='create',
                conditions=dict(method='POST'))
    map.connect('attribution_delete', '/attributions/{attribution_id}/delete',
                controller='attribution',
                action='delete',
                conditions=dict(method='POST'))
    
    #
    # Narrow Classification maintenance
    #
    map.connect('narrow_classifications', '/narrow_classifications',
                controller='narrow_classification',
                action='index')
    map.connect('narrow_classification_update', '/narrow_classifications/{narrow_classification_id}/update',
                controller='narrow_classification',
                action='update',
                conditions=dict(method='POST'))
    map.connect('narrow_classification_create', '/narrow_classifications/create',
                controller='narrow_classification',
                action='create',
                conditions=dict(method='POST'))
    map.connect('narrow_classification_delete', '/narrow_classifications/{narrow_classification_id}/delete',
                controller='narrow_classification',
                action='delete',
                conditions=dict(method='POST'))
    
    #
    # Field completion lookups
    #
    map.connect('/lookup/:field',
                controller='search',
                action='lookup')

    #
    # Search
    #
    # Rerouting search to search tab
    # see new tabs section
    map.connect('search', '/search',
                controller='search',
                action='search')

    #
    # Quick stats
    #
    map.connect('stats', '/stats',
                   controller='search', action='bucket_stats')
    

    #
    # Multiple work paths
    #
    map.connect('in_process', '/works;in_process',
                controller='work',
                action='index',
                complete=False,
                template='progressing_tab.mako')
    map.connect('contributed', '/works;contributed',
                controller='work',
                action='index',
                complete=True,
                template='contributed_tab.mako')
    map.connect('count', '/works;count',
                controller='work',
                action='count')
    map.connect('add_dialog', '/works;add_multiple',
                controller='work', 
                action='add_multiple')
    map.connect('feed', '/works/feed/{date}',
                controller='work', 
                action='feed')

    #
    # Individual work paths
    #
    map.connect('/work',
                controller='work',
                action='create',
                conditions=dict(method='POST'))
    map.connect('reserve', '/work/{uuid}/reserve',
                controller='work',
                action='reserve',
                conditions=dict(method='POST'))
    map.connect('unreserve', '/work/{uuid}/unreserve',
                controller='work',
                action='unreserve',
                conditions=dict(method='POST'))
    map.connect('mark_reviewed', '/work/{uuid}/mark_reviewed',
                controller='work',
                action='mark_reviewed',
                conditions=dict(method='POST'))
    map.connect('assign', '/work/{uuid}/assign',
                controller='work',
                action='assign',
                conditions=dict(method='POST'))
    map.connect('contribute', '/work/{uuid}/contribute',
                controller='work',
                action='contribute',
                conditions=dict(method='POST'))
    map.connect('publish', '/work/{uuid}/publish',
                controller='work',
                action='publish',
                conditions=dict(method='POST'))
    map.connect('delete_work', '/work/{uuid}',
                controller='work', 
                action='delete',
                conditions=dict(method='DELETE'))
    map.connect('update', '/work/{uuid}',
                controller='work',
                action='update',
                conditions=dict(method='POST'))
    map.connect('edit', '/work/{uuid}/edit',
                controller='work',
                action='edit',
                template='work_form.mako')
    map.connect('admin_edit', '/work/{uuid}/edit/admin',
                controller='work',
                action='edit',
                template='search/quick_edit.mako',
                force_full_view=True)
    map.connect('show', '/work/{uuid}.{format}',
                controller='work',
                action='show', conditions=dict(method='GET'))
    map.connect('tooltip', '/work/{uuid}/tooltip',
                controller='work', 
                action='tooltip')
    map.connect('suppress', '/work/{uuid}/suppress',
                controller='work',
                action='suppress',
                suppress='true')
    map.connect('unsuppress', '/work/{uuid}/unsuppress',
                controller='work',
                action='suppress',
                suppress='false')
    map.connect('rollback', '/work/{uuid}/rollback/{history_id}',
                controller='work',
                action='rollback')
    map.connect('preview', '/work/{uuid}/preview',
                controller='work',
                action='preview')
    map.connect('publication_destinations', '/work/{uuid}/publication_destinations',
                controller='work',
                action='publication_destination')
    
    #
    # Copy work workflow
    #
    map.connect('copy_execute', '/work/{uuid}/copy',
                controller='work',
                conditions=dict(method='POST'),
                action='copy')
    map.connect('copy_select_fields', '/work/{uuid}/copy',
                controller='work',
                conditions=dict(method='GET'),
                action='copy_select_fields')
    map.connect('copy_select_works', '/work/{uuid}/copy;works',
                controller='work',
                action='copy_select_works')
    map.connect('copy_confirm', '/work/{uuid}/copy;confirm',
                controller='work',
                action='copy_confirm')

    #
    # Editorial copy work workflow
    #
    map.connect('editorial_copy_execute', '/editorial/work/{uuid}/copy',
                controller='work',
                conditions=dict(method='POST'),
                action='copy',
                owned_only=False)
    map.connect('editorial_copy_select_fields', '/editorial/work/{uuid}/copy',
                controller='work',
                conditions=dict(method='GET'),
                action='copy_select_fields',
                owned_only=False)
    map.connect('editorial_copy_select_works', '/editorial/work/{uuid}/copy;works',
                controller='work',
                action='copy_select_works',
                owned_only=False)
    map.connect('editorial_copy_confirm', '/editorial/work/{uuid}/copy;confirm',
                controller='work',
                action='copy_confirm',
                owned_only=False)

    #
    # Multicontribute workflow
    #
    map.connect('multicontribute', '/works/contribute;list',
                controller='work',
                action='index',
                complete=False,
                valid=True,
                template='dialogs/contribute/multicontribute.mako')
    map.connect('/works/contribute;confirm',
                controller='work',
                action='multicontribute_confirm');
    map.connect('multicontribute_execute', '/works/contribute',
                controller='work', 
                action='multicontribute')

    #
    # Multireserve
    # 
    map.connect('multi-reserve', '/works/multi-reserve', controller='work',
                action='multireservation', reserve='true')
    map.connect('multi-unreserve', '/works/multi-unreserve', controller='work',
                action='multireservation', reserve='false')
    
    #
    # Multideletion workflow
    #
    map.connect('multidelete', '/works/delete;list',
                controller='work',
                action='index',
                complete=False,
                template='dialogs/delete/multidelete.mako')
    map.connect('/works/delete;confirm',
                controller='work',
                action='multidelete_confirm');
    map.connect('multidelete_execute', '/works/delete',
                controller='work', 
                action='multidelete')

    
    # Special route for flash based image upload
    map.connect('flash_upload', '/flash/works',
                controller='upload',
                action='flash');
    
    # These routes are used for duplicating fields on the work form
    map.connect('/form/new_creator/{index}',
                controller='work_form',
                action='new_creator')
    map.connect('/form/new_complex_title/{index}',
                controller='work_form',
                action='new_complex_title')
    map.connect('/form/new_title/{index}',
                controller='work_form',
                action='new_title')
    map.connect('/form/new_location_country/{index}',
                controller='work_form',
                action='new_location_country')
    map.connect('/form/new_period_dynasty/{index}',
                controller='work_form',
                action='new_period_dynasty')
    map.connect('/form/new_creator_nationality/{creator_index}/{index}',
                controller='work_form',
                action='new_creator_nationality')
    map.connect('/form/new_creator_extent/{creator_index}/{index}',
                controller='work_form',
                action='new_creator_extent')
    
    # Image functions
    map.connect('replace', '/work/{uuid}/media',
                controller='media',
                action='replace',
                condition=dict(method='POST'))
    map.connect('/work/{uuid}/media',
                controller='media',
                action='show',
                size=0)
    map.connect('/work/{uuid}/media/size{size}',
                controller='media',
                action='show')
    map.connect('/work/{uuid}/media/flip',
                controller='media',
                action='flip')
    map.connect('/work/{uuid}/media/cinema',
                controller='media',
                action='cinema')

    # Tabbed interface routing
    map.connect('/home', controller='home', action='home')
    map.connect('/contribute', controller='home', action='contribute')
    map.connect('bucket', '/edit', controller='search',
                action='editorial_bucket')
    map.connect('/administer', controller='home', action='administer')

    # Default mappings
#    map.connect('/{controller}/{action}/{uuid}')

    map.connect('/{controller}/{action}')
    #map.connect('/{controller}/{action}/{id}')

    return map
