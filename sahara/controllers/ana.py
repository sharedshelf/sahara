import logging

import simplejson

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons import g

from sahara.lib.base import BaseController, render

from SOAPpy import WSDL

log = logging.getLogger(__name__)

wsdlFile = g.WSDL_ADDR + '/anaservice-1/AnaMatchService?wsdl'
server = WSDL.Proxy(wsdlFile)

class AnaController(BaseController):

    def creator_map(self):
        response.headers['content-type'] = 'application/json'
        return simplejson.dumps(self._get_creator_map(request.params['query']));

    def _get_creator_map(self, name):
        try:
            return self._unwrap_soap_crap(server.getMatchesMap(name+'*'))
        except:
            log.error("Exception thrown in ana lookup name='%s'" % name)
            return []

    def _unwrap_soap_crap(self, result):
        """Unwrap SOAPpy wrappers and produce an array of dict()s"""
        results = []
        lines = result[0]
        # Singular results come back unwrapped
        if len(lines) > 1:
            for line in lines:
                results.append(self._parse_values(line.entry))
        else:
            results.append(self._parse_values(lines[0]))

        return dict(ResultSet=dict(Results=results))

    def _parse_values(self, entries):
        data = dict()
        for e in entries:
            if 'preferred' == e['key']:
                data['name'] = e['value'].split('(')[0].strip()
            data[e['key']] = e['value']
        return data

