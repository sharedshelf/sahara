import logging

from sahara.lib.base import *
from sahara.model import Attribution, Session as db
from sqlalchemy.orm.exc import NoResultFound
from sahara.lib.access_control import restricted_to

log = logging.getLogger(__name__)

class AttributionController(BaseController):

    def __before__(self, action, **params):
        super(AttributionController, self).__before__(action)
        if not session['user'].is_admin:
           log.warn("Unprivileged user(%s) trying to access admin function" % session['user'].id)
           abort(403, 'Permission denied')

    @jsonify
    @restricted_to(g.ADMINS)
    def index(self):
        """Generate listings of attributions."""
        attributions = db.query(Attribution).order_by('value').all()
        
        attributions_dict = {}
        for a in attributions:
            attributions_dict[a.id] = a.value
        return attributions_dict
    
    @jsonify
    @restricted_to(g.ADMINS)
    def create(self):
        """Create a new attribution."""
        try:
            attribution = Attribution()
            attribution.value = request.POST['value']
            db.add(attribution)
            db.commit()
            return { attribtution.id: attribution.value }
        except:
            log.critical("Failure to create attribution.", exc_info=True)
            return dict(message='An error occurred creating the attribution.')
        
    @jsonify
    @restricted_to(g.ADMINS)
    def update(self, attribution_id):
        """Update attribution with a particular id."""
        try:
            attribution = db.query(Attribution).filter_by(id = attribution_id).one()
            attribution.value = request.POST['value']
            db.add(attribution)
            db.commit()
            return dict(success=True, message='Attribution successfully updated.')
            
        except NoResultFound:
            return abort(404, "No attribution found with that id.")
        except:
            log.critical("Failure to update attribution", exc_info=True)
            return dict(message='An error occurred saving the attribution.',
                        attribution_id=attribution_id)
    
    @jsonify
    @restricted_to(g.ADMINS)
    def delete(self, attribution_id):
        """Delete attribution with a particular id."""
        try:
            attribution = db.query(Attribution).filter_by(id = attribution_id).one()
            db.delete(attribution)
            db.commit()
            return dict(success=True, message='Attribution successfully deleted.')
        
        except NoResultFound:
            response.status = 404
            return dict(success=False, message="No attribution found with that id.")
        except:
            log.critical("Failure to delete attribution", exc_info=True)
            return dict(message='An error occurred deleting the attribution.',
                        attribution_id=attribution_id)
