import logging

import simplejson
from sahara.lib.base import *
from sahara.model import Session as db
from sahara.model import Work
from sahara.lib import helpers as h
from sahara.lib.search_engine import SearchEngine

log = logging.getLogger(__name__)

class EditorialBucketController(BaseController):
    DEFAULT_FIELDS = ['uuid', 'media_uuid', 'suppress', 'creator_name', 'title',
                      'city_county', 'country', 'display_date', 'contributor', 'assignment',
                      'reserved_by']

    def bucket_search(self, locked_only=False, sort=None):
        """
        Run a simple search, and render results.

        Uses SOLR to run the actual search and solrpy library to paginate
        the response.
        """
        if not session['user'].is_privileged:
            response.status = 401
            return dict(complete=False, uuid=uuid, message='Insufficient authorization level.')
        else:
            query = session['user'].retrieve_editorial_bucket_query()
            print "QUERY: %s" % query
            s = SearchEngine(g.SOLR_URL)
            limit = min(300, h.string_to_int(request.params.get('rows', '25'), default=25))
            page = h.string_to_int(request.params.get('page', '1'), default=1)
            try:
                paginated_response = s.query(query.encode('utf-8'),
                                             rows=limit,
                                             fields=self.DEFAULT_FIELDS,
                                             highlight=self.DEFAULT_FIELDS)
                log.info("Search returned %d results" % paginated_response.count)
                rdata = dict(phrase=query,
                             paginated_response=paginated_response,
                             results=paginated_response.page(page),
                             rows=paginated_response.page_size)
                return render('edit_tab.mako', rdata)
            except:
                log.error('Problem querying SOLR', exc_info=True)
                return render('edit_tab.mako', dict(
                        username=session['user'].email,
                        paginated_response=paginated_response,
                        phrase=query,
                        error=True))
                              



