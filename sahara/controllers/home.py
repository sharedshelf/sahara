import logging

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons import g

from pylons.error import error_template
from pylons.middleware import error_mapper, ErrorDocuments, ErrorHandler

from sahara.lib.base import BaseController, render, jsonify
from sahara.lib.authentication import artstor
from sahara.lib.access_control import *
from sahara.model import search, WorkPublication, History, Session as db

log = logging.getLogger(__name__)

class HomeController(BaseController):
    unrestricted = ['terms', 'object_totals']
    
    def contribute(self):
        """Render the contribute tab"""
        return render('contribute_tab.mako')

    def home(self):
        """Render the home tab for privledged users, and the contributor page for the rest"""
        if session['user'].is_privileged is True:
            totals = artstor.get_collection_totals()
            user_totals = search.get_totals_for_user(session['user'])
            c.members_collection_total = totals['members_objects']
            c.editors_collection_total = totals['editors_objects']
            c.waiting = user_totals['waiting']
            c.locked = user_totals['reserved']
            c.num_of_contributors = db.query(History.updated_by).distinct().count()
            return render('home_tab.mako')
        else:
            return render('contribute_tab.mako')

    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def search(self):
        """Render the search tab"""
        return render('search_tab.mako')
    
    @restricted_to(g.ADMINS)
    def administer(self):
        """Render the administration tab"""
        return render('admin_tab.mako')

    def terms(self):
        """Render terms and conditions page."""
        if 'user' in session.keys():
            c.username = session['user'].email
        session['seen_terms'] = True
        session.save()
        return render('terms.mako')
        
    def welcome(self):
        """Render the welcome page."""
        log.debug("rendering welcome page")
        return render('welcome.mako')

    @jsonify
    def object_totals(self):
        return artstor.get_collection_totals()
                
