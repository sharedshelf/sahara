import logging

import simplejson

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to

from sahara.lib.base import *
from sahara.lib.authentication import artstor
from routes import url_for
from sahara.model import Session as db, UserCredentials

log = logging.getLogger(__name__)

class LoginController(BaseController):
    """
    Simple login class.

    Only used for development, for production all authentication
    occurs using the SDL site.
    """
    unrestricted = ['login', 'authenticate']
    
    def login(self):
        """Show login form. Submits to /login/authenticate"""
        c.error = ''
        return render('login.mako')


    def authenticate(self):
        """Verify username and password"""
        log.info("Processing login.")
        # Both fields filled?
        form_username = str(request.params.get('username'))
        form_password = str(request.params.get('password'))
        
        # Use artstor authentication module
        auth = artstor.authenticate(form_username,
                                    form_password,
                                    g.ARTSTOR_API+'collection/secure/login')
        
        if auth != None:
            log.info("Successful login. [%s]" % auth['username'])
            session['userid'] = auth['id']
            session['username'] = auth['username']
            session.save()

            response.set_cookie(auth['cookie']['name'],
                                auth['cookie']['value'],
                                path='/')
            # Make user agree to terms
            return redirect_to(url_for('home'))
        else:
            c.error = 'Invalid username/password'
            return render('login.mako')                                

    @jsonify
    def logout(self):
        """Logout the user and redirect to login page"""
        session.clear()
        session.save()
            
        response.delete_cookie(g.ARTSTOR_COOKIE)
        return dict(status='ok', 
                    artstor_logout=g.ARTSTOR_LOGOUT_URL,
                    artstor_login=g.ARTSTOR_LOGIN_URL)



