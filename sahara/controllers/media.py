import logging
import urllib2
import re
import simplejson
from datetime import datetime

import sqlalchemy as sa
from sahara.lib.base import *
from sahara.model import Session as db, Work, Status

from sahara.lib.filereposter import repost
from sahara.lib.filevalidation import *
from sahara.lib.dastor import get_metadata, fix_exif_date

log = logging.getLogger(__name__)

class MediaController(BaseController):
    """Media manipuliation controller

    Handles serving and manipulation the media attached to a Work"""

    def show(self, uuid=None, size='0'):
        try:
            if not self.is_admin():
                work = Work.query().filter_by(created_by=session['userid'],
                                              uuid=uuid).one()
            else:
                work = Work.query().filter_by(uuid=uuid).join(Status).one()
                
            return redirect_to(str(g.ASSET_URL + "%s_size%s" %
                                   (work.media_uuid, size)))
        except sa.orm.exc.NoResultFound:
            response.status = '404 Not Found'
            return 'Invalid UUID'

    @jsonify
    def replace(self, uuid=None):
        response.content_type = 'text/html' # cause ajaxform sucks
        try:
            if not self.is_admin():
                work = Work.query(). \
                    filter_by(uuid=uuid, created_by=session['userid']). \
                    join(Status).filter_by(complete=False).one()
            else:
                work = Work.query().filter_by(uuid=uuid).join(Status).one()

            afile = request.params['asset-file']

            if afile != {}:
                try:
                    validate(afile, g.MIN_IMAGE_SIZE)
                    # Post to dastor
                    data = simplejson.loads(repost(afile.file, g.DASTOR + '/stor'))
                    print data
                    # Get extra data from dastor
                    ex_data = get_metadata(data['id'],
                                           ['CreateDate', 'ImageWidth',
                                            'ImageHeight'])
                    work.media_uuid = data['id']
                    work.media_filename = afile.filename
                    work.media_captured_on = fix_exif_date(ex_data['CreateDate'])

                    try:
                        work.media_category = data['type']
                        work.media_format = data['format']
                    except KeyError:
                        pass

                    db.add(work)
                    db.commit()
                    log.info('Replacement successful')
                    return dict(success=True, type=work.media_category)
                except InvalidImageException:
                    log.warn('Could not get image size from upload')
                    return dict(success=False,
                             message='Invalid or corrupt file')
                except ImageSizeException:
                    log.warn('Rejecting image, too small')
                    return dict(success=False,
                             message='Image does not meet size requirements')
                except:
                    log.warn('General failure replacing asset')
                    return dict(success=False, message='The administrator has been notifed.')

        except sa.orm.exc.NoResultFound:
            response.status = '404 Not Found'
            return 'Invalid UUID'

        
    def flip (self, uuid=None):
        """ Rotate and Flip Dialog"""
        try:
            if not self.is_admin:
                c.work = Work.query(). \
                    filter_by(uuid=uuid, created_by=session['userid']). \
                    join(Status).one()
            else:
                c.work = Work.query().filter_by(uuid=uuid).join(Status).one()

            if 'action' in request.params.keys():
                # Get the UUID from the request params or use the default
                if request.params['uuid'] != 'undefined':
                    dastor_uuid = request.params['uuid']
                else:
                    dastor_uuid = c.work.media_uuid

                # Call DASTOR to do the actual image manipulation
                res = urllib2.urlopen( \
                     g.DASTOR+"/Rotate/%s/%s" \
                         % (request.params['action'], dastor_uuid))
                
                flip_response = res.read()

                if request.params['action'] == 'DONE':
                    # Save the placeholder image as the new preferred image
                    [uuid, filename] = flip_response.split("|",1)
                    c.work.media_uuid = uuid
                    data = get_metadata(uuid, ['ImageWidth',
                                                     'ImageHeight'])
                    c.work.media_width = data['ImageWidth']
                    c.work.media_height = data['ImageHeight']
                    Session.commit()
                else:
                    # Send link to placeholder image
                    return g.ASSET_URL + flip_response
            else:
                return render('dialogs/rotate_and_flip.mako')

        except sa.orm.exc.NoResultFound:
            response.status = '404 Not Found'
            return 'Invalid UUID'


    def cinema (self, uuid=None):
        """ Show Movie Preview """
        try:
            work = Work.query(). \
                filter_by(uuid=uuid, created_by=session['userid']). \
                join(Status).one()           
            url =  g.ASSET_URL + work.media_uuid +"."+ work.media_format     
            return render('theater.mako', {'movie': url, 'width' : work.media_width, 'height': work.media_height})

        except sa.orm.exc.NoResultFound:
            response.status = '404 Not Found'
            return 'Invalid UUID'

    
