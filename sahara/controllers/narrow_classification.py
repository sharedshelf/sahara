import logging

from sahara.lib.base import *
from sahara.model import NarrowClassification, Session as db
from sqlalchemy.orm.exc import NoResultFound
from sahara.lib.access_control import restricted_to

log = logging.getLogger(__name__)

class NarrowClassificationController(BaseController):

    def __before__(self, action, **params):
        super(NarrowClassificationController, self).__before__(action)
        if not session['user'].is_admin:
            log.warn("Unprivileged user(%s) trying to access admin function" % session['user'].id)
            abort(403, 'Permission denied')

    @jsonify
    @restricted_to(g.ADMINS)
    def index(self):
        """Generate listings of roles."""
        narrow_classifications = db.query(NarrowClassification).order_by('value').all()
        
        narrow_classifications_dict = {}
        for n in narrow_classifications:
            narrow_classifications_dict[n.id] = n.value
        return narrow_classifications_dict
        
    @jsonify
    @restricted_to(g.ADMINS)
    def create(self):
        """Create a new narrow classification."""
        try:
            narrow_classification = NarrowClassification()
            narrow_classification.value = request.POST['value']
            db.add(narrow_classification)
            db.commit()
            return { narrow_classification.id: narrow_classification.value }
        except:
            log.critical("Failure to create narrow classification.", exc_info=True)
            return dict(message='An error occurred creating the narrow classification.')

    @jsonify
    @restricted_to(g.ADMINS)
    def update(self, narrow_classification_id):
        """Update narrow classification with a particular id."""
        try:
            narrow_classification = db.query(NarrowClassification).filter_by(id = narrow_classification_id).one()
            narrow_classification.value = request.POST['value']
            db.add(narrow_classification)
            db.commit()
            return dict(success=True, message='Narrow classification successfully updated.')

        except NoResultFound:
            return abort(404, "No narrow classification found with that id.")
        except:
            log.critical("Failure to update narrow classification", exc_info=True)
            return dict(message='An error occurred saving the narrow classification.',
                        narrow_classification_id=narrow_classification_id)

    @jsonify
    @restricted_to(g.ADMINS)
    def delete(self, narrow_classification_id):
        """Delete narrow classification with a particular id."""
        try:
            narrow_classification = db.query(NarrowClassification).filter_by(id = narrow_classification_id).one()
            db.delete(narrow_classification)
            db.commit()
            return dict(success=True, message='Narrow classification successfully deleted.')

        except NoResultFound:
            response.status = 404
            return dict(success=False, message="No narrow classification found with that id.")
        except:
            log.critical("Failure to delete narrow classification", exc_info=True)
            return dict(message='An error occurred deleting the narrow classification.',
                        narrow_classification_id=narrow_classification_id)
