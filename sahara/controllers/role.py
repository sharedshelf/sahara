import logging

from sahara.lib.base import *
from sahara.model import Role, Session as db
from sqlalchemy.orm.exc import NoResultFound
from sahara.lib.access_control import restricted_to

log = logging.getLogger(__name__)

class RoleController(BaseController):

    def __before__(self, action, **params):
        super(RoleController, self).__before__(action)
        if not session['user'].is_admin:
            log.warn("Unprivileged user(%s) trying to access admin function" % session['user'].id)
            abort(403, 'Permission denied')

    @jsonify
    @restricted_to(g.ADMINS)
    def index(self):
        """Generate listings of roles."""
        roles = db.query(Role).order_by('value').all()
        
        roles_dict = {}
        for r in roles:
            roles_dict[r.id] = r.value
        return roles_dict

    @jsonify
    @restricted_to(g.ADMINS)
    def create(self):
        """Create a new role."""
        try:
            role = Role()
            role.value = request.POST['value']
            db.add(role)
            db.commit()
            return { role.id: role.value }
        except:
            log.critical("Failure to create role.", exc_info=True)
            return dict(message='An error occurred creating the role.')

    @jsonify
    @restricted_to(g.ADMINS)
    def update(self, role_id):
        """Update role with a particular id."""
        try:
            role = db.query(Role).filter_by(id = role_id).one()
            role.value = request.POST['value']
            db.add(role)
            db.commit()
            return dict(success=True, message='Role successfully updated.')

        except NoResultFound:
            return abort(404, "No role found with that id.")
        except:
            log.critical("Failure to update role", exc_info=True)
            return dict(message='An error occurred saving the role.',
                        role_id=role_id)

    @jsonify
    @restricted_to(g.ADMINS)
    def delete(self, role_id):
        """Delete role with a particular id."""
        try:
            role = db.query(Role).filter_by(id = role_id).one()
            db.delete(role)
            db.commit()
            return dict(success=True, message='Role successfully deleted.')

        except NoResultFound:
            response.status = 404
            return dict(success=False, message="No role found with that id.")
        except:
            log.critical("Failure to delete role", exc_info=True)
            return dict(message='An error occurred deleting the role.',
                        role_id=role_id)
