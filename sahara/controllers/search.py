import logging

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons import g

from pylons.error import error_template
from pylons.middleware import error_mapper, ErrorDocuments, ErrorHandler

from sahara.lib.base import BaseController, render, jsonify
from sahara.lib.access_control import restricted_to
from sahara.lib.authentication import artstor
from sahara.model import search
from sahara.lib import helpers as h

log = logging.getLogger(__name__)

class SearchController(BaseController):
    VALID_SORTS = ['contribution_date_asc', 'contribution_date_desc', 'contributor_asc', 'contributor_desc', 'title_sort_asc', 'title_sort_desc']
    FILTERS = dict(editable="Images you can edit", unsuppressed="Unsuppressed images", suppressed="Suppressed images",
                   members="Members' Collection images", editors="Editors Choice Collection images")

    def search(self):
        """
        Run a simple search, and render results.

        Uses SOLR to run the actual search and solrpy library to paginate
        the response.
        """
        if not session['user'].is_privileged:
            response.status = 401
            return dict(complete=False, uuid=uuid, message='Insufficient authorization level.')
        else:
            if len(request.params.keys()) is 0:
                return render('search_tab.mako')
            else:
                c.view = request.params.get('view', 'thumb')
                c.rows = min(300, h.string_to_int(request.params.get('rows', '25'), default=25))
                c.page = h.string_to_int(request.params.get('page', '1'), default=1)
                c.filter = request.params.get('filter', None)
                c.filters = self.FILTERS
                c.sort = self._calculate_sort_order(request.params.get('sort', 'contribution_date_asc'))
                c.phrase = request.params['phrase']
                c.perview_works = session['user'].editable_work_ids
                if c.filter is not None:
                    search_query = self._add_filter_critieria(c.phrase, c.filter)
                else:
                    search_query = request.params['phrase']
                r = search.Solr(g.SOLR_URL).keyword_search(search_query, page=c.page, rows=c.rows, sort=c.sort)
                paginated_results = r['paginated_response']
                c.page = c.page if c.page <= paginated_results.num_pages else max(1, paginated_results.num_pages)
                if paginated_results.count > 0:
                    c.results = r['results'].result
                    reserved_by_ids = [x.get('reserved_by') for x in c.results if 'reserved_by' in x.keys()]
                    c.reserved_map = h.map_profile_ids_to_profile_data(reserved_by_ids)
                    c.phrase = request.params['phrase']
                return render('search_results_tab.mako', dict(search_result=paginated_results))
                
                
    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def editorial_bucket(self):
        c.view = request.params.get('view', 'thumb')
        c.rows = min(300, h.string_to_int(request.params.get('rows', '25'), default=25))
        c.page = h.string_to_int(request.params.get('page', '1'), default=1)
        c.sort = self._calculate_sort_order(request.params.get('sort', 'contribution_date_asc'))
        c.locked = 'locked' in request.params
        c.username = session['user'].email
        #try:
        solr_query = session['user'].perview_search_query
        if c.locked is True:
            r = search.query_works_reserved_to_user(session['user'], page=c.page, rows=c.rows, sort=c.sort)
        else:
            r = search.query_works_in_users_bucket(session['user'], page=c.page, rows=c.rows, sort=c.sort)
        paginated_results = r['paginated_response']
        if paginated_results.count > 0:
            c.results = r['results'].result
            reserved_by_ids = [x.get('reserved_by') for x in c.results if 'reserved_by' in x.keys()]
            c.reserved_map = h.map_profile_ids_to_profile_data(reserved_by_ids)
        return render('edit_tab.mako', dict(search_result=paginated_results))
        #except:
        #    c.error=True
        #    c.results_returned = 0
        #    c.response=None
        #    log.error('Problem querying SOLR', exc_info=True)
        #    return render('edit_tab.mako', dict())

    @restricted_to(g.ADMINS)
    def bucket_stats(self):
        s = search.Solr(g.SOLR_URL)
        res = s.search_query('complete:true -reviewed:[* TO *]', rows=1)
        c.total = res.numFound
        c.buckets = dict()
        tally = set()
        from sahara.model import EditorialBucket, Session as db
        for bucket in db.query(EditorialBucket).all():
            c.buckets[bucket.id] = dict(query="complete:true -reviewed:[* TO *] %s" % bucket.query)
            res = s.search_query(c.buckets[bucket.id]['query'], rows=1000000, fields=['uuid'])
            c.buckets[bucket.id]['total'] = res.numFound
            c.buckets[bucket.id]['set'] = set([r['uuid'] for r in res.results])
            tally = tally.union(c.buckets[bucket.id]['set'])
        for key in c.buckets:
            source = c.buckets[key]['set']
            for x in c.buckets:
                if key == x:
                    continue
                if len(source.intersection(c.buckets[x]['set'])) > 0:
                    if c.buckets[key].has_key('overlap'):
                        c.buckets[key]['overlap'].append(dict(ids=source.intersection(c.buckets[x]['set']), num=len(source.intersection(c.buckets[x]['set'])), source=x))
                    else:
                        c.buckets[key]['overlap'] = [dict(ids=source.intersection(c.buckets[x]['set']), num=len(source.intersection(c.buckets[x]['set'])), source=x)]
        c.num_in_buckets = len(tally)
        c.num_unassigned = int(c.total) - c.num_in_buckets
        return render('bucket_stats.mako')
        
        
            
    def lookup(self, field):
        return search.Solr(g.SOLR_URL).lookup(field, request.params['query'], session['user'])

    
    def _calculate_sort_order(self, sort_param):
        if sort_param in self.VALID_SORTS:
            parts = sort_param.split("_")
            return "_".join(parts[:-1]) + " " + parts[-1]
        else:
            return 'contribution_date asc'

    def _add_filter_critieria(self, query, filter_):
        if filter_ == 'editable':
            if session['user'].is_admin:
                return "complete:true %s" % query
            else:
                return "((complete:true && %s) || assignment:%s) && %s" % (session['user'].perview_search_query, session['user'].id, query)
        elif filter_ == 'suppressed':
            return "suppress:true && %s" %  query
        elif filter_ == 'unsuppressed':
            return "suppress:false && %s" %  query
        elif filter_ == 'members':
            return "published_to:%s && %s" % (g.MEMBERS_COLLECTION, query)
        elif filter_ == 'editors':
            return "published_to:%s && %s" % (g.EDITORS_COLLECTION, query)
        else:
            return query
