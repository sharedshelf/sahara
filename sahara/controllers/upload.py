import logging
import simplejson
from datetime import datetime

from beaker.session import CookieSession
from pylons import config

from sahara.lib.base import *
from sahara.lib import form_parser, helpers as h
from sahara.model import Session as db
from sahara.model import Work, Status
from sahara.lib.filereposter import repost
from sahara.lib.filevalidation import *
from sahara.lib.dastor import get_metadata, fix_exif_date, format_exif_date, tag_normalize

log = logging.getLogger(__name__)

class UploadController(BaseController):
    unrestricted = ['flash']
    
    def flash(self):
        """Handle file uploads from YUI uploader, based on Adobe Flash."""
        if 'user' in session:
            user = session['user']
        else:
            session['user'] = self._decode_user(request.params['cookie'])
        if session['user'] is not None:
            works = Work.query().filter_by(created_by=session['user'].id).join(Status). \
                filter(Status.contributed_on == None).count()
            if works < g.MAX_SLOTS:
                afile = request.params['asset_file']
                work = Work(created_by=session['user'].id)
                if afile != {}:
                    try:
                        validate(afile, g.MIN_IMAGE_SIZE)
                        # Store the image in DASTOR
                        rawdata = simplejson.loads(repost(afile, g.DASTOR+'/stor/'))
                        data = tag_normalize(rawdata)
                        work.media_filename = afile.filename
                        work.media_uuid = data['id']
                        work.media_uploaded_on = datetime.now()
                        if data.has_key('created_on'):
                            work.media_captured_on = fix_exif_date(data['created_on'])
                            data['image_date'] = format_exif_date(work.media_captured_on )
                        else:
                            work.media_captured_on = None
                        work.media_category = data['type'] \
                            if data.has_key('type') else None
                        work.media_format = data['format'] \
                           if data.has_key('format') else None
                        form_parser.parse(work, data, userid=session['user'].id)
                        db.add(work)
                        db.commit()
                        return "created\r\nid: %s" % work.uuid
                    except InvalidImageException:
                        log.warn('Could not get image size from upload')
                        return 'Error: Invalid or Corrupt Image File'
                    except ImageSizeException:
                        log.warn('Rejecting image, too small')
                        return 'Error: image does not meet size requirements'
            else:
                log.warn('Rejecting upload, queue is full')
                return 'Error: Queue is full'
        else:
            log.error('Invalid user during create request')
            abort(500, 'Invalid user')

    def _decode_user(self, raw_cookie):
        """
        Decode the encrypted Beaker session by hand.

        For special cases when cookies can't be passed as part of the
        request headers (Such as flash based image upload) this function
        will create a new session and decode the session information by
        hand.
        
        Parameters:
    
        raw_cookie - This takes the form of 'session.key=encrypted_cookie_data'.

        """
        
        try:
            ses = CookieSession({'cookie':str(raw_cookie)},
                                key=config['beaker.session.key'],
                                encrypt_key=config['beaker.session.encrypt_key'],
                                validate_key=config['beaker.session.validate_key'])

            if 'user' in ses:
                return ses['user']
            else:
                return None
        except:
            return None
