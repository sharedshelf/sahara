import logging

import urllib, urllib2

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons.decorators import jsonify

from sahara.lib.base import BaseController, render, g
from sahara.lib.authentication import artstor
from routes import url_for
from sahara.model import Session as db, UserCredentials

log = logging.getLogger(__name__)

class UserController(BaseController):

    def index(self):
        """
        Render the user profile editor.

        Call the SDL API for the users profile information, and render
        the profile editor using the extracted information.
        """
        profile = artstor.get_profile(self._extract_artstor_cookie(), g.ARTSTOR_API)
        return render('dialogs/user_profile.mako', dict(profile=profile))

    def update(self):
        """Call into the SDL API and update the users profile"""
        if artstor.validate_email(request.params['email']):
            status = artstor.update_profile(request.params,
                                            self._extract_artstor_cookie(),
                                            g.ARTSTOR_API)
            return "Your profile has been updated"
        else:
            return "Invalid email address specified"

    def password(self):
        """Call into the SDL API and update the users password"""
        if request.params['password'] == request.params['passwordConfirm']:
            if artstor.validate_password(request.params['password']):
                try:
                    new_cookie = artstor.update_passwd(request.params['oldPassword'],
                                                       request.params['password'],
                                                       self._extract_artstor_cookie(),
                                                       g.ARTSTOR_API)
                    response.set_cookie(g.ARTSTOR_COOKIE,
                                        new_cookie['value'],
                                        path='/',
                                        domain='.artstor.org')
                    return 'Your password has been updated'
                except ValueError:
                    return 'Incorrect original password'
                except:
                    log.error('Error updating password', exc_info=True)
                    return 'Error updating password'
            else:
                return 'Password must be between 7 and 20 characters long'
        else:
            return 'Passwords do not match'
            
    @jsonify    
    def priveleged_users(self):
        id_map = artstor.get_profile_id_to_email_map(g.ADMINS + g.REVIEWERS + g.CATALOGERS)
        return dict(admins = [id_map.get(profile_id, "Unknown") for profile_id in g.ADMINS],
                    reviewers = [id_map.get(profile_id, "Unknown") for profile_id in g.REVIEWERS],
                    catalogers = [id_map.get(profile_id, "Unknown") for profile_id in g.CATALOGERS])

    
    def _extract_artstor_cookie(self):
        if g.ARTSTOR_COOKIE in request.cookies.keys():
            return "%s=%s" % (g.ARTSTOR_COOKIE, request.cookies[g.ARTSTOR_COOKIE])
