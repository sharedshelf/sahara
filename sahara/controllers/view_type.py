import logging

from sahara.lib.base import *
from sahara.model import ViewType, Session as db
from sqlalchemy.orm.exc import NoResultFound
from sahara.lib.access_control import restricted_to

log = logging.getLogger(__name__)

class ViewTypeController(BaseController):

    def __before__(self, action, **params):
        super(ViewTypeController, self).__before__(action)
        if not session['user'].is_admin:
            log.warn("Unprivileged user(%s) trying to access admin function" % session['user'].id)
            abort(403, 'Permission denied')

    @jsonify
    @restricted_to(g.ADMINS)
    def index(self):
        """Generate listings of view types."""
        view_types = db.query(ViewType).order_by('value').all()
        
        view_types_dict = {}
        for v in view_types:
            view_types_dict[v.id] = v.value
        return view_types_dict

    @jsonify
    @restricted_to(g.ADMINS)
    def create(self):
        """Create a new view type."""
        try:
            view_type = ViewType()
            view_type.value = request.POST['value']
            db.add(view_type)
            db.commit()
            return { view_type.id: view_type.value }
        except:
            log.critical("Failure to create view type.", exc_info=True)
            return dict(message='An error occurred creating the view type.')
    
    @jsonify
    @restricted_to(g.ADMINS)
    def update(self, view_type_id):
        """Update view type with a particular id."""
        try:
            view_type = db.query(ViewType).filter_by(id = view_type_id).one()
            view_type.value = request.POST['value']
            db.add(view_type)
            db.commit()
            return dict(success=True, message='View type successfully updated.')
            
        except NoResultFound:
            return abort(404, "No view type found with that id.")
        except:
            log.critical("Failure to update view type", exc_info=True)
            return dict(message='An error occurred saving the view type.',
                        view_type_id=view_type_id)

    @jsonify
    @restricted_to(g.ADMINS)
    def delete(self, view_type_id):
        """Delete view type with a particular id."""
        try:
            view_type = db.query(ViewType).filter_by(id = view_type_id).one()
            db.delete(view_type)
            db.commit()
            return dict(success=True, message='View type successfully deleted.')

        except NoResultFound:
            response.status = 404
            return dict(success=False, message="No view type found with that id.")
        except:
            log.critical("Failure to delete view type", exc_info=True)
            return dict(message='An error occurred deleting the view type.',
                        view_type_id=view_type_id)
