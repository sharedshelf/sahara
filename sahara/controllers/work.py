import logging
import urllib2
import re
import simplejson
from datetime import datetime

import ImageFile
import jibberish

from sahara.lib.base import *
from sahara.lib import helpers as h
from sahara.model import XMLSerializer, Session as db
from sahara.model import Work, Status, Location, Creator, Keyword
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import or_

from sahara.lib import form_parser
from sahara.lib.access_control import restricted_to, with_scoped_works_query
from sahara.lib.filereposter import repost
from sahara.lib.record_copy import copy_work
from sahara.lib.dastor import get_metadata, fix_exif_date, tag_normalize
from sahara.lib.authentication import artstor
from sahara.model.search import Solr
log = logging.getLogger(__name__)

# for work creation, please see the upload controller.

class WorkController(BaseController):
    unrestricted = ['feed', 'show']

    def __before__(self, action, **params):
        """Creates correctly filtered queries for use in retrieving works.

        self.work_q or self.works_q should be used whenever possible, if
        they are not used the created_by filter must be explicity set to
        prevent users from seeing records that don't belong to them.

        """
        super(WorkController, self).__before__(action)
        if action not in self.unrestricted:
            self.owned_works = Work.query().filter_by(created_by=session['user'].id)
            if session['user'].is_privileged:
                if session['user'].is_admin:
                    self.accessible_works = Work.query()
                else:
                    self.accessible_works = Work.query().filter(or_(Work.created_by==session['user'].id,
                                            Work.uuid.in_(session['user'].editable_work_ids)))
            else:
                self.accessible_works = self.owned_works

    def index(self, complete=False, template=None, valid=None):
        """Generate listings of works using various templates and filters."""
        c.size = self._determine_thumbnail_size()
        c.sort = self._determine_sort_order(request.params)
        if valid is None:
            c.works = self._find(self.owned_works, complete=complete, order=c.sort)
        else:
            c.works = self._find(self.owned_works, complete=complete, valid=valid, order=c.sort)
        return render(template)
    
    def edit(self, uuid, template='work_form.mako', force_full_view=False):
        """Generate the form to edit the specified work"""
        if force_full_view is True:
            c.full = True
        else:
            c.full = h.is_full_view()
        if session['user'].is_privileged:
            c.work = Work.query().filter_by(uuid=uuid).one()
            if c.work.status.contributed_on is not None:
                c.current_collection = c.work.publication_destination.artstor_collection_id
                c.cataloger = session['user'].is_cataloger
                editors_ids = [hist.updated_by for hist in c.work.history]
                editors_ids.append(c.work.updated_by)
                c.imageviewer_url = artstor.get_image_viewer_link(c.work.uuid)
                c.editors = h.map_profile_ids_to_profile_data(editors_ids)
        else:
            c.work = self.accessible_works.filter_by(uuid=uuid).join(Status).one()
        return render(template)

    def show(self, uuid, format):
        work = Work.query().filter_by(uuid=uuid).join(Status).one()
        if 'xml' == format:
            response.content_type = 'text/xml'
            return XMLSerializer().serialize(work)
        
    @jsonify
    def update(self, uuid):
        """Parse form and update the specified work"""
        db.rollback()
        try:
            log.info("Updating %s" % uuid)
            work = self.accessible_works.filter(Work.uuid == uuid).one()
            form_parser.parse(work, request.POST, userid=session['user'].id)
            work.update_status()
            work.updated_by = session['user'].id
            db.add(work)
            db.commit()
            return dict(message='Changes saved', valid=work.status.valid,
                        uuid=work.uuid, success=True)
        except NoResultFound:
            return abort(404, "No work found with that id")
        except:
            log.critical("Failure to submit document", exc_info=True)
            return dict(message='An error occurred saving the document, ' \
                            'administrator notified.',
                        valid=work.status.valid, success=False,
                        uuid=uuid)

    @jsonify
    def delete(self, uuid=None):
        """Delete the specified work"""
        try:
            work = self.owned_works.join(Status).filter(Work.uuid == uuid).\
                filter(Status.contributed_on == None).one()
            db.delete(work)
            db.commit()
            Solr(g.SOLR_URL).delete_work(work)

            return dict(success=True, message='Asset successfully removed.')

        except NoResultFound:
            response.status = 404
            return dict(success=False, message="Invalid UUID")


    ###########################################################################
    #
    # Work related ancillary methods
    #
    ###########################################################################
    def add_multiple(self):
        """Render the flash based multiple file uploader"""
        works = self.owned_works.join(Status).filter(Status.contributed_on == None).count()
        c.remaining = g.MAX_SLOTS - works
        if works < g.MAX_SLOTS:
            return render('dialogs/upload_dialog.mako')
        else:
            return render('dialogs/maximum_reached_dialog.mako')

    
    def tooltip(self, uuid=None):
        """Renders asset file information"""
        c.work = Work.query().filter(Work.uuid == uuid).one()
        return render('work_tooltip.mako')

    @jsonify
    def feed(self, date='01-01-1971-00-00-00'):
        """
        Render an XML representation of all changed works.

        Parameters:
        date - The start date from which all changes will be returned.
        """
        date = datetime.strptime(date, '%m-%d-%Y-%H-%M-%S')
        works = db.query(Work.uuid).join(Status).filter(Status.last_published >= date).all()

        return dict(uuids=[w.uuid for w in works], media_url=g.ASSET_URL)

    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def rollback(self, uuid, history_id):
        try:
            c.full = True # Force full template view
            c.work = self.accessible_works.filter(Work.uuid == uuid).one()
            editors_ids = [hist.updated_by for hist in c.work.history]
            editors_ids.append(c.work.updated_by)
            c.editors = h.map_profile_ids_to_profile_data(editors_ids)
            if history_id != 'current':
                c.work.rollback(int(history_id), session['user'].id)
            return render('search/quick_edit.mako')
        except:
            log.error('Error rolling back work', exc_info=True)
            raise

    @jsonify
    def count(self):
        """
        Returns the number of works the current user has uploaded.

        The works are segmented by thier contribution status.
        """
        completed = self.owned_works.join(Status).filter(Status.contributed_on != None).count()
        incomplete = self.owned_works.join(Status).filter(Status.contributed_on == None).count()
        return dict(contributed=completed, progressing=incomplete)

    @jsonify
    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def reserve(self, uuid):
        """
        Reserve the work to current user.
        
        Puts current user id into reserved_by of the work.
        """
        try:
            work = self.accessible_works.filter(Work.uuid == uuid).one()
                
            if work.reserved_by() is not None and not session['user'].is_admin:
                return dict(complete = False, uuid=uuid, message='Record already locked.')
            else:
                work.reserve(session['user'].id)
                db.add(work)
                db.commit()                
                return dict(message='Work successfully reserved.',
                            uuid=uuid,
                            complete=True)
                            
        except NoResultFound:
            return abort(404, "No work found with that id")
        except:
            log.critical('Work reservation failed', exc_info=True)
            return dict(message='There was a problem reserving the document, administrator notified.',
                        uuid=uuid,
                        complete=False)
    
    @jsonify
    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def unreserve(self, uuid):
        """
        Remove work reservation (only allowed to admin or original reserver).

        Removes user id from reserved_by of the work.
        """
        try:
            work = self.accessible_works.filter(Work.uuid == uuid).one()
            if work.reserved_by() and (work.reserved_by() != session['user'].id) and not session['user'].is_admin:
                return dict(complete=False, uuid=uuid, message='Insufficient authorization level.')
            else:
                work.reserve(None)
                db.add(work)
                db.commit()

                return dict(message='Work reservation successfully cleared.',
                            uuid=uuid,
                            complete=True)

        except NoResultFound:
            return abort(404, "No work found with that id.")
        except:
            log.critical('Work reservation failed', exc_info=True)
            return dict(message='There was a problem reserving the document, administrator notified.',
                        uuid=uuid,
                        complete=False)
        
    @jsonify
    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def multireservation(self, reserve):
        try:
            results = dict()
            works = self.accessible_works.filter(Work.uuid.in_(request.params.getall('uuids'))).all()
            for work in works:
                if reserve == 'true':
                    if work.reserved_by() is not None and not session['user'].is_admin:
                        results[work.uuid] = dict(success=False, message='Record already locked.')
                    else:
                        work.reserve(session['user'].id)
                        db.add(work)
                        results[work.uuid] = dict(success=True, message='Work successfully reserved.')
                elif reserve == 'false':
                    if work.reserved_by() and (work.reserved_by() != session['user'].id) and not session['user'].is_admin:
                        results[work.uuid] = dict(success=False, message='Insufficient authorization level.')
                    else:
                        work.reserve(None)
                        db.add(work)
                        results[work.uuid] = dict(success=True, message='Work reservation successfully cleared.')
            db.commit()
            return dict(results=results)
        except:
            log.critical('Work reservation failed', exc_info=True)
            abort(500)
    
    @jsonify
    @restricted_to(g.ADMINS, g.REVIEWERS)
    def mark_reviewed(self, uuid):
        """
        Mark the work reviewed.

        Timestamps the reviewed field in the work's status.
        """
        try:
            work = self.accessible_works.filter(Work.uuid == uuid).one()
                
            work.mark_reviewed(session['user'].id)
            db.add(work)
            db.commit()
                
            return dict(message='Work has been successfully marked as reviewed.',
                        uuid=uuid,
                        complete=True)
        except NoResultFound:
            return abort(404, "No work found with that id.")
        except:
            log.critical('Failed trying to mark the work as reviewed.', exc_info=True)
            return dict(message='There was a problem marking the work as reviewed, ' \
                            'administrator notified.',
                        uuid=uuid,
                        complete=False)

    @jsonify
    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def assign(self, uuid):
        """
        Assign the work to one or more userids.
        """
        try:
            work = self.accessible_works.filter(Work.uuid == uuid).one()
            work.assign_to(request.POST.getall('userid'))
            db.add(work)
            db.commit()
                
            return dict(message='Work has been successfully assigned.',
                        uuid=uuid,
                        complete=True)
        except NoResultFound:
            return abort(404, "No work found with that id.")
        except:
            log.critical('Failed trying to assign the work.', exc_info=True)
            return dict(message='There was a problem assigning the work, administrator notified.',
                        uuid=uuid,
                        complete=False)
            
        
    @jsonify
    def contribute(self, uuid):
        """
        Mark a work as complete.

        Takes the uuid of the work to contribute.  All validation routines
        are run against the work, and if it passes the work is marked as
        complete.  Once that happens a regular user can no longer edit
        thier assets data.
        """
        if request.method == 'POST':
            try:
                log.info("Contributing %s" % uuid)
                if not session['user'].is_privileged:
                    work = self.owned_works.join(Status).filter(Status.contributed_on == None) \
                    .filter(Work.uuid == uuid).one()
                else:
                    work = self.accessible_works.filter(Work.uuid == uuid).one()
                form_parser.parse(work, request.POST, userid=session['user'].id)
                if work.is_contributable()[0] is True:
                    work.contribute(g.MEMBERS_COLLECTION)
                    work.updated_by = session['user'].id
                    db.add(work)
                    db.commit()
                    return dict(message='Thank you for your contribution',
                                uuid=uuid,
                                complete=True)
                else:
                    return dict(
                        message="<br/>".join(work.is_contributable()[1]),
                        uuid=uuid,
                        complete=False)

            except:
                log.critical('Failure to contribute document', exc_info=True)
                return dict(message='There was a problem contributing the ' \
                            'document, administrator notified',
                            uuid=uuid,
                            complete=False)

    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def publication_destination(self, uuid):
        c.uuid = uuid
        try:
            c.current_repository = Work.query().filter(Work.uuid == uuid).one().publication_destination.artstor_collection_id
        except:
            c.current_repository = g.MEMBERS_COLLECTION
        return render('dialogs/publication_destination.mako')
        
        
    @jsonify
    @restricted_to(g.ADMINS, g.REVIEWERS, g.CATALOGERS)
    def publish(self, uuid):
        """
        Publish a work to a particular collection.
        """
        if request.method == 'POST':
            try:
                log.info("publishing %s" % uuid)
                q = self.accessible_works.filter(Work.uuid==uuid)
                print q
                work = q.one()
                form_parser.parse(work, request.POST, userid=session['user'].id)
                if work.is_contributable()[0] is True:
                    # Catalogers cannot change the collection a work publishes to.
                    if session['user'].is_cataloger:
                        log.debug('User is cataloger, preventing destination change')
                        destination_collection = work.publication_destination.artstor_collection_id
                    else:
                        destination_collection = request.params['collection']
                    log.debug("Destination collection: " + str(destination_collection))
                    work.mark_reviewed(session['user'].id)
                    work.contribute(destination_collection)
                    db.add(work)
                    db.commit()
                    return dict(message='Publishing initiated',
                                uuid=uuid,
                                complete=True)
                else:
                    return dict(
                        message="<br/>".join(work.is_contributable()[1]),
                        uuid=uuid,
                        complete=False)

            except:
                log.critical('Failure to publish document', exc_info=True)
                return dict(message='There was a problem publishing the ' \
                            'document, administrator notified',
                            uuid=uuid,
                            complete=False)



    def preview(self, uuid):
        if not session['user'].is_privileged:
            response.status = 401
            return dict(complete=False, uuid=uuid, message='Insufficient authorization level.')
        else:
            try:
                work = Work.query().filter(Work.uuid == uuid).one()
                html = jibberish.transform_sahara_xml_to_artstor_html(work.to_xml())
                return str(html)
            except NoResultFound:
                return abort(404, "No work found with that id.")
            except:
                log.critical('Preview of work failed', exc_info=True)
                return "Work preview has failed, the administrator has been notified"


    ###########################################################################
    #
    # Multi-contribute workflow methods
    #
    ###########################################################################
    def multicontribute_confirm(self):
        """Display a summary of the contribution to be performed"""
        params = request.POST
        uuids = params.getall('uuid')
        c.works = self.owned_works.filter(Work.uuid.in_(uuids)).all()
        return render('dialogs/contribute/multicontribute_confirm.mako')

    def multicontribute(self):
        """
        Marks multiple works as complete.

        Takes the uuids of the works to contribute.  Works are filtered
        to ensure validity and marked as complete.  Once that happens a
        regular user can no longer edit thier assets data.
        """
        params = request.POST
        uuids = params.getall('uuid')
        works = self.owned_works.filter(Work.uuid.in_(uuids)).join(Status).\
            filter(Status.contributed_on == None).filter(Status.valid == True).all()
        for work in works:
            work.contribute(g.MEMBERS_COLLECTION)
            work.updated_by = session['user'].id
            db.add(work)
        db.commit()
        return render('dialogs/contribute/multicontribute_complete.mako')


    ###########################################################################
    #
    # Multi-delete workflow methods
    #
    ###########################################################################
    def multidelete_confirm(self):
        """Display a summary of the deletion to be performed"""
        params = request.POST
        uuids = params.getall('uuid')
        c.works = self.owned_works.filter(Work.uuid.in_(uuids)).all()
        return render('dialogs/delete/multidelete_confirm.mako')

    def multidelete(self):
        """
        Deletes multiple works.

        The works are filtered based on owner and complete status.  Completed
        works cannot be deleted
        """
        params = request.POST
        uuids = params.getall('uuid')
        works = self.owned_works.filter(Work.uuid.in_(uuids)).join(Status).\
            filter(Status.contributed_on == None).all()
        solr = Solr(g.SOLR_URL)
        for work in works:
            db.delete(work)
            solr.delete_work(work)
        db.commit()
        return render('dialogs/delete/multidelete_complete.mako')


    ###########################################################################
    #
    # Multi-copy workflow methods
    #
    ###########################################################################
    def copy_select_fields(self, uuid, owned_only=True):
        """Display a list of major field sections to select from"""
        return render('dialogs/copy/select_fields.mako');

    def copy_select_works(self, uuid, owned_only=True):
        """Display a list of all non-complete works except the source work"""
        if owned_only is True:
            c.works = self.owned_works.join(Status)\
                .filter(Status.contributed_on == None) \
                .filter("uuid != '%s'" % uuid)\
                .order_by(Status.contributed_on.desc()).all()
            c.priveleged = False
        else: 
            q = self.accessible_works.join(Status).filter(Status.reserved_by == session['user'].id)\
                .filter("uuid != '%s'" % uuid)
            c.priveleged = True
            c.sort = self._determine_sort_order(request.params)
            c.works = q.order_by(self._sort_works(c.sort)).all()
        return render('dialogs/copy/select_images.mako')

    def copy_confirm(self, uuid, owned_only=True):
        """Display a summary of the copy to be performed"""
        params = request.POST
        c.fields = params.getall('fields')
        dest_uuids = params.getall('works')
        if owned_only is True:
            c.works = self.owned_works.filter(Work.uuid.in_(dest_uuids)).all()
        else:
            c.works = self.accessible_works.filter(Work.uuid.in_(dest_uuids)).all()
        return render('dialogs/copy/confirm_copy.mako')

    def copy(self, uuid, owned_only=True):
        """
        Copies a source works data to a group of other works.

        Takes two parameters:
        fields - A list of field groups to copy.
        works - The destination list.
        """
        params = request.POST
        dest_uuids = params.getall('works')
        if owned_only is True:
            works = self.owned_works.filter(Work.uuid.in_(dest_uuids)).all()
        else:
            works = self.accessible_works.join(Status).filter(Status.reserved_by == session['user'].id).filter(Work.uuid.in_(dest_uuids)).all()
        try:
            copy_work(uuid, fields=params.getall('fields'), destinations=works)
            Session.commit()
            return render('dialogs/copy/copy_complete.mako', dict(success=True))
        except:
            log.error("Failed to copy record", exc_info=True)
            Session.rollback()
            return render('dialogs/copy/copy_complete.mako', dict(success=False))


    @jsonify
    @restricted_to(g.ADMINS)
    def suppress(self, uuid, suppress):
        """An administration call to delete/undelete a work from SDL"""
        if session['user'].is_privileged:
            try:
                work = Work.query().filter(Work.uuid == uuid).join(Status).one();
                work.suppressed = (suppress == 'true')
                db.commit()
                return dict(success=True)
            except:
                log.error('Updating suppresion failed', exc_info=True)
                return dict(success=False)

    #
    # Internal methods
    #
    def _determine_thumbnail_size(self):
        """Get the default thumbnail size from the browser cookie"""
        try:
            size = int(request.cookies['thumbnailsize'])
        except:
            size = 0
        return size

    def _determine_sort_order(self, params):
        sort = 'date-desc'
        if 'assetsort' in request.cookies.keys():
            sort = request.cookies['assetsort']
        if 'sort' in params.keys():
            sort = params['sort']
        return sort

    def _sort_works(self, order):
        if 'date-desc' == order:
            return Work.created_on.desc()
        elif 'date-asc' == order:
            return Work.created_on.asc()
        elif 'filename-asc' == order:
            return Work.media_filename.asc()
        elif 'filename-desc' == order:
            return Work.media_filename.desc()
        elif 'capture-asc' == order:
            return [Work.media_captured_on.asc(), Work.uuid.desc()]
        elif 'capture-desc' == order:
            return [Work.media_captured_on.desc(), Work.uuid.desc()]
        else:
            return Work.created_on.desc()

    def _find(self, works_query, complete=False, valid=None, order='date-desc'):
        print "COMPLETE: %s" % complete
        q = works_query.join(Status)
        if complete is False or complete == 'False':
            q = q.filter(Status.contributed_on == None)
        else:
            q = q.filter(Status.contributed_on != None)
        if valid is not None:
            q = q.filter(Status.valid == valid)
        return q.order_by(self._sort_works(order)).all()
