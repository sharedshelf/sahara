import logging

from sahara.lib.base import *
from sahara.lib import helpers as h
from sahara.model import ComplexTitle, Title, Location, Creator, Country, \
    Period, Extent, Nationality

log = logging.getLogger(__name__)

class WorkFormController(BaseController):

    def new_creator(self, index=0):
        return render('work_form/creator_section.mako',
                      dict(index=int(index),
                           creator=Creator(),
                           full=h.is_full_view()))
    
    def new_complex_title(self, index=0):
        return render('work_form/complex_title_field.mako',
                      dict(index=int(index),
                           title=ComplexTitle()))

    def new_title(self, index=0):
        return render('work_form/title_field.mako',
                      dict(index=int(index),
                           title=Title()))

    def new_location_country(self, index=0):
        return render('work_form/location_country_field.mako',
                      dict(index=int(index),
                           country=Country()))

    def new_period_dynasty(self, index=0):
        return render('work_form/period_dynasty_field.mako',
                      dict(index=int(index),
                           period_dynasty=Period()))
        
    def new_creator_nationality(self, creator_index=0, index=0):
        if 'value' in request.params:
            nationality = Nationality(request.params['value'])
        else:
            nationality = Nationality()

        readonly = True if 'readonly' in request.params else False
        return render('work_form/creator_nationality_field.mako',
                      dict(index=int(index),
                           readonly=readonly,
                           creator_index=int(creator_index),
                           nationality=nationality,
                           full=h.is_full_view()))

    def new_creator_extent(self, creator_index=0, index=0):
        return render('work_form/creator_extent_field.mako',
                      dict(index=int(index),
                           creator_index=creator_index,
                           extent=Extent()))
