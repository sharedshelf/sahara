import lipsum
from datetime import datetime
from fixture import DataSet, SQLAlchemyFixture
from fixture.style import NamedDataStyle
from sahara.datasets.lookup_tables import AttributionData, CountryData, ClassificationData, RoleData, ViewTypeData
from pylons import g

text_gen = lipsum.Generator()

def load_data():
    from sahara import model
    from sahara.model import metadata, Session, Status

    dbfixture = SQLAlchemyFixture(
        env=model,
        engine=metadata.bind,
        style=NamedDataStyle()
        )
    print "Dropping and recreating database."
    metadata.drop_all(metadata.bind)
    metadata.create_all()
    print "Populating fixture data"
    data = dbfixture.data(WorkData, NameData, CountryData, ClassificationData, TitleData, LocationData, \
                              CreatorData, ChronologyData, PhysicalDescriptionData, SourceData, RightsData, \
                              AssignmentData, HistoryData, EditorialBucketReviewerData)
    data.setup()
    metadata.create_all(checkfirst=True)
    engine = metadata.bind
    connection = engine.connect()
    db = Session(bind=connection)
    for status in db.query(Status).all():
        status.contributed_on = datetime.now()
    db.commit()
    db.flush()
    from sahara.model.search import Solr
    Solr(g.SOLR_URL).reindex()
    

class EditorialBucketData(DataSet):
    class bucket_one:
        name = 'American Bucket'
        query = 'country:"United States" && avg_date:[1801 TO 1900]'
    class bucket_two:
        name = 'France Bucket'
        query = 'country:France && avg_date:[1901 TO 2000]'

        
class EditorialBucketReviewerData(DataSet):
    class reviewer_one:
        bucket = EditorialBucketData.bucket_one
        reviewer_id = 149215
    class reviewer_two:
        bucket = EditorialBucketData.bucket_one
        reviewer_id = 149216
    class reviewer_three:
        bucket = EditorialBucketData.bucket_two
        reviewer_id = 149217

        
class WorkData(DataSet):
    class work_one:
        uuid = 'UUIDONE'
        created_by = 149223
        updated_by = 149223
        media_uuid = 'UUIDONE'
    class work_two:
        uuid = 'UUIDTWO'
        updated_by = 149223
        created_by = 149223
        media_uuid = 'UUIDTWO'
    class work_three:
        uuid = 'UUIDTHREE'
        updated_by = 149223
        created_by = 149223
        media_uuid = 'UUIDTHREE'
    class work_four:
        uuid = 'UUIDFOUR'
        updated_by = 149223
        created_by = 149223
        media_uuid = 'UUIDFOUR'
    class work_five:
        uuid = 'UUIDFIVE'
        updated_by = 149223
        created_by = 149223
        media_uuid = 'UUIDFIVE'

class HistoryData(DataSet):
    class work_one_history:
        updated_by = 149223
        work = WorkData.work_one
    class work_two_history:
        updated_by = 149223
        work = WorkData.work_two
    class work_three_history:
        updated_by = 149223
        work = WorkData.work_three
    class work_four_history:
        updated_by = 149223
        work = WorkData.work_four
    class work_five_history:
        updated_by = 149223
        work = WorkData.work_five
        
#class StatusData(DataSet):
#    class status_for_work_one:
#        work = WorkData.work_one
#        complete = True
#    class status_for_work_two:
#        work = WorkData.work_two
#        complete = True
#        reserved_by = 149223
#    class status_for_work_three:
#        work = WorkData.work_three
#        complete = True
#    class status_for_work_four:
#        work = WorkData.work_four
#        complete = True
#    class status_for_work_five:
#        work = WorkData.work_five
#        complete = True
class TitleData(DataSet):
    class nyse:
        value = u'New York Stock Exchange'
    class empire_state_building:
        value = u'Empire State Building'
    class chrysler_building:
        value = u'Chrysler Building'
    class eiffel_tower:
        value = u'Eiffel Tower'
    class louvre:
        value = u'The Louvre'
        

class NameData(DataSet):
    class work_one_name:
        work = WorkData.work_one
        view_type = ViewTypeData.exterior
        classifications = [ClassificationData.photographs]
        titles = [TitleData.nyse]
    class work_two_name:
        work = WorkData.work_two
        view_type = ViewTypeData.exterior
        classifications = [ClassificationData.photographs]
        titles = [TitleData.empire_state_building]
    class work_three_name:
        work = WorkData.work_three
        view_type = ViewTypeData.exterior
        classifications = [ClassificationData.photographs]
        titles = [TitleData.chrysler_building]
    class work_four_name:
        work = WorkData.work_four
        view_type = ViewTypeData.exterior
        classifications = [ClassificationData.photographs]
        titles = [TitleData.eiffel_tower]
    class work_five_name:
        work = WorkData.work_five
        view_type = ViewTypeData.exterior
        classifications = [ClassificationData.photographs]
        titles = [TitleData.louvre]

        
class PhysicalDescriptionData(DataSet):
    class work_one_physical:
        work = WorkData.work_one
        description = text_gen.generate_paragraph()
        commentary = text_gen.generate_paragraph()
    class work_two_physical:
        work = WorkData.work_two
        description = text_gen.generate_paragraph()
        commentary = text_gen.generate_paragraph()
    class work_three_physical:
        work = WorkData.work_three
        description = text_gen.generate_paragraph()
        commentary = text_gen.generate_paragraph()
    class work_four_physical:
        work = WorkData.work_four
        description = text_gen.generate_paragraph()
        commentary = text_gen.generate_paragraph()
    class work_five_physical:
        work = WorkData.work_five
        description = text_gen.generate_paragraph()
        commentary = text_gen.generate_paragraph()

        
class CreatorData(DataSet):
    class work_one_creator:
        name = u'Unknown'
        work = WorkData.work_one
    class work_two_creator:
        name = u'Unknown'
        work = WorkData.work_two
    class work_three_creator:
        name = u'Unknown'
        work = WorkData.work_three
    class work_four_creator:
        name = u'Unknown'
        work = WorkData.work_four
    class work_five_creator:
        name = u'Unknown'
        work = WorkData.work_five

        
class LocationData(DataSet):
    class work_one_location:
        city_county = u'New York'
        countries = [CountryData.united_states]
        work = WorkData.work_one
    class work_two_location:
        city_county = u'New York'
        countries = [CountryData.united_states]
        work = WorkData.work_two
    class work_three_location:
        city_county = u'New York'
        countries = [CountryData.united_states]
        work = WorkData.work_three
    class work_four_location:
        city_county = u'Paris'
        countries = [CountryData.france]
        work = WorkData.work_four
    class work_five_location:
        city_county = u'Paris'
        countries = [CountryData.france]
        work = WorkData.work_five

        
class SourceData(DataSet):
    class work_one_source:
        photographer = u'Will Groppe'
        contributor = u'Will Groppe'
        work = WorkData.work_one
    class work_two_source:
        photographer = u'Will Groppe'
        contributor = u'Will Groppe'
        work = WorkData.work_two
    class work_three_source:
        photographer = u'Will Groppe'
        contributor = u'Will Groppe'
        work = WorkData.work_three
    class work_four_source:
        photographer = u'Will Groppe'
        contributor = u'Will Groppe'
        work = WorkData.work_four
    class work_five_source:
        photographer = u'Will Groppe'
        contributor = u'Will Groppe'
        work = WorkData.work_five

        
class ChronologyData(DataSet):
    class work_one_dates:
        display_date = u'1805-1890'
        earliest_date = 1805
        latest_date = 1890
        work = WorkData.work_one
    class work_two_dates:
        display_date = u'1850-1890'
        earliest_date = 1850
        latest_date = 1890
        work = WorkData.work_two
    class work_three_dates:
        display_date = u'1905-1940'
        earliest_date = 1905
        latest_date = 1940
        work = WorkData.work_three
    class work_four_dates:
        display_date = u'1910-1930'
        earliest_date = 1910
        latest_date = 1930
        work = WorkData.work_four
    class work_five_dates:
        display_date = u'1950-1990'
        earliest_date = 1950
        latest_date = 1990
        work = WorkData.work_five


class RightsData(DataSet):
    class work_one_rights:
        image_copyright = text_gen.generate_sentence()
        image_ownership = True
        work = WorkData.work_one
    class work_two_rights:
        image_copyright = text_gen.generate_sentence()
        image_ownership = True
        work = WorkData.work_two
    class work_three_rights:
        image_copyright = text_gen.generate_sentence()
        image_ownership = True
        work = WorkData.work_three
    class work_four_rights:
        image_copyright = text_gen.generate_sentence()
        image_ownership = True
        work = WorkData.work_four
    class work_five_rights:
        image_copyright = text_gen.generate_sentence()
        image_ownership = True
        work = WorkData.work_five

class AssignmentData(DataSet):
    class work_one_assignment:
        work = WorkData.work_three
        user_id = 149218

