from fixture import DataSet

class AttributionData(DataSet):
	class after:
		value = u'after'
	class assistant_to:
		value = u'assistant to'
	class associate_of:
		value = u'associate of'
	class atelier_of:
		value = u'atelier of'
	class attributed_to:
		value = u'attributed to'
	class circle_of:
		value = u'circle of'
	class copyist_of:
		value = u'copyist of'
	class follower_of:
		value = u'follower of'
	class formerly_attributed_to:
		value = u'formerly attributed to'
	class manner_of:
		value = u'manner of'
	class manufactory_of:
		value = u'manufactory of'
	class office_of:
		value = u'office of'
	class possibly_by:
		value = u'possibly by'
	class probably_by:
		value = u'probably by'
	class pupil_of:
		value = u'pupil of'
	class school_of:
		value = u'school of'
	class studio_of:
		value = u'studio of'
	class style_of:
		value = u'style of'
	class workshop_of:
		value = u'workshop of'


class ClassificationData(DataSet):
	class architecture_and_city_planning:
		value = u'Architecture and City Planning'
	class decorative_arts_utilitarian_objects_and_interior_design:
		value = u'Decorative Arts, Utilitarian Objects and Interior Design'
	class drawings_and_watercolors:
		value = u'Drawings and Watercolors'
	class fashion_costume_and_jewelry:
		value = u'Fashion, Costume and Jewelry'
	class film_audio_video_and_digital_art:
		value = u'Film, Audio, Video and Digital Art'
	class garden_and_landscape:
		value = u'Garden and Landscape'
	class graphic_design_and_illustration:
		value = u'Graphic Design and Illustration'
	class humanities_and_social_sciences:
		value = u'Humanities and Social Sciences'
	class manuscripts_and_manuscript_illuminations:
		value = u'Manuscripts and Manuscript Illuminations'
	class maps_charts_and_graphs:
		value = u'Maps, Charts and Graphs'
	class paintings:
		value = u'Paintings'
	class performing_arts_including_performance_art:
		value = u'Performing Arts including Performance Art'
	class photographs:
		value = u'Photographs'
	class prints:
		value = u'Prints'
	class science_technology_and_industry:
		value = u'Science, Technology and Industry'
	class sculpture_and_installations:
		value = u'Sculpture and Installations'


class CountryData(DataSet):
	class afghanistan:
		value = u'Afghanistan'
	class albania:
		value = u'Albania'
	class algeria:
		value = u'Algeria'
	class andorra:
		value = u'Andorra'
	class angola:
		value = u'Angola'
	class argentina:
		value = u'Argentina'
	class armenia:
		value = u'Armenia'
	class australia:
		value = u'Australia'
	class austria:
		value = u'Austria'
	class azerbaijan:
		value = u'Azerbaijan'
	class bahamas:
		value = u'Bahamas'
	class bahrain:
		value = u'Bahrain'
	class bangladesh:
		value = u'Bangladesh'
	class barbados:
		value = u'Barbados'
	class belgium:
		value = u'Belgium'
	class belize:
		value = u'Belize'
	class belorussia:
		value = u'Belorussia'
	class benin:
		value = u'Benin'
	class bhutan:
		value = u'Bhutan'
	class bolivia:
		value = u'Bolivia'
	class botswana:
		value = u'Botswana'
	class brazil:
		value = u'Brazil'
	class bulgaria:
		value = u'Bulgaria'
	class burkina_faso:
		value = u'Burkina Faso'
	class burundi:
		value = u'Burundi'
	class cambodia:
		value = u'Cambodia'
	class cameroon:
		value = u'Cameroon'
	class canada:
		value = u'Canada'
	class central_african_republic:
		value = u'Central African Republic'
	class chad:
		value = u'Chad'
	class chile:
		value = u'Chile'
	class china:
		value = u'China'
	class colombia:
		value = u'Colombia'
	class congo:
		value = u'Congo'
	class congo_republic:
		value = u'Congo Republic'
	class costa_rica:
		value = u'Costa Rica'
	class cote_d_ivoire:
		value = u'Cote d\'Ivoire'
	class croatia:
		value = u'Croatia'
	class cuba:
		value = u'Cuba'
	class cyprus:
		value = u'Cyprus'
	class czech_republic:
		value = u'Czech Republic'
	class denmark:
		value = u'Denmark'
	class dominican_republic:
		value = u'Dominican Republic'
	class ecuador:
		value = u'Ecuador'
	class egypt:
		value = u'Egypt'
	class el_salvador:
		value = u'El Salvador'
	class equatorial_guinea:
		value = u'Equatorial Guinea'
	class eritrea:
		value = u'Eritrea'
	class ethiopia:
		value = u'Ethiopia'
	class fiji:
		value = u'Fiji'
	class finland:
		value = u'Finland'
	class france:
		value = u'France'
	class gabon:
		value = u'Gabon'
	class gambia:
		value = u'Gambia'
	class georgia:
		value = u'Georgia'
	class germany:
		value = u'Germany'
	class ghana:
		value = u'Ghana'
	class greece:
		value = u'Greece'
	class greenland:
		value = u'Greenland'
	class guatemala:
		value = u'Guatemala'
	class guinea:
		value = u'Guinea'
	class guinea_bissau:
		value = u'Guinea-Bissau'
	class haiti:
		value = u'Haiti'
	class honduras:
		value = u'Honduras'
	class hungary:
		value = u'Hungary'
	class iceland:
		value = u'Iceland'
	class india:
		value = u'India'
	class indonesia:
		value = u'Indonesia'
	class iran:
		value = u'Iran'
	class iraq:
		value = u'Iraq'
	class ireland:
		value = u'Ireland'
	class israel:
		value = u'Israel'
	class italy:
		value = u'Italy'
	class jamaica:
		value = u'Jamaica'
	class japan:
		value = u'Japan'
	class jordan:
		value = u'Jordan'
	class kazakhstan:
		value = u'Kazakhstan'
	class kenya:
		value = u'Kenya'
	class kuwait:
		value = u'Kuwait'
	class laos:
		value = u'Laos'
	class latvia:
		value = u'Latvia'
	class lebanon:
		value = u'Lebanon'
	class liberia:
		value = u'Liberia'
	class libya:
		value = u'Libya'
	class lithuania:
		value = u'Lithuania'
	class luxembourg:
		value = u'Luxembourg'
	class macedonia:
		value = u'Macedonia'
	class madagascar:
		value = u'Madagascar'
	class malawi:
		value = u'Malawi'
	class malaysia:
		value = u'Malaysia'
	class mali:
		value = u'Mali'
	class malta:
		value = u'Malta'
	class marshall_islands:
		value = u'Marshall Islands'
	class mauritania:
		value = u'Mauritania'
	class mexico:
		value = u'Mexico'
	class micronesia:
		value = u'Micronesia'
	class monaco:
		value = u'Monaco'
	class mongolia:
		value = u'Mongolia'
	class morocco:
		value = u'Morocco'
	class mozambique:
		value = u'Mozambique'
	class myanmar:
		value = u'Myanmar'
	class namibia:
		value = u'Namibia'
	class nepal:
		value = u'Nepal'
	class netherlands:
		value = u'Netherlands'
	class new_zealand:
		value = u'New Zealand'
	class nicaragua:
		value = u'Nicaragua'
	class niger:
		value = u'Niger'
	class nigeria:
		value = u'Nigeria'
	class north_korea:
		value = u'North Korea'
	class norway:
		value = u'Norway'
	class pakistan:
		value = u'Pakistan'
	class palau:
		value = u'Palau'
	class panama:
		value = u'Panama'
	class papua_new_guinea:
		value = u'Papua New Guinea'
	class paraguay:
		value = u'Paraguay'
	class peru:
		value = u'Peru'
	class philippines:
		value = u'Philippines'
	class poland:
		value = u'Poland'
	class portugal:
		value = u'Portugal'
	class republic_of_san_marino:
		value = u'Republic of San Marino'
	class romania:
		value = u'Romania'
	class russia:
		value = u'Russia'
	class rwanda:
		value = u'Rwanda'
	class samoa:
		value = u'Samoa'
	class saudi_arabia:
		value = u'Saudi Arabia'
	class senegal:
		value = u'Senegal'
	class serbia_and_montenegro:
		value = u'Serbia and Montenegro'
	class sierra_leone:
		value = u'Sierra Leone'
	class singapore:
		value = u'Singapore'
	class slovakia:
		value = u'Slovakia'
	class solomon_islands:
		value = u'Solomon Islands'
	class somalia:
		value = u'Somalia'
	class south_africa:
		value = u'South Africa'
	class south_korea:
		value = u'South Korea'
	class spain:
		value = u'Spain'
	class sri_lanka:
		value = u'Sri Lanka'
	class sudan:
		value = u'Sudan'
	class sweden:
		value = u'Sweden'
	class switzerland:
		value = u'Switzerland'
	class syria:
		value = u'Syria'
	class taiwan:
		value = u'Taiwan'
	class tajikistan:
		value = u'Tajikistan'
	class tanzania:
		value = u'Tanzania'
	class thailand:
		value = u'Thailand'
	class tibet:
		value = u'Tibet'
	class togo:
		value = u'Togo'
	class tonga:
		value = u'Tonga'
	class trinidad_and_tobago:
		value = u'Trinidad and Tobago'
	class tunisia:
		value = u'Tunisia'
	class turkey:
		value = u'Turkey'
	class uganda:
		value = u'Uganda'
	class ukraine:
		value = u'Ukraine'
	class united_kingdom:
		value = u'United Kingdom'
	class united_states:
		value = u'United States'
	class uruguay:
		value = u'Uruguay'
	class uzbekistan:
		value = u'Uzbekistan'
	class vanuatu:
		value = u'Vanuatu'
	class venezuela:
		value = u'Venezuela'
	class vietnam:
		value = u'Vietnam'
	class yemen:
		value = u'Yemen'
	class zambia:
		value = u'Zambia'
	class zimbabwe:
		value = u'Zimbabwe'


class NarrowClassificationData(DataSet):
	class agricultural_buildings:
		value = u'Agricultural Buildings'
	class commercial_buildings:
		value = u'Commercial Buildings'
	class cultural_landscapes:
		value = u'Cultural Landscapes'
	class designed_landscapes:
		value = u'Designed Landscapes'
	class educational_buildings:
		value = u'Educational Buildings'
	class exhibition_performance_recreation:
		value = u'Exhibition/Performance/Recreation'
	class industrial_buildings:
		value = u'Industrial Buildings'
	class monuments_and_memorials:
		value = u'Monuments and memorials'
	class military_buildings_fortification:
		value = u'Military buildings/Fortification'
	class public_civic_healthcare:
		value = u'Public/Civic/Healthcare'
	class public_works_transportation:
		value = u'Public works/Transportation'
	class residential_buildings:
		value = u'Residential Buildings'
	class religious_buildings:
		value = u'Religious Buildings'
	class unbuilt_projects:
		value = u'Unbuilt Projects'


class ViewTypeData(DataSet):
	class aerial_view:
		value = u'Aerial View'
	class general_view:
		value = u'General View'
	class exterior:
		value = u'Exterior'
	class interior:
		value = u'Interior'
	class detail_view:
		value = u'Detail View'
	class map:
		value = u'Map'
	class drawing:
		value = u'Drawing'
	class elevation__drawing_:
		value = u'Elevation (drawing)'
	class plan__drawing_:
		value = u'Plan (drawing)'
	class section:
		value = u'Section'
	class model:
		value = u'Model'


class RoleData(DataSet):
	class archaeologist:
		value = u'archaeologist'
	class architect:
		value = u'architect'
	class architectural_firm:
		value = u'architectural firm'
	class artist:
		value = u'artist'
	class builder:
		value = u'builder'
	class carpenter:
		value = u'carpenter'
	class carver:
		value = u'carver'
	class civil_engineer:
		value = u'civil engineer'
	class client:
		value = u'client'
	class construction_firm:
		value = u'construction firm'
	class contractor:
		value = u'contractor'
	class craftsman:
		value = u'craftsman'
	class design_firm:
		value = u'design firm'
	class designer:
		value = u'designer'
	class developer:
		value = u'developer'
	class donor:
		value = u'donor'
	class draftsman:
		value = u'draftsman'
	class engineer:
		value = u'engineer'
	class engineering_firm:
		value = u'engineering firm'
	class engraver:
		value = u'engraver'
	class environmental_designer:
		value = u'environmental designer'
	class furniture_designer:
		value = u'furniture designer'
	class garden_designer:
		value = u'garden designer'
	class illuminator:
		value = u'illuminator'
	class industrial_designer:
		value = u'industrial designer'
	class interior_designer:
		value = u'interior designer'
	class joiner:
		value = u'joiner'
	class landscape_architect:
		value = u'landscape architect'
	class lighting_designer:
		value = u'lighting designer'
	class lithographer:
		value = u'lithographer'
	class mason:
		value = u'mason'
	class master_builder:
		value = u'master builder'
	class model_maker:
		value = u'model maker'
	class muralist:
		value = u'muralist'
	class painter:
		value = u'painter'
	class patron:
		value = u'patron'
	class photographer:
		value = u'photographer'
	class planning_commission:
		value = u'planning commission'
	class preservationist:
		value = u'preservationist'
	class printmaker:
		value = u'printmaker'
	class sculptor:
		value = u'sculptor'
	class surveyor:
		value = u'surveyor'
	class urban_planner:
		value = u'urban planner'
	class urban_planning_firm:
		value = u'urban planning firm'
    
