import logging
import inspect
from decorator import decorator, FunctionMaker

from sqlalchemy import or_
from pylons import session
from pylons.controllers.util import redirect
from sahara.model import Work

log = logging.getLogger(__name__)


def restricted_to(*userids):
    """Restricts pylons actions to certain users provided with args and lists of user ids."""
    @decorator
    def enforce_restrictions(action, *args, **kw):
        current_user_id = session['user'].id
        
        for arg in userids:
            if hasattr(arg, '__iter__'):
                for item in arg:
                    if hasattr(item, '__iter__'):
                        if current_user_id in item:
                            return action(*args, **kw)
                    elif current_user_id == item:
                        return action(*args, **kw)
            elif current_user_id == arg:
                return action(*args, **kw)
            else:
                log.error("Unauthorized access, user %d does not have access." % current_user_id)
                return redirect("/")
    return enforce_restrictions


@decorator
def with_scoped_works_query(f, *a, **kw):
    """Adds a filtered Work query to the keyword parameters of the decorated function"""
    index = inspect.getargspec(f)[0].index('works_query')
    if index < 0:
        raise NameError("Expected function with works_query keyword parameter")
    else:
        if index < len(a):
            arg_list = list(a)
            if current_user.is_admin:
                arg_list[index] = Work.query()
            elif current_user.is_privileged:
                arg_list[index] = Work.query().\
                    filter(or_(Work.created_by==current_user.id, Work.uuid.in_(current_user.editable_work_ids())))
            else:
                arg_list[index] = Work.query().filter_by(created_by=current_user.id)
            return f(arg_list, **kw)
        else:
            return f(*q, **kw)



                

