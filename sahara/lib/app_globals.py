"""The application's Globals object"""
import os
from ConfigParser import SafeConfigParser

from pylons import config
from sahara.lib.dastor import create_tag_mapping

config_reader = SafeConfigParser()

class Globals(object):
    """Globals acts as a container for objects available throughout the
    life of the application

    """
    def __init__(self):
        """One instance of Globals is created during application
        initialization and is available during requests via the
        'app_globals' variable
        """

        root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

        config_reader.read(root_path + '/../user_privileges.ini')
        self.ADMINS = [int(c) for c in (config_reader.get('users', 'ADMINS').split(','))]
        self.REVIEWERS = [int(c) for c in (config_reader.get('users', 'REVIEWERS').split(','))]
        self.CATALOGERS = [int(c) for c in (config_reader.get('users', 'CATALOGERS').split(','))]

        self.ASSET_URL = config['ASSET_URL']
        self.DASTOR = config['DASTOR']
        self.WSDL_ADDR = config['WSDL_ADDR']
        self.ANA_URL = config['ANA_URL']
        self.SOLR_URL = config['SOLR_URL']
        
        self.ARTSTOR_API = config['ARTSTOR_API']
        self.ARTSTOR_COOKIE = config['ARTSTOR_COOKIE']
        self.ARTSTOR_LOGIN_URL = config['ARTSTOR_LOGIN_URL']
        self.ARTSTOR_LOGOUT_URL = config['ARTSTOR_LOGOUT_URL']
        self.COLLECTION_URL = config['COLLECTION_URL']
        self.IMAGEVIEWER_URL = config['IMAGEVIEWER_URL']
        self.API_LOGIN = config['API_LOGIN']
        self.API_PASSWD = config['API_PASSWD']
        self.MEMBERS_COLLECTION = config['MEMBERS_COLLECTION']
        self.EDITORS_COLLECTION = config['EDITORS_COLLECTION']
        self.HARVESTER_DB = config['HARVESTER_DB']

        self.MAX_SLOTS = int(config['MAX_SLOTS'])
        self.MAIL_SERVER = config['MAIL_SERVER'] 
        self.MAIL_SENDER = config['MAIL_SENDER'] 
        self.MIN_IMAGE_SIZE = int(config['MIN_IMAGE_SIZE'])
        self.tag_listing = eval(config['tag_listing'])
        self.tag_dict = create_tag_mapping(self.tag_listing)
        



        
