import re
import logging
import cookielib
import base64
import urllib, urllib2
import simplejson

from pylons import g

log = logging.getLogger(__name__)

def get_collection_totals():
    fh = _authenticated_api_call(g.ARTSTOR_API + 'collection/secure/collections/' + g.MEMBERS_COLLECTION)
    members_data = simplejson.loads(fh.read())
    fh.close()
    fh = _authenticated_api_call(g.ARTSTOR_API + 'collection/secure/collections/' + g.EDITORS_COLLECTION)
    editors_data = simplejson.loads(fh.read())
    fh.close()
    return dict(members_objects = members_data['objCount'], editors_objects = editors_data['objCount'])

def get_profile_id_to_email_map(profile_ids):
    """Authenticate with our API user and call into ARTstor to retrieve a JSON response mapping
    profile_ids to other user attributes.  Result comes back as a list, so map it to a dict with
    profile_id as the key"""
    if len(profile_ids) > 0:
        print profile_ids.__repr__()
        params = "ids="+",".join([str(id) for id in profile_ids])
        fh = _authenticated_api_call(g.ARTSTOR_API + 'collection/secure/user?' + params)
        users_list = simplejson.loads(fh.read())
        fh.close()
        users_data = dict()
        for u in users_list:
            u['full_name'] = _presentable_name(u)
            users_data[u['profileId']] = u
        log.critical(users_data.__repr__())
        return users_data
    else:
        return dict()

def authenticate(username, password, url='http://sahara.stage.artstor.org/collection/secure/login'):
    """
    >>> authenticate('groppe@sahara.org', 'artstor')['id']
    149178
    >>> authenticate('bogus', 'bogus')

    """

    cookies = cookielib.LWPCookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies))
    urllib2.install_opener(opener)
    cred = urllib.urlencode({'j_username': username.lower(),
                             'j_password': password.lower()})
    request = urllib2.Request(url, cred)
    request.add_header('Accept', '*/*')

    fh = opener.open(request)
    raw_resp = fh.read()
    print raw_resp
    data = simplejson.loads(raw_resp)
    fh.close()

    as_cookie = _get_artstor_cookie(cookies)
    userid = decode_cookie(as_cookie['value'])['id']
    
    try:
        if data[u'status']:
            return dict(id = userid,
                        username=data[u'user'][u'username'],
                        institution=int(data[u'user'][u'institutionId']),
                        cookie=_get_artstor_cookie(cookies))
        else:
            return None
    except KeyError:
        log.error("Error processing login - response was %s" % raw_resp)
        raise

def get_profile(cookie, url='http://sahara.stage.artstor.org/'):
    """
    >>> cookie = authenticate('groppe@sahara.org', 'artstor')['cookie']
    >>> p = get_profile("%s=%s" % (cookie['name'], cookie['value']))
    >>> p['email']
    'groppe@sahara.org'

    """
    cookies = cookielib.LWPCookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies))
    urllib2.install_opener(opener)
    request = urllib2.Request(url + 'collection/secure/profile')
    request.add_header('Cookie', cookie)
    request.add_header('Accept', '*/*')

    fh = opener.open(request)
    raw_resp = fh.read()
    data = simplejson.loads(raw_resp)
    fh.close()
    return data['profileResult']['user']
    
def update_passwd(old, new, cookie, url='http://sahara.stage.artstor.org/'):
    """
    >>> cookie = authenticate('groppe@sahara.org', 'artstor')['cookie']
    >>> p = update_passwd('artstor', 'artstor', "%s=%s" % (cookie['name'], cookie['value']))
    'groppe@sahara.org'

    """
    pass_data = urllib.urlencode({'_method': 'updatePassword',
                                  'oldPassword': old,
                                  'password': new})
    cookies = cookielib.LWPCookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies))
    urllib2.install_opener(opener)
    request = urllib2.Request(url + 'collection/secure/profile', pass_data)
    request.add_header('Cookie', cookie)
    request.add_header('Accept', '*/*')

    try:
        fh = opener.open(request)
        raw_resp = fh.read()
        fh.close()
    
        data = simplejson.loads(raw_resp)
        if data['statusCode'] is 0:
            return _get_artstor_cookie(cookies)
        else:
            raise ValueError("Invalid status code")
    except:
        raise "Error updating password"


def update_profile(data, cookie, url='http://sahara.stage.artstor.org/'):
    """
    >>> cookie = authenticate('groppe@sahara.org', 'artstor')['cookie']
    >>> data = dict(firstName='William', lastName='Groppe', email='wfg@artstor.org')
    >>> p = update_profile(data, "%s=%s" % (cookie['name'], cookie['value']))
    'groppe@sahara.org'

    """
    profile = dict(data)
    profile['_method'] = 'updateProperties'
    cookies = cookielib.LWPCookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies))
    urllib2.install_opener(opener)
    request = urllib2.Request(url + 'collection/secure/profile', urllib.urlencode(profile))
    request.add_header('Cookie', cookie)
    request.add_header('Accept', '*/*')

    fh = opener.open(request)
    raw_resp = fh.read()
    data = simplejson.loads(raw_resp)
    fh.close()
    return data


def validate_email(email):
    if len(email) > 7:
        if re.match("^.+\\@(\\[?)[a-zA-Z0-9\\-\\.]+\\.([a-zA-Z]{2,3}|[0-9]{1,3})(\\]?)$", email) != None:
            return True
    return False

def validate_password(password):
    return len(password) >= 7 and len(password) <= 20

def _get_artstor_cookie(cookies):
    cookie = [c for c in cookies if c.name == 'ARTSTOR_HASHED_REM_ME']
    return dict(name=cookie[0].name, value=cookie[0].value)

def _authenticated_api_call(url):
    cookie = authenticate(g.API_LOGIN, g.API_PASSWD, g.ARTSTOR_API + 'collection/secure/login')['cookie']
    cookies = cookielib.LWPCookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies))
    urllib2.install_opener(opener)
    request = urllib2.Request(url)
    request.add_header('Cookie', "%s=%s" %(cookie['name'], cookie['value']))
    request.add_header('Accept', '*/*')
    return opener.open(request)

    
def _test():
	import doctest
	doctest.testmod()


def decode_cookie(encoded_artstor_info):
    """
    >>> decode_cookie('ZWRAc2FoYXJhLm9yZzoxMjM4NTEzNDkxODQ2OjU1YzAzNDI1NGNiMjY3Nzk5ZDAzNDJiZjJhMzVmMTUzOmMzYzYzMzg4LWE3NzYtNDY3ZS05MjM4LTBkYzhlZWVmMWNkZTpjMGE4NWFhNzMwZDdhOGVhMzFiZDc2Yjc0MDdmYmIwOGQ1OTM3OGNiY2M2NDoxNDc2NzU=')
    {'id': 147675, 'username': 'ed@sahara.org'}
    """
    artstor_info = base64.b64decode(encoded_artstor_info)
    elements = artstor_info.split(':')
    return dict(id=elements[5], username=elements[0])

def encrypt(string, key='R1@t5&*43,.:{=zDy)_y+}]|y&7k5/#3gt,{szq!`:+', salt=0xE3):
    buf = [ord(c) for c in string]
    for index, next_salt in enumerate(buf):
        buf[index] ^= ord(key[index % len(key)]) ^ salt
        salt = next_salt
    return base64.b64encode(''.join([chr(b) for b in buf]))

def decrypt(base64_str, key='R1@t5&*43,.:{=zDy)_y+}]|y&7k5/#3gt,{szq!`:+', salt=0xE3):
    buf = [ord(c) for c in base64.b64decode(base64_str)]
    for index in range(0, len(buf)):
        buf[index] ^= salt ^ ord(key[index % len(key)])
        salt = buf[index]
    return ''.join([chr(b) for b in buf])

def get_image_viewer_link(uuid):
    from sahara.lib import harvester
    artstor_id = harvester.sahara_id_to_artstor_id(uuid)
    return g.IMAGEVIEWER_URL+"?id=%s&source=imata" % urllib.quote(encrypt(uuid))


def _presentable_name(user_hash):
    last = user_hash.get('lName', None)
    first = user_hash.get('fName', None)
    if first is None and last is None:
        return user_hash.get('email', 'Unknown')
    else:
        return ' '.join([x for x in [first, last] if x is not None])

    
if __name__ == '__main__':
    print "Logging in..."
    auth = authenticate('groppe@sahara.org', 'artstor123', url="http://sahara.artstor.org/collection/secure/login")
    print "Auth id: %s" % auth['id']
    print "Cookie: [%s] %s" % ( auth['cookie']['name'], auth['cookie']['value'] )
    print "Changing password..."
    as_cookie = "%s=%s" % (auth['cookie']['name'], auth['cookie']['value'])
    try:
        data = update_passwd('artstor123', 'artstor', as_cookie, url="http://sahara.artstor.org/")
        print "Original Cookie"
        print auth['cookie']
        print "New Cookie"
        print data['cookie']
        assert auth['cookie']['value'] != data['cookie']['value']
        as_cookie = "%s=%s" % (data['cookie']['name'], data['cookie']['value'])
        data = update_passwd('artstor', 'artstor123', as_cookie, url="http://sahara.artstor.org/")
    except:
        raise
    
