"""The base Controller API

Provides the BaseController class for subclassing.
"""
import logging
from pylons.controllers import WSGIController
from pylons.templating import render_mako as render
from pylons.controllers.util import redirect_to, abort
from pylons import request, session, response, g, tmpl_context as c
from pylons.decorators import jsonify
from routes import url_for

from sahara.model import Session
from sahara.model.user import User
from sahara.lib.authentication.artstor import decode_cookie

log = logging.getLogger(__name__)

class BaseController(WSGIController):
    unrestricted = []

    def __before__(self, action):
        if action not in self.unrestricted:
            if g.ARTSTOR_COOKIE in request.cookies.keys():
                try:
                    creds = decode_cookie(request.cookies[g.ARTSTOR_COOKIE])
                    if 'user' not in session or int(creds['id']) != session['user'].id:
                        session.clear()
                        session.save()
                        session['user'] = User(creds)
                        session.save()
                    c.username = session['user'].email
                    c.user = session['user']
                except:
                    log.error('Problem decoding ARTstor cookie', exc_info=True)
                    return redirect_to(g.ARTSTOR_LOGIN_URL)

                if 'seen_terms' not in session:
                    return redirect_to(url_for('terms'))
                    
            else:
                return redirect_to(g.ARTSTOR_LOGIN_URL)

    def __call__(self, environ, start_response):
        """Invoke the Controller"""
        # WSGIController.__call__ dispatches to the Controller method
        # the request is routed to. This routing information is
        # available in environ['pylons.routes_dict']
        try:
            return WSGIController.__call__(self, environ, start_response)
        finally:
            Session.remove()


    def is_admin(self):
        return True
        
    

            


    
