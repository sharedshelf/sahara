import csv

from sahara.model import *
from sahara.model import Session as db

class BucketQuery(object):

    def __init__(self, earliest, latest, countries):
        self.earliest = int(earliest)
        self.latest = int(latest)
        self.countries = [c.strip() for c in countries.split(',')]

    def create_bucket_query(self):
        q = 'avg_date:[%d TO %d]' % (self.earliest, self.latest)
        q += ' && (%s)' % ' || '.join(['country:"%s"' % c for c in self.countries])
        return q
        

class CSVBucketAssignments(object):

    def __init__(self):
        self.admins = []
        self.reviewers = []
        self.catalogers = []
        self.buckets = dict()
        

    def parse_csv_file(self, path):
        sheet = csv.reader(open(path))
        for row in sheet:
            try:
                if row[1] is not None:
                    int(row[1])
                    self._append_to_role(row[4].upper(), row[1])
                    if row[4].strip().upper() == 'CONTENT EDITOR':
                        if row[1] in self.buckets.keys():
                            self.buckets[row[1]].append(BucketQuery(row[5], row[6], row[7]))
                        else:
                            self.buckets[row[1]] = [BucketQuery(row[5], row[6], row[7])]
            except (IndexError, ValueError):
                print 'Rejecting bucket definition for: %s' % ', '.join(row)
        self.stats()

    def print_buckets(self):
        for key in self.buckets.keys():
            print self.buckets[key].create_bucket_query()

    def load_buckets(self):
        for key in self.buckets.keys():
            e = EditorialBucket()
            if len(self.buckets[key]) == 1:
                e.query = self.buckets[key][0].create_bucket_query()
            else:
                e.query = "("+"||".join(["(%s)" % b.create_bucket_query() for b in self.buckets[key]])+")"
            er = EditorialBucketReviewer()
            er.reviewer_id = int(key)
            e.reviewers.append(er)
            db.add(e)
            db.commit()

    def write_user_role_inifile(self, path):
        f = open(path, 'w')
        f.write('[users]\n')
        f.write('ADMINS: %s\n' % ','.join(set(self.admins)))
        f.write('REVIEWERS: %s\n' % ','.join(set(self.reviewers)))
        f.write('CATALOGERS: %s\n' % ','.join(set(self.catalogers)))
        f.close()

    def stats(self):
        print 'Found %d admins, %d reviewers, and %d catalogers.' % (len(self.admins), len(self.reviewers), len(self.catalogers))
        print 'Found %d buckets.' % len(self.buckets.items())
        print '\n\nDETAILS:'
        print 'ADMIN: %s' % ','.join(self.admins)
        print 'REVIEWERS: %s' % ','.join(self.reviewers)
        print 'CATALOGERS: %s\n' % ','.join(self.catalogers)
        for key in self.buckets.keys():
            if len(self.buckets[key]) > 1:
                print '%d has multiple buckets assigned' % int(key)

    def _append_to_role(self, role, profile_id):
        if role.strip() == 'ADMIN':
            self.admins.append(profile_id)
        elif role.strip() == 'CONTENT EDITOR':
            self.reviewers.append(profile_id)
        elif role.strip() == 'LIBRARIAN EDITOR':
            self.catalogers.append(profile_id)
        else:
            pass

        
    
