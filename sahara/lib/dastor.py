import urllib2
import re
from datetime import datetime

from paste.util.multidict import MultiDict

from pylons import g
import simplejson

"""Helper classes for using DASTOR"""

def get_metadata(uuid, keys):
    """Get asset metadata from dastor"""
    ex_data = dict()
    json = simplejson.loads(
        urllib2.urlopen("%s/stor/%s.xml" % (g.DASTOR, uuid)).read())
    for key in keys:
        if key in json.keys():
            ex_data[key] = json[key]
        else:
            ex_data[key] = None
    return ex_data


def fix_exif_date(date):
    try:
        d = date.strip()
        badformat = d.find('-')
        if badformat > 0:
            d = d[0:badformat]
        if len(d) == 10:
            return datetime.strptime(d, '%Y-%m-%d')
        elif len(d) == 19:
            return datetime.strptime(d, '%Y:%m:%d %H:%M:%S')
        else:
            return None
    except:
        return None

def format_exif_date(dt):
    if dt == None:
        return None
    else:
        return dt.strftime("%A %B %d, %Y -- %I:%M:%S %p")


def tag_normalize(data):
    """ Converts incoming tags into form that the database can understand """
    tags = MultiDict([])
    for k,v in data.iteritems():
        if g.tag_dict.has_key(k):
            tags.add(g.tag_dict[k], v)
        else:
            tags.add(k,v)
    return tags


def create_tag_mapping(tag_listing):
    """ Create dictionary for normalization of incoming tags """
    tag_dict = dict()
    for tags in tag_listing:
        for k in tags[1]:
            tag_dict[k] = tags[0]
    return tag_dict




#How to create configuration file for dastor on sahara setup
import os
def write_exif_config():
    flat_tag_list = list()
    for tags in g.tag_listing:
        flat_tag_list.extend(tags[1])

    FILE = open(g.TAG_CONF,"w")  # TAG_CONF doesn't exist yet
    FILE.writelines([tg + os.linesep for tg in flat_tag_list])
    FILE.close()


#   CreateDate
#   Description
#   Copyright
# 
#   City
#   State
#   GPSLatitude
#   GPSLongitude
# 
#   Creator
#   Keywords
