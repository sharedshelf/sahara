import re
from datetime import datetime

def parse_date(input_string):
    date_string = input_string.upper()
    year_string = re.sub(r'\d+,\s', '', date_string) # Jan 26, 1960
    year_string = re.sub(r'\d.+ QUARTER', '', date_string)
    if is_month_day_year_format(year_string):
        grab_year = re.compile(r'\d{1,2}/\d{1,2}/(\d{2}|\d{4})')
        year = int(grab_year.search(year_string).group(1))
        if year < 100:
            year += 1900
    elif is_range(year_string):
        return parse_ranges(year_string)
    else:
        year = int(re.sub('\D+', '', year_string))
    year *= calculate_year_multiplier(date_string)
    if not is_bc(date_string) and calculate_year_multiplier(date_string) == 100:
        return adjust_for_quarters(date_string) + year - 100, year - 1
    elif is_bc(date_string):
        return year * -1, year * -1
    elif is_reverse_chronology(date_string):
        year = datetime.now().year - year
    return year, year


def is_bc(date_string):
    return re.search('^-|BC|B\.C\.|BCE|MILLION|M\.', date_string)

def is_reverse_chronology(date_string):
    return re.search('BP|YEARS AGO', date_string)

def calculate_year_multiplier(date_string):
    million = re.search('MILLION|M\.', date_string)
    century = re.search('CENT$| C. $| C $|C$|CENTURY$', date_string)
    if million:
        return 1000000
    elif century:
        return 100
    else:
        return 1

def is_range(date_string):
    return re.search('-|;|\sAND\s|\sTO\s', date_string)
    
def parse_ranges(date_string):
    """Split date ranges based on: (-,;,and,to)"""
    start, end = re.split('-|;|\sAND\s|\sTO\s', date_string)
    if int(end) < 100:
        end = (int(start[:2]) * 100) + int(end)
    return int(start), int(end)
    
def crosses_bc_ad_boundary(date_string):
    if is_bc(date_string):
        if re.search(r'AD|A\.D\.|CE', date_string) is not None:
            return True
    return False

def is_month_day_year_format(date_string):
    return re.search('\d{1,2}/\d{1,2}/\d{2,4}', date_string)

def adjust_for_quarters(date_string):
    if re.search('2ND\sQUARTER|SECOND QUARTER', date_string):
        return 25
    elif re.search('3RD\sQUARTER|THIRD QUARTER', date_string):
        return 50
    elif re.search('4TH\sQUARTER|FOURTH QUARTER', date_string):
        return 75
    else:
        return 0
    

if __name__ == "__main__":
    print "input: 2004"
    print parse_date("2004")
    print "input: 1920 C.E."
    print parse_date("1920 C.E.")
    print "input: 19th Century"
    print parse_date("19th Century")
    print "input: 400BC"
    print parse_date("400BC")
    print "input: 1 million years ago"
    print parse_date("1 million years ago")
    print "input: 30 years ago"
    print parse_date("30 years ago")
    print "input: 3rd quarter of the 18th century"
    print parse_date("3rd quarter of the 18th century")
    print "input: 1920-25"
    print parse_date("1920-25")
    print "input: 1920-1925"
    print parse_date("1920-1925")    
    print "input: 1920 to 1965"
    print parse_date("1920 to 1965")
    print "input: 1320-45"
    print parse_date("1320-45")
    
