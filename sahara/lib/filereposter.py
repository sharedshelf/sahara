import sys

from multipart_post_handler import MultipartPostHandler
import urllib2, cookielib


def repost(data, destination):
    """
    Use HTTP POST to send the contents of a file to the destination.

    Params:
    data - The data to be POSTed
    destination - The URL end point to call.

    Open questions:
    Do we need to have all the data in memory?  Perhaps a file
    descriptor?  Can urllib2 handle that case?
    """
    cookies = cookielib.CookieJar()
    opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cookies),
                                  MultipartPostHandler)
    
    params = {'file': data}
    return opener.open(destination, params).read()


def main(argv=None):
    """Expects to be passed a file to repost, and a destination URL"""
    if argv == None:
        argv = sys.argv
        
    fd = open(argv[1])
    print repost(fd, argv[2])
    
if __name__ == '__main__':
    main()
      
