import ImageFile

class InvalidImageException(Exception):
    pass

class ImageSizeException(Exception):
    pass

IMAGE = 0
VIDEO = 10
AUDIO = 20

def validate(uploaded_file, min_size):
    """
    Determine type and validity of an uploaded file.

    Initially we check if the file has an extension we
    support.  Secondly, if the uploaded file appears to
    be an image, we use PIL to open it and validate the size.
    """
    file_type = determine_file_type(uploaded_file.filename)
    if file_type is IMAGE:
        (xsize, ysize) = (0,0)
        if uploaded_file != {}:
            parser = ImageFile.Parser()
            data = uploaded_file.file.read() # read(1024) works for nearly all images
            if data:
                parser.feed(data)
            if parser.image:
                (xsize, ysize) =  parser.image.size
            if  xsize == 0 or ysize == 0:
                raise InvalidImageException
            if xsize < min_size  and ysize < min_size :
                raise ImageSizeException


def determine_file_type(filename):
    normalized_filename = filename.lower()
    if normalized_filename.endswith(('jpg', 'gif', 'tif', 'png',)):
        return IMAGE
    elif normalized_filename.endswith(('mov', 'swf',)):
        return VIDEO
    elif normalized_filename.endswith('mp3'):
        return AUDIO
    else:
        raise Exception('Unknown format')


    
        
