import re

import sqlalchemy
from pylons import request
from webob import Request

from sahara.model import *
from sahara.model.sa_models import Name

__all__ = ['parse', 'create_multidict_from_http_encoded_string']

def create_multidict_from_http_encoded_string(http_encoded_string):
    req = Request({})
    req.method = 'POST'
    req.charset = 'utf8'
    req.body = http_encoded_string.encode('utf8')
    return req.POST

def parse(work, params, userid=None):
    """
    Parses form data and creates an object tree with a Work root.

    We expect a standard paster MultiDict as provided by
    request.params.  Fields are decoded based on our hierarchy
    encoding scheme.
    """
    update_work_fields(work, params)
    update_work_name(work, params)
    update_work_creators(work, params)
    update_work_location(work, params)
    update_work_chronology(work, params)
    update_work_physical_description(work, params)
    update_work_keywords(work, params)
    update_work_source(work, params)
    update_work_rights(work, params)
    update_work_editorial_notes(work, params, userid)

def update_work_fields(work, params):
    return update_record_columns(work, params)

def update_work_editorial_notes(work, params, userid):
    editorial_note = work.editorial_note if work.editorial_note is not None else EditorialNote()
    editorial_note.updated_by = userid
    update_record_columns(editorial_note, params, "editorial_note_")
    work.editorial_note = editorial_note
    return work

def update_work_name(work, params):
    name = work.name if work.name is not None else Name()
    update_record_columns(name, params)
    update_name_complex_titles(name, params)
    update_name_titles(name, params)
    update_name_view_type(name, params)
    update_name_classifications(name, params)
    work.name = name
    return work

def update_work_creators(work, params):
    creator_ids = parse_unique_numbers(find('creator-', params.keys()), 'creator-')
    updated_creators = []
    for cid in creator_ids:
        keys = find("creator-%d" % cid, params.keys())
        creator = work.creators[cid] if len(work.creators) > cid else Creator()
        if creator not in work.creators:
            work.creators.append(creator)
        prefix = "creator-%d" % cid
        update_creator_fields(creator, prefix, params)
        update_creator_attribution(creator, prefix, params)
        update_creator_extents(creator, prefix, params)
        update_creator_roles(creator, prefix, params)
        update_creator_nationalities(creator, prefix, params)
        updated_creators.append(creator)
    # Remove any excess creators from record
    work.creators = updated_creators
    return work

def update_work_location(work, params):
    l = work.location if work.location is not None else Location()
    update_record_columns(l, params)
    l.countries = []
    for country in dedup(params.getall('country')):
        country_id = parse_integer(country)
        if country_id is not None:
            l.countries.append(Country.get(country))
    work.location = l
    return work

def update_work_chronology(work, params):
    c = work.chronology if work.chronology is not None else Chronology()
    update_record_columns(c, params)
    update_chronology_periods(c, params)
    work.chronology = c
    return work

def update_work_physical_description(work, params):
    pd = work.physical_description if work.physical_description is not None else PhysicalDescription()
    update_record_columns(pd, params)
    work.physical_description = pd
    return work

def update_work_source(work, params):
    source = work.source if work.source is not None else Source()
    update_record_columns(source, params)
    work.source = source
    return work

def update_work_rights(work, params):
    rights = work.rights if work.rights is not None else Rights()
    update_record_columns(rights, params)
    work.rights = rights
    return work

def update_name_complex_titles(name, params):
    if 'complex_title' in params.keys():
        complex_titles = dedup(params.getall('complex_title'))
        name.complex_titles = []
        for complex_title in complex_titles:
            instance = ComplexTitle.find_or_create_by(value=complex_title)
            name.complex_titles.append(instance)
    return name

def update_name_titles(name, params):
    if 'title' in params.keys():
        titles = dedup(params.getall('title'))
        name.titles = [] # clear titles
        for title in titles:
            instance = Title.find_or_create_by(value=title)
            name.titles.append(instance)
    return name

def update_name_view_type(name, params):
    if 'view_type' in params.keys():
        view_type_id = parse_integer(params.get('view_type'))
        if view_type_id is not None:
            name.view_type = ViewType.get(view_type_id)
        else:
            name.view_type = None
    return name
    
def update_name_classifications(name, params):
    if 'classifications' in params.keys():
        classifications = dedup(params.getall('classifications'))
        name.classifications = []
        for classification in classifications:
            c = Classification.get_by(value=classification)
            if c is not None:
                name.classifications.append(c)
            else:
                log.error('Invalid value for classification: %s' % classification)
        if u'Architecture and City Planning' in classifications or \
                u'Garden and Landscape' in classifications:
            update_name_narrow_classification(name, params)
        else:
            name.narrow_classification = None
    return name

def update_name_narrow_classification(name, params):
    narrow_id = parse_integer(params.get('narrow_classification', None))
    if narrow_id is not None:
        name.narrow_classification = \
            NarrowClassification.get(parse_integer(narrow_id))
    else:
        name.narrow_classification = None
    return name

def update_chronology_periods(chronology, params):
    if 'period_dynasty' in params.keys():
        pds = dedup(params.getall('period_dynasty'))
        chronology.period_dynasty = []
        for pd in pds:
            instance = Period.find_or_create_by(value=pd)
            chronology.period_dynasty.append(instance)
    return chronology

def update_work_keywords(work, params):
    if params.get('keywords') is not None:
        keywords = dedup(params.get('keywords').split(','))
        work.keywords = []
        for keyword in keywords:
            instance = Keyword.find_or_create_by(value=keyword)
            work.keywords.append(instance)
    return work

def update_creator_fields(creator, prefix, params):
    return update_record_columns(creator, params, prefix=prefix+'_')

def update_creator_attribution(creator, prefix, params):
    if prefix + '_attribution' in params.keys():
        attribution_id = parse_integer(params.get("%s_attribution" % prefix))
        if attribution_id is not None:
            creator.attribution = Attribution.get(attribution_id)
        else:
            creator.attribution = None
    return creator

def update_creator_extents(creator, prefix, params):
    if prefix + '_extent' in params.keys():
        extents = dedup(params.getall("%s_extent" % prefix))
        creator.extents = []
        for extent in extents:
            instance = Extent.find_or_create_by(value=extent)
            creator.extents.append(instance)
    return creator

def update_creator_roles(creator, prefix, params):
    if prefix + '_role' in params.keys():
        roles = dedup(params.getall("%s_role" % prefix))
        creator.roles = []
        for role_id in roles:
            creator.roles.append(Role.get(role_id))

def update_creator_nationalities(creator, prefix, params):
    if prefix + '_nationality' in params.keys():
        nationalities = dedup(params.getall("%s_nationality" % prefix))
        creator.nationalities = []
        for nationality in nationalities:
            instance = Nationality.find_or_create_by(value=nationality)
            creator.nationalities.append(instance)
    return creator

def find(value, list_):
    return [v for v in list_ if v[:len(value)] == value]

def parse_unique_numbers(src_array, prefix):
    found = list()
    pat = re.compile(prefix+'(\d+)')
    for item in src_array:
        m = pat.match(item)
        if m is not None:
            found.append(int(m.groups()[0]))
    return list(set(found))

def update_record_columns(instance, params, prefix=''):
    for c in instance.__table__.columns:
        if c.name in instance.__class__.form_deny:
            continue
        if prefix+c.name not in params.keys():
            continue
        if c.type.__class__ == sqlalchemy.types.Boolean:
            instance.__setattr__(c.name, parse_boolean(params[prefix+c.name]))
        elif c.type.__class__ == sqlalchemy.types.Integer:
            try:
                instance.__setattr__(c.name, int(params[prefix+c.name]))
            except ValueError:
                instance.__setattr__(c.name, None)
        elif c.type.__class__ == sqlalchemy.types.Float:
            try:
                instance.__setattr__(c.name, float(params[prefix+c.name]))
            except ValueError:
                instance.__setattr__(c.name, None)
        else:
            if params[prefix+c.name] is None or len(params[prefix+c.name]) is 0:
                instance.__setattr__(c.name, None)
            else:
                instance.__setattr__(c.name, params[prefix+c.name])
    return instance

def dedup(values):
    deduped = []
    for v in values:
        if len(v) > 0 and v not in deduped:
            deduped.append(v)
    return deduped

def parse_boolean(value):
    if value is True or value is False:
        return value
    test = str(value).strip().lower()
    return not test in ['false', 'f', 'no', '0', '']

def parse_integer(value):
    if value is not None:
        try:
            return int(value)
        except (TypeError, ValueError):
            return None
    else:
        return None
