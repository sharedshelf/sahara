import logging
from sqlalchemy.ext.sqlsoup import SqlSoup
from pylons import g

log = logging.getLogger(__name__)

def sahara_id_to_artstor_id(sahara_id):
    try:
        db = SqlSoup(g.HARVESTER_DB)
        record = db.transfers.filter(db.transfers.imata_id==sahara_id).first()
        return record.artstor_id
    except:
        log.error("Unable to get artstor_id for sahara_id[%s]" % sahara_id)
        return None
