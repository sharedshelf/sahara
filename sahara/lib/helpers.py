import re

from routes import url_for
from pylons import request, session, config
from webhelpers.util import html_escape as escape
from pylons import tmpl_context as c


from sahara.lib.authentication.artstor import get_profile_id_to_email_map

"""Helper functions

Consists of functions to typically be used within templates, but also
available to Controllers. This module is available to both as 'h'.
"""
# Import helpers as desired, or define your own, ie:
# from webhelpers.html.tags import checkbox, password

from sahara.model import *

def classifications():
    return Session.query(Classification).all()

def narrow_classifications():
    return Session.query(NarrowClassification).all()

def attributions():
    return Session.query(Attribution).all()

def roles():
    return Session.query(Role).all()

def viewtypes():
    return Session.query(ViewType).all()

def countries():
    return Session.query(Country).order_by(Country.value).all()

def is_full_view():
    if 'view' in request.cookies.keys():
        if 'brief' == request.cookies['view']:
            return False
    return True

def indexed_label(name, index):
    if 0 == index:
        return name
    else:
        return name + " (%d)" % (index + 1)

def is_admin():
    return session['user'].is_admin

def get_editorial_notes(work):
    if work.editorial_note:
        return work.editorial_note
    else:
        return EditorialNote()

def get_titles(name):
    if len(name.titles) > 0:
        return name.titles
    else:
        return[Title()]

def get_complex_titles(name):
    if len(name.complex_titles) > 0:
        return name.complex_titles
    else:
        return[ComplexTitle()]

def get_creators(work):
    if len(work.creators) > 0:
        return work.creators
    else:
        return [Creator()]

def get_creator_extents(creator):
    if len(creator.extents) > 0:
        return creator.extents
    else:
        return [Extent()]

def get_creator_nationalities(creator):
    if len(creator.nationalities) > 0:
        return creator.nationalities
    else:
        return [Nationality()]

def get_countries(location):
    if len(location.countries) > 0:
        return location.countries
    else:
        return [Country()]

def get_periods(chronology):
    if len(chronology.period_dynasty) > 0:
        return chronology.period_dynasty
    else:
        return [Period()]

def document_form_type(name):
    if 'view' in request.cookies:
        if name == request.cookies['view']:
            return 'selected'

def editors_email(profile_id):
    if profile_id in c.editors:
        return c.editors[profile_id].get('email', 'Unknown')
    else:
        return 'Unknown(%s)' % profile_id

def map_profile_ids_to_profile_data(ids):
    return get_profile_id_to_email_map(ids)

def is_creator_locked(creator):
    return creator.ana_id is not None and len(creator.ana_id) > 0

def is_locked(work):
    print "is_locked:" + str(work.status.reserved_by)
    return work.status.reserved_by is not None

def is_locked_by(work, user_id):
    return work.status.reserved_by == user_id
    
def is_locked_by_others(work):
    b = is_locked(work) and not session['user'].has_reserved(work.uuid)
    print "\nis_locked_by_others:" + str(b)
    return b

def is_suppressed(work):
    return work.status.suppress == True

def is_readonly(work):
    if session['user'].is_admin is True:
        return is_locked_by_others(work)
    else:
        print "is_readonly - work in perview?" + str(work.uuid in session['user'].editable_work_ids)
        return (not work.uuid in session['user'].editable_work_ids) or is_locked_by_others(work)

def button(text, onclick=None, id=None, classes=''):
    html = '<a href="#"'
    html += " class='button %s'" % classes
    if onclick is not None:
        html += """ onclick="%s; return false;" """ % onclick
    if id is not None:
        html += """ id="%s" """ % id
    html += '><span class="button-text">%s</span></a>' % text
    return html
        
def get_list_with_default(dictionary, key):
    if dictionary.has_key(key):
        if type(dictionary[key]) is list:
            return [escape(x) for x in dictionary[key]]  
        else:
            return [escape(dictionary[key])]
    else:
        return ['&nbsp;']

def get_search_result_value(dictionary, key):
    if dictionary.has_key(key):
        if type(dictionary[key]) is list:
            value = dictionary[key][0]
        else:
            value = dictionary[key]
        if len(value) > 20:
            split_words = [escape(value[x:x+10]) for x in range(0, len(value), 10)]
            return '<wbr/>'.join(split_words)
        else:
            return value
    else:
        return ''
    
def search_link(text, page=1, view=None, classname=""):
    if view is None:
        view = c.view
    return "<a class='nav-control %s' href='%s?phrase=%s&amp;rows=%s&amp;page=%s&amp;view=%s&amp;sort=%s&amp;filter=%s'>%s</a>" % (
        classname, url_for('search'), c.phrase, c.rows, page, view, c.sort, c.filter, text)

def bucket_link(text, page=1, view=None, classname=""):
    if view is None:
        view = c.view
    return "<a class='nav-control %s' href='%s?rows=%s&amp;page=%s&amp;view=%s&amp;sort=%s'>%s</a>" % (
        classname, url_for('bucket'), c.rows, page, view, c.sort, text)

def string_to_int(string, default=0):
    try:
        return int(string)
    except:
        return default

def string_to_boolean(string, default=False):
    try:
        if string[0] in ['T', 't']:
            return True
        else:
            return False
    except:
        return default

def get_route_path(route_name):
    return config['routes.map']._routenames[route_name].routepath

def work_in_perview(work_data, perview):
    return work_data['uuid'] in perview

def work_is_locked(work_data):
    return 'reserved_by' in work_data.keys()

def work_locked_by(work_data, user_id):
    return work_data['reserved_by'] == user_id
    
# def search_result_out_of_perview(search_result):
#     if session['user'].is_admin is True:
#         return False
#     else:
#         if type(c._editable_work_ids) is not list:
#             c._editable_work_ids = session['user'].editable_works()
#         return not work_in_perview(search_result, c._editable_works)

# def search_result_locked_to_me(search_result):
#     return search_result.get('reserved_by', 0) == session['user'].id

def search_result_is_locked(search_result):
    return search_result.get('reserved_by', 0) != 0
    
def search_result_access_icon(search_result):
    if session['user'].id == search_result.get('reserved_by', -1):
        return '<p class="unlock">\
                  <img src="images/locked-editable-icon.png" alt="Unlock, so others can edit this record.">\
                  <a href="#">Unlock, so others can edit this record.</a>\
                </p>\
                <p style="display:none" class="lock">\
                  <img src="images/editable-icon.png" alt="Lock, so only you can edit this record.">\
                  <a href="#">Lock, so only you can edit this record.</a>\
                </p>'
    elif search_result_is_locked(search_result):
        return '<p>\
                  <img src="images/locked-icon.png" alt="Record is being edited by another editor.">\
                  <span href="#">Record is being edited by %s</span>\
                </p>' % c.reserved_map.get(search_result.get('reserved_by', -1), dict()).get('email', 'Unknown')
    elif session['user'].can_edit(search_result['uuid']):
        return '<p class="lock">\
                  <img src="images/editable-icon.png" alt="Lock, so only you can edit this record.">\
                  <a href="#">Lock, so only you can edit this record.</a>\
                </p>\
                <p style="display:none" class="unlock">\
                  <img src="images/locked-editable-icon.png" alt="Unlock, so others can edit this record.">\
                  <a href="#">Unlock, so others can edit this record.</a>\
                </p>'
    else:
        return '<p>\
            <img src="images/locked-icon.png" alt="You do not have permission to edit this record.">\
            <span href="#">You do not have permission to edit this record.</span>\
            </p>' 

def search_result_access_icon_small(search_result):
    if session['user'].id == search_result.get('reserved_by', -1):
        return '<p class="unlock">\
                  <img src="images/locked-editable-icon.png" width="35">\
                </p>\
                <p style="display:none" class="lock">\
                  <img src="images/editable-icon.png" width="35">\
                </p>'
        #return '<img src="images/locked-editable-icon.png" alt="" width="35">'
    elif search_result_is_locked(search_result):
        return '<img src="images/locked-icon.png" alt="" width="35">'
    elif session['user'].can_edit(search_result['uuid']):
        return '<p class="lock">\
                  <img src="images/editable-icon.png" width="35">\
                </p>\
                <p style="display:none" class="unlock">\
                  <img src="images/locked-editable-icon.png" width="35">\
                </p>'
        #return '<img src="images/editable-icon.png" alt="" width="35">'
    else:
        return '<img src="images/locked-icon.png" alt="" width="35">'

def original_contributor(work, editor_map):
    try:
        if work.history is not None and len(work.history) > 0:
            return editors_email(work.history[-1].updated_by)
        else:
            return editors_email(work.updated_by)
    except IndexError:
        return 'unknown'
