from pylons import request

# IMPORTANT:  The cache is stored in the request for two reasons.
# 1.  Pylons lib functions presist through requests
# 2.  The user object is serialized in a secure cookie, adding the cache exceeds the maximum cookie length
#
# Don't change this unless change how sessions are stored.
def memoize(func):
    def cache(*args, **kw):
        key = '_%s_result' % func.__name__
        if hasattr(request, key) is True:
            return getattr(request, key)
        else:
            setattr(request, key, func(*args, **kw))
            return getattr(request, key)
    return cache
