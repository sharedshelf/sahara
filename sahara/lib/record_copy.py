from sahara.model import *

log = logging.getLogger(__name__)

__all__ = ['copy_work']

def copy_work(uuid, fields=[], destinations=None):
    """
    Copies field sets from source work to multiple destinations.

    Params:
    uuid - The work record to use as source
    fields - Groups of fields to copy
    dest_uuids - The uuids of the destination works

    """
    src = Work.get_by(uuid=uuid)
    if 'title' in fields:
        _copy_title(src, destinations)
    if 'creator' in fields:
        _copy_creator(src, destinations)
    if 'location' in fields:
        _copy_location(src, destinations)
    if 'chronology' in fields:
        _copy_chronology(src, destinations)
    if 'physical' in fields:
        _copy_physical(src, destinations)
    if 'subject' in fields:
        _copy_subject(src, destinations)
    if 'source' in fields:
        _copy_source(src, destinations)
    if 'rights' in fields:
        _copy_rights(src, destinations)
    for work in destinations: # Update status for all the copied records
        work.update_status()

def _copy_title(src, destinations=None):
    for d in destinations:
        n = Name()
        n.image_view = src.name.image_view
        n.narrow_classification = src.name.narrow_classification
        n.view_type = src.name.view_type
        n.titles = []
        for t in src.name.titles:
            n.titles.append(t)
        n.complex_titles = []
        for ct in src.name.complex_titles:
            n.complex_titles.append(ct)
        n.classifications = []
        for c in src.name.classifications:
            n.classifications.append(c)
        d.name = n

def _copy_creator(src, destinations=None):
    for d in destinations:
        d.creators = []
        for creator in src.creators:
            nc = Creator()
            nc.attribution_id = creator.attribution_id
            nc.name = creator.name
            nc.ana_id = creator.ana_id
            nc.earliest_date = creator.earliest_date
            nc.latest_date = creator.latest_date
            for nationality in creator.nationalities:
                nc.nationalities.append(nationality)
            for extent in creator.extents:
                nc.extents.append(extent)
            for role in creator.roles:
                nc.roles.append(role)
            d.creators.append(nc)

def _copy_location(src, destinations=None):
    for d in destinations:
        l = Location()
        l.street_address = src.location.street_address
        l.city_county = src.location.city_county
        l.state_province = src.location.state_province
        l.latitude = src.location.latitude
        l.longitude = src.location.longitude
        l.repository = src.location.repository
        l.repository_id = src.location.repository_id
        for country in src.location.countries:
            l.countries.append(country)
        d.location = l

def _copy_chronology(src, destinations=None):
    for d in destinations:
        c = Chronology()
        c.display_date = src.chronology.display_date
        c.earliest_date = src.chronology.earliest_date
        c.latest_date = src.chronology.latest_date
        c.period_dynasty = []
        for period in src.chronology.period_dynasty:
            c.period_dynasty.append(period)
        d.chronology = c

def _copy_physical(src, destinations=None):
    for d in destinations:
        p = PhysicalDescription()
        p.material_technique = src.physical_description.material_technique
        p.measurements = src.physical_description.measurements
        p.description = src.physical_description.description
        p.commentary = src.physical_description.commentary
        p.style = src.physical_description.style
        d.physical_description = p
        
def _copy_subject(src, destinations=None):
    for d in destinations:
        d.keywords = []
        for keyword in src.keywords:
            d.keywords.append(keyword)

def _copy_source(src, destinations=None):
    for d in destinations:
        s = Source()
        s.information_source = src.source.information_source
        s.photographer = src.source.photographer
        s.contributor = src.source.contributor
        s.inst_contributor = src.source.inst_contributor
        s.image_date = src.source.image_date
        s.image_earliest_date = src.source.image_earliest_date
        s.image_latest_date = src.source.image_latest_date
        d.source = s
        
def _copy_rights(src, destinations=None):
    for d in destinations:
        r = Rights()
        r.image_copyright = src.rights.image_copyright
        r.image_ownership = src.rights.image_ownership
        r.academic_publishing = src.rights.academic_publishing
        r.other_contexts = src.rights.other_contexts
        d.rights = r
        
        
