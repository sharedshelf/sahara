from sqlalchemy import Integer, String, Unicode, Boolean, UnicodeText, DateTime
from sqlalchemy import Column, ForeignKey, Float, Table, func
from sqlalchemy.orm import relation
from sqlalchemy.orm.attributes import InstrumentedAttribute
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.ext.orderinglist import ordering_list
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.orm import scoped_session, sessionmaker, Mapper

from sahara.model.sa_extensions import *

Session = scoped_session(sessionmaker(autoflush=False, autocommit=False))

from validators import *
from serializers import *
from base import SaharaBase, before_update

# this will be called in config/environment.py
def init_model(engine):
    metadata.bind = engine

def create_extended_mapper(class_, local_table=None, *args, **params):
    return Mapper(class_, local_table,
                  extension=CallbackMapperExtension(), *args,  **params)


from sa_models import *

metadata = Base.metadata
