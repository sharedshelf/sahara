import sys
from datetime import datetime

from sqlalchemy.orm.interfaces import MapperExtension, SessionExtension
from sqlalchemy.orm.attributes import instance_state, get_history
from sqlalchemy.sql.expression import func

from sahara.model import Session


def before_update(*args):
    class_locals = sys._getframe(1).f_locals
    if '_call_before_update' not in class_locals:
        class_locals['_call_before_update'] = []
    class_locals['_call_before_update'].extend(args)

                
class SaharaBase(object):
    """
    This class provides some custom extensions to the ORM layer.
    """

    @classmethod
    def get_by(cls, **kwargs):
        return Session.query(cls).filter_by(**kwargs).first()

    @classmethod
    def get(cls, id):
        return Session.query(cls).get(id)

    @classmethod
    def query(cls):
        return Session.query(cls)

    @classmethod
    def find_or_create_by(cls, **kwargs):
        instance = cls.get_by(**kwargs)
        if instance is not None:
            return instance
        else:
            instance = cls()
            for key in kwargs.keys():
                instance.__setattr__(key, kwargs[key])
            Session.add(instance)
            return instance

    def validate(self, errors=None):
        """Determines if a record is valid.

        Since validity can't be enforced by the database because of
        design constraints, validity is tested here.  This function returns
        either (True, None) or (False, errors), errors being a array of
        error messages suitable for presentation to the user.
        
        """
        errors = [] if (errors is None) else errors

        if hasattr(self, '_validators') is True:
            for validator in self._validators:
                valid, message = validator(self)
                if valid is not True:
                    errors.append(message)
                
        if len(errors) == 0:
            return (True, None)
        else:
            return (False, errors)

    def has_changes(self, attribute):
        """Helper method for testing for attribute changes."""
        return get_history(instance_state(self), attribute).has_changes()



