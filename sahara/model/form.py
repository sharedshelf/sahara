
def parse(work, params):
    params.setdefault(None)
    for c in work.table.columns:
        if c in work.__class__.form_deny:
            continue
        if c.type == types.Boolean:
            work.__setattr__(c, bool(params[c]))
        elif c.type == types.Integer:
            try:
                work.__setattr__(c, int(params[c]))
            except TypeError:
                pass
        else:
            work.__setattr__(c, params[c])
    clear_list(work.titles)
    titles = params.getall('titles')
    [work.titles.append(Title(t)) for t in titles]
    classifications = params.getall('classifications')
    clear_list(work.classifications)
    [work.classifications.append(Classification(c)) for c in classifications]
    periods = params.getall('period_dynasty')
    clear_list(work.periods)
    [work.period_dynasty.append(PeriodDynasty(p)) for p in periods]
    keywords = params.get('keywords').split(',')
    clear_list(work.keywords)
    [work.keywords.append(Keyword(k)) for k in keywords]
    
def clear_list(collection):
    for item in collection:
        collection.remove(item)
