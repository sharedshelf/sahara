from sahara.model import metadata, Work, Status, Name, ComplexTitle, WorkPublication, Session
from pylons import g

def migrate_to_one_point_zero(collection_id=None):
    if collection_id is None:
        print "Please supply the collection_id of the Members' Collection. (Usually g.MEMBERS_COLLECTION)"
    else:
        print "Starting migration to SPOT 1.0"
        print "Creating additional tables"
        metadata.create_all(checkfirst=True)
        engine = metadata.bind
        connection = engine.connect()
        db = Session(bind=connection)
        print "Adding columns to existing tables"
        db.execute('alter table status add reserved_by integer;')
        db.flush()
        db.commit()
        db.execute('alter table status add contributed_on timestamp without time zone;')
        db.flush()
        db.commit()
        db.execute('alter table status add reviewed timestamp without time zone;')
        db.flush()
        db.commit()
        db.execute('alter table status add last_published timestamp without time zone;')
        db.flush()
        db.commit()
        names = db.query(Name).all()
        print "Building name/complex_title relations"
        for name in names:
            if name.complex_title is not None:
                name.complex_titles.append(ComplexTitle.find_or_create_by(value=name.complex_title))
                db.commit()
        print "Update all work records, converting complete to contribution on, and add WorkPublication"
        works = Work.query().join(Status).filter("status.complete = true").all()
        for work in works:
            if len(work.history) > 0:
                work.status.contributed_on = work.history[-1].updated_on
                work.status.last_published = work.history[-1].updated_on
            else:
                work.status.contributed_on = work.updated_on
                work.status.last_published = work.updated_on
            publication = WorkPublication()
            publication.artstor_collection_id = collection_id
            publication.work_id = work.id
            db.add(publication)
        db.commit()
        print "Drop column complete from the status table"
        db.execute('alter table status drop complete;')
        db.flush()
        db.commit()
        print "Migration complete"
        from sahara.model.search import Solr
        s = Solr(g.SOLR_URL)
        s.reindex()
    


    
