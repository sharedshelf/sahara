from datetime import datetime

from sqlalchemy.orm.interfaces import MapperExtension

class CallbackMapperExtension(MapperExtension):

    def before_update(self, mapper, conn, instance):
        if hasattr(instance, 'updated_on'):
            instance.updated_on = datetime.now()
        if hasattr(instance, 'before_update'):
            instance.before_update()
        if hasattr(instance.__class__, '_call_before_update'):
            calls = instance.__class__._call_before_update
            for call in calls:
                if hasattr(instance, call) is True:
                    getattr(instance, call).__call__()
                else:
                    raise Exception("Expected callback '%s' missing" % call)
