"""
Database model description and object relational mapper definitions.

This is the core of the IMATA/Sahara application.  All user
provided data is stored in the database objects defined with-in
this module.

The module depends on the pylons global application variables to
resolve links to the media thumbnails associated with works.
"""

import cgi
import uuid
import logging
from datetime import datetime

import yaml

from pylons import g

from sahara.model import *
from sahara.model.search import Solr
from sqlalchemy.orm import synonym

log = logging.getLogger(__name__)

# Set custom base class for inheriting from
Base = declarative_base(cls=SaharaBase, mapper=create_extended_mapper)

# Straight many to many relation tables
titles_classifications = Table('names_classifications', Base.metadata,
                              Column('id', Integer, primary_key=True),
                              Column('name_id', Integer,
                                     ForeignKey('names.id', ondelete="CASCADE")),
                              Column('classification_id', Integer,
                                     ForeignKey('classifications.id'))
                              )

creators_roles = Table('creators_roles', Base.metadata,
                       Column('id', Integer, primary_key=True),
                       Column('creator_id', Integer,
                              ForeignKey('creators.id', ondelete="CASCADE")),
                       Column('role_id', Integer, ForeignKey('roles.id'))
                       )


# TODO: This table has no foreign key on reviewer_id.
# Users are not currently stored in this database, it should be
# decided if we want to pull users into a table or reference them
# by their id straight to the separate system (which implies no foreign key).
#editorial_bucket_reviewers = Table('editorial_bucket_reviewers', Base.metadata,
#                                  Column('id', Integer, primary_key=True),
#                                  Column('editorial_bucket_id', Integer, 
#                                         ForeignKey('editorial_buckets.id', ondelete="CASCADE")),
#                                  Column('reviewer_id', Integer))


class Status(Base):
    """
    Stores simple status flags associated with a Work.

    This class implements a simple state tracking system as a Work moves
    through the system.  Currently is tracks two states valid and complete.

    ''valid'' state represents a Work that has passed all validations and is
    available for contribution.  ''complete'' state represents a Work that
    has been contributed and may no longer be edited by a regular user.  The
    'suppress' state indicates an administrator has removed the image from
    the Sahara Digital Library.
    
    """
    __tablename__ = 'status'
    
    id = Column(Integer, primary_key=True)
    valid = Column(Boolean, server_default='f')
    suppress = Column(Boolean, server_default='f')
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'), unique=True)
    reserved_by = Column(Integer)
    reviewed = Column(DateTime)
    contributed_on = Column(DateTime)
    last_published = Column(DateTime)
    
    def getcomplete(self):
        return self.contributed_on != None
    
    def setcomplete(self, value):
        if value is True:
            self.contributed_on = datetime.now()
            
    complete = property(getcomplete, setcomplete)
    
class Work(Base):
    """
    Represents an individual work, all other information is associated back to a Work.
    """
    __tablename__ = 'works'

    id = Column(Integer, primary_key=True)
    uuid = Column(String(40), unique=True, nullable=False)
    created_by = Column(Integer, nullable=False)
    created_on = Column(DateTime, default=func.now())
    updated_by = Column(Integer, nullable=False)
    updated_on = Column(DateTime, default=func.now())
    media_uuid = Column(String(40), unique=True)
    media_uploaded_on = Column(DateTime)
    media_filename = Column(Unicode(255))
    media_captured_on = Column(DateTime)
    media_type = Column(String(5), default='image')
    media_width = Column(Integer)
    media_height = Column(Integer)
    media_category = Column(String(3))
    media_format = Column(String(4))

    name = relation('Name', lazy=False, uselist=False, backref='work', cascade='all')
    creators = relation('Creator', lazy=False, order_by="Creator.id.asc()", backref='work', cascade='all')
    location = relation('Location', lazy=False, uselist=False, backref='work', cascade='all')
    chronology = relation('Chronology', lazy=False, uselist=False, backref='work', cascade='all')
    physical_description = relation('PhysicalDescription', lazy=False, uselist=False, backref='work',
                                    cascade='all')
    source = relation('Source', lazy=False, uselist=False, backref='work', cascade='all')
    rights = relation('Rights', lazy=False, uselist=False, backref='work', cascade='all')
    status = relation('Status', lazy=False, uselist=False, backref='work', cascade='all')
    history = relation('History', backref='work', cascade='all', order_by='History.updated_on.desc()')
    editorial_note = relation('EditorialNote', lazy=False, uselist=False, backref='work', cascade='all')
    assignments = relation('Assignment', lazy=False, backref='work', cascade='all')
    
    # Association proxies to preserve ordering
    workskeywords = relation('WorksKeywords', backref='work', cascade='all, delete-orphan',
                             collection_class=ordering_list('position'))
    keywords = association_proxy('workskeywords', 'keyword')
    publication_destination = relation('WorkPublication', lazy=False, uselist=False, backref='work', cascade='all')

    # Call backs
    before_update('update_search_engine', 'save_interesting_history')

    # Validations
    must_exist('name', 'Name relation must exist')
    at_least_one('creators', 'Must have as least one Creator relation')
    must_exist('location', 'Location relation must exist')
    must_exist('chronology', 'Chronology relation must exist')
    must_exist('physical_description', 'PhysicalDescription relation must exist')
    must_exist('source', 'Source relation must exist')
    must_exist('rights', 'Rights relation must exist')

    # Prevent updates from form submittal
    form_deny = ['id', 'uuid', 'created_on', 'created_by',
                 'updated_on', 'updated_by', 'media_uuid',
                 'media_uploaded_on', 'media_filename',
                 'media_captured_on', 'media_type']

    def __init__(self, **kwargs):
        super(Work, self).__init__(**kwargs)
        self.uuid = str(uuid.uuid4())
        self.updated_by = self.created_by
        self.status = Status()

    def _get_suppressed(self):
        return self.status.suppress

    def _set_suppressed(self, suppress):
        log.critical("SUPPRESSING! with: %s" % str(suppress))
        stamp = datetime.now()
        self.updated_on = stamp
        self.status.suppress = suppress
        self.status.last_published = stamp
#        db.add(self.status)

    suppressed = property(_get_suppressed, _set_suppressed)
    
    def brief_filename(self, size=0):
        """
        Return a partial filename depending on the size parameter.

        Basic formula is 15 more characters for each increase in size.

        """
        assert(size > -1 and size < 3)
        length = (size + 1) * -15
        return unicode(self.media_filename)[length:]

    def thumbnail(self, size=0):
        """Return the URL for this works thumbnail based on requested size""" 
        return g.ASSET_URL+self.media_uuid+'_size'+str(size)

    def is_edited(self):
        """Helper method for displaying unedited records."""
        if self.updated_on is None:
            return False
        return True

    def update_status(self):
        if self.status is None:
            self.status = Status()
        complete, errors = self._check_for_completeness()
        if complete is True:
            self.status.valid = True
        else:
            self.status.valid = False

    def is_contributable(self):
        return self._check_for_completeness()

    def reserve(self, userid):
        self.status.reserved_by = userid
        self.update_timestamp()#search_engine()

    def rollback(self, history_id, userid = None):
        from sahara.lib.form_parser import parse, create_multidict_from_http_encoded_string
        entry = [h for h in self.history if h.id == history_id]
        if(len(entry) > 0):
            parse(self, create_multidict_from_http_encoded_string(entry[0].data), userid)
        else:
            log.warn('Invalid history_id(%s) specified for work(%s)' % (history_id, self.id))
            raise "Invalid history specified for work"
                
    def reserved_by(self):
        return self.status.reserved_by
        
    def mark_reviewed(self, user_id):
        self.update_timestamp()
        reviewed_stamp = datetime.now()
        self.updated_by = user_id
        self.status.reviewed = reviewed_stamp
        self.status.last_published = reviewed_stamp
    
    def reviewed(self):
        return self.status.reviewed
    
    def assign_to(self, userids):
        for assignment in self.assignments:
            Session.delete(assignment)
        self.status.reserved_by = None
        for userid in userids:
            assignment = Assignment()
            assignment.user_id = int(userid)
            self.assignments.append(assignment)
        self.update_timestamp()

    def contribute(self, artstor_collection_id):
        self.update_timestamp()
        if self.publication_destination is None:
            self.publication_destination = WorkPublication()
        self.publication_destination.artstor_collection_id = artstor_collection_id
        contributed_stamp = datetime.now()
        self.status.reserved_by = None
        self.status.contributed_on = contributed_stamp
        self.status.last_published = contributed_stamp

    def _check_for_completeness(self):
        """Invokes all validations, and returns either (True, None) or (False, [Errors])"""
        errors = self.validate()[1]
        for attr in dir(self.__class__):
            if type(getattr(self.__class__, attr)) is InstrumentedAttribute:
                relation = getattr(self, attr)
                if relation is not None and hasattr(relation, 'validate'):
                   errors = getattr(relation, 'validate').__call__(errors)[1] 
        if errors is None:
            return (True, None)
        else:
            return (False, errors)

    def update_timestamp(self):
        self.updated_on = datetime.now()

    def contribution_date(self):
        return self.status.contributed_on

    def contributed_by(self):
        assert(self.status.complete)
        if len(self.history) > 0:
            return self.history[-1].updated_by
        else:
            return self.updated_by

    def save_interesting_history(self):
        """
        Saves a history of edits starting when contribution occurs.
        
        Retrieves the original record, and saves a HTTP POST serialization
        to the history table.  Changes to media_uuid are not saved as they
        represent changes to the asset and are explicitly exempted per
        the spec.
        """
        if self.status.complete is True and \
                self.has_changes('media_uuid') is False:
            updated_serialized = unicode(HTTPPostSerializer().serialize(self))
            # Start a new session so we can fetch the original record
            # TODO: Find a better way to do this
            ns = Session.session_factory()
            orig = ns.query(Work).filter_by(id=self.id).one()
            orig_serialized = unicode(HTTPPostSerializer().serialize(orig))
            # Only update history if something changed
            # Compare dicts as key position is not guaranteed in HTTP serialization
            if dict(cgi.parse_qsl(orig_serialized)) != dict(cgi.parse_qsl(updated_serialized)):
                log.debug("Saving history!")
                h = History(data=orig_serialized,
                            updated_on = orig.updated_on,
                            updated_by = orig.updated_by)
                self.history.append(h)
            ns.close()
        
    def update_search_engine(self):
        """Updates the external search engine with the new Work data."""
        try:
            Solr(g.SOLR_URL).add_work(self)
        except:
            print "Can't add to SOLR"
            log.critical("Can't add to SOLR!!!", exc_info=True)

    def to_xml(self):
        return XMLSerializer().serialize(self)
    
    @classmethod # Slower, but correct.
    def mark_for_republication(self, ids):
        from sahara.model import Session as db
        now = datetime.now()
        log.critical("Works affected: %d" % len(ids))
        works = db.query(Work).filter(Work.id.in_(ids)).all()
        for work in works:
            work.status.last_published = now
            work.updated_on = now
            db.add(work)
        db.commit()

class Name(Base):
    """Stores the name/title information about a Work"""
    
    __tablename__ = 'names'

    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))

    namescomplextitles = relation('NamesComplexTitles', backref='name',
							     cascade='all, delete-orphan',
								 collection_class=ordering_list('position'))
    complex_titles = association_proxy('namescomplextitles', 'complex_title')

    complex_title = Column(Unicode(255))
    # Association proxy to preserve ordering
    namestitles = relation('NamesTitles', backref='name',
                          cascade='all, delete-orphan',
                          collection_class=ordering_list('position'))
    titles = association_proxy('namestitles', 'title')
    classifications = relation('Classification',
                               secondary=titles_classifications,
                               backref='title')
    image_view = Column(Unicode(255))
    view_type_id = Column(Integer, ForeignKey('view_types.id'))
    narrow_classification_id = Column(Integer, ForeignKey('narrow_classifications.id'))

    # Validations (see lib/sahara_base_model.py for implementation)
    must_assign('view_type', 'View Type is required.')
    at_least_one('classifications', 'Broad Classification is required.')
    at_least_one('titles', 'Title/Name of Work is required.')
    
    # Prevent updates from form submittal
    form_deny = ['id', 'work_id']

    def validate(self, errors=None):
        """Custom validation to ensure narrow classification is non-null in special case."""
        status, errors = super(Name, self).validate(errors)
        classifications = [c.value for c in self.classifications]
        if u'Architecture and City Planning' in classifications or \
                u'Garden and Landscape' in classifications:
            if self.narrow_classification == None:
                if status is True:
                    status = False
                    errors = ['Name/Title - Narrow classification is required']
                else:
                    errors.append('Name/Title - Narrow classification is required')
        return (status, errors)


class Creator(Base):
    """
    Stores the creator information about a Work.

    One work can have multiple creators, so an association_proxy is used to
    maintain the ordering.

    """
    __tablename__ = 'creators'

    # Prevent updates from form submittal
    form_deny = ['id', 'work_id']

    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))
    attribution_id = Column(Integer, ForeignKey('attributions.id'))
    name = Column(Unicode(255))
    ana_id = Column( String(10))
    earliest_date = Column(Integer)
    latest_date = Column(Integer)

    # Relations
    creatorsnationalities = relation('CreatorsNationalities',
                                     backref='creator',
                                     cascade='all, delete-orphan',
                                     collection_class=ordering_list('position'))
    nationalities = association_proxy('creatorsnationalities', 'nationality')
    creatorsextents = relation('CreatorsExtents',
                               backref='creator',
                               cascade='all, delete-orphan',
                               collection_class=ordering_list('position'))
    extents = association_proxy('creatorsextents', 'extent')
    roles = relation('Role',
                     secondary=creators_roles,
                     backref='creators')

    
class Location(Base):
    """Represents location information about a Work"""
    __tablename__ = 'locations'

    # Validations (see lib/sahara_base_model.py for implementation)
    not_blank('city_county', 'City/County is required')
    at_least_one('countries', 'Country is required.')
    must_match('latitude',
               '^[+|-]\d+\.\d+$',
               'Latitude must be in form +/-###.######')
    must_match('longitude',
               '^[+|-]\d+\.\d+$',
               'Longitude must be in form +/-###.######')

    # Prevent updates from form submittal
    form_deny = ['id', 'work_id']

    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))
    street_address = Column(Unicode(255))
    city_county = Column(Unicode(255))
    state_province = Column(Unicode(255))
    latitude = Column(String(40))
    longitude = Column(String(40))
    repository = Column(Unicode(255))
    repository_id = Column(Unicode(255))

    # Relations
    locationscountries = relation('LocationsCountries',
                                  backref='location',
                                  cascade='all, delete-orphan',
                                  collection_class=ordering_list('position'))
    countries = association_proxy('locationscountries', 'country')

    
class Chronology(Base):
    """Represents date information about a Work"""
    
    __tablename__ = 'chronologies'

    # Validations (see lib/sahara_base_model.py for implementation)
    not_blank('display_date', 'Date is required.')

    # Prevent updates from form submittal
    form_deny = ['id', 'work_id']

    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))
    display_date = Column(Unicode(255))
    earliest_date = Column(Integer)
    latest_date = Column(Integer)
    chronologiesperiods = relation('ChronologiesPeriods',
                                   backref='chronology',
                                   cascade='all, delete-orphan',
                                   collection_class=ordering_list('position'))
    period_dynasty = association_proxy('chronologiesperiods', 'period')


class PhysicalDescription(Base):
    """Represents physical description information about a Work"""

    __tablename__ = 'physical_descriptions'

    # Prevent updates from form submittal
    form_deny = ['id', 'work_id']

    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))
    material_technique = Column(Unicode(255))
    measurements = Column(Unicode(255))
    description = Column(UnicodeText)
    commentary = Column(UnicodeText)
    style = Column(Unicode(255))


class Source(Base):
    """Represents source information about a Work"""
    
    __tablename__ = 'sources'

    # Validations (see lib/sahara_base_model.py for implementation)
    not_blank('photographer', 'Photographer is required.')
    not_blank('contributor',  'Contributor is required.')

    # Prevent updates from form submittal
    form_deny = ['id', 'work_id']

    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))
    information_source = Column(Unicode(255))
    photographer = Column(Unicode(255))
    contributor = Column(Unicode(255))
    inst_contributor = Column(Unicode(255))
    image_date = Column(Unicode(255))
    image_earliest_date = Column(Integer)
    image_latest_date = Column(Integer)
    

class Rights(Base):
    """Represents rights information about a Work."""
    
    __tablename__ = 'rights'

    # Validations (see lib/sahara_base_model.py for implementation)
    not_blank('image_copyright', 'Copyright of Photograph/Image ' \
                  'is required.')
    must_equal('image_ownership', True, 'You must have the rights to ' \
                   'this image in order to contribute it.')

    # Prevent updates from form submittal
    form_deny = ['id', 'work_id']

    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))
    image_copyright = Column(Unicode(255))
    image_ownership = Column(Boolean)
    academic_publishing = Column(Boolean)
    other_contexts = Column(String(15))

    
class History(Base):
    """
    Stores simple history of edits for the work.  It uses a serialized version
    of the work to simplify storage.
    """
    __tablename__ = 'history'
    
    id = Column(Integer, primary_key=True)
    data = Column(UnicodeText)
    updated_by = Column(Integer, nullable=False)
    updated_on = Column(DateTime, default=func.now())
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))

    def init(self, **kwargs):
        if 'data' in kwargs.keys():
            self.data = kwargs['data']
        if 'updated_by' in kwagrs.keys():
            self.updated_by = kwargs['updated_by']
        if 'updated_on' in kwargs.keys():
            self.updated_on = kwargs['updated_on']
    

class NamesComplexTitles(Base):
	"""A position tracking linking table between Name and ComplexTitle"""
	
	__tablename__ = 'names_complex_titles'
	
	work_id = Column(Integer, ForeignKey('names.id', ondelete='CASCADE'), primary_key=True)
	complex_title_id = Column(Integer, ForeignKey('complex_titles.id'), primary_key=True)
	position = Column(Integer)
	
	complex_title = relation('ComplexTitle')
	
	def __init__(self, complex_title):
		self.complex_title = complex_title

class ComplexTitle(Base):
	"""Represents a complex title related to one or more Name objects."""
	__tablename__ = 'complex_titles'
	
	id = Column(Integer, primary_key=True)
	value = Column(Unicode(255), unique=True)
	namescomplextitles = relation('NamesComplexTitles')
	names = association_proxy('namescomplextitles', 'name')
	
	def __init__(self, value=None):
		self.value = value
	
	def __unicode__(self):
		return self.value


class NamesTitles(Base):
    """A position tracking linking table between Name and Title."""
    
    __tablename__ = 'names_titles'

    work_id = Column(Integer, ForeignKey('names.id', ondelete='CASCADE'), primary_key=True)
    title_id = Column(Integer, ForeignKey('titles.id'), primary_key=True)
    position = Column(Integer)

    title = relation('Title')

    def __init__(self, title):
        self.title = title
        
    
class Title(Base):
    """Represents a title related one or more Name objects."""
    __tablename__ = 'titles'
    
    id = Column(Integer, primary_key=True)
    value = Column(Unicode(255), unique=True)
    namestitles = relation('NamesTitles')
    names = association_proxy('namestitles', 'name')

    def __init__(self, value=None):
        self.value = value
        
    def __unicode__(self):
        return self.value

    
class Classification(Base):
    """Represents a classification related one or more Name objects."""
    
    __tablename__ = 'classifications'
    
    id = Column(Integer, primary_key=True)
    value = Column(Unicode(255), unique=True)

    def __unicode__(self):
        return self.value


class CreatorsNationalities(Base):
    """A position tracking linking table between Creator and Nationalities."""

    __tablename__ = 'creators_nationalities'

    creator_id = Column(Integer, ForeignKey('creators.id', ondelete='CASCADE'), primary_key=True)
    nationality_id = Column(Integer, ForeignKey('nationalities.id'), primary_key=True)
    position = Column(Integer, primary_key=True)

    nationality = relation('Nationality')

    def __init__(self, nationality):
        self.nationality = nationality

    
class Nationality(Base):
    """Represents a nationality related one or more Creator objects."""
    
    __tablename__ = 'nationalities'
    
    id = Column(Integer, primary_key=True)
    value = Column(Unicode(255), unique=True)
    creatornationalities = relation('CreatorsNationalities')
    nationalities = association_proxy('creatornationalities', 'creator')

    def __init__(self, value=None):
        super(Nationality, self).__init__()
        self.value = value
        
    def __unicode__(self):
        return self.value


class CreatorsExtents(Base):
    """A position tracking linking table between Creator and Extent."""

    __tablename__ = 'creators_extents'

    creator_id = Column(Integer, ForeignKey('creators.id', ondelete='CASCADE'), primary_key=True)
    extent_id = Column(Integer, ForeignKey('extents.id'), primary_key=True)
    position = Column(Integer)

    extent = relation('Extent')

    def __init__(self, extent):
        self.extent = extent

        
class Extent(Base):
    """Represents a extent related one or more Creator objects."""
    
    __tablename__ = 'extents'
    
    id = Column(Integer, primary_key=True)
    value = Column(Unicode(255), unique=True)
    creatorextents = relation('CreatorsExtents')
    creators = association_proxy('creatorextents', 'creator')

    def __unicode__(self):
        return self.value

class Role(Base):
    """Represents a role related one or more Creator objects."""
    __tablename__ = 'roles'

    id = Column(Integer, primary_key='id')
    _value = Column('value', Unicode(255), unique=True)
    
    def __unicode__(self):
        return self.value

    def _get_value(self):
        return self._value

    def _set_value(self, new_value):
        self._value = new_value
        if self.has_changes("_value"):
            work_ids = [c.work_id for c in self.creators]
            Work.mark_for_republication(work_ids)

    value = synonym('_value', descriptor=property(_get_value, _set_value))

class LocationsCountries(Base):
    """A position tracking linking table between Location and Country."""

    __tablename__ = 'locations_countries'

    location_id = Column(Integer, ForeignKey('locations.id', ondelete='CASCADE'), primary_key=True)
    country_id = Column(Integer, ForeignKey('countries.id'), primary_key=True)
    position = Column(Integer)

    country = relation('Country')

    def __init__(self, country):
        self.country = country

    
class Country(Base):
    """Represents a country related one or more Location objects."""
    __tablename__ = 'countries'
    
    id = Column(Integer, primary_key=True)
    value = Column(Unicode(255), unique=True)
    locationscountries = relation('LocationsCountries')
    locations = association_proxy('locationscountries', 'location')

    def __unicode__(self):
        return self.value

    
class ChronologiesPeriods(Base):
    """A position tracking linking table between Chronology and Period."""
    __tablename__ = 'chronologies_periods'

    work_id = Column(Integer, ForeignKey('chronologies.id', ondelete='CASCADE'), primary_key=True)
    period_id = Column(Integer, ForeignKey('periods.id'), primary_key=True)
    position = Column(Integer)

    period = relation('Period')

    def __init__(self, period):
        self.period = period
        

class Period(Base):
    """Represents a period related one or more Chronology objects."""
    
    __tablename__ = 'periods'
    
    id = Column(Integer, primary_key=True)
    value = Column(Unicode(255), unique=True)
    chronologiesperiods = relation('ChronologiesPeriods')
    chronologies = association_proxy('chronologiesperiods', 'chronology')
    
    def __unicode__(self):
        return self.value

    
class WorksKeywords(Base):
    """A position tracking linking table between Work and Keyword."""

    __tablename__ = 'works_keywords'

    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'), primary_key=True)
    keyword_id = Column(Integer, ForeignKey('keywords.id'), primary_key=True)
    position = Column(Integer)

    keyword = relation('Keyword')

    def __init__(self, keyword=None):
        self.keyword = keyword


class Keyword(Base):
    """Represents a keyword related to many Work objects."""

    __tablename__ = 'keywords'
    
    id = Column(Integer, primary_key=True)
    value = Column(Unicode(255), unique=True)
    workskeywords = relation('WorksKeywords')
    works = association_proxy('workskeywords', 'work')

    def __unicode__(self):
        return self.value

    
class ViewType(Base):
    """Represents a viewtype related to many Name objects."""

    __tablename__ = 'view_types'
    
    id = Column('id', Integer, primary_key=True)
    _value = Column('value', Unicode(255), unique=True)
    names = relation('Name', backref='view_type')

    def __unicode__(self):
        return self.value

    def _get_value(self):
        return self._value

    def _set_value(self, new_value):
        self._value = new_value
        if self.has_changes("_value"):
            work_ids = [n.work_id for n in self.names]
            Work.mark_for_republication(work_ids)

    value = synonym('_value', descriptor=property(_get_value, _set_value))
    
class NarrowClassification(Base):
    """Represents a narrowclassification related to many Names."""
    
    __tablename__ = 'narrow_classifications'
    
    id = Column(Integer, primary_key=True)
    _value = Column('value', Unicode(255), unique=True)
    names = relation('Name', backref='narrow_classification')

    def __unicode__(self):
        return self.value

    def _get_value(self):
        return self._value

    def _set_value(self, new_value):
        self._value = new_value
        if self.has_changes("_value"):
            work_ids = [n.work_id for n in self.names]
            Work.mark_for_republication(work_ids)

    value = synonym('_value', descriptor=property(_get_value, _set_value))
            
class Attribution(Base):
    """Represents an attribution related to many Creator objects."""

    __tablename__ = 'attributions'
    
    id = Column(Integer, primary_key=True)
    _value = Column('value', Unicode(255), unique=True)
    creators = relation('Creator', backref='attribution')
    
    def __unicode__(self):
        return self.value
    
    def _get_value(self):
        return self._value

    def _set_value(self, new_value):
        self._value = new_value
        if self.has_changes("_value"):
            work_ids = [c.work_id for c in self.creators]
            Work.mark_for_republication(work_ids)

    value = synonym('_value', descriptor=property(_get_value, _set_value))

class UserCredentials(Base):
    __tablename__ = 'user_credentials'

    userid = Column(Integer, primary_key=True)
    memberid = Column(Integer,unique=True)
    name = Column(String(50), nullable=False)
    password = Column(String(40))
    email_me = Column(Boolean, server_default='t')
    survey_me = Column(Boolean, server_default='t')

    def __unicode__(self):
        return self.value

class EditorialNote(Base):
    __tablename__ = 'editorial_notes'
    
    form_deny = ['id', 'work_id']
    
    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'))
    body = Column(UnicodeText)
    updated_by = Column(Integer, nullable=False)
    updated_on = Column(DateTime, default=func.now())

class EditorialBucketReviewer(Base):
    __tablename__ = 'editorial_bucket_reviewers'
    
    id = Column(Integer, primary_key=True)
    editorial_bucket_id = Column(Integer, ForeignKey('editorial_buckets.id', ondelete='CASCADE'))
    reviewer_id = Column(Integer)
    
class EditorialBucket(Base):
    __tablename__ = 'editorial_buckets'
    
    id = Column(Integer, primary_key=True)
    name = Column(Unicode(255))
    query = Column(UnicodeText)
    reviewers = relation('EditorialBucketReviewer', backref='bucket')
    updated_on = Column(DateTime, default=func.now())
    
class WorkPublication(Base):
    __tablename__ = 'works_publications'

    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'), nullable=False, primary_key=True)
    artstor_collection_id = Column(Integer, primary_key=True)
    
class Assignment(Base):
    __tablename__ = 'assignments'
    
    id = Column(Integer, primary_key=True)
    work_id = Column(Integer, ForeignKey('works.id', ondelete='CASCADE'), nullable=False)
    user_id = Column(Integer, nullable=False)
