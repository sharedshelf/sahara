import logging

from pylons import g

import solr
from sahara.lib.date_parser import parse_date

DEFAULT_FIELDS = ['uuid']
DISPLAY_FIELDS = ['score', 'uuid', 'media_uuid', 'suppress', 'creator_name', 'title', 'city_county', 'country',
                  'display_date', 'contributor', 'assignment', 'reserved_by', 'contribution_date', 'suppress']

log = logging.getLogger(__name__)


def _in_bucket_query(base_query, user_id):
    if base_query is not None:
        return "assignment: %s || (complete:true && -assignment:[* TO *] && -reviewed:[* TO *] && %s)" % (user_id, base_query)
    else:
        return "complete:true && -reviewed:[* TO *] && assignment: %s" % user_id
        
def _in_perview_query(base_query, user_id):
    if base_query is not None:
        return "assignment: %s || (complete:true && %s)" % (user_id, base_query)
    else:
        return "assignment: %s" % user_id

def _locked_works_query(user_id):
        return "-reviewed:[* TO *] && reserved_by:%d" % user_id

def get_totals_for_user(user):
    s = Solr(g.SOLR_URL)
    return dict(
        waiting = s.bucket_count_for_user(user.perview_search_query, user.id),
        reserved = s.locked_work_count_for_user(user.id))

def query_works_reserved_to_user(user, page=1, rows=25, sort=None):
    return Solr(g.SOLR_URL).locked_works(user.id, page=page, rows=rows, sort=sort)

# Users perview is determined by base query plus anything assigned explicitly to him
def query_works_in_users_perview(user, page=1, rows=25, sort=None):
    return Solr(g.SOLR_URL).editorial_bucket(_in_perview_query(user.perview_search_query, user.id), page=page, rows=rows, sort=sort)

# Users bucket is determined by the base query minus anything assigned to others plus
# anything assigned to him.
def query_works_in_users_bucket(user, page=1, rows=25, sort=None):
    return Solr(g.SOLR_URL).editorial_bucket(_in_bucket_query(user.perview_search_query, user.id), page=page, rows=rows, sort=sort)
                                                                         
def query_work_ids_editable_by_user(user):
    return Solr(g.SOLR_URL).editable_work_ids(_in_perview_query(user.perview_search_query, user.id))

def query_work_ids_reserved_by_user(user):
    return Solr(g.SOLR_URL).reserved_work_ids(user.id)

def query_work_ids_reserved_by_no_one():
    return Solr(g.SOLR_URL).unreserved_work_ids()


class Solr(object):
    
    def __init__(self, solr_url):
        self.conn = solr.SolrConnection(solr_url)

    def bucket_count_for_user(self, bucket_query, user_id):
        response = self.search_query(_in_bucket_query(bucket_query, user_id), rows=1)
        return response.numFound
    
    def locked_work_count_for_user(self, user_id):
        response = self.search_query(_locked_works_query(user_id), rows=1)
        return response.numFound
    
    def editable_work_ids(self, bucket_query):
        response = self.search_query(bucket_query, rows=100000000)
        return [r['uuid'] for r in response.results]

    def reserved_work_ids(self, user_id):
        response = self.search_query(_locked_works_query(user_id), rows=100000000)
        return [r['uuid'] for r in response.results]

    def unreserved_work_ids(self):
        response = self.search_query('-reserved_by:[* TO *]', rows=100000000)
        return [r['uuid'] for r in response.results]

    def editorial_bucket(self, bucket_query, rows=25, page=1, sort='contribution_date asc'):
        return self.paginated_search_query(bucket_query, fields=DISPLAY_FIELDS, rows=rows, page=page, sort=sort) 

    
    # def editable_works(self, user, rows=25, page=1, sort='contribution_date_asc'):
    #     sort, direction = self._parse_sort(sort)
    #     return self.paginated_search_query(user.editable_work_query_params(),
    #                                        fields=DISPLAY_FIELDS, rows=rows,
    #                                        page=page, sort=sort, sort_order=direction)

    def locked_works(self, user_id, rows=25, page=1, sort='contribution_date asc'):
        return self.paginated_search_query(_locked_works_query(user_id),
                                           fields=DISPLAY_FIELDS, rows=rows, page=page,
                                           sort=sort)

    def keyword_search(self, params, rows=25, page=1, sort='uuid asc'):
        params +=' complete:true'
        return self.paginated_search_query(params, fields=DISPLAY_FIELDS, rows=rows, page=page, sort=sort)

    def lookup(self, field, query, user):
        return '\n'.join(self._lookup_query(field, query, user.id))
        
    def search_query(self, params, fields=DEFAULT_FIELDS, **kw):
        return self._query_without_pagination(params, fields=fields, **kw)

    def paginated_search_query(self, params, fields=DEFAULT_FIELDS, rows=300, page=1, **kwargs):
        pages = self._query(params.encode('utf-8'), rows=rows, fields=fields, **kwargs)
        if pages.count == 0:
            records = None
        else:
            if page > pages.num_pages:
                page = pages.num_pages
            records = pages.page(page)
        return dict(paginated_response = pages,
                    results = records,
                    rows = pages.page_size)

    def add_work(self, work):
        """Adds a work to the search index."""
        fw = self._flatten_work(work)
        self.conn.add(**fw)
        self.conn.commit()

    def delete_work(self, work):
        """Deletes a work from the search index."""
        self.conn.delete(work.uuid)

    def reindex(self):
        print 'Rebuilding SOLR search index, please wait'
        self._delete_all()
        from sahara.model import *
        for work in Session.query(Work).all():
            work.update_search_engine()
        print 'Reindexing complete'

    def _lookup_query(self, field, query, profile_id):
        """Issues a query to the search engine tailored for autocomplete like behavior"""
        lc_query = query.lower()
        field = "%s_lookup" % field
        q = "+%s:%s OR created_by:%s" % (field, lc_query, profile_id)
        response = self.conn.query(q, fields=field, score=True, rows=200)
        matches = []
        # Unpackage results.  Solr may return duplicates, so dedup
        for hit in response.results:
            try:
                if type(hit[field]) is list:
                    for value in hit[field]:
                        if value not in matches:
                            matches.append(value)
                            if len(matches) == 10:
                                break
                else:
                    if hit[field] not in matches:
                        matches.append(hit[field])
                        if len(matches) == 10:
                            break
            except KeyError:
                pass
        # filter solr results for only exact matches
        return [m for m in matches if m.lower()[:len(query)] == lc_query]

    def _query_without_pagination(self, q, **kwargs):
        """Simple wrapper around SolrPy query."""
        log.debug("Querying SOLR with: '%s' (%s)" % (q, kwargs.__repr__()))
        if 'sort' in kwargs.keys():
            kwargs['sort'] = kwargs.get('sort', '') + ', uuid asc'
        else:
            kwargs['sort'] = 'uuid asc'
        return self.conn.query(q, **kwargs)
        
    def _query(self, q, **kwargs):
        """Simple wrapper around SolrPy query with pagination."""
        result = self._query_without_pagination(q, **kwargs)
        return solr.SolrPaginator(result)

    # def _parse_sort(self, sort):
    #     """Parses sort into separate arguments.  e.g. uuid_asc -> sort=uuid, sort_order=asc"""
    #     if sort is None:
    #         return None, None
    #     else:
    #         parts = sort.split('_')
    #         return ('_'.join(parts[0:-1]), parts[-1])

    def _delete_all(self):
        """Delete all records in the search index."""
        self.conn.delete_query('uuid: [* TO *]')
        self.conn.optimize()
    
    def _flatten_work(self, work):
        """
        Takes a work instance and creates a flattened dictionary, suitable for indexing.
        
        Creator relations get prefixed with 'creator_'.
        """
        flat_work = dict()
        flat_work.update(self._filter_model(work))
        flat_work.update(self._filter_model(work.status))
        flat_work.update(self._filter_model(work.name))
        flat_work.update(self._filter_model(work.location))
        flat_work.update(self._filter_model(work.chronology))
        flat_work.update(self._filter_model(work.physical_description))
        flat_work.update(self._filter_model(work.source))
        flat_work.update(self._filter_model(work.rights))
        if work.name.view_type:
            flat_work['view_type'] = work.name.view_type.value
        if work.name.narrow_classification:
            flat_work['narrow_classification'] = work.name.narrow_classification.value
        flat_work['classification'] = [c.value for c in work.name.classifications]
        flat_work['title'] = [t.value for t in work.name.titles]
        if len(work.name.titles) > 0:
            flat_work['title_sort'] = work.name.titles[0]
        flat_work['complex_title'] = [c.value for c in work.name.complex_titles]
        flat_work['period_dynasty'] = [p.value for p in work.chronology.period_dynasty]
        flat_work['keyword'] = [k.value for k in work.keywords]
        flat_work['country'] = [c.value for c in work.location.countries]
        flat_work['assignment'] = [a.user_id for a in work.assignments]
        if work.status.contributed_on is not None:
            flat_work['complete'] = True
            flat_work['contribution_date'] = work.contribution_date()
            flat_work['contributed_by'] = work.contributed_by()
            flat_work['published_to'] = work.publication_destination.artstor_collection_id
        else:
            flat_work['complete'] = False
        for creator in work.creators:
            flat_creator = self._filter_model(creator)
            flat_creator['nationality'] = [n.value for n in creator.nationalities]
            flat_creator['extent'] = [e.value for e in creator.extents]
            flat_creator['role'] = [r.value for r in creator.roles]
            self._prefixed_update(flat_work, flat_creator, prefix='creator')
        flat_work['avg_date'] = self._calculate_average_date(work.chronology)
        return flat_work

    def _prefixed_update(self, dest, src, prefix=''):
        for key in src:
            prefixed_key = "%s_%s" % (prefix, key)
            if prefixed_key not in dest.keys():
                dest[prefixed_key] = []
            if type(src[key]) is list:
                dest[prefixed_key].extend(src[key])
            else:
                dest[prefixed_key].append(src[key])
    
    def _filter_model(self, instance):
        filtered = dict()
        for k in vars(instance):
            if instance.__getattribute__(k) is not None:
                if k[0] is not '_' and k[-2:] is not 'id':
                    filtered[k] = instance.__getattribute__(k)
        return filtered

    def _first_title(self, work):
        try:
            return work.name.titles[0].value
        except:
            return None

    def _calculate_average_date(self, chronology):
        print "chronology.earliest_date: %s[%s]" % (type(chronology.earliest_date), str(chronology.earliest_date))
        if chronology.earliest_date is not None and chronology.latest_date is not None:
            return (chronology.earliest_date + chronology.latest_date) / 2
        elif chronology.earliest_date is not None:
            return chronology.earliest_date
        elif chronology.latest_date is not None:
            return chronology.latest_date
        else:
            try:
                print "Calling date parser with: %s" % chronology.display_date
                start, end = parse_date(chronology.display_date)
                print "Date parser returned (%d, %d)" % (start, end)
                if start > end or start > 3000 or end > 3000:
                    return None
                return (start + end) /2
            except:
                return None


