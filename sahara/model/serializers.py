import logging
from pylons import g
from xml.dom.minidom import Document
from urllib import urlencode, quote_plus

from sahara.lib.authentication.artstor import get_profile_id_to_email_map

log = logging.getLogger(__name__)

class XMLSerializer(object):

    def serialize(self, work):
        self.doc = Document()
        root = self.doc.appendChild(self.doc.createElement('sahara'))
        admin = root.appendChild(self._create('admin'))
        admin.appendChild(self._nodeWithText('uuid', work.uuid))
        admin.appendChild(self._nodeWithText('filename', work.media_filename))
        admin.appendChild(self._nodeWithText('updated_on', work.updated_on))
        admin.appendChild(self._nodeWithText('media_uuid', work.media_uuid))
        admin.appendChild(self._nodeWithText('media_width', work.media_width))
        admin.appendChild(self._nodeWithText('media_height', work.media_height))
        admin.appendChild(self._nodeWithText('media_category', work.media_category))
        admin.appendChild(self._nodeWithText('media_type', work.media_type))
        admin.appendChild(self._nodeWithText('media_category', work.media_category))
        admin.appendChild(self._nodeWithText('created_by', work.created_by))
        admin.appendChild(self._nodeWithText('suppress', str(work.status.suppress)))
        if work.publication_destination is None:  # Could happen if queue is not empty before upgrade
            log.critical('No collection specified in work scheduled for publication')
            admin.appendChild(self._nodeWithText('collection', str(g.MEMBERS_COLLECTION)))
        else:
            admin.appendChild(self._nodeWithText('collection', str(work.publication_destination.artstor_collection_id)))
        data = root.appendChild(self._create('data'))
        self._serialize_title(data, work.name)
        self._serialize_creator(data, work.creators)
        self._serialize_location(data, work.location)
        self._serialize_chronology(data, work.chronology)
        self._serialize_physical(data, work.physical_description)
        self._serialize_subject(data, work)
        self._serialize_source(data, work.source)
        self._serialize_rights(data, work.rights)
        try:
            if work.publication_destination.artstor_collection_id == int(g.EDITORS_COLLECTION):
                self._serialize_edit_history(data, work)
        except:
            log.critical('Could not serialize edit history!', exc_info=True)
        return root.toxml()

    def _serialize_title(self, root, record):
        title_info = root.appendChild(self._create('TitleInformation'))
        for complex_title in record.complex_titles:
            if self._has_content(complex_title):
                title_info.appendChild(self._nodeWithText('ComplexTitle', complex_title))
        for title in record.titles:
            if self._has_content(title):
                title_info.appendChild(self._nodeWithText('Title', title))
        if self._has_content(record.image_view):
            title_info.appendChild(self._nodeWithText('ImageView', record.image_view))
        if self._has_content(record.view_type):
            title_info.appendChild(self._nodeWithText('ViewType', record.view_type))
        for classification in record.classifications:
            title_info.appendChild(self._nodeWithText('Classification', classification))
        if self._has_content(record.narrow_classification):
            title_info.appendChild(self._nodeWithText('NarrowClassification',
                                                record.narrow_classification))
        return title_info

    def _serialize_creator(self, root, creators):
        for creator in creators:
            c = root.appendChild(self._create('Creator'))
            if self._has_content(creator.name):
                c.appendChild(self._nodeWithText('Name', creator.name))
            for nationality in creator.nationalities:
                if self._has_content(nationality):
                    c.appendChild(self._nodeWithText('Nationality', nationality))
            if self._has_content(creator.attribution):
                c.appendChild(self._nodeWithText('Attribution', creator.attribution))
            for role in creator.roles:
                if self._has_content(role):
                    c.appendChild(self._nodeWithText('Role', role))
            for extent in creator.extents:
                if self._has_content(extent):
                    c.appendChild(self._nodeWithText('Extent', extent))
                    
    def _serialize_location(self, root, location):
        l = root.appendChild(self._create('Location'))
        if self._has_content(location.street_address):
            l.appendChild(self._nodeWithText('StreetAddress', location.street_address))
        if self._has_content(location.city_county):
            l.appendChild(self._nodeWithText('CityCounty', location.city_county))
        if self._has_content(location.state_province):
            l.appendChild(self._nodeWithText('StateProvince', location.state_province))
        for country in location.countries:
            if self._has_content(country):
                l.appendChild(self._nodeWithText('Country', country))
        if self._has_content(location.latitude):
            l.appendChild(self._nodeWithText('Latitude', location.latitude.decode('ascii', 'ignore')))
        if self._has_content(location.longitude):
            l.appendChild(self._nodeWithText('Longitude', location.longitude.decode('ascii', 'ignore')))
        if self._has_content(location.repository):
            l.appendChild(self._nodeWithText('Repository', location.repository))
        if self._has_content(location.repository_id):
            l.appendChild(self._nodeWithText('RepositoryID', location.repository_id))
                
    def _serialize_chronology(self, root, record):
        chronology = root.appendChild(self._create('Chronology'))
        if self._has_content(record.display_date):
            chronology.appendChild(self._nodeWithText('DisplayDate', record.display_date))
        if self._has_content(record.earliest_date):
            chronology.appendChild(self._nodeWithText('DateEarliest', record.earliest_date))
        if self._has_content(record.latest_date):
            chronology.appendChild(self._nodeWithText('DateLatest', record.latest_date))
        for period in record.period_dynasty:
            if self._has_content(period):
                chronology.appendChild(self._nodeWithText('PeriodDynasty', period))

    def _serialize_physical(self, root, record):
        physical = root.appendChild(self._create('Physical'))
        if self._has_content(record.material_technique):
            physical.appendChild(self._nodeWithText('MaterialTechnique',
                                 record.material_technique))
        if self._has_content(record.measurements):
            physical.appendChild(self._nodeWithText('Measurements', record.measurements))
        if self._has_content(record.description):
            physical.appendChild(self._nodeWithText('Description', record.description))
        if self._has_content(record.commentary):
            physical.appendChild(self._nodeWithText('Commentary', record.commentary))
        if self._has_content(record.style):
            physical.appendChild(self._nodeWithText('Style', record.style))

    def _serialize_subject(self, root, record):
        if len(record.keywords) > 0:
            subject = root.appendChild(self._create('Subject'))
            for keyword in record.keywords:
                if self._has_content(keyword):
                    subject.appendChild(self._nodeWithText('Keyword', keyword))

    def _serialize_source(self, root, record):
        source = root.appendChild(self._create('Source'))
        if self._has_content(record.information_source):
            source.appendChild(self._nodeWithText('InformationSource',
                               record.information_source))
        if self._has_content(record.photographer):
            source.appendChild(self._nodeWithText('Photographer', record.photographer))
        if self._has_content(record.contributor):
            source.appendChild(self._nodeWithText('Contributor', record.contributor))
        if self._has_content(record.inst_contributor):
            source.appendChild(self._nodeWithText('InstitutionalContributor',
                               record.inst_contributor))
        if self._has_content(record.image_date):
            source.appendChild(self._nodeWithText('ImageDateDisplay', record.image_date))
        if self._has_content(record.image_earliest_date):
            source.appendChild(self._nodeWithText('ImageEarliestDate',
                               record.image_earliest_date))
        if self._has_content(record.image_latest_date):
            source.appendChild(self._nodeWithText('ImageLatestDate',
                               record.image_latest_date))

    def _serialize_rights(self, root, record):
        rights = root.appendChild(self._create('Rights'))
        if self._has_content(record.image_copyright):
            rights.appendChild(self._nodeWithText('CopyrightImage', record.image_copyright))
                             
    def _serialize_edit_history(self, root, work):
        history = root.appendChild(self._create('EditHistory'))
        priveleged_users = self._all_priveleged_users()
        editor_ids = [x.updated_by for x in work.history if x.updated_by in priveleged_users]
        editor_ids.append(work.updated_by)
        editors = get_profile_id_to_email_map(set(editor_ids))
        for editor_id in editors.keys():
            name = editors.get(editor_id).get('full_name', 'Unknown')
            if name != 'Unknown':
                history.appendChild(self._nodeWithText('Editor', name))
        history.appendChild(self._nodeWithText('LastEdited', work.updated_on.strftime('%m/%d/%Y')))

    def _create(self, name):
        return self.doc.createElement(name)

    def _nodeWithText(self, name, value):
        e = self._create(name)
        if type(value) in [str, unicode]:
            e.appendChild(self.doc.createTextNode(value))
        elif type(value) == 'instance':
            e.appendChild(self.doc.createTextNode(value.value))
        else:
            e.appendChild(self.doc.createTextNode(unicode(value)))
        return e

    def _has_content(self, value):
        if value == None:
            return False
        elif type(value) in [str, unicode]:
            return value is not None and len(value.strip()) > 0
        elif hasattr(value, 'value'):
            return self._has_content(value.value)
        else:
            return value is not None

    def _all_priveleged_users(self):
        users = list(g.ADMINS)
        users.extend(g.REVIEWERS)
        users.extend(g.CATALOGERS)
        return users


class HTTPPostSerializer(object):
    
    def serialize(self, work):
        form = dict()

        if work.keywords is not None:
            form['keywords'] = ','.join([k.value for k in work.keywords])
        if work.name is not None:
            self._serialize_name(work.name, form)
        for index, creator in enumerate(work.creators):
            self._serialize_creator(creator, index, form)
        if work.location is not None:
            self._serialize_location(work.location, form)
        if work.chronology is not None:
            self._serialize_chronology(work.chronology, form)
        if work.physical_description is not None:
            self._serialize_physical_description(work.physical_description, form)
        if work.source is not None:
            self._serialize_source(work.source, form)
        if work.rights is not None:
            self._serialize_rights(work.rights, form)
        # convert all values to utf-8 so urlencode can handle them
        for k in form.keys():
            form[k] = self._utf_8_encode(form[k])
        return urlencode(form, doseq=True)

    def _serialize_name(self, name, form):
        self.store_collection_or_placeholder(form, 'complex_title', [w.value for w in name.complex_titles]) 
        self.store_collection_or_placeholder(form, 'title', [w.value for w in name.titles]) 
        self.if_exists_store(form, 'view_type', name.view_type_id)
        self.if_exists_store(form, 'image_view', name.image_view)
        self.store_collection_or_placeholder(form, 'classifications', [w.value for w in name.classifications]) 
        self.if_exists_store(form, 'narrow_classification', name.narrow_classification_id)

    def _serialize_creator(self, creator, index, form):
        self.if_exists_store(form, 'creator-%d_name' % index, creator.name)
        self.if_exists_store(form, 'creator-%d_attribution' % index, creator.attribution_id)
        self.store_collection_or_placeholder(form, 'creator-%d_nationality' % index,
                                             [n.value for n in creator.nationalities]) 
        self.store_collection_or_placeholder(form, 'creator-%d_extent' % index,
                                             [n.value for n in creator.extents]) 
        self.store_collection_or_placeholder(form, 'creator-%d_role' % index,
                                             [n.id for n in creator.roles]) 

    def _serialize_location(self, location, form):
        self.if_exists_store(form, 'street_address', location.street_address)
        self.if_exists_store(form, 'city_county', location.city_county)
        self.if_exists_store(form, 'state_province', location.state_province)
        self.store_collection_or_placeholder(form, 'country', [c.id for c in location.countries]) 
        self.if_exists_store(form, 'latitude', location.latitude)
        self.if_exists_store(form, 'longitude', location.longitude)
        self.if_exists_store(form, 'repository', location.repository)
        self.if_exists_store(form, 'repository_id', location.repository_id)
        
    def _serialize_chronology(self, chronology, form):
        self.if_exists_store(form, 'display_date', chronology.display_date)
        self.if_exists_store(form, 'earliest_date', chronology.earliest_date)
        self.if_exists_store(form, 'latest_date', chronology.latest_date)
        self.store_collection_or_placeholder(form, 'period_dynasty', [p.value for p in chronology.period_dynasty]) 
        
    def _serialize_physical_description(self, physical_description, form):
        self.if_exists_store(form, 'material_technique', physical_description.material_technique)
        self.if_exists_store(form, 'measurements', physical_description.measurements)
        self.if_exists_store(form, 'description', physical_description.description)
        self.if_exists_store(form, 'commentary', physical_description.commentary)
        self.if_exists_store(form, 'style', physical_description.style)

    def _serialize_source(self, source, form):
        self.if_exists_store(form, 'information_source', source.information_source)
        self.if_exists_store(form, 'photographer', source.photographer)
        self.if_exists_store(form, 'contributor', source.contributor)
        self.if_exists_store(form, 'inst_contributor', source.inst_contributor)
        self.if_exists_store(form, 'image_date', source.image_date)
        self.if_exists_store(form, 'image_earliest_date', source.image_earliest_date)
        self.if_exists_store(form, 'image_latest_date', source.image_latest_date)
        
    def _serialize_rights(self, rights, form):
        self.if_exists_store(form, 'image_copyright', rights.image_copyright)
        self.if_exists_store(form, 'image_ownership', rights.image_ownership)
        self.if_exists_store(form, 'academic_publishing', rights.academic_publishing)
        
    def _utf_8_encode(self, value):
        if type(value) in [str, unicode]:
            return value.encode('utf-8')
        elif type(value) is list:
            return [self._utf_8_encode(iv) for iv in value]
        else:
            return value
                
    def if_exists_store(self, form, key, value):
        if value is not None:
            form[key] = value

    def store_collection_or_placeholder(self, form, key, collection):
        if len(collection) > 0:
            form[key] = collection
        else:
            form[key] = ''
