from pylons import config
from sahara.lib.memoize import memoize
from sahara.model import search

class User(object):

    def __init__(self, artstor_creds):
        self.id = int(artstor_creds['id'])
        self.email = artstor_creds['username']
        self.first_name = " "
        self.last_name = " "
        g = config['pylons.app_globals']
        self.admin = True if self.id in g.ADMINS else False
        self.reviewer = True if self.id in g.REVIEWERS else False
        self.cataloger = True if self.id in g.CATALOGERS else False
        if self.admin or self.reviewer or self.cataloger:
            self.privileged = True
            self.solr_url = g.SOLR_URL
        else:
            self.privileged = False

    @property
    def is_privileged(self):
        return self.privileged

    @property
    def is_admin(self):
        return self.admin
        
    @property
    def is_reviewer(self):
        return self.reviewer
        
    @property
    def is_cataloger(self):
        return self.cataloger

    @property
    @memoize
    def editable_work_ids(self):
        if self.is_admin is True:
            return search.query_work_ids_reserved_by_no_one()
        else:
            return search.query_work_ids_editable_by_user(self)
        
    def can_edit(self, work_id):
        return work_id in self.editable_work_ids

    @memoize
    def has_reserved(self, work_id):
        return work_id in search.query_work_ids_reserved_by_user(self)

    @property
    @memoize
    def perview_search_query(self):
        if self.is_reviewer:
            assigned_bucket = self._get_assigned_bucket()
            if assigned_bucket is not None:
                return assigned_bucket.bucket.query
            else:
                return None
        elif self.is_cataloger:
            return None
        elif self.is_admin:
            return self._search_query_for_unassigned_records()
        else:
            return '-uuid:[* TO *]' # Empty set

    def _get_assigned_bucket(self):
        from sahara.model import EditorialBucketReviewer
        return EditorialBucketReviewer.get_by(reviewer_id=self.id)

    def _search_query_for_unassigned_records(self):
        from sahara.model import EditorialBucket, Session as db
        buckets = db.query(EditorialBucket).all()
        query = "NOT (" + ") || (".join([b.query for b in buckets]) + ")"
        return "-avg_date:[* TO *] && %s" % query
        
