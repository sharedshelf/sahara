import sys
import re

__all__ = ["not_blank", "at_least_one", "must_exist", "must_equal",
           "must_assign", "must_match"]

def not_blank(property_, message):
    """
    Validate that the given property isn't blank.

    The current stack frame is retrieved, and an anonymous
    function is added to the _validators list.
    """
    validations = _get_frame_storage(sys._getframe(1))
    def validate(instance):
        v = getattr(instance, property_)
        if v is not None and len(v) > 0:
            return (True, None)
        else:
            return (False, message)
    validations.append(validate)
        
def at_least_one(property_, message):
    validations = _get_frame_storage(sys._getframe(1))
    def validate(instance):
        if 0 == len(getattr(instance, property_)):
            return (False, message)
        else:
            return (True, None)
    validations.append(validate)

def must_exist(property_, message):
    validations = _get_frame_storage(sys._getframe(1))
    def validate(instance):
        if getattr(instance, property_) is not None:
            return (True, None)
        else:
            return (False, message)
    validations.append(validate)

def must_equal(property_, value, message):
    validations = _get_frame_storage(sys._getframe(1))
    def validate(instance):
        if getattr(instance, property_) == value:
            return (True, None)
        else:
            return (False, message)
    validations.append(validate)

def must_assign(property_, message):
    validations = _get_frame_storage(sys._getframe(1))
    def validate(instance):
        if getattr(instance, property_) is not None:
            return (True, None)
        else:
            return (False, message)
    validations.append(validate)

def must_match(property_, regex, message):
    validations = _get_frame_storage(sys._getframe(1))
    pat = re.compile(regex)
    def validate(instance):
        if getattr(instance, property_) is not None:
            if pat.match(getattr(instance, property_)) is not None:
                return (True, None)
            else:
                return (False, message)
        else:
            return (True, None)
    validations.append(validate)

def _get_frame_storage(frame):
    storage = frame.f_locals
    if '_validators' not in storage:
        storage['_validators'] = []
    return storage['_validators']
