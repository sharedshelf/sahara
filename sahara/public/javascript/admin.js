(function(){

  var EDIT_LINK_MARKUP = '<a href="#" title="" class="edit">Edit</a>',
      SAVE_CANCEL_LINKS_MARKUP =  '<a href="#" title="" class="save">Save</a> | ' + 
                                  '<a href="#" title="" class="cancel">Cancel</a>',
      
      listTable = $('#controlled-lists-dialog table'),
      currentEditedElement;
  
  $('#controlled-lists-form').submit(function(){
    
    var url = getIndexUrl();
    
    url && $.getJSON(url, function(data){
      var rowsMarkup = '';
      for (var id in data) {
        rowsMarkup += ('<tr id="row' + id + '"><td>' + data[id] + '</td><td>' + EDIT_LINK_MARKUP + '</td></tr>');
      }
      listTable.find('tbody').html(rowsMarkup);
      
      var dialogEl = $('#controlled-lists-dialog'),
          idOfSelectedRadio = $('#controlled-lists-form :radio:checked')[0].id,
          selectedLabel = $('label[for="' + idOfSelectedRadio + '"]');
      
      dialogEl.find('h2').html(selectedLabel.html());
      dialogEl.jqmShow();
    });
    
    return false;
  });
  
  $(document).keyup(function(e) {
    if (e.keyCode === 27 /* esc */) {
      $('.dialog').jqmHide();
    }
  });
  
  $('.close-trigger').click(function(){
    $('.dialog').jqmHide();
    return false;
  });
  
  function getUrl(url) {
    return $('#controlled-lists-form input:checked').parent().find(url);
  }
  function getIndexUrl() {
    return getUrl('.index-url').val();
  }
  function getCreateUrl() {
    return getUrl('.create-url').val();
  }
  function getUpdateUrl(id) {
    return getUrl('.update-url').val().replace(/\{\w+_id\}/, id);
  }
  function getDeleteUrl(id) {
    return getUrl('.delete-url').val().replace(/\{\w+_id\}/, id);
  }
      
  function findValueCellByTarget(el) {
    return el.parents('tr').find('td:eq(0)');
  }

  function cancelEditing(el) {
    var cell = findValueCellByTarget(el),
        parentRow = el.parents('tr');
    
    if (parentRow.hasClass('active')) {
      parentRow.remove();
    }
    else {
      parentRow.removeClass('active');
      cell.html(cell.data('originalValue'));
      el.parent().html(EDIT_LINK_MARKUP);
    }
    listTable.removeClass('active');
  }
  
  function enterEditMode(el) {
    var cell = findValueCellByTarget(el),
        originalValue = cell.text();

    cell.data('originalValue', originalValue);
    cell.html('<input type="text" value="' + originalValue.replace(/"/g, "&quot;") + '">');
    el.parent().html(SAVE_CANCEL_LINKS_MARKUP);
  }
  
  function onSuccess(data) {
    var evaledData = eval('(' + data + ')'),
        message = (evaledData && evaledData.message) ? evaledData.message : "";
        
    updateStatus(message);
    sortTable();
    $('#controlled-lists-confirm-dialog').jqmHide();
  }
  
  function updateStatus(message) {
    $('#controlled-lists-dialog .status-message').html(message).css({ visibility: "visible" });
  }
  
  function sortTable() {
    
    var rows = [];
    $('#controlled-lists-dialog tbody tr').each(function(i, row) {
      rows.push({
        value: $(row).find('td:first-child').html(),
        id: row.id
      });
    });
    
    rows.sort(function(row1, row2) {
      var value1 = row1.value, value2 = row2.value;
      return value1 > value2 ? 1 : value1 < value2 ? -1 : 0;
    });
    
    var rowsMarkup = '';
    $.each(rows, function(i, row) {
      rowsMarkup += [
        '<tr id="row', row.id, '"><td>', row.value, '</td><td>', EDIT_LINK_MARKUP, '</td></tr>'
      ].join('');
    });
    
    listTable.find('tbody').html(rowsMarkup);
  }
  
  function getValueFromElement(el) {
    return findValueCellByTarget(el).find('input').val();
  }
  function isNewValue(el) {
    return !el.closest('tr').attr('id');
  }
  
  function saveEditing(el) {    
    
    var value = getValueFromElement(el);
    
    if (value.length > 100) {
      $('#controlled-lists-dialog .status-message').html('');
      cancelEditing(el);
      return;
    }
    
    currentEditedElement = el;
    
    $('#controlled-lists-confirm-dialog .type').html(isNewValue(el) ? 'new value' : 'edit');
    $('#controlled-lists-confirm-dialog .info')[isNewValue(el) ? 'hide' : 'show']();
    
    $('#controlled-lists-confirm-dialog').jqmShow();
  }

  $(document).click(function(e, el) {

    var target = $(e.target);

    if (target.is('a.edit')) {
      enterEditMode(target);
      return false;
    }
    else if (target.is('a.cancel')) {
      cancelEditing(target);
      return false;
    }
    else if (target.is('a.save')) {
      saveEditing(target);
      return false;
    }
  });
  
  $('.add-new').click(function(){
    var newRow = $('<tr class="active"><td></td><td>' + EDIT_LINK_MARKUP + '</td></tr>');
    
    listTable.prepend(newRow);
    listTable.addClass('active');
    
    enterEditMode(newRow.find('.edit'));
    
    return false;
  });
  
  $('#controlled-lists-confirm-dialog .yes').live('click', function(){
    
    var cell = findValueCellByTarget(currentEditedElement),
        parentEl = currentEditedElement.parents('tr'),
        rowId = parseInt(parentEl.attr('id').slice(3), 10),
        newValue = getValueFromElement(currentEditedElement);
        
    parentEl.removeClass('active');
    listTable.removeClass('active');

    if (!isNaN(rowId)) {
      if (newValue === '') {
        url = getDeleteUrl(rowId);
        $.post(url, onSuccess);
        parentEl.fadeOut('slow', function() {
          $(this).remove();
        });
      }
      else {
        url = getUpdateUrl(rowId);
        $.post(url, { value: newValue }, onSuccess);
      }  
    }
    else {
      $.post(getCreateUrl(), { value: newValue }, function(data){
        
        var evaledData = eval('(' + data + ')');
        
        var newId = (function(data){ 
          for (var prop in data) return parseInt(prop, 10); 
        })(evaledData);
        
        if (newId) {
          parentEl.attr('id', 'row' + newId);
        }
        
        updateStatus('Controlled list saved.');
        sortTable();
        $('#controlled-lists-confirm-dialog').jqmHide();
      });
    }

    cell.text(newValue);
    currentEditedElement.parent().html(EDIT_LINK_MARKUP);
    
    return false;
  });
  
  $('#controlled-lists-confirm-dialog .no').live('click', function(){
    $('#controlled-lists-confirm-dialog').jqmHide();
    return false;
  });
  
  $(document).ready(function(){
    // initialize dialogs
    $('#controlled-lists-dialog').jqm({ modal: true, toTop: true, ajax: '' });
    $('#controlled-lists-confirm-dialog').jqm({ modal: true, toTop: true, ajax: '' });
  });
  
})();