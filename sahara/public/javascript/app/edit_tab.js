(function($){
  $(function(){
    
    $('p.lock a, p.unlock a').live('click', function(){
      
      var thisEl = $(this),
          containerEl = thisEl.closest('tr'),
          shouldLock = thisEl.closest('p').hasClass('lock'),
          uuid = $.trim(thisEl.closest('tr').find('.uuid').html());
          
      $.post('/work/' + uuid + (shouldLock ? '/reserve' : '/unreserve'), function(){
        containerEl.find('p.'+(shouldLock ? 'lock' : 'unlock')).hide();
        containerEl.find('p.'+(shouldLock ? 'unlock' : 'lock')).show();
      });
      return false;
    });
    
    $('.lock-all').click(function(){
		  $('#wait-for .message').html("Locking records...");
			$('#wait-for').show();
			var uuids = [];
			$('#content tr .uuid').each(function() { uuids.push($(this).html()); });
			$.ajax({
			  type: 'POST',
				url: '/works/multi-reserve',
				data: {uuids: uuids},
				dataType: 'json',
				success: function(data) {
				  for (var key in data.results) {
					  if (data.results[key].success) {
						  var el = $('#content tr :contains(' + key + ')').parent();
						  $(el).find('td.status-wrapper p.lock').hide();
						  $(el).find('td.status-wrapper p.unlock').show();
						}
					}
					$('.list-view-table .lock img').each(function() {
					  this.src = 'images/locked-icon.png';
					});
				  $('#wait-for').hide();
			  }
			});
			return false;
    });
    
    $('.unlock-all').click(function(){
		  var uuids = [];
			$('#wait-for .message').html("Unlocking records...");
			$('#wait-for').show();
			$('#content tr .uuid').each(function() {uuids.push($(this).html());});
			$.ajax({
			  type: 'POST',
				url: '/works/multi-unreserve',
				data: {uuids: uuids},
				dataType: 'json',
				success: function(data) {
				  for (var key in data.results) {
					  if (data.results[key].success) {
						  var el = $('#content tr :contains('+key+')').parent();
						  $(el).find('td.status-wrapper p.unlock').hide();
						  $(el).find('td.status-wrapper p.lock').show();
						}
					}
					$('.list-view-table .unlock img').each(function() {
					  this.src = 'images/editable-icon.png';
					});
				  $('#wait-for').hide();
				}
			});
			return false;
    });
    
    function getPageNum() {
      var href = window.location.href;
      if (href.indexOf('page=') === -1) {
        return 1;
      }
      else {
        var match = href.match(/(?:[?&])page=(\d+)/);
        if (match && match[1]) {
          return parseInt(match[1], 10);
        }
      }
    }
    
    function getLastPageNum() {
      return parseInt($('#last-page-num').val(), 10);
    }
    
    function goToPageNum(pageNum) {
      var href = window.location.href;
      if (href.indexOf('page=') === -1) {
        href += ((href.indexOf('?') > -1 ? '&' : '?') + 'page=' + pageNum);
      }
      else {
        href = href.replace(/([?&])page=\d+/, '$1page=' + pageNum);
      }
      window.location.href = href;
    }
    
    $('.nav-control-section .text').keyup(function(e){
      if (e.keyCode == 13 /* Enter */) {
        var pageNum = parseInt(this.value, 10);
        if (!isNaN(pageNum)) {
          var isNumInRange = (pageNum > 1 && pageNum < getLastPageNum());
          if (isNumInRange) {
            goToPageNum(pageNum);
          }
        }
      }
    });
    
    $('.nav-control-section .nav-control').live('click', function(){
      var currentPageNum = getPageNum(),
          lastPageNum = getLastPageNum(),
          el = $(this);
      
      if (el.is(':contains("«")')) {
        if (currentPageNum > 1) {
          goToPageNum(1);
        }
      }
      else if (el.is(':contains("‹")')) {
        if (currentPageNum > 1) {
          goToPageNum(currentPageNum - 1);
        }
      }
      else if (el.is(':contains("»")')) {
        if (currentPageNum < lastPageNum) {
          goToPageNum(lastPageNum);
        }
      }
      else if (el.is(':contains("›")')) {
        if (currentPageNum < lastPageNum) {
          goToPageNum(currentPageNum + 1);
        }
      }
      
      return false;
    });
    
    $('.list-view-table tr a').hover(
      function(){
        $(this).siblings('.tooltip').show();
      },
      function(){
        $(this).siblings('.tooltip').hide();
      });
    $('.list-view-table tr div.tooltip').mouseover(
      function(){
        $(this).show();
      });
    $('.list-view-table tr div.tooltip').mouseout(
      function(){
        $(this).hide();
      });
    
    var thumbViewTableEl = $('.thumb-view-table'),
        listViewTableEl = $('.list-view-table'),

        thumbViewTriggerEl = $('.thumb-view-trigger'),
        listViewTriggerEl = $('.list-view-trigger');

    function selectListView() {
      listViewTriggerEl.addClass('selected');
      thumbViewTriggerEl.removeClass('selected');
      thumbViewTableEl.addClass('hidden');
      listViewTableEl.removeClass('hidden');
    }

    function selectThumbView() {
      thumbViewTriggerEl.addClass('selected');
      listViewTriggerEl.removeClass('selected');
      thumbViewTableEl.removeClass('hidden');
      listViewTableEl.addClass('hidden');
    }

    /*listViewTriggerEl.click(function(){
      selectListView();
      return false;
    });

    thumbViewTriggerEl.click(function(){
      selectThumbView();
      return false;
    });
    */

    // by default, select thumb view on page load
    // selectThumbView();
    
  });
})(jQuery);