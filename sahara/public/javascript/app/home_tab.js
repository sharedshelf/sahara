(function($){
  $(function(){
    
    var formEl = $('#search-form'),
        inputEl = $('[name="phrase"]'),
        errorEl = $('.errors');
        
    inputEl.focus();
    
    formEl.submit(function(){
      if (!inputEl.val()) {
        errorEl.removeClass('hidden');
        return false;
      }
      errorEl.addClass('hidden');
    });
    
    var win;
    function openWindow(url) {
      if (!win || win.closed) {
        win = window.open(url, 'win');
      } else {
        win.focus();
      }
    }
    $('#sdl').click(function(){
      openWindow($(this).attr('href'));
      return false;
    });
  });
})(jQuery);