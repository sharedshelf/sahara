(function($){
  
  $(function(){
    $('.view-collection').click(function(){
      window.open(this.href);
      return false;
    });
    $('.my-profile').click(function(){
      IMATA.dialogs.show_profile();
      return false;
    });
    $('.logout').click(function(){
      logout();
      return false;
    });
    $('.info-tools').click(function(){
      show_tools_menu();
      return false;
    });
    $('.selector-small').click(function(){
      IMATA.thumbnails.small();
      return false;
    });
    $('.selector-medium').click(function(){
      IMATA.thumbnails.medium();
      return false;
    });
    $('.selector-large').click(function(){
      IMATA.thumbnails.large();
      return false;
    });
    
    function changeSortParam(value) {
      var str = document.location.href,
          result = '',
          match = str.match(/sort=[^&$]+/);

      if (match) {
        result = str.replace(/sort=[^&$]+/, 'sort=' + value);
      }
      else {
        var queryChar = document.URL.indexOf('?') > -1 ? '&' : '?';
        result = (str + queryChar + 'sort=' + value);
      }
      return result;
    }
    
    $('.sort').live('click', function(){
      
      var sortBy = ($(this)[0].className.match(/(?:\s|^)by-(\S+)/) || { })[1],
          sortDirection = $(this).hasClass('desc') 
            ? 'asc' : $(this).hasClass('asc') 
              ? 'desc' : 'desc';
      
      $('.sort').removeClass('asc desc');
      $(this).addClass(sortDirection);
      
      switch(sortBy) {
        case 'date-contributed':
          document.location.href = changeSortParam('contribution_date_' + sortDirection);
          break;
        
        case 'contributor':
          document.location.href = changeSortParam('contributor_' + sortDirection);
          break;
          
        case 'title':
          document.location.href = changeSortParam('title_sort_' + sortDirection);
          break;
      }
      return false;
    });
  });
})(jQuery);