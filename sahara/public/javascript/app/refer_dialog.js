(function(){
  
  function interpolate(string, data) {
    return String(string).replace(/#\{[^}]+\}/g, function(match) {
      var name = match.substring(2, match.length - 1);
      return data[name] || '';
    });
  }

  function showError(errorMessage) {
    $('#refer-dialog .status-message').html(errorMessage).css({ visibility: "visible" });
  }
  function checkForValidationError() {
    var anyRadioButtonSelected = $('#refer-dialog :radio:checked').size() > 0,
        anySubsequentCheckboxSelected = $('#refer-dialog .role:has(:radio:checked) :checkbox:checked').size() > 0;

    if (!anyRadioButtonSelected) {
      return 'Please select a role (Administrator, Content Editor, or Librarian Editor)';
    }
    else if (!anySubsequentCheckboxSelected) {
      var selectedSectionName = $('#refer-dialog :radio:checked').next('label').text();
      return ('Please select one or more ' + selectedSectionName);
    }
    return '';
  }
  function buildSelectedNamesList() {
    var selectedUsers = $.map($('#refer-dialog .list :checkbox:checked'), function(el){
      return { 
        name: $(el).next('label').text(),
        id: $(el).attr('id')
      };
    });
    var namesList = $.map(selectedUsers, function(user){
      return '<li id="' + user.id + '">' + user.name + '</li>';
    });
    return namesList.join('');
  }
  function buildReferralList(users) {
    var list = '', 
        listItemTemplate = '<li><input id="user-#{profileId}" type="checkbox"><label for="user-#{profileId}">#{email}</label></li>',
        uid;

    users.sort(function(user1, user2) {
      var email1 = user1.full_name, email2 = user2.full_name;
      return email1 > email2 ? 1 : email1 < email2 ? -1 : 0;
    });
    
    $.each(users, function(i, user) {
      list += interpolate(listItemTemplate, { profileId: user.profileId, email: user.full_name });
    });

    return list;
  }
  function buildReferralLists() {
    $.getJSON('/priveleged-users', function(data){
      if (!data) return;

      if (data.admins) {
        var adminsList = buildReferralList(data.admins);
        $('ul.administrators').html(adminsList);
      }
      if (data.catalogers) {
        var catalogersList = buildReferralList(data.catalogers);
        $('ul.librarians').html(catalogersList);
      }
      if (data.reviewers) {
        var reviewersList = buildReferralList(data.reviewers);
        $('ul.content-editors').html(reviewersList);
      }
    });
  }
  
  function selectRoleColumn(roleElement) {
    var radioEl = roleElement.find('h3 :radio');
    radioEl.attr('checked', true).focus();
    roleElement.find('ul').show();
    
    selection.save();
    
    var roleElementSiblings = roleElement.siblings();
    roleElementSiblings.find('ul').hide();
    
    roleElementSiblings.find(':checkbox').attr('checked', false);
  }
  
  function showReferDialog() {
    var dialog = $('#refer-dialog');
    var thumbUrl = $('#work-image-url').val();
    if (thumbUrl) {
      dialog.find('.thumb').attr('src', thumbUrl);
    }

    var roles = dialog.find('.role');
    roles.find('ul').hide();
    
    roles.find(':checkbox').attr('checked', false);
    
    var filename = $('#work-filename').val();
    dialog.find('.filename').html(filename);
    
    roles.find('h3 :radio').attr('checked', false);

    dialog.jqmShow();
  }
  
  function initDialogs() {
    var options = { modal: true, toTop: true, ajax: '' };
    $('#refer-dialog').jqm(options);
    $('#refer-dialog-cancel').jqm(options);
    $('#refer-dialog-confirm').jqm(options);
    $('#edit-save-confirm-dialog').jqm(options);
  }
  
  var selection = (function(){
    var _selection = {
      columnName: '',
      selectedIds: [ ]
    };
    function storeInCookie(){
      $.cookie('refer-column-name', _selection.columnName);
      $.cookie('refer-ids', _selection.selectedIds.join(','));
    }
    function save() {
      if (this.shouldRemember) {
        var columnName = $('#refer-dialog :radio:checked').siblings('label').text();
        _selection.columnName = columnName;
        
        var selectedIds = $.map($('#refer-dialog :radio:checked').closest('.role').find('.list :checkbox:checked'), 
          function(el) { return el.id; });
          
        _selection.selectedIds = selectedIds;
        
        storeInCookie();
      }
    }
    function initialize() {
      
      var columnName = $.cookie('refer-column-name');
      var ids = $.cookie('refer-ids');
      
      if (!columnName) return;
      
      var columnToSelect = $('#refer-dialog h3 label:contains("' + columnName + '")').closest('.role');
      selectRoleColumn(columnToSelect);
      
      if (!ids) return;
      
      setTimeout(function(){
        $.each(ids.split(','), function(i, elementId) {
          $('#' + elementId)[0].checked = true;
        });
      }, 500);
    }
    return {
      save: save,
      initialize: initialize,
      shouldRemember: false
    }
  })();
  
  function initConfirmDialogBeforeClosing() {
    var changelog = $('#changelog'),
        changelogToggler = $('#changelog-toggler');

    changelog.hide();

    changelogToggler.click(function(){
      changelog.toggle();
      changelogToggler.html(changelog.is(':visible') ? 'Hide' : 'Show');
      return false;
    });
    $('.edit-dialog-close-button').click(function(){
      if ($('#work-document').attr('changed') == "true") {
        $('#edit-save-confirm-dialog').jqmShow();
      }
      else {
        $('#asset-edit-dialog').jqmHide();
      }
      return false;
    });

    $('#edit-save-confirm-dialog .no').click(function(){
      $('#edit-save-confirm-dialog').jqmHide();
      $('#asset-edit-dialog').jqmHide()
    });
    $('#edit-save-confirm-dialog .yes').click(function(){
      $('#edit-save-confirm-dialog').jqmHide();
      // save record
      $('#save-trigger').click();
      // and mark record as not changed
      $('#work-document').attr('changed', false);
    });
  }
  
  function initReferDialogs() {
    $('#refer-trigger').click(window.showReferDialog = function(){
      
      buildReferralLists();
      showReferDialog();
      
      selection.initialize();

      return false;
    });

    $('.any :checkbox').click(function(){
      $(this).parents('.role').find('ul:eq(1) li :checkbox').attr('checked', this.checked);
      selection.save();
    });
    $('.list :checkbox').live('click', function(){
      selection.save();
    });

    // selecting one role column deselects other columns
    $('h3 :radio').click(function(){
      selectRoleColumn($(this).parents('.role'));
    });

    $('#remember-selection').click(function(){
      selection.shouldRemember = this.checked;
      if (selection.shouldRemember) {
        selection.save();
        $.cookie('remember-selection', 'true');
      }
      else {
        $.cookie('remember-selection', '');
      }
    });
    
    if ($.cookie('remember-selection') === 'true') {
      $('#remember-selection').click();
    }
    
    $('#refer-dialog .next').click(function(){
      var error = checkForValidationError();
      if (error) {
        showError(error);
      }
      else {
        $('#refer-dialog').jqmHide();
        $('#refer-dialog-confirm ul.floated').html(buildSelectedNamesList());
        $('#refer-dialog-confirm').jqmShow();
        
        var imgUrl = $('#work-image-url').val();
        $('.refer-list img').attr('src', imgUrl);
        
        var filename = $('#work-filename').val();
        $('#refer-dialog-confirm .filename').html(filename);
      }
      return false;
    });
    $('#refer-dialog .cancel').click(function(){
      $('#refer-dialog').jqmHide();
      $('#refer-dialog-cancel').jqmShow();
      return false;
    });
    $('#refer-dialog-confirm .cancel').click(function(){
      $('#refer-dialog-confirm').jqmHide();
      $('#refer-dialog-cancel').jqmShow();
      return false;
    });
    $('#refer-dialog-confirm .back').click(function(){
      $('#refer-dialog-confirm').jqmHide();
      $('#refer-dialog').jqmShow();
      return false;
    });
    $('#refer-dialog-confirm .refer').click(function(){

      var UUID = $('#work-id').val();
      if (!UUID) return false;
      
      var selectedIds = $.map($('.refer-list ul li'), function(el){
        return parseInt($(el).attr('id').replace('user-', ''), 10);
      });

      $.post('/work/' + UUID + '/assign', { userid: selectedIds });
      
      // hide dialogs
      $('#refer-dialog-confirm').jqmHide();
      $('#asset-edit-dialog').jqmHide();
      
      // remove entire row that corresponds to this record
      $('.uuid:contains('+ UUID +')').closest('tr').fadeOut();
      
      // reload page if no records are left
      if ($('tr:has(.uuid)').length === 0) {
        window.location.reload();
      }
      
      return false;
    });
    $('#refer-dialog-cancel .yes').click(function(){
      $('#refer-dialog-cancel').jqmHide();
      return false;
    });
    $('#refer-dialog-cancel .no').click(function(){
      $('#refer-dialog-cancel').jqmHide();
      $('#refer-dialog').jqmShow();
      return false;
    });
  }

  initDialogs();
  initReferDialogs();
  initConfirmDialogBeforeClosing();
  
  
})();