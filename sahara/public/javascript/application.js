var IMATA = {
    assets: {
        selected: undefined,
        set_selected: function(id){
            IMATA.assets.selected = id;
        },
        get_selected: function(){
            return IMATA.assets.selected;
        },
        clear_selected: function(){
            IMATA.assets.selected = undefined;
        },
        load_tab: function(tab){
            if (!tab) {
                tab = $('#folder-wrapper').data('selected.tabs');
            }
            $('#folder-wrapper').tabs('load', tab);
            
            setTimeout(function(){
              // refresh layout
              $('#dialog').jqmShow().jqmHide()
            }, 100);
        },
        update_count: function(){
            $.getJSON(IMATA_PATH + '/works;count', {}, function(data){
                $('#progressing-count').text("(" + data.progressing + ")");
                $('#contributed-count').text("(" + data.contributed + ")");
            });
        },
        recalc_thumbs_height: function(){
            var els = $('.asset'), max = 0;
            if (els.length) {
                $.each(els, function(id, el){
                    var height = $(el).height();
                    if (height > max) {
                        max = height;
                    }
                });
                els.height(max);
            }
        },
        select_single: function(uuid, img_el){
            if(uuid != IMATA.assets.get_selected) {
                $('div.img-wrapper').removeClass('selected');
                $(img_el).parent().addClass('selected');
                IMATA.assets.set_selected(uuid);
                if ($(img_el).hasClass('complete')) {
                    IMATA.document.show(uuid);
                } else {
                    if (IMATA.assets.selected && IMATA.document.changed()) {
                        IMATA.document.save(true);
                        IMATA.document.edit(uuid);
                    }
                    else {
                        IMATA.document.edit(uuid);
                    }
                }
            }
        },
        remove_selected: function(){
            if (IMATA.assets.selected) {
                var id = IMATA.assets.selected;
                IMATA.dialogs.confirm("Delete selected image?", function(){
                    $.ajax({
                        type: "DELETE",
                        dataType: 'json',
                        url: IMATA_PATH + '/work/' + id,
                        success: function(resp){
                            $('#' + id).parent().parent().remove();
                            IMATA.assets.update_count();
                            if ($('div.assets img') == null) {
                                load_tab();
                            }
                            IMATA.document.clear();
                            IMATA.dialogs.alert(resp.message);
                        },
                        error: function(request, status, error){
                            IMATA.dialogs.alert(status);
                        }
                    });
                });
            }
            else {
                if ($('div.assets div.img-wrapper').attr('id') == null) {
                    IMATA.dialogs.alert('The image panel is empty.');
                }
                else {
                    IMATA.dialogs.alert('Please select an image');
                }
            }
        },
        resort: function(order){
            $.cookie('assetsort', order, {
                expires: 365,
                path: '/'
            });
            IMATA.assets.load_tab();
        },
        remove_multiple: function(){
            $('#dialog').jqm({
                ajax: IMATA_PATH + '/works;delete',
                closeClass: 'close',
                modal: true,
                overlay: 30
            });
            $('#dialog').jqmShow();
        }
    },
    /*
     * Stub for interfacing with STOR
     */
    stor: {},
    /*
     * Thumbnail functions
     */
    thumbnails: {
        highlight: function(){
            if ($.cookie('thumbnailsize') == null) {
                $.cookie('thumbnailsize', 0, {
                    expires: 365,
                    path: '/'
                });
            }
            $('#left-column div.settable-button').removeClass('current-setting');
            if ('0' == $.cookie('thumbnailsize')) {
                $('#thumbnail-small').parent().parent().addClass('current-setting');
            }
            if ('1' == $.cookie('thumbnailsize')) {
                $('#thumbnail-medium').parent().parent().addClass('current-setting');
            }
            if ('2' == $.cookie('thumbnailsize')) {
                $('#thumbnail-large').parent().parent().addClass('current-setting');
            }
        },
        small: function(){
            $.cookie('thumbnailsize', 0, {
                expires: 365,
                path: '/'
            });
            IMATA.thumbnails.highlight();
            IMATA.assets.load_tab(); // reload
        },
        medium: function(){
            $.cookie('thumbnailsize', 1, {
                expires: 365,
                path: '/'
            });
            IMATA.thumbnails.highlight();
            IMATA.assets.load_tab(); // reload
        },
        large: function(){
            $.cookie('thumbnailsize', 2, {
                expires: 365,
                path: '/'
            });
            IMATA.thumbnails.highlight();
            IMATA.assets.load_tab(); // reload
        }
    },
    /*
     * Dialog functions
     */
    dialogs: {
        // Shows a confirmation dialog, pass it a message and
        // callback functions for yes and no buttons.
        confirm: function(message, yes, no){
            $('#confirm-dialog div.message').html(message);
            // clear any previous binding
            $('#confirm-dialog-yes').unbind();
            $('#confirm-dialog-yes').click(function(){
                $('#confirm-dialog').jqmHide();
                yes && yes();
            });
            $('#confirm-dialog-no').unbind();
            $('#confirm-dialog-no').click(function(){
                $('#confirm-dialog').jqmHide();
                no && no();
            });
            $('#confirm-dialog').jqm({
                modal: true,
                overlay: 10
            });
            $('#confirm-dialog').jqmShow();
        },
        disable_controls: function(){
            $('div.dialog-footer a.button').addClass('disabled');
            $('div.dialog-footer a.button').attr('disabled', 'disabled');
        },
        enable_controls: function(){
            $('div.dialog-footer a.button').removeClass('disabled');
            $('div.dialog-footer a.button').removeAttr('disabled');
        },
        // shows misc messages
        alert: function(info, addendum){
            $('#alert-dialog').jqm({
                modal: true,
                overlay: 10
            }).find('#info-major').html(info);
            if (addendum != null) {
                $('#alert-dialog').find('#info-minor').html(addendum);
            }
            else {
                $('#alert-dialog').find('#info-minor').html('');
            }
            $('#alert-dialog').jqmAddClose($('#close_info')).jqmShow();
        },
        copy: function(){
            if (IMATA.document.changed()) {
                IMATA.document.save(true);
            }
            $('#dialog').jqm({
                ajax: IMATA_PATH + '/multicopy_field_selection.html',
                closeClass: 'close',
                modal: true,
                overlay: 30
            });
            $('#dialog').jqmShow();
        },
        close_all: function(){ // Close any open dialogs
            $('#dialog').jqmHide();
            $('#text-dialog').jqmHide();
            $('#confirm-dialog').jqmHide();
        },
        close: function(selector){
            $('#' + selector).jqmHide();
        },
        show_text: function(content, title){
            $('#text-dialog').jqm({
                modal: true,
                overlay: 10
            }).find('#text-dialog-title').html(title);
            $.ajax({
                type: 'GET',
                url: content,
                success: function(data){
                    $('#text-dialog').find('div.text-content').html(data);
                },
                error: function(data){
                    $('#text-dialog').html("Error retrieving document");
                }
            });
            $('#text-dialog').jqmAddClose($('#close')).jqmShow();
        },
        show_profile: function(){
            $('#dialog').jqm({
                ajax: IMATA_PATH + '/profile',
                closeClass: 'close',
                modal: true,
                overlay: 30,
                onLoad: function(h){
                    $('#user-profile-tabs').tabs();
                }
            });
            $('#dialog').jqmShow();
        },
        image_manipulation: function(id){
            var image;
            $('#transform-dialog').jqm({
                ajax: IMATA_PATH + '/work/' + id + '/media/flip',
                closeClass: 'close',
                modal: true,
                overlay: 30,
                onLoad: function(){
                    $('#image-manipulation-tabs').parent().tabs();
                    if ($('#' + id + ' img').hasClass('complete')) {
                        $('#image-manipulation-tabs')
                          .tabs('disable', 1)
                          .tabs('disable', 2);
                    }
                    IMATA.upload.ajaxify_replacement_form(function(){
                        var reload = new Image();
                        reload.onload = function(){
                            $('#information-shim').css({
                                height: (256 - this.height) / 2
                            });
                            $('#information-panel-img').attr('src', this.src);
                            $('#shim').css({
                                height: (256 - this.height) / 2
                            });
                            $('#flip-img').attr('src', this.src);
                        };
                        reload.src = '/work/' + id + '/media/size2?nocache=' + Math.random();
                        update_image($('#' + id + ' img'), '/work/' + id + '/media/size0?nocache=' + Math.random());
                    });
                }

            });
            $('#transform-dialog').jqmShow();
        },
        admin_edit: function(uuid){
            var src;
	    $('#wait-for .message').html("Loading...");
	    $('#wait-for').show();
	    $('#asset-edit-dialog').load(IMATA_PATH + '/work/'+uuid+'/edit/admin', function() {
					     $('#admin-asset-manipulation-tabs').wrap('<div>').parent().tabs();
					     IMATA.document.instrument();
					     $('div.editorial-notes').click(toggle_editorial_information);
					     if ($('#admin-asset-information-pane div.status').hasClass('suppress')) {
						 $('#toggle_suppression span').text('UNSUPPRESS');
					     } else {
						 $('#toggle_suppression span').text('SUPPRESS');
					     }
					     $('#asset-edit-dialog').jqm({
									  model: true,
									  overlay: 30,
									  closeClass: 'close'
									 });
					     $('#wait-for').hide();
					     $('#asset-edit-dialog').jqmShow();
					 });
/*            $('#asset-edit-dialog').jqm({
                ajax: IMATA_PATH + '/work/' + uuid + '/edit/admin',
                closeClass: 'close',
                modal: true,
                overlay: 30,
                onLoad: function(h){
                    $('#admin-asset-manipulation-tabs').wrap('<div>').parent().tabs();
                    IMATA.document.instrument();
		    $('div.editorial-notes').click(toggle_editorial_information);
                    if ($('#admin-asset-information-pane div.status').hasClass('suppress')) {
                        $('#toggle_suppression span').text('UNSUPPRESS');
                    }
                    else {
                        $('#toggle_suppression span').text('SUPPRESS');
                    }
                }
            });
            $('#asset-edit-dialog').jqmShow();*/
        }
    },
    /*
     * Upload related functions
     */
    upload: {
        prompt: function(){
            window.open('works;add_multiple', 'newwindow', config = 'height=600,width=425,toolbar=no,scrollbars=no,resizable=no,location=no,navigation=no,directories=no,status=no,copyhistory=no');
        },
        ajaxify_replacement_form: function(callback){
            $('form.file-upload').each(function(){
                var target = this;
                $(target).ajaxForm({
                    dataType: 'json',
                    success: function(res){
                        $('.dialog-footer div.wait').hide();
                        $(target).show();
                        IMATA.dialogs.enable_controls();
                        if (res.success) {
                            IMATA.dialogs.alert('Asset was replaced successfully');
                            callback(res);
                        }
                        else {
                            IMATA.dialogs.alert('Error processing upload', res.message);
                        }
                    },
                    resetForm: true,
                    beforeSubmit: function(){
                        $(target).hide();
                        IMATA.dialogs.disable_controls();
                        $('.dialog-footer div.wait').show();
                    }
                });
            });
        },
        complete: function(errors){
            IMATA.assets.update_count();
            if (errors) {
                IMATA.dialogs.alert("Upload complete (with errors)");
                IMATA.assets.load_tab();
                IMATA.assets.update_count();
            }
            else {
                IMATA.dialogs.alert("Upload complete");
                IMATA.assets.load_tab();
                IMATA.assets.update_count();
            }
        }
    },
    /*
     * Initialize IMATA functions
     *
     */
    init: function(){
        var selected_tab;
        var lists = $('#folder-wrapper');
        if (lists.length) {
            lists.tabs({
                ajaxOptions: {
                    async: false,
                    cache: false
                },
                cache: false,
                spinner: ' ',
                select: function(event, ui){
                    IMATA.document.clear();
                    IMATA.assets.clear_selected();
                    return true;
                },
                load: function(ui, tabInstance){
                    $('a.goto-processing').unbind();
                    $('a.goto-processing').click(function(event){
                        event.preventDefault();
                        list.tabs('select', 0);
                    });
                    if (IMATA.assets.get_selected() != undefined) {
                        var search_string = '#' + IMATA.assets.get_selected();
                        try {
                            $(search_string).addClass('selected');
                            $('div.assets').scrollTo(search_string);
                        }
                        catch (e) {
                        }
                    } else {
                        selected_tab = $('#folder-wrapper').data('selected.tabs');
                        if (selected_tab == 0) {
                            $('div.assets div.img-wrapper :first').click();
                        } else if(selected_tab == 1) {
                            $('#contributed div.assets div.img-wrapper img[alt="thumbnail"]:first').click();
                        }
                    }
                    JT_init();
                    IMATA.thumbnails.highlight();

                    if (tabInstance && tabInstance.panel) {
                        var firstThumb = $(tabInstance.panel).find('.img-wrapper')[0];
                        if (firstThumb) {
                            var id = firstThumb.id;
                        }
                    }
/*                    if (id) {
                        $('#' + id).addClass('selected');
                        IMATA.assets.set_selected(id);
                        if ($('#' + id + ' img').hasClass('complete')) {
                            IMATA.document.show(id);
                        }
                        else {
                            IMATA.document.edit(id);
                        }
                    }
*/
                  return true;
                }
            });
            IMATA.assets.recalc_thumbs_height();
            IMATA.assets.update_count();
            IMATA.thumbnails.highlight();
        }
    }
};


$(document).ready(function(){
    $(window).unbind('click.tools-menu');
    $(window).bind('click.tools-menu', function(){
        $('#tools-menu').hide();
    });
    $('#help-btn').click(function(){
        showText($('#help-btn').attr('href'), 'HELP');
        return false;
    });
    var menu = $('#tools-menu');
    $(document.documentElement).click(function(e){
        if (!$(e.target).parents('#tools-menu').length) {
            menu.hide();
        }
    });
    IMATA.init();
    var splitter = $("#splitter");
    if (splitter.length) {
        splitter.splitter({
            type: 'v',
            initA: true
        });
        // Firefox doesn't fire resize on page elements
        $(window).bind("resize", function(){
            $("#splitter").trigger("resize");
        }).trigger("resize");
    }
    $('.autocomplete-result').css({
        display: 'none'
    });
});


function quick_edit_save(id, success) {
    var img_size1, img_size2;
    if (success) {
        img_size2 = new Image();
        img_size2.onload = function(){
            $('div.asset-data img').attr('src', this.src);
        };
        img_size2.src = 'work/' + id + '/media/size2?nocache=' + Math.random();
        img_size1 = new Image();
        img_size1.onload = function(){
            $('#' + id + ' img').attr('src', this.src);
        };
        img_size1.src = 'work/' + id + '/media/size1?nocache=' + Math.random();
        alert('Changes have been saved');
    }
    else {
        alert("Failed to save changes, administrator notified");
    }
}


function getSelectedDialogAssets(){
    var uuids = [];
    $('#dialog-assets div.selected').each(function(){
        uuids.push($(this).attr('id').split('_')[1]);
    });
    return uuids;
}

function show_tools_menu(){
    $('#tools-menu').show();
}

function initUpdateProfileValidation(){

    // init validation on before updating profile form

    if (arguments.callee.inited)
        return;
    arguments.callee.inited = true;

    var container = $('#profile-tab');
    var textFields = container.find('input[type="text"]');
    var errorLog = container.find('.error-message');

    $('#update-profile-form').bind('submit', function(){

        var isEmpty = false;
        $.each(textFields, function(i, field){
            if (/^\s*$/.test(field.value)) {
                isEmpty = true;
            }
        });
        if (isEmpty) {
            errorLog.text('Please fill all of the values');
            return false;
        }
        return true;
    });
}

function showText(content, title){
    $('#text-dialog').jqm({
        modal: true,
        overlay: 10
    }).find('#title').html(title);
    $.ajax({
        type: 'GET',
        url: content,
        success: function(data){
            $('#text-dialog').find('div.text-content').html(data);
            if (typeof scrollDocumentToTop != 'undefined') {
                scrollDocumentToTop();
            }
        },
        error: function(data){
            $('#text-dialog').html("Error retrieving document");
        }
    });
    $('#text-dialog').jqmAddClose($('#close')).jqmShow();
}

function onContainerCollapse(sType, aArgs) {
    $(aArgs[0]._elContainer).hide();
}

function onContainerExpand(sType, aArgs){
    var inputEl = aArgs[0]._elTextbox;
    var containerEl = aArgs[0]._elContainer;
    $(containerEl).find('div.yui-ac-content').css('width', $(inputEl).width() + 'px');
    $(containerEl).find('div.yui-ac-content').css('margin-top', $(inputEl).height() + 3 + 'px');
    $(containerEl).show();
}

function initAutocompleter(inputEl, containerEl, dataSourceUrl){
//    YAHOO.example.BasicRemote = (function(){
    (function(){
        // Use an XHRDataSource
        var oDS = new YAHOO.util.XHRDataSource(dataSourceUrl);
        // Set the responseType
        oDS.responseType = YAHOO.util.XHRDataSource.TYPE_TEXT;
        // Define the schema of the delimited results
        oDS.responseSchema = {
            recordDelim: "\n",
            fieldDelim: "\t"
        };
        // Enable caching
        oDS.maxCacheEntries = 10;

        // Instantiate the AutoComplete
        var oAC = new YAHOO.widget.AutoComplete(inputEl, containerEl, oDS);

        oAC.autoHighlight = false;
        oAC.animVert = false;

        try {
            oAC.containerExpandEvent.subscribe(onContainerExpand);
            oAC.containerCollapseEvent.subscribe(onContainerCollapse);
        }
        catch (e) {
            // alert(e.message);
        }


        return {
            oDS: oDS,
            oAC: oAC
        };
    })();
}

/*function logout(){
    $.getJSON('/logout', function(data){
        if (data.status == 'ok') {
            $.getJSON(data.artstor_logout, function(as_data){
                if (as_data.status == 'ok') {
                    window.location = data.artstor_login;
                }
            });
        }
    });
}
*/
/*
 * Stor functions need to maintain a bit of state, so add them
 * to imata within a closure.
 */
(function(){
    var new_id;
    var uuid;
    IMATA.stor.save = function(id){
        if(uuid != id) { new_id = undefined; uuid = id; }
        if (new_id != null) {
            $('.dialog-footer div.wait').show();
            IMATA.dialogs.disable_controls();
            IMATA.stor.update(id, function(success){
                $('.dialog-footer div.wait').hide();
                if (success) {
                    $('#asset-transform-pane span.error').text('Your changes have been saved');
                    update_image($('#' + id + ' img'), '/work/' + id + '/media/size0');
                    var new_img = new Image();
                    new_img.onload = function(){
                        $('#information-shim').css({
                            height: (256 - this.height) / 2
                        });
                        $('#information-panel-img').attr('src', this.src);
                    };
                    new_img.src = '/work/' + id + '/media/size2?nocache=' + Math.random();
                }
                else {
                    $('#asset-transform-pane span.error').text('Failed to save changes, the administrator has been notified');
                }
                IMATA.dialogs.enable_controls();
            });
        } else {
            IMATA.dialogs.alert("No edits have been made to this image.");
        }
    };
    IMATA.stor.admin_save = function(id){
        if(uuid != id) { new_id = undefined; uuid = id; }
        if (new_id != null) {
            $('.dialog-footer div.wait').show();
            IMATA.dialogs.disable_controls();
            IMATA.stor.update(id, function(success){
                if (success) {
                    $('#admin-asset-transform-pane span.error').text('Your edits have been saved and will be reflected in the SAHARA Collection within 15 minutes.');
                    $('.dialog-footer div.wait').hide();
                    var new_img = new Image();
                    new_img.onload = function(){
                        $('#' + id + ' img').attr('src', this.src);
                    };
                    new_img.src = '/work/' + id + '/media/size1?nocache=' + Math.random();
                    $('#admin-asset-information-pane div.asset-image').css({
                        'background-image': 'url(work/' + id + '/media/size1?nocache=' + Math.random() + ')'
                    });
                    IMATA.dialogs.alert('Changes have been saved');
                }
                else {
                    $('#admin-asset-transform-pane span.error').text('Failed to save changes, the administrator has been notified');
                }
                IMATA.dialogs.enable_controls();
            });
        } else {
            $('#admin-asset-transform-pane span.error').text('Image is unchanged, nothing to save.');
        }
    };
    IMATA.stor.replace = function(form, before_cb, result_cb, error_cd){
        var file = $(form + ' :input[name="asset-file"]').attr('value');
        if (file.length > 0) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: result_cb,
                beforeSubmit: before_cb,
                resetForm: true
            });
        }
        else {
            IMATA.dialogs.alert('Please choose a replacement file');
        }
    };
    IMATA.stor.update = function(id, callback){
        $.ajax({
            async: false,
            type: 'GET',
            url: IMATA_PATH + '/work/' + id + '/media/flip?action=DONE&uuid=' + new_id,
            success: function(data){
                new_id = undefined;
                callback(true);
            },
            error: function(request, status, error){
                new_id = undefined;
                callback(false, request, status, error);
            }
        });
    };
    IMATA.stor.flip = function(mode, id){
        var new_img;
        if(uuid != id) { new_id = undefined; uuid = id; }
        disable_buttons('#asset-transform-pane div.dialog-footer');
        $('span.error').text(' ');
        $('#flip-img').attr('src', 'images/loading.gif');
        $.ajax({
            async: false,
            type: 'GET',
            url: IMATA_PATH + '/work/' + id + '/media/flip?action=' + mode + '&uuid=' + new_id + '&iesucks=' + Math.random(),
            success: function(new_url){
                enable_buttons('#asset-transform-pane div.dialog-footer');
                IMATA.dialogs.enable_controls();
                new_id = new_url.split('/').pop();
                new_img = new_url + '?bogus=' + Math.random();
                new_img = new Image();
                new_img.onload = function(){
                    $('#flip-img').attr('src', this.src);
                };
                new_img.src = new_url + "?bogus=" + Math.random();
            },
            error: function(request, status, error){
                enable_buttons('#asset-transform-pane div.dialog-footer');
                IMATA.dialogs.alert('Flip/Rotate Failed', 'Status: ' + status);
            }
        });
    };
    IMATA.stor.replace_reset = function(){
        $('form.replace-asset')[0].reset();
    };
})();

function admin_replace_asset(uuid){
    var thumbnail;
    var flipimg;
    IMATA.stor.replace('form.replace-asset', function(){
        $('form.replace-asset').hide();
        IMATA.dialogs.disable_controls();
        $('.dialog-footer div.wait').show();
    }, function(res){
        $('.dialog-footer div.wait').hide();
        $('form.replace-asset').show();
        IMATA.dialogs.enable_controls();
        if (res.success) {
            if (res.type == 'img') {
                $('#admin-asset-manipulation-tabs').tabs('enable', 1);
            }
            else {
                $('#admin-asset-manipulation-tabs').tabs('disable', 1);
            }
            thumbnail = new Image();
            thumbnail.onload = function(){
                $('#' + uuid + ' img').attr('src', this.src);
            };
            flipimg = new Image();
            flipimg.onload = function(){
                $('#flip-img').attr('src', this.src);
            };
            thumbnail.src = '/work/' + uuid + '/media/size1?nocache=' + Math.random();
            flipimg.src = '/work/' + uuid + '/media/size2?nocache=' + Math.random();
            $('#admin-asset-information-pane div.asset-image').css({
                'background-image': 'url(work/' + uuid + '/media/size1?nocache=' + Math.random() + ')'
            });
            $('#admin-asset-information-pane span.error').text('Image was replaced successfully');
        }
        else {
            $('#admin-asset-information-pane span.error').text('Error processing upload: '+ res.message);
        }
    });
}

function replace_asset(uuid){
    var tn_size = $.cookie('thumbnailsize');
    var thumbnail;
    var flipimg;
    IMATA.stor.replace('form.replace-asset', function(){
        $('form.replace-asset').hide();
        IMATA.dialogs.disable_controls();
        $('.dialog-footer div.wait').show();
    }, function(res){
        $('.dialog-footer div.wait').hide();
        $('form.replace-asset').show();
        IMATA.dialogs.enable_controls();
        if (res.success) {
            if (res.type == 'img') {
                $('#image-manipulation-tabs').tabs('enable', 1);
            }
            else {
                $('#image-manipulation-tabs').tabs('disable', 1);
            }
            thumbnail = new Image();
            thumbnail.onload = function(){
                $('#' + uuid + ' img').attr('src', this.src);
            };
            flipimg = new Image();
            flipimg.onload = function(){
                $('#flip-img').attr('src', this.src);
                $('#information-panel-img').attr('src', this.src);
            };

            thumbnail.src = '/work/' + uuid + '/media/size'+tn_size+'?nocache=' + Math.random();
            flipimg.src = '/work/' + uuid + '/media/size2?nocache=' + Math.random();
            $('#asset-replacement-pane span.error').text('Image was successfully replaced.');
        }
        else {
            $('#asset-replacement-pane span.error').text('Error processing upload: ' + res.message);
        }
    });
}

function update_image(img_ele, url){
    if (!url) {
        url = $(img_ele).attr('src');
    }
    var reload = new Image();
    reload.onload = function(){
        $(img_ele).attr('src', this.src);
    };
    reload.src = url + '?nocache=' + Math.random();
}

function save_document(){
    IMATA.document.save(false, function(response){
        if (response.valid == true) {
            $('#' + response.uuid).addClass('valid').parent().find('img.valid-img').show();
        }
        else {
            $('#' + response.uuid).removeClass('valid').parent().find('img.valid-img').hide();
        }
        //IMATA.document.scrollToTop();
        IMATA.document.enable();
    });
}

function contribute_document(){
    IMATA.document.contribute(function(response){
        if (response.complete) {
            $('#work-document').remove();
            $('#document-container div.column-header').hide();
            $('#edit-form-status-box').html(response.message);
            $('#' + response.uuid).parent().parent().remove();
            if (0 == $('#assets img').length) {
                $('#folder-wrapper').tabs('load', 0);
            }
            IMATA.assets.update_count();
        }
    });
}

function update_document(uuid){
    IMATA.document.contribute(function(response){
        if (response.complete) {
            if ($('table.tabular-results').length == 0) {
                $.get("/search?phrase=uuid%3A+" + uuid + "&amp;rows=1", function(data){
                    // Update the search display with the new data
                    var resp = $(data);
                    $('#' + uuid + ' .creator').html(resp.find('div.creator').html());
                    $('#' + uuid + ' .title').html(resp.find('div.title').html());
                    $('#' + uuid + ' .city').html(resp.find('div.city').html());
                    $('#' + uuid + ' .country').html(resp.find('div.country').html());
                    $('#' + uuid + ' .date').html(resp.find('div.date').html());
                    $('#' + uuid + ' .contributor').html(resp.find('div.contributor').html());
                });
            }
            else {
                $.get("/search?phrase=uuid%3A+" + uuid + "&amp;rows=1&amp;tabular=True", function(data){
                    // Update the search display with the new data
                    var resp = $(data);
                    $('#' + uuid + ' .creator').html(resp.find('.creator').html());
                    $('#' + uuid + ' .title').html(resp.find('.title').html());
                    $('#' + uuid + ' .city').html(resp.find('.city').html());
                    $('#' + uuid + ' .country').html(resp.find('.country').html());
                    $('#' + uuid + ' .date').html(resp.find('.date').html());
                    $('#' + uuid + ' .contributor').html(resp.find('.contributor').html());
                });

            }
            $('#edit-form-status-box').text('Your edits have been saved and will be updated in the SAHARA Collection within 15 minutes');
        }
        else {
          $('div.admin-document-edit-form').scrollTop(0);
        }
    });
}

function close_dialog(id) {
    $(id).jqmHide();
}

function select_destination_for_publishing(uuid) {
  $('#dialog').jqm({
	  closeClass: 'close',
		modal: true,
		overlay: 30
	});
  $('#dialog').load(IMATA_PATH + '/work/'+uuid+'/publication_destinations', function() {
	  $('#dialog').jqmShow();
	});
}

function determine_destination(uuid) {
    var collection_id = $('#publication_destination_form :input[name="collection"]').fieldValue();
    close_dialog('#dialog');
    IMATA.document.publish(uuid, collection_id[0]);
}

function toggle_selection(element){
    $(element).toggleClass('selected');
}

function disable_buttons(selector){
    $(selector + ' a').addClass('disabled');
    $(selector + ' span.button-text').addClass('disabled');
    $(selector + ' a').enable(false);
}

function enable_buttons(selector){
    $(selector + ' a').removeClass('disabled');
    $(selector + ' span.button-text').removeClass('disabled');
    $(selector + ' a').enable(true);
}

/*
 * Simple workflow skeleton
 */
function Workflow(steps){
    this.steps = steps.slice();
    var step = 0;
    this.begin = function(args){
        step = 0;
        this.steps[step].begin(args);
    };
    this.next = function(args){
        if (step < this.steps.length) {
            if (this.steps[step].end(args) == true) {
                step += 1;
                if (this.steps[step]) {
                  this.steps[step].begin(args);
                }
            }
        }
    };
    this.prev = function(args){
        if (step > 0) {
            if (this.steps[step].rewind(args) == true) {
                step -= 1;
                if (this.steps[step]) {
                  this.steps[step].begin(args);
                }                
            }
        }
    };
}


/*
 * Delete multiple workflow
 */
(function(){
    var works = new Array();
    var container;
    var sort_order;
    var workflow;

    function select_works(){
        this.begin = function(){
            $(container).load(IMATA_PATH + '/works/delete;list', highlight_selected);
        };
        this.rewind = function(){
            return false;
        };
        this.end = function(){
            works = new Array();
            $(container + ' div.selected').each(function(){
                works.push($(this).attr('id').split('_')[1]);
            });
            if (works.length == 0) {
                IMATA.dialogs.alert("Please select an asset");
                return false;
            }
            else {
                return true;
            }
        };
    };

    function confirm(){
        this.begin = function(){
            $.post(IMATA_PATH + '/works/delete;confirm', serialize_state(), function(resp){
                $(container).html(resp);
            });
        };
        this.rewind = function(){
            return true;
        };
        this.end = function(){
            return true;
        };
    };

    function result(){
        this.begin = function(){
            $.post(IMATA_PATH + '/works/delete', serialize_state(), function(resp){
                $(container).html(resp);
                // Reload the progressing tab so contributed works disappear
                IMATA.assets.load_tab();
                IMATA.assets.update_count();
            });
            works = [];
        };
        this.rewind = function(){
            return false;
        };
        this.end = function(){
            $(container).jqmHide();
            return true;
        };
    };

    workflow = new Workflow([new select_works(), new confirm(), new result()]);
    workflow.start = function(dialog){
        container = dialog;
        $(container).jqm({
            closeClass: 'close',
            modal: true,
            overlay: 30
        });
        IMATA.assets.remove_multiple.begin();
        $(container).jqmShow();
    };

    workflow.close = function(){
        works = [];
        $(container).jqmHide();
    };

    workflow.resort = function(order){
        $(container).load(IMATA_PATH + '/works/delete;list?sort=' + order, highlight_selected);
    };

    workflow.select_all = function(){
        $(container + ' div.img-wrapper').addClass('selected');
    };

    workflow.deselect_all = function(){
        $(container + ' div.img-wrapper').removeClass('selected');
    };

    function highlight_selected(){
        for (uuid in works) {
            $('#da_' + works[uuid]).addClass('selected');
        }
    }

    function serialize_state(){
        return {
            uuid: works
        };
    }

    IMATA.assets.remove_multiple = workflow;
})();

function expatiate(id, category, format, width, height){
    if (category == "vid" && format == "mov") {
        show_theater(id, width, height);
    }
    else {
        IMATA.dialogs.image_manipulation(id);
    }
}

function logout() {
  $.getJSON('/logout', function(data) {
	if(data.status == 'ok') {
	  // Call the ARTstor SDL logout method, but ignore the results
	  var el = document.createElement('img');
	  el.src = data.artstor_logout;
	  window.location = data.artstor_login;
	}
  });
}


/************************************************************
 * Search related functions
 ************************************************************/
function toggle_search_example(){
    $('#search_example').toggle();
    if ($('#search_example').is(':visible')) {
        $('#search_example_toggle').text('Hide example');
    }
    else {
        $('#search_example_toggle').text('Show example');
    }
}

function show_thumbnail(element, url, suppress){
    var img = new Image();
    var pos = $(element).position();
    var div_offset = $('#search-results div.table-container').scrollTop();
    var width = $(element).width();
    var height = $(element).height();
    $('#search_thumbnail').css({
        top: pos.top - div_offset + 97,
        left: pos.left + width
    });
    img.src = url;
    $('#search_thumbnail').show();
    $('#search_thumbnail').html(img);
    if ('True' == suppress) {
        $('#thumbnail_suppression_flag').css({
            top: pos.top - div_offset + 92,
            left: pos.left + width - 8
        });
        $('#thumbnail_suppression_flag').show();
    }
}

function hide_thumbnail(){
    $('#thumbnail_suppression_flag').hide();
    $('#search_thumbnail').hide();
}

function search_validate(){
    var value = $('#simple-search input[name="phrase"]').attr('value');
    if (value.length == 0) {
        $('#simple-search span.error-message').text('Please enter your search criteria');
        return false;
    }
    else {
        return true;
    }
}

function toggle_editorial_information() {
    var visible = $('div.editorial-section').is(':visible');
    if(visible) {
	$('div.editorial-notes').addClass('collapsed');
	$('div.editorial-section').hide();
    } else {
	$('div.editorial-section').show();
	$('div.editorial-notes').removeClass('collapsed');
    }
}

function search_clear_error(){
    $('#simple-search span.error-message').html('&nbsp;');
}

function rollback(uuid, history_id) {
    $('#admin-asset-edit-dialog div.admin-document-edit-form').load(
	    IMATA_PATH+"work/"+uuid+"/rollback/"+history_id+" div.replacable_form");
}

function preview(uuid) {
    $('#dialog').html(
      "<div id='artstor-preview-content'>"
	    + "<div class='dialog-header'>"
    	+ "<div class='close-button'>"
    	+ "<a href='#' class='button' onclick='closeDialog(); return false;'>"
    	+ "<span class='button-text'> </span>"
    	+ "</a>"
    	+ "CLOSE"
    	+ "</div>"
    	+ "<img src='images/dialog-icon.png'/>&nbsp;ARTSTOR PREVIEW"
    	+ "</div>"
    	+ "<div class='dialog-content'>"
    	+ "</div>"
    	+ "<div class='dialog-footer'>"
    	+ "</div>"
    + "</div>");
  
  $("#dialog div.dialog-content").load(IMATA_PATH+"work/"+uuid+"/preview", null, function() {
    $('#dialog').jqm({
      closeClass: 'close-button',
      modal: true,
      overlay: 30
    });
    $('#dialog').jqmShow();
  });
}

(function($){
  
  var errorMessages = [];
  
  function validateFormData() {
    
    var form = $('#work-document');
    
    function checkTextFieldValidity(selector, message) {
      var element = form.find(selector);
      if (!element.val()) {
        errorMessages.push(message);
      }
    }
    function checkCheckboxGroupValidity(selector, message) {
      var element = form.find(selector),
          isSelected = false;
      element.each(function(i, el) {
        if (el.checked) {
          isSelected = true; 
          return;
        }
      });
      if (!isSelected) {
        errorMessages.push(message);
      }
    }
    
    errorMessages.length = 0;

    checkTextFieldValidity('[name="title"]', 'Title/Name of Work is a required field.');
    checkTextFieldValidity('[name="view_type"]', 'View Type is a required field.');
    
    checkCheckboxGroupValidity('[name="classifications"]', 'Broad Classification is a required field.');
    
    //checkTextFieldValidity('[name="narrow_classification"]', 'Narrow Classification is a required field.');
    checkTextFieldValidity('[name="city_county"]', 'City/County is a required field.');
    checkTextFieldValidity('[name="country"]', 'Country is a required field.');
    checkTextFieldValidity('[name="display_date"]', 'Display date is a required field.');
    checkTextFieldValidity('[name="photographer"]', 'Photographer is a required field.');
    checkTextFieldValidity('[name="contributor"]', 'Contributor is a required field.');
    checkTextFieldValidity('[name="image_copyright"]', 'Copyright of Photograph/Image is a required field.');
    
    if ($('[name="image_ownership"]:checked').val() === 'False') {
      errorMessages.push('You may only share those images that you have created ' +
        '(or images that you have received express authorization from the creator to share.');
    }
    
    var isValid = (errorMessages.length === 0);
    
    return isValid;
  }
  
  $('#save-trigger').live('click', function() {
    var uuid = $('#work-id').val(),
        isRecordLocked = ($('#is-work-locked').val() === 'True');
    
    if (typeof uuid !== 'undefined') {
      
      var editorialNoteValue = $('#asset-edit-dialog [name="editorial_note_body"]').val(),
          editorialNodeParam = '&editorial_note_body=' + encodeURIComponent(editorialNoteValue);
          
      $.post('/work/' + uuid, $('#work-document').serialize() + editorialNodeParam, function(response) {
        if (!response) return;
        var evaledResponse = eval('(' + response + ')');
        if (evaledResponse && evaledResponse.message) {
	        if (evaledResponse.success) {
	          IMATA.document.clearChanges();
	        }
          var message = evaledResponse.message;
          if (!isRecordLocked) {
            message += ' (<a href="#" id="lock-trigger">Keep record locked, so only you can edit it.</a>)';
          }
          $('#document-messaging').html(message);
        }
      });
    }
    return false;
  });
  
  $('#lock-trigger').live('click', function(){
    var uuid = $('#work-id').val();
    if (typeof uuid !== 'undefined') {
      var recordRow = $('.uuid:contains("' + uuid + '")').closest('tr');
      recordRow.find('p.lock a').click();
      var parent = $(this).parent();
      $(this).remove();
      parent.html(parent.html().replace(/\s*\(\s*\)\s*/, ''));
      $('#is-work-locked').val('True');
    }
    return false;
  });
  
  $('.show-example').live('click', function(){
    var screenshotEl = $('.search-screenshot');
    screenshotEl.toggleClass('hidden');
    $(this).html(screenshotEl.hasClass('hidden') ? 'Show example' : 'Hide example');
    return false;
  });
  
  window.setTimeout(function(){
    if ($('.column-footer').length > 0) {
      $('#footer').css('z-index', -1);
    }
  }, 750);
  
})(jQuery);