
function add_autocompletion(element){
    var id = $(element).attr('id');
    var url = $(element).attr('data-url');
    $(element).after("<div id='completer-" + id + "' class='autocomplete-result yui-skin-sam' display='none'></div>");
    initAutocompleter(id, 'completer-' + id, url);
}

function ana_creator_lookup(element){
    var id = $(element).attr('id');
    var index = id.split('-').pop();
    var url = $(element).attr('data-url');
    $(element).after("<div id='completer-" + id + "' class='autocomplete-result yui-skin-sam' display='none'></div>");
    YAHOO.example.BasicRemote = function(){
        // Use an XHRDataSource
        var oDS = new YAHOO.util.XHRDataSource("ana/creator");
        oDS.responseSchema = {
            resultsList: 'ResultSet.Results',
            fields: ['name', 'preferred', 'subjectid', 'nationality']
        };
        
        // Enable caching
        oDS.maxCacheEntries = 10;
        
        // Instantiate the AutoComplete
        var oAC = new YAHOO.widget.AutoComplete(id, 'completer-' + id, oDS);
        
        oAC.formatResult = function(oResultData, sQuery, sResultMatch){
            return oResultData.preferred;
        };
        oAC.resultTypeList = false;
        oAC.autoHighlight = false;
        oAC.animVert = false;
        
        try {
            oAC.containerExpandEvent.subscribe(onContainerExpand);
            oAC.containerCollapseEvent.subscribe(onContainerCollapse);
        } 
        catch (e) {
            alert(e.message);
        }
        
        oAC.itemSelectEvent.subscribe(function(sType, aArgs){
            var myAC = aArgs[0];
            var elLI = aArgs[1];
            var oData = aArgs[2];
            jQuery(':input[name="creator-' + index + '_ana_id"]').attr('value', oData.subjectid);
            jQuery(':input[name="creator-' + index + '_name"]').attr('value', oData.name);
            jQuery(':input[name="creator-' + index + '_name"]').attr('readonly', 'readonly');
            jQuery('div#creator-' + index + '-name label').addClass('locked');
            jQuery('#creator-' + index + '-unlink').show();
            
            // Get rid of excess nationality entries
            var divs = $(':input[name="creator-' + index + '_nationality"]').length;
            for (var i = 1; i < divs; i++) {
                $('#creator-' + index + '_nationality-' + i).remove();
            }
            var nationalities = oData.nationality.split('|');
            if (nationalities.length > 0) {
                $('#creator-' + index + '_nationality-0 :input').attr('value', nationalities.shift());
                $('#creator-' + index + '_nationality-0 :input').attr('readonly', 'readonly');
                $('#creator-' + index + '_nationality-0 label').addClass('locked');
                $('#creator-' + index + '_nationality-0 div.field-controls').hide();
                for (i = 0; i < nationalities.length; i++) {
                    var nationality = nationalities[i].replace(/^\s+|\s+$/g, '');
                    if (nationality.length > 0) {
                        addCreatorNationality('creator-' + index, {
                            'readonly': true,
                            'value': nationality
                        });
                    }
                }
            }
        });
        return {
            oDS: oDS,
            oAC: oAC
        };
    }();
}

function updateDate(datename){
    var period = $(':input[name="widget-ys-' + datename + '-sel"] ' +
    'option[selected]').text();
    var datenum = parseInt($(':input[name="widget-ys-' + datename + '-abs"]').attr('value'));
    if (datenum) {
        if (period == 'CE(AD)') {
            $(':input[name="' + datename + '"]').attr('value', (0 + datenum).toString());
        }
        else {
            $(':input[name="' + datename + '"]').attr('value', (0 - datenum).toString());
        }
    }
    else {
        $(':input[name="' + datename + '"]').attr('value', '');
    }
}

function updateDateDisplay(datename, value){
    $(':input[name=widget-ys-' + datename + '-abs]').attr('value', Math.abs(value));
    if (value >= 0) {
        $(':input[name=widget-ys-' + datename + '-sel] option:contains(CE)').select(true);
        $(':input[name=widget-ys-' + datename + '-sel] option:contains(BCE)').select(false);
    }
    else {
        $(':input[name=widget-ys-' + datename + '-sel] option:contains(CE)').select(false);
        $(':input[name=widget-ys-' + datename + '-sel] option:contains(BCE)').select(true);
    }
}

function updateNarrowClassification(){
    var enable = false;
    $('div#classifications :input').each(function(){
        if ($(this).attr('checked') &&
        ($(this).attr('value') == 'Architecture and City Planning' ||
        $(this).attr('value') == 'Garden and Landscape')) {
            enable = true;
        }
    });
    if (enable) {
        $(':input[name="narrow_classification"]').enable(true);
        $('#narrow_classification label').addClass('required');
    }
    else {
        $(':input[name="narrow_classification"]').attr('selectedIndex', -1);
        $('#narrow_classification label').removeClass('required');
        $(':input[name="narrow_classification"]').enable(false);
    }
    
}

function enable_other_contexts(){
    var enable = $(':input[name="other_contexts_switch"]').attr('checked');
    $(':checked[name="other_contexts"]').removeAttr('checked');
    $(':input[name="other_contexts"]').enable(enable);
    $('#OCNONE').enable(!enable);
    if (!enable) {
        $('#OCNONE').attr('checked', true);
    }
    else {
        $('#CCANL').attr('checked', true);
    }
}

function traverseDomTree(element, func){
    $(element).children().each(function(){
        if ($(this).children().length) {
            traverseDomTree($(this), func);
        }
        func($(this));
    });
}

function stringStartsWith(source, start){
    if (!source) {
        return false;
    }
    return source.substring(0, start.length) == start;
}

function findLast(id){
    var last = id;
    var candidate = incrementFormElementId(last);
    while ($('#' + candidate).length) {
        last = candidate;
        candidate = incrementFormElementId(candidate);
    }
    return last;
}

function incrementFormElementId(id){
    var num = parseInt(formElementNumber(id));
    return id.substring(0, id.lastIndexOf('-')) + '-' + (num + 1);
}

function formElementName(id){
    return id.substring(id.lastIndexOf('_') + 1, id.length).split('-')[0];
}

function formElementNumber(id){
    return Number(id.substring(id.lastIndexOf('-') + 1, id.length));
}

function formElementPath(id){
    return id.substring(0, id.lastIndexOf("_"));
}

function nextElementId(id){
    return incrementFormElementId(findLast(id));
}

function isFirst(id){
    return Number(id.substring(id.lastIndexOf('-') + 1, id.length)) == 0;
}

function store_ana_result(target, data){
    var today = new Date();
    var base = target.parent().parent().attr('id');
    $(':input[name=' + base + '_name]').attr('value', data.name);
    $(':input[name=' + base + '_ana_id]').attr('value', data.subjectid);
    updateDateDisplay(base + '_earliest_date', parseInt(data.earliestDate));
    $(':input[name=' + base + '_earliest_date]').attr('value', data.earliestDate);
    if (data.latestDate <= today.getFullYear()) {
        updateDateDisplay(base + '_latest_date', parseInt(data.latestDate));
        $(':input[name=' + base + '_latest_date]').attr('value', data.latestDate);
    }
    else {
        $(':input[name=' + base + '_latest_date]').attr('value', '');
    }
    $(target).attr('readonly', 'readonly');
    $(target).parent().find('label').addClass('locked');
    
    // Get rid of excess nationality entries
    var divs = $(':input[name="' + base + '_nationality"]').length;
    for (var i = 1; i < divs; i++) {
        $('#' + base + '_nationality-' + i).remove();
    }
    var nationalities = data.nationality.split('|');
    if (nationalities.length > 0) {
        $('#' + base + '_nationality-0 :input').attr('value', nationalities.shift());
        $('#' + base + '_nationality-0 :input').attr('readonly', 'readonly');
        $('#' + base + '_nationality-0 label').addClass('locked');
        $('#' + base + '_nationality-0 div.field-controls').hide();
        for (i = 0; i < nationalities.length; i++) {
            var nationality = nationalities[i].replace(/^\s+|\s+$/g, '');
            if (nationality.length > 0) {
                addCreatorNationality(base, {
                    'readonly': true,
                    'value': nationality
                });
            }
        }
    }
    
    $('#' + base + '-unlink').show();
}


(function(){

    IMATA.document = {
        init: function(complete){
            IMATA.document.reflect_type();
            IMATA.document.reenumerate();
            if (!complete) {
                $('#document-container div.required').show();
                IMATA.document.rebind_change_events();
                $('#post-contribution-document-controls').hide();
                $('#pre-contribution-document-controls').show();
                IMATA.document.instrument();
            }
            else {
                $('#document-container div.required').hide();
                $('#work-document :input').enable(false);
                $('#work-document :text').attr('readonly', true);
                $('#work-document :text').enable(true);
                $('#work-document div.hint').hide();
                $('#work-document .document-control').remove();
                $('#work-document .field-controls').remove();
                $('#post-contribution-document-controls').show();
                $('#pre-contribution-document-controls').hide();
            }
        },
        
        instrument: function(){
            // Fix for checkbox click events getting called twice
            $('div.scrolling-checkbox-selector :input[type="checkbox"]').click(function(event){
                event.stopPropagation();
            });
            updateNarrowClassification();
            // Fix for Safari
            $('#work-document').submit(function(e){
                e.stopPropagation();
                e.preventDefault();
                return false;
            });
            /*
             * Loop thru the autocomplete fields, and turn on
             * autocompletion.
             */
            $('input.autocomplete').each(function(){
                add_autocompletion(this);
            });
            $('input.ana-lookup').each(function(){
                ana_creator_lookup(this);
            });
            IMATA.document.install_numeric_validator(":input[name='image_earliest_date']");
            IMATA.document.install_numeric_validator(":input[name='image_latest_date']");
            IMATA.document.install_numeric_validator(":input[name='widget-ys-earliest_date-abs']");
            IMATA.document.install_numeric_validator(":input[name='widget-ys-latest_date-abs']");
        },
        
        install_numeric_validator: function(selector) {
            var message = "<span class='invalid'>[invalid]</span>";
            $(selector).blur(function() {
                if ($(this).attr('value').length > 0) {
                    if (!$(this).attr('value').match(/^\d+$/)) {
                        if ($(this).parent().find('div.hint span').length == 0) {
                            $(this).parent().find('div.hint').append(message);
                        }
                    } else {
                        $(this).parent().find('div.hint span').empty();
                    }
                }
            });    
        },
        
        register_change: function(){
            $('#work-document').attr('changed', true);
        },
        
	clearChanges: function() {
            $('#work-document').attr('changed', false);
	},
        
        unlink_creator: function(selector){
            $('#' + selector + ' :input[name=' + selector + '_name]').attr('value', '');
            $('#' + selector + ' :input[name=' + selector + '_nationality]').attr('value', '');
            $('#' + selector + ' label.locked').removeClass('locked');
            $('#' + selector + ' :input').removeAttr('readonly');
            $('#' + selector + ' :input').enable();
            $('#' + selector + '-unlink').hide();
            $('#' + selector + ' a[href="javascript:addCreatorNationality(\'' + selector + '\')"]').parent().show();
        },
        
        reset: function(){
            $('#work-document').clearForm();
        },
        
        save: function(background, callback){
            var response;
            if (background == true) {
                $('#work-document').ajaxSubmit({
                    success: function(){
                        if (callback) {
                            callback();
                        }
                    }
                });
            }
            else {
                disable_buttons('#right-column div.column-footer');
                $('#work-document').ajaxSubmit({
                    success: function(data){
                        enable_buttons('#right-column div.column-footer');
                        $('#work-document').attr('changed', false);
                        response = eval('(' + data + ')');
                        $('#document-messaging').text(response.message);
                        IMATA.document.scrollToTop();
                        callback(response);
                    },
                    error: function(){
                        enable_buttons('#right-column div.column-footer');
                        IMATA.dialogs.alert('Error saving document, administrator notified');
                    }
                });
            }
        },
        
        contribute: function(callback){
            var response;
            var url = $('#work-document').attr('action');
            $('#work-document').ajaxSubmit({
                url: url + '/contribute',
                success: function(data){
                    response = eval('(' + data + ')');
                    $('#document-messaging').html(response.message);
                    callback(response);
                },
                error: function(msg){
                    IMATA.dialogs.alert('Error saving document, administrator notified');
                }
            });
        },
        
        publish: function(uuid, collection_id){
            var response;
            var url = $('#work-document').attr('action');
            $('#work-document').ajaxSubmit({
                url: url + '/publish?collection='+collection_id,
                success: function(data) {
                  response = eval('(' + data + ')');
	                if (response.complete == true) {
		                close_dialog('#asset-edit-dialog');
		                // remove entire row that corresponds to this record
		                $('.uuid:contains('+ uuid +')').closest('tr').fadeOut();
                    $.get(IMATA_PATH + '/publication_success.html', function(data) {
                      $('#dialog').html(data).jqmShow();
                    });
	                } 
	                else {
		                $('#document-messaging').html(response.message);
	                }
                },
                error: function(msg){
                  IMATA.dialogs.alert('Error publishing document, administrator notified');
                }
            });
        },
        
        disable: function(){
        
            $('#right-column div.column-footer a.button').addClass('disabled');
            $('#right-column div.column-footer a.button').click(function(){
                return false;
            });
        },
        
        
        enable: function(){
            $('#right-column div.column-footer a.button').removeClass('disabled');
            $('#right-column div.column-footer a.button').unbind('click');
        },
        
        
        reenumerate: function(){
            IMATA.document.enumerateElements('complex-title');
            IMATA.document.enumerateElements('title');
            IMATA.document.enumerateElements('period_dynasty');
            IMATA.document.enumerateElements('location_country');
            IMATA.document.enumerateElements('creator');
        },
        
        
        remove_field: function(name){
            $('#' + name).slideUp('reg', function(){
                $('#' + name).remove();
                IMATA.document.reenumerate();
                IMATA.document.rebind_change_events();
            });
        },
        
        
        add_creator: function(){
            var last = findLast('creator-0');
            var num = formElementNumber(last);
            $.get(IMATA_PATH + '/form/new_creator/' + (num + 1), function(data){
                $('#' + last).after(data);
                $('creator-' + (num + 1)).hide();
                $('creator-' + (num + 1)).fadeIn();
                IMATA.document.ensureVisible('#creator-' + (num + 1)+' input');
                IMATA.document.enumerateSection('creator');
                IMATA.document.rebind_change_events();
                ana_creator_lookup($('#creator-' + (num + 1) + ' input.ana-lookup'));
            });
        },
        
        
        add_period_dynasty: function(){
            var last = findLast('period_dynasty-0');
            var num = formElementNumber(last);
            $.get(IMATA_PATH + '/form/new_period_dynasty/' + (num + 1), function(data){
                $('#' + last).after(data);
                $('period_dynasty-' + (num + 1)).hide();
                $('period_dynasty-' + (num + 1)).fadeIn();
                IMATA.document.ensureVisible('#period_dynasty-' + (num + 1));
                IMATA.document.enumerateElements('period_dynasty');
                IMATA.document.rebind_change_events();
                add_autocompletion($('#period_dynasty-' + (num + 1) + ' input.autocomplete'));
            });
        },
        
        add_complex_title: function() {
          var last = findLast('complex-title-0');
          var num = formElementNumber(last);
          $.get(IMATA_PATH + '/form/new_complex_title/' + (num + 1), function(data){
              $('#' + last).after(data);
              $('#complex-title-' + (num + 1)).hide();
              $('#complex-title-' + (num + 1)).fadeIn();
              IMATA.document.enumerateElements('complex-title');
              IMATA.document.ensureVisible('#complex-title-' + (num + 1));
              add_autocompletion($('#complex-title-' + (num + 1) + ' input.autocomplete'));
              $('#complex-title-' + (num + 1)).focus();
          });
        },
        
        add_title: function(){
            var last = findLast('title-0');
            var num = formElementNumber(last);
            $.get(IMATA_PATH + '/form/new_title/' + (num + 1), function(data){
                $('#' + last).after(data);
                $('#title-' + (num + 1)).hide();
                $('#title-' + (num + 1)).fadeIn();
                IMATA.document.enumerateElements('title');
                IMATA.document.ensureVisible('#title-' + (num + 1));
                add_autocompletion($('#title-' + (num + 1) + ' input.autocomplete'));
                $('#title-' + (num + 1)).focus();
            });
        },
        
        
        add_creator_nationality: function(creator, params){
            var last = findLast(creator + '_nationality-0');
            var num = formElementNumber(last);
            var creator_index = creator.split('-')[1];
            $.get(IMATA_PATH + '/form/new_creator_nationality/' + creator_index + '/' + (num + 1), params, function(data){
                $('#' + last).after(data);
                $('#' + creator + '_nationality-' + (num + 1)).hide();
                $('#' + creator + '_nationality-' + (num + 1)).fadeIn();
                IMATA.document.enumerateElements(creator + '_nationality');
                IMATA.document.ensureVisible('#' + creator + '_nationality-' + (num + 1));
                IMATA.document.rebind_change_events();
            });
        },
        
        
        add_creator_extent: function(creator){
            var last = findLast(creator + '_extent-0');
            var num = formElementNumber(last);
            var creator_index = creator.split('-')[1];
            $.get(IMATA_PATH + '/form/new_creator_extent/' + creator_index + '/' + (num + 1), function(data){
                $('#' + last).after(data);
                $('#' + creator + '_extent-' + (num + 1)).hide();
                $('#' + creator + '_extent-' + (num + 1)).fadeIn();
                IMATA.document.enumerateElements(creator + '_extent');
                IMATA.document.ensureVisible('#' + creator + '_extent-' + (num + 1));
                IMATA.document.rebind_change_events();
            });
        },
        
        
        add_location_country: function(){
            var last = findLast('country-0');
            var num = formElementNumber(last);
            $.get(IMATA_PATH + '/form/new_location_country/' + (num + 1), function(data){
                $('#' + last).after(data);
                document.getElementById('country-' + (num + 1)).scrollIntoView();
                $('country-' + (num + 1)).hide();
                $('country-' + (num + 1)).fadeIn();
                IMATA.document.enumerateElements('country');
                IMATA.document.rebind_change_events();
            });
        },
        
        
        highlight_section: function(name){
            $('#document div.section div.' + name).addClass('active');
        },
        
        
        clear_section_highlights: function(){
            $('#document div.section div.document-section-heading').removeClass('active');
        },
        
        
        toggle_checkbox: function(checkbox){
            if ($(checkbox).attr('checked') == false) {
                $(checkbox).attr('checked', true);
            }
            else {
                $(checkbox).attr('checked', false);
            }
        },
        
        
        fix_checkbox_click_events: function(){
            $('div.scrolling-checkbox-selector :input[type="checkbox"]').unbind();
            $('div.scrolling-checkbox-selector :input[type="checkbox"]').click(function(event){
                event.stopPropagation();
            });
        },
        
        rebind_change_events: function(){
            // Track all changes so that document can be saved on various actions
            $('#work-document :input').unbind('change');
            $('#work-document :input').bind('change', IMATA.document.register_change);
        },
        
        
        clear: function(){
            $('#document').empty();
        },
        
        
        scrollToTop: function(){
            $('#document').scrollTop(0);
        },
        
        
        edit: function(id){
            $('#document-wait-message').text('Loading...');
            $('#document-wait').show();
            $.ajax({
                type: 'GET',
                url: IMATA_PATH + '/work/' + id + '/edit?' + new Date().getTime(),
                success: function(resp){
                    $('#document-wait').hide();
                    $('#document').html(resp);
                    IMATA.document.init(false);
                    $('#work-document', this).ajaxForm();
                },
                error: function(){
                    $('#document-wait').show();
                    $('#document-wait').hide();
                    alert('Error retrieving document, administrator has been notified');
                }
            });
        },
        
        
        show: function(id){
            $('#document-wait-message').text('Loading...');
            $('#document-wait').show();
            $.ajax({
                type: 'GET',
                url: IMATA_PATH + '/work/' + id + '/edit?' + new Date().getTime(),
                success: function(data){
                    $('#document').html(data);
                    $('#document-wait').hide();
                    IMATA.document.init(true);
                    IMATA.document.scrollToTop();
                },
                error: function(data){
                    $('#document-wait').hide();
                    $('#document-container').append('<br/><br/><div id="document-messaging"/>');
                    $('#document-messaging').html("Error retrieving document, administrator has been notified");
                }
            });
        },
        
        
        enumerateElements: function(name){
            var elements = $('div.numbered-form-field-' + name);
            var text;
            for (var loop = 1; loop < elements.length; loop++) {
                text = $(elements[loop]).find('label').text();
                if (text.indexOf('(') > -1) {
                    text = text.replace(/\(\d\)/, '(' + (loop + 1) + ')');
                }
                else {
                    text = text + ' (' + (loop + 1) + ')';
                }
                $(elements[loop]).find('label').text(text);
            }
        },
        
        
        enumerateSection: function(name){
            var elements = $('div.numbered-section-' + name);
            var text;
            for (var loop = 1; loop < elements.length; loop++) {
                text = $(elements[loop]).find('span.label').text();
                if (text.indexOf('(') > -1) {
                    text = text.replace(/\(\d\)/, '(' + (loop + 1) + ')');
                }
                else {
                    text = text + ' (' + (loop + 1) + ')';
                }
                $(elements[loop]).find('span.label').text(text);
            }
        },
        
        
        ensureVisible: function(id){
            var container = $('#document');
            var top = $(id).offset().top + $(id).height();
            var offset = top - container.height();
            if (offset > 0) {
                container.animate({
                    scrollTop: container.scrollTop() + offset
                }, 1000);
            }
        },

        
        changed: function(){
          $('#work-document :input').blur();
          $('#work-document :text').blur();
          return ($('#work-document').attr('changed') == "true");
        },
        
        
        suppress: function(uuid, action){
            disable_buttons('#admin-asset-information-pane div.dialog-footer');
            $.ajax({
                type: 'POST',
                url: IMATA_PATH + '/work/' + uuid + '/' + action,
                success: function(msg){
                  
                  var uuid = $('#work-id').val();
                  if (uuid) {
                    var suppressIcon = $('.uuid:contains("'+uuid+'")').closest('tr').find('.suppress-icon');
                  }
                  
                  if (action == 'suppress') {
                    $('#' + uuid + ' div.status').addClass('suppress');
                    $('#admin-asset-information-pane div.status').addClass('suppress');
                    $('#toggle_suppression span').text('UNSUPPRESS');
                    suppressIcon.show();
                  }
                  else {
                    $('#' + uuid + ' div.status').removeClass('suppress');
                    $('#admin-asset-information-pane div.status').removeClass('suppress');
                    $('#toggle_suppression span').text('SUPPRESS');
                    suppressIcon.hide();
                  }
                  enable_buttons('#admin-asset-information-pane div.dialog-footer');
                  IMATA.dialogs.alert('Asset has been ' + action + 'ed.', 'This change will be reflected in the SAHARA' +
                  ' collection within 30 minutes');
                },
                error: function(){
                    IMATA.dialogs.alert('Suppression request failed, administator notified');
                    enable_buttons('#admin-asset-information-pane div.dialog-footer');
                }
            });
        },
        
        
        set_type: function(name){
            var id = IMATA.assets.get_selected();
            $.cookie('view', name, {
                expires: 365,
                path: '/'
            });
            // Change highlighting
            IMATA.document.reflect_type();
            // Update document if one is present
            if (id) {
                if (IMATA.document.changed()) {
                    $('#document-messaging').hide(); // Don't show the save message
                    IMATA.document.save(true, function(){
                        IMATA.document.edit(id);
                    });
                }
                else {
                    IMATA.document.edit(id);
                }
            }
        },
        
        reflect_type: function(){
            var view_type = $.cookie('view');
            $('#ffields-brief').parent().removeClass('current-setting');
            $('#ffields-full').parent().removeClass('current-setting');
            if (view_type == 'brief') {
                $('#ffields-brief').parent().addClass('current-setting');
            }
            else {
                $('#ffields-full').parent().addClass('current-setting');
            }
        }
        
    };
})();



(function(){
    var source_id;
    var fields_list = new Array();
    var dest_list = new Array();
    var container;
    var sort_order;
    var workflow;
    var basepath;
    
    function select_fields(){
        this.begin = function(){
            $(container).load(basepath + source_id + '/copy', function(){
                for (var field in fields_list) {
                    $(container + ' :input[name="' + fields_list[field] + '"]').attr('checked', true);
                }
            });
        };
        this.rewind = function(){
            return false;
        };
        this.end = function(){
            fields_list = new Array();
            $(container + ' :input[checked]').each(function(){
                fields_list.push($(this).attr('name'));
            });
            if (fields_list.length > 0) {
                return true;
            }
            else {
                IMATA.dialogs.alert('Please select at least one section to copy');
                return false;
            }
        };
    };
    
    
    function select_works(){
        this.begin = function(){
            $(container).load(basepath + source_id + '/copy;works', highlight_selected);
        };
        this.rewind = function(){
            return true;
        };
        this.end = function(){
            dest_list = new Array();
            $(container + ' div.selected').each(function(){
                dest_list.push($(this).attr('id').split('_')[1]);
            });
            if (dest_list.length > 0) {
                return true;
            }
            else {
                IMATA.dialogs.alert('Please select at least one asset');
                return false;
            }
        };
    };
    
    
    function confirm(){
        this.begin = function(){
            $.post(basepath + source_id + '/copy;confirm', serialize_state(), function(resp){
                $(container).html(resp);
            });
        };
        this.rewind = function(){
            return true;
        };
        this.end = function(){
            return true;
        };
    };
    
    function result(){
        this.begin = function(){
            $.post(basepath + source_id + '/copy', serialize_state(), function(resp){
                $(container).html(resp);
            });
            fields_list = [];
            dest_list = [];
        };
        this.rewind = function(){
            return false;
        };
        this.end = function(){
            $(container).jqmHide();
            IMATA.assets.load_tab();
            return true;
        };
    };
    
    
    workflow = new Workflow([new select_fields(), new select_works(), new confirm(), new result()]);
    workflow.start = function(dialog, uuid, editorial){
        source_id = uuid;
        container = dialog;
        if(editorial == true) {
	    basepath = IMATA_PATH + '/editorial/work/';
	} else {
	    basepath = IMATA_PATH + '/work/';
	}
        $(container).jqm({
            closeClass: 'close',
            modal: true,
            overlay: 30
        });
        IMATA.document.multicopy.begin();
        $(container).jqmShow();
    };
    
    workflow.close = function(){
        fields_list = [];
        dest_list = [];
        $(container).jqmHide();
    };
    
    workflow.resort = function(order){
        $(container).load(basepath + source_id + '/copy;works?sort=' + order, highlight_selected);
    };
    
    workflow.select_all_fields = function(){
        $(container + ' :input').attr('checked', true);
    };
    
    workflow.deselect_all_fields = function(){
        $(container + ' :input').removeAttr('checked');
    };
    
    workflow.select_all_images = function(){
        $(container + ' div.img-wrapper').addClass('selected');
    };
    
    workflow.deselect_all_images = function(){
        $(container + ' div.img-wrapper').removeClass('selected');
    };
    
    function highlight_selected(){
        for (uuid in dest_list) {
            $('#cm_' + dest_list[uuid]).addClass('selected');
        }
    }
    
    function serialize_state(){
        return {
            fields: fields_list,
            works: dest_list
        };
    }
    
    IMATA.document.multicopy = workflow;
})();


/*
 * Multicontribute workflow
 */
(function(){
    var works = new Array();
    var container;
    var sort_order;
    var workflow;
    
    function select_works(){
        this.begin = function(){
            $(container).load(IMATA_PATH + '/works/contribute;list', highlight_selected);
        };
        this.rewind = function(){
            return false;
        };
        this.end = function(){
            works = new Array();
            $(container + ' div.selected').each(function(){
                works.push($(this).attr('id').split('_')[1]);
            });
            if (works.length > 0) {
                return true;
            }
            else {
                alert("Please select an asset");
                return false;
            }
        };
    };
    
    function confirm(){
        this.begin = function(){
            $.post(IMATA_PATH + '/works/contribute;confirm', serialize_state(), function(resp){
                $(container).html(resp);
            });
        };
        this.rewind = function(){
            return true;
        };
        this.end = function(){
            return true;
        };
    };
    
    function result(){
        this.begin = function(){
            $.post(IMATA_PATH + '/works/contribute', serialize_state(), function(resp){
                $(container).html(resp);
                // Reload the progressing tab so contributed works disappear
                //$('#folder-wrapper > ul').tabs('load', 0);
                $('#folder-wrapper').tabs('load', 0);
                IMATA.assets.update_count();
            });
            works = [];
        };
        this.rewind = function(){
            return false;
        };
        this.end = function(){
            $(container).jqmHide();
            return true;
        };
    };
    
    workflow = new Workflow([new select_works(), new confirm(), new result()]);
    workflow.start = function(dialog){
        var valid_imgs = $('#progressing .valid');
        if (valid_imgs.length > 0) {
            container = dialog;
            $(container).jqm({
                closeClass: 'close',
                modal: true,
                overlay: 30
            });
            IMATA.document.multicontribute.begin();
            $(container).jqmShow();
        }
        else {
            IMATA.dialogs.alert('Your images do not meet the SAHARA Editorial Guidelines', 'Please complete all required fields to contribute an image');
        }
    };
    
    workflow.close = function(){
        works = [];
        $(container).jqmHide();
    };
    
    workflow.resort = function(order){
        $(container).load(IMATA_PATH + '/works/contribute;list?sort=' + order, highlight_selected);
    };
    
    workflow.select_all_works = function(){
        $(container + ' div.img-wrapper').addClass('selected');
    };
    
    workflow.deselect_all_works = function(){
        $(container + ' div.img-wrapper').removeClass('selected');
    };
    
    function highlight_selected(){
        for (uuid in works) {
            $('#mc_' + works[uuid]).addClass('selected');
        }
    }
    
    function serialize_state(){
        return {
            uuid: works
        };
    }
    
    IMATA.document.multicontribute = workflow;
})();


function toggle_suppression(uuid){
    var action = $('#admin-asset-information-pane div.status').hasClass('suppress') ? 'unsuppress' : 'suppress';
    if (action == 'suppress') {
        IMATA.dialogs.confirm('Are you sure that you want to suppress this ' +
        'image?<br><font size=\'-2\'>Suppressed images ' +
        'may not be accessed or viewed by all users of ' +
        'SAHARA</font>', function(){
            IMATA.document.suppress(uuid, 'suppress');
        });
    }
    else {
        IMATA.dialogs.confirm('Are you sure that you want to unsuppress this ' +
        'image?<br><font size=\'-2\'>Unsuppressed images ' +
        'may be accessed or viewed by all users of ' +
        'SAHARA</font>', function(){
            IMATA.document.suppress(uuid, 'unsuppress');
        });
    }
}
