YAHOO.example.BasicRemote = (function() {
    
  // Use an XHRDataSource
  var oDS = new YAHOO.util.XHRDataSource("ana/creator");
  oDS.responseSchema = {
    resultsList: 'ResultSet.Results',
    fields: ['name', 'preferred', 'subjectid', 'nationality']
  };
  
  // Enable caching
  oDS.maxCacheEntries = 10;
  
  // Instantiate the AutoComplete
  var oAC = new YAHOO.widget.AutoComplete(
    "input-creator-${index}",
    "completer-creator-${index}", 
    oDS
  );
  
  oAC.formatResult = function(oResultData, sQuery, sResultMatch) {
    return oResultData.preferred;
  }
  oAC.resultTypeList = false;
  oAC.autoHighlight = false;
  oAC.animVert = false;
  oAC.useIFrame = true;
  
  oAC.itemSelectEvent.subscribe(function(sType, aArgs) {
    var myAC = aArgs[0];
    var elLI = aArgs[1];
    var oData = aArgs[2];
    jQuery(':input[name="creator-${index}_ana_id"]').attr('value', oData.subjectid);
    jQuery(':input[name="creator-${index}_name"]').attr('value', oData.name);
    jQuery(':input[name="creator-${index}_name"]').attr('readonly', 'readonly');
    jQuery('div#creator-${index}-name label').addClass('locked');
  
    // Get rid of excess nationality entries
    var divs = $(':input[name="creator-${index}_nationality"]').length;
    for (var i = 1; i < divs; i++) {
      $('#creator-${index}_nationality-'+i).remove();
    }
    var nationalities = oData.nationality.split('|');
    if (nationalities.length > 0) {
      $('#creator-${index}_nationality-0 :input').attr('value', nationalities.shift());
      $('#creator-${index}_nationality-0 :input').attr('readonly', 'readonly');
      $('#creator-${index}_nationality-0 label').addClass('locked');
      $('#creator-${index}_nationality-0 div.field-controls').hide();
      for (i = 0; i < nationalities.length; i++) {
        var nationality = nationalities[i].replace(/^\s+|\s+$/g, '');
        if (nationality.length > 0) {
  	      addCreatorNationality('creator-${index}', {'readonly':true, 'value':nationality});
        }
      }
    }
    $('#creator-${index}-unlink').show();
  });

  return {
    oDS: oDS,
    oAC: oAC
  }
})();