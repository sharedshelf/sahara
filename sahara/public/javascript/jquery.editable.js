(function($) {
	
	// define the plugin
	$.fn.editable = function(url) {
		
		return this.each(function() {
			var target = this;
			var form = build_form_for(target);
			$(target).before(form);
			$(form).submit(function(event) {
				var new_name = $(form).find(':text').attr('value');
				var old_name = $(form).find(':hidden').attr('value');
				var form_data = form.serialize();
				event.preventDefault();
				$(target).text("Saving...");
			 	$(form).hide();
			 	$(target).show();
				$.ajax({ type: "POST",
						url: url, 
					 	data: form_data,
					 	success: function() {
					 		$(target).text(new_name);
					 	},
					 	error: function(request, status, error) {
							$(target).text(old_name);
						}
				});
				return false;
			});

			$(target).dblclick(function() {
			 	$(target).hide();
			 	$(form).show();
			 	$(form).find('input._edit-in-place-field').focus();
			});
			
			$(form).find('input._edit-in-place-field').each(function() {
				$(target).blur(function() { 
					$(form).hide(); 
					$(target).show(); 
				});
				$(this).keydown(function(e) {
					if(27 == e.which) {
				 		$(this).blur();
				 	} 
					if(13 == e.which) { $(this).submit(); }
				});
			});
		});
		
	};
	
	function build_form_for(element) {
		var form, hidden, textfield;
		var value = $.trim($(element).text());
		
		form = $('<form/>');
		$(form).addClass('_edit-in-place-form');
		$(form).css('display', 'none');

		hidden = $('<input type="hidden"/>');
		$(hidden).attr('name', 'original');
		$(hidden).attr('value', value);

		textfield = $('<input type="textfield"/>');
		$(textfield).attr('name', 'new');
		$(textfield).attr('value', value);
		$(textfield).attr('autocomplete','off');        
		$(textfield).addClass('_edit-in-place-field');

		$(form).append(hidden);
		$(form).append(textfield);
		
		$(textfield).blur(function() {
			$(form).hide();
			$(element).show();
		});
		return form;
	};
	
})(jQuery);