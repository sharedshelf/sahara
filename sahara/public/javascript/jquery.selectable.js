(function($) {

	// define the plugin
	$.fn.selectable = function(options) {
		var defaults = {
			before: undefined,
			after : undefined,
			multi : false
		};

		var opts = $.extend(defaults, options);
		var target = this;

		return this.each(function() {
			// $(this).unbind('mouseenter.selectable');
			// $(this).unbind('mouseleave.selectable');
			// $(this).bind('mouseenter.selectable', function(e) {
			// 	$(document).add('body').bind('keydown', keyDownListener);
			// });
			// $(this).bind('mouseleave.selectable', function(e) {
			// 	$(document).add('body').unbind('keydown', keyDownListener);
			// });
			$(this).find('div.img-wrapper').unbind('click', selectableClickListener).
				bind('click', selectableClickListener);
		});

		// function keyDownListener(e) {
		// 	if(224 == e.which && e.metaKey || e.ctrlKey) {
		// 		$(target).find('img').removeClass('selected');
		// 		$(target).find('img').addClass('selected');
		// 		e.preventDefault();
		// 		e.stopPropagation();
		// 	}
		// }

		function selectableClickListener(e) {
			var parent, target;
			// e.shiftKey
			// if(e.metaKey || e.ctrlKey) {
			// 	$(this).removeClass('selected');
			// 	$(this).addClass('selected');
			// } else if(e.shiftKey) {
			// 	$(this).removeClass('selected');
			// 	$(this).addClass('selected');
			// 	parent = $(this).parent();
			// 	if($(parent).prev('div img.selected').length) {
			// 		target = $(parent).prev('div');
			// 		while(!$(target).find('img').hasClass('selected')) {
			// 			target.find('img').addClass('selected');
			// 			target = $(target).prev('div');
			// 		}
			// 	} else if($(parent).next('div img.selected').length) {
			// 		target = $(parent).next('div');
			// 		while(!$(target).find('img').hasClass('selected')) {
			// 			target.find('img').addClass('selected');
			// 			target = $(target).next('div');
			// 		}
			// 	}
			// } else {

			// before hook
			if(opts.before) { opts.before(); }
			if(opts.multi) {
				if($(this).hasClass('selected')) {
					$(this).removeClass('selected');
				} else {
					$(this).addClass('selected');
				}
			} else {
				$(target).find('div.img-wrapper').removeClass('selected');
				$(this).addClass('selected');
			}
			// }
			// after hook
			if(opts.after) { opts.after(); }
		}

	};

})(jQuery);