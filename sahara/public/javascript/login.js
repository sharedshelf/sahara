(function($){
  $(function(){
    $('#fail').hide();
    $('#content').show();
    $('#first').focus();
    $('#login-btn').click(function(){
      $('#login-form').submit();
      return false;
    });
  });
})(jQuery);