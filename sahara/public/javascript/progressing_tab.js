function show_theater(id, width, height) {
  var size =  ', width=' + width + ', height=' + height;
  var params = 'toolbar=0, location=0, directories=0, status=0, menubar=0, scrollbars=0';
  var cinema = window.open('/work/' + id + '/media/cinema', 'View', params + size , true);
  cinema.moveTo(100,100);
}
function progressing_sort_change(new_sort) {
  $.cookie('assetsort', new_sort, { expires: 365, path: '/'});
  IMATA.assets.load_tab();
}
function expatiate(id,category,format,width,height) {
  if (category == "vid" && format == "mov") {
    show_theater(id, width, height);
  } else {
    IMATA.dialogs.image_manipulation(id);
  }
}