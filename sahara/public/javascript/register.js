function showText(content, title) {
    $('#text-dialog').jqm({modal:true, overlay:10}).find('#title').html(title);
     $.ajax({
     type: 'GET',
     url: content,
     success: function(data)
       {
       $('#text-dialog').find('div.text-content').html(data);
       scrollDocumentToTop();
       },
     error: function(data)
       {
         $('#text-dialog').html("Error retrieving document");
       }
     });
     $('#text-dialog').jqmAddClose($('#close')).jqmShow();
}
function closeDialog() {
  $('#text-dialog').jqmHide();
}
function clear_all() {
    display_error("&nbsp;");
    with (document.registration) {
        memberid.value = "";
        email.value = "";
        remail.value = "";
        apassword.value = "";
        repassword.value = "";
    }
}
function display_error(msg) {
    $('#error').html(msg);
}
function minSize(field, sz, msg){
        if(field.value.length < sz){
                display_error(msg);
                field.focus();  
                return false;
        }
        return true;
}
function valid_email(field, msg){
    apos=field.value.indexOf("@");
    dotpos=field.value.lastIndexOf(".");
    if (apos<1||dotpos-apos<2) {
        display_error(msg);
        field.focus();  
        return false;
      }
    return true;
}  
function valid_password(field,msg) {
    return true;
}
function valid_memberid(field,msg) {
    return true;
}
function matching (fieldA, fieldB, msg) {
    if (fieldA.value.toLowerCase() == fieldB.value.toLowerCase()) {
        return true;
    }
    display_error(msg);
    fieldA.focus();
    return false;
}
function submit_if() {
    display_error("&nbsp;");
    with (document.registration) {
        if (minSize(memberid, 1, "Membership ID is invalid.")) {
        if (minSize(email, 5, "Invalid email address.")) {
        if (minSize(apassword, 7, "Passwords need to be at least 7 characters long.")) {
        if (valid_memberid(memberid, "Membership ID is invalid.")) {
        if (valid_email(email,"Invalid email address.")) {
        if (valid_password(apassword,"One of the characters of your password is not supported.")) {
        if (matching(email, remail, "Email addresses do not match.")) {
        if (matching(apassword, repassword, "Passwords do not match.")) {
            document.registration.submit()
        } } } } } } } }
    }
}