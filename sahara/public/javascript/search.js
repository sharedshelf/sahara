(function($){
  $(function(){
    
    // init error message when no value is provided
    $('#search-form').submit(function(){
      var isValueBlank = /^\s*$/.test(this.elements['q'].value);
      if (isValueBlank) {
        $('.errors').removeClass('hidden');
        return false;
      }
      else {
        this.submit();
      }
    });
    
    // focus search field
    $('#search-form [name="q"]').focus();
    
    // init screenshot toggling
    $('.show-example').click(function(){
      var screenshotEl = $('.search-screenshot');
      screenshotEl.toggle();
      this.innerHTML = screenshotEl.is(':visible') ? 'Hide example' : 'Show example';
      return false;
    });
  });
})(jQuery);