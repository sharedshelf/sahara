// (function(){
//   window.documentChanged = function() {
//     $('#work-document').attr('changed', true);
//   };

//   window.unlinkCreator = function(selector) {
// 	console.log(selector);
//     $('#'+selector+' :input[name='+selector+'_name]').attr('value', '');
//     $('#'+selector+' :input[name='+selector+'_nationality]').attr('value', '');
//     $('#'+selector+' label.locked').removeClass('locked');
//     $('#'+selector+' :input').removeAttr('readonly');
//     $('#'+selector+' :input').enable();
//     $('#'+selector+'-unlink').hide();
//     $('#'+selector+' a[href="javascript:addCreatorNationality(\''+selector+'\')"]').parent().show();
//   };

//   window.clearDocumentForm = function() {
//     $('#work-document').clearForm();
//   };

//   window.saveDocumentForm = function(callback) {
//     disableDocumentActions();
//     $('#work-document').ajaxSubmit({
//       target:  '#document-messaging',
//       success: callback
//     });
//   };

//   window.contributeDocument = function() {
//     var url = $('#work-document').attr('action');
//     $('#work-document').ajaxSubmit({url: url+';contribute',
//   							    target: '#document-messaging'});
//   };

//   window.disableDocumentActions = function() {
//     $('#right-column div.column-footer a.button').addClass('disabled');
//     $('#right-column div.column-footer a.button').click(function() { return false; });
//   };

//   window.enableDocumentActions = function() {
//     $('#right-column div.column-footer a.button').removeClass('disabled');
//     $('#right-column div.column-footer a.button').unbind('click');
//   };

//   window.resetEnumeration = function() {
//     IMATA.document.enumerateElements('title');
//     IMATA.document.enumerateElements('period_dynasty');
//     IMATA.document.enumerateElements('location_country');
//     IMATA.document.enumerateElements('creator');
//   };

//   window.removeField = function(name) {
//     $('#'+name).slideUp('reg', function() {
//       $('#'+name).remove();
//       resetEnumeration();
//     });
//   };

//   window.addCreator = function() {
//     var last = findLast('creator-0');
//     var num = formElementNumber(last);
//     $.get(IMATA_PATH + '/form/new_creator/'+(num+1), function(data) {
//       $('#'+last).after(data);
//       $('creator-'+(num+1)).hide();
//       $('creator-'+(num+1)).fadeIn();
//       IMATA.document.ensureVisible('#creator-'+(num+1));
//       IMATA.document.enumerateSection('creator');
//     });
//   };

//   window.addPeriodDynasty = function() {
//     var last = findLast('period_dynasty-0');
//     var num = formElementNumber(last);
//     $.get(IMATA_PATH + '/form/new_period_dynasty/'+(num+1), function(data) {
//       $('#'+last).after(data);
//       $('period_dynasty-'+(num+1)).hide();
//       $('period_dynasty-'+(num+1)).fadeIn();
//       IMATA.document.ensureVisible('#period_dynasty-'+(num+1));
//       IMATA.document.enumerateElements('period_dynasty');
//     });
//   };

//   window.addTitle = function() {
//     var last = findLast('title-0');
//     var num = formElementNumber(last);
//     $.get(IMATA_PATH + '/form/new_title/'+(num+1), function(data) {
//       $('#'+last).after(data);
//       $('#title-'+(num+1)).hide();
//       $('#title-'+(num+1)).fadeIn();
//       IMATA.document.enumerateElements('title');
//       IMATA.document.ensureVisible('#title-'+(num+1));
//     });
//   };

//   window.addCreatorNationality = function(creator, params) {
//     var last = findLast(creator+'_nationality-0');
//     var num = formElementNumber(last);
//     var creator_index = creator.split('-')[1];
//     // console.log('creator_index = '+creator_index);
//     $.get(IMATA_PATH + '/form/new_creator_nationality/'+creator_index+'/'+(num+1), params, function(data) {
//       $('#'+last).after(data);
//       $('#'+creator+'_nationality-'+(num+1)).hide();
//       $('#'+creator+'_nationality-'+(num+1)).fadeIn();
//       IMATA.document.enumerateElements(creator+'_nationality');
//       IMATA.document.ensureVisible('#'+creator+'_nationality-'+(num+1));
//     });
//   };

//   window.addCreatorExtent = function(creator) {
//     var last = findLast(creator+'_extent-0');
//     var num = formElementNumber(last);
//     var creator_index = creator.split('-')[1];
//     $.get(IMATA_PATH + '/form/new_creator_extent/'+creator_index+'/'+(num+1), function(data) {
//       $('#'+last).after(data);
//       $('#'+creator+'_extent-'+(num+1)).hide();
//       $('#'+creator+'_extent-'+(num+1)).fadeIn();
//       IMATA.document.enumerateElements(creator+'_extent');
//       IMATA.document.ensureVisible('#'+creator+'_extent-'+(num+1));
//     });
//   };

//   window.addLocationCountry = function() {
//     var last = findLast('country-0');
//     var num = formElementNumber(last);
//     $.get(IMATA_PATH + '/form/new_location_country/'+(num+1), function(data) {
//       $('#'+last).after(data);
//       document.getElementById('country-'+(num+1)).scrollIntoView();
//       $('country-'+(num+1)).hide();
//       $('country-'+(num+1)).fadeIn();
//       IMATA.document.enumerateElements('country');
//     });
//   };

//   window.highlightSection = function(name) {
//     $('#document div.section div.' + name).addClass('active');
//   };

//   window.clearHighlighting = function() {
//     $('#document div.section div.document-section-heading').removeClass('active');
//   };

//   window.toggleCheckbox = function(checkbox) {
//     if ($(checkbox).attr('checked') == false) {
//       $(checkbox).attr('checked', true);
//     } else {
//       $(checkbox).attr('checked', false);
//     }
//   };
// })();