From: %s
To: %s
Subject: Thank you for registering to the SAH Architecture Resources Network


<h3>Thank you for registering to the SAH Architecture Resources Network</h3>

> Contribute your images to SAHARA at <a href="sah.artstor.org/contribute">http://sah.artstor.org/contribute</a>.
> Explore SAHARA contributions from SAH members at <a href="http://sah.artstor.org">sah.artstor.org</a>.

Society of Architectural Historians. All rights reserved.
SAH | 1365 North Astor Street | Chicago, Illinois 60610-2144

You received this email because you are a SAH member. Please note: This email is automated, please do not respond to this message.

<a href="http://sah.artstor.org/unsubscribe.html">Unsubscribe</a> | <a href="http://sah.artstor.org/privacy.html">Privacy Policy</a>

