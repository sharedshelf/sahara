<%inherit file="base.mako"/>

<div class="administer-wrapper">
  
  <h2>Administer</h2>
  
  <h3>Statistics</h3>
  <div class="section">
    <p class="uppercased">Browse Sahara user statistics</p>
    <a href="http://stats.artstor.org/stats/sahara.asp" title="" class="button">
      <span>Go</span>
    </a>
  </div>
  
  <h3>Users</h3>
  <div class="section">
    <p class="uppercased">Manage Sahara users</p>
    <a href="http://sahara.artstor.org/saharatools/" title="" class="button manage-users">
      <span>Go</span>
    </a>
  </div>
  
  <h3>Controlled Lists</h3>
  <div class="section">
    <p>Select one of the controlled lists below</p>
    <p class="note">
      Note: Broad Classification and Country cannot be edited. Changes to these lists must be coordinated through ARTstor.
    </p>
    <form style="clear:both; padding-top: 2em" id="controlled-lists-form">
      <ul>
        <li>
          <input type="radio" name="controlled-lists" value="" id="view-type" checked="checked">
          <label for="view-type">View type</label>
          <div style="display:none">
            <input class="index-url" value="${h.get_route_path('view_types')}">
            <input class="create-url" value="${h.get_route_path('view_type_create')}">
            <input class="update-url" value="${h.get_route_path('view_type_update')}">
            <input class="delete-url" value="${h.get_route_path('view_type_delete')}">
          </div>
        </li>
        <li>
          <input type="radio" name="controlled-lists" value="" id="narrow-classification">
          <label for="narrow-classification">Narrow classification</label>
          <div style="display:none">
            <input class="index-url" value="${h.get_route_path('narrow_classifications')}">
            <input class="create-url" value="${h.get_route_path('narrow_classification_create')}">
            <input class="update-url" value="${h.get_route_path('narrow_classification_update')}">
            <input class="delete-url" value="${h.get_route_path('narrow_classification_delete')}">
          </div>
        </li>
        <li>
          <input type="radio" name="controlled-lists" value="" id="role">
          <label for="role">Role</label>
          <div style="display:none">
            <input class="index-url" value="${h.get_route_path('roles')}">
            <input class="create-url" value="${h.get_route_path('role_create')}">
            <input class="update-url" value="${h.get_route_path('role_update')}">
            <input class="delete-url" value="${h.get_route_path('role_delete')}">
          </div>
        </li>
        <li>
          <input type="radio" name="controlled-lists" value="" id="attribution">
          <label for="attribution">Attribution</label>
          <div style="display:none">
            <input class="index-url" value="${h.get_route_path('attributions')}">
            <input class="create-url" value="${h.get_route_path('attribution_create')}">
            <input class="update-url" value="${h.get_route_path('attribution_update')}">
            <input class="delete-url" value="${h.get_route_path('attribution_delete')}">
          </div>
        </li>
        <li>
          <span class="submit-wrapper">
            <input type="submit" value="Edit" title="Edit">
          </span>
        </li>
      </ul>
    </form>
  </div>
</div>

<div style="display:none;" id="controlled-lists-dialog" class="dialog">
  ${controlled_lists_dialog()}
</div>
<div style="display:none;" id="controlled-lists-confirm-dialog" class="dialog">
  ${controlled_lists_confirm_dialog()}
</div>

<script src="/javascript/admin.js" type="text/javascript"></script>

<%def name="css()"></%def>
<%def name="javascript()"></%def>
<%def name="tools_menu()"></%def>
<%def name="navigation()">
  <%include file="navigation.mako" />
</%def>
<%def name="controlled_lists_dialog()">
  <%include file="controlled_lists_dialog.mako" />
</%def>
<%def name="controlled_lists_confirm_dialog()">
  <%include file="controlled_lists_confirm_dialog.mako" />
</%def>
