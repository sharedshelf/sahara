<%page args="notify"/>

<%
   sort_keys = ['date-desc', 'capture-desc', 'filename-asc']
   sort_desc = ['Date added to image panel', 'Date of capture', 'Filename']
%>

<div class='sort-controls'>
  <span>Sort by:</span><form>
	<select onchange="${notify}($(this).find('option[selected]').attr('value'))">
	  %for index, order in enumerate(sort_keys):
	  %if c.sort == order:
	  <option value="${order}" selected="true">${sort_desc[index]}</option>
	  %else:
	  <option value="${order}">${sort_desc[index]}</option>
	  %endif
	  %endfor
	</select>
  </form>
</div>
