<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us">
  <head>
    <title>SAHARA</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    
    ${next.css()}
    
    <link rel="stylesheet" href="stylesheets/dialog.css" type="text/css">
    <link rel="stylesheet" href="stylesheets/imata.css" type="text/css">
    <link rel="stylesheet" href="stylesheets/sticky-footer.css" type="text/css">
    
    <!--[if IE]>
      <link rel="stylesheet" href="stylesheets/ie.css" type="text/css" media="all">
    <![endif]-->
    
    <!--[if lt IE 8]>
      <style type="text/css">
        .administer-wrapper .submit-wrapper { width: 9.75em; float: none; }
      </style>
    <![endif]-->
    
    <script type="text/javascript">
      // Set the base path for all javascript calls.
      // Helpful for hiding behind proxies
      var IMATA_PATH = '${h.url_for("home")[0:-1]}';
    </script>
    
    <script type="text/javascript" src="javascript/jquery.js"></script>
    <script type="text/javascript" src="javascript/jqModal.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/application.js"></script>
    <script type="text/javascript" src="javascript/app/nav_bar.js"></script>
    
    ${next.javascript()}
  </head>
  <body>
    
    <div id="wrap">
    	<div id="main" class="clearfix">
        <div id="header">
          <div class="logo">
            Info &amp; Tools
            <a href="#" class="button info-tools">
              <span class="tools-button"></span>
            </a>
          </div>
          <img src="images/icon.gif" alt="icon">
          <span id="welcome">Welcome, ${c.username}.</span>
<!--          <a href="#" class="my-profile" title="">My Profile</a> -->
          <a href="#" class="logout" title="">Log Out</a>
        </div>

        <div id="tagline">
          <a href="${g.COLLECTION_URL}" class="button view-collection">
            <span class="button-text">View Collection</span>
          </a>
          SAH Architecture Resources Archive
        </div>

        <div id="navigation">
          ${next.navigation()}
        </div>

        <div id="content">
          ${next.body()}
        </div>

        ${tools_menu()}

        <%include file="dialogs.mako" />
        
        <div id="wait-for"><div class="message"></div></div>
    	</div>
    </div>
    <div id="footer">
      <a href="#" onclick="IMATA.dialogs.show_text('/fullterms.html', 'TERMS AND CONDITIONS OF USE')">SAHARA Terms of Use</a>
      <a href="#" onclick="IMATA.dialogs.show_text('/privacy.html', 'PRIVACY POLICY')">SAHara Privacy Policy</a>
    </div>
  </body>
</html>

<%def name="tools_menu()">
  <ul id="tools-menu" style="display: none;">
    <li>
      <a href="http://www.saharaonline.org" target="blank">About SAHARA Project</a>
    </li>
    <li>
      <a href="http://sah.org/index.php?submenu=GUIDELINES&src=gendocs&ref=Cataloging%20Guidelines--revised&category=SAHARA"
         target="blank">Cataloging Guidelines</a>
    <li>
      <a href="${g.COLLECTION_URL}" target="blank">View SAHARA Collection</a>
    </li>
    <li>
      %if session['user'].is_privileged:
      <a href="editorial-help.html" title="Help" id="help-btn">Help</a>
      %else:
      <a href="user-help.html" title="Help" id="help-btn">Help</a>
      %endif
    </li>
  </ul>
</%def>
