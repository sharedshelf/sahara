<h1>Bucket Stats</h1>
<br/>
<br/>
<br/>
<h3>Total images marked as complete and unreviewed: <b>${c.total}</b></h3>
<br/><br/>
Bucket breakdown<br/>
<hr>
<table>
%for key in c.buckets:
  <tr>
    <td width="100" valign="top">${key}</td>
    <td width="100" valign="top"><b>${c.buckets[key]['total']}</b></td>
    <td width="200" valign="top">
      %if c.buckets[key].has_key('overlap'):
      %for overlap in c.buckets[key]['overlap']:
      ${"%d records overlap with %d<br/>" % (overlap['num'], overlap['source'])|n}
      %endfor
      %endif
    <td>${c.buckets[key]['query']}</td>
  </tr>
%endfor
</table>
<h3>In buckets: <b>${c.num_in_buckets}</b></h3>
<h3>In admin queue: <b>${c.num_unassigned}</b></h3>
