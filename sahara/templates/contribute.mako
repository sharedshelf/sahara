<div id="splitter">
  <div class="float-left" id="left-column">
    <div class="section-heading">
      <img class="name" src="images/select_an_image.png" alt="select an image">
      <div class="controls">
        <label>Thumbnails:</label>
        <div class="settable-button">
          <a href="#" class="button selector-small">
            <span class="thumbnail-box" id="thumbnail-small"></span>
          </a>
        </div>
        <div class="settable-button">
          <a href="#" class="button selector-medium">
            <span class="thumbnail-box" id="thumbnail-medium"></span>
          </a>
        </div>
        <div class="settable-button">
          <a href="#" class="button selector-large">
            <span class="thumbnail-box" id="thumbnail-large"></span>
          </a>
        </div>
      </div>
    </div>
    <div id="folder-wrapper">
      <ul>
        <li>
          <a href="${h.url_for('in_process')}">
            <span>
              <b>In Progress</b>
              <span id="progressing-count" class="inline">
                (0)
              </span>
            </span>
          </a>
        </li>
        <li><a href="${h.url_for('contributed')}">
            <span>
              <b>Contributed</b>
              <span id="contributed-count" class="inline">
                (0)
              </span>
            </span>
          </a>
        </li>
      </ul>
    </div>
  </div>
  <div id="right-column">
    <%include file="document_panel.mako" args="work=None, full=True"/>
  </div>
</div>
