<%inherit file="base.mako"/>

<%include file="contribute.mako"/>

<%def name="css()">
  <link rel="stylesheet" href="stylesheets/docit.css" type="text/css" media="screen">
  <link rel="stylesheet" href="stylesheets/ui.tabs.css" type="text/css" media="screen">
  <link rel="stylesheet" href="stylesheets/autocomplete.css" type="text/css" media="screen">
  <link rel="stylesheet" href="stylesheets/ui.autocomplete.css" type="text/css" media="screen">
</%def>

<%def name="navigation()">
  <%include file="navigation.mako"/>
</%def>

<%def name="javascript()">
  <script type="text/javascript" src="javascript/yui/yahoo-dom-event.js"></script>
  <script type="text/javascript" src="javascript/yui/connection-min.js"></script>
  <script type="text/javascript" src="javascript/yui/animation-min.js"></script>
  <script type="text/javascript" src="javascript/yui/datasource-min.js"></script>
  <script type="text/javascript" src="javascript/yui/autocomplete-min.js"></script>
  <script type="text/javascript" src="javascript/ui/ui.core.js"></script>
  <script type="text/javascript" src="javascript/ui/ui.tabs.js"></script>
  <script type="text/javascript" src="javascript/jquery.form.js"></script>
  <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
  <script type="text/javascript" src="javascript/splitter.js"></script>
  <script type="text/javascript" src="javascript/jquery.scrollTo-min.js"></script>
  <script type="text/javascript" src="javascript/jquery.selectable.js"></script>
  <script type="text/javascript" src="javascript/jquery.positionBy.js"></script>
  <script type="text/javascript" src="javascript/jquery.dimensions.js"></script>
  <script type="text/javascript" src="javascript/work_edit_form.js"></script>
  <script type="text/javascript" src="javascript/jtip.js"></script>
  <script type="text/javascript" src="javascript/jquery.bt.js"></script>
  <script type="text/javascript" src="javascript/docit.js"></script>
</%def>