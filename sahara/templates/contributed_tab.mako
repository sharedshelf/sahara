<div id="contributed" style="padding-top: 1em;">
  
  <%include file="asset_sort_controls.mako" args="notify='IMATA.assets.resort'"/>
  
  <div class="assets">
    <p style="margin-left:2em;">Contributed images cannot be edited.</p>
    
    %if len(c.works) > 0:
    %for work in c.works:
    <div class="asset">
      <div class="thumbnail-size${c.size}">
        <div id="${work.uuid}" class="img-wrapper valid">
          <img src="images/valid.png" class="valid-img">
          <img class="complete" src="${work.thumbnail(c.size)}"
               alt="thumbnail"
	             onclick="IMATA.assets.select_single('${work.uuid}', this)"
               ondblclick="expatiate('${work.uuid}',
                           '${work.media_category}', '${work.media_format}',
                           '${work.media_width}', '${work.media_height}');">
          </img>
        </div>
        <p class="caption">
          <a href="${h.url_for('tooltip', uuid=work.uuid)}" class="jTip"
             id="caption-${work.uuid}"
             name="Asset Information">
            ${work.brief_filename(c.size)}
          </a>
        </p>
      </div>
    </div>
    %endfor
    %else:
    <div class="empty">
      No images have been contributed to the AVRN
      Collection. To get started, simply:
      <ol style="margin-top: 2em;">
        <li>Go to the "In progress" tab above</li>
        <li>Add your images</li>
        <li>Describe them</li>
        <li>Contribute them</li>
      </ol>
      <a href="#" class="button goto-processing">
        <span class="button-text">GO TO THE "In progress" TAB</span>
      </a>
    </div>
    %endif
  </div>
  <div class="column-footer">
    <div id="terms-footer">
      <a href="javascript:showText('${h.url_for('fullterms')}',
               'TERMS AND CONDITIONS OF USE')"> SAHARA Terms of Use</a> |
      <a href="javascript:showText('${h.url_for('privacy')}', 'PRIVACY POLICY')">
        SAHARA Privacy Policy</a>
    </div>
  </div>
</div>



