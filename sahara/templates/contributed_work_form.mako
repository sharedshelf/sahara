<%page args="work=None, full=True"/>

<div class='asset_data'>
  <img src='${work.thumbnail(size=2)}' alt='thumbnail'></img>
  <div class='change-history'>
    <h3>Change log</h3>
    ${work.updated_on}<br/>
    ${work.updated_by}
  </div>
</div>
<div class='document-form'></div>

<div class="section-heading">
  <div class="controls">
    <label>
      List of fields:
    </label>
    <div class="settable-button">
      <a href="javascript:IMATA.document.set_type('brief')" class="button" id="ffields-brief">
        <div class="button-text">BRIEF</div>
      </a>
    </div>
    <div class="settable-button">
      <a href="javascript:IMATA.document.set_type('full')" class="button" id="ffields-full">
        <div class="button-text">FULL</div>
      </a>
    </div>
  </div>
  <img src="images/describe_the_image.png" class="name" alt="Describe the Image"/>
  <div id="document-wait" class="hidden">
    <img src="images/loading.gif" border="0" alt="Loading"/>
    <span id="document-wait-message">&nbsp;</span>
  </div>
</div>
<div id='document-container'>
  <hr>
  <div class='column-header'>
    %if work is not None:
    <img src="images/required.png" border="0"/>
    Required fields: Must be populated to contribute.
    %endif
  </div>

  <div id="document"></div>

  <div class='column-footer'>
    <div id='contribute-multiple-footer' style="position: absolute; top: 3em; right: 0.75em">
      <a href="javascript:IMATA.dialogs.multiple_contribute()">
        Contribute Multiple Images
      </a>
    </div>
    <div style="float: left; position: relative; display: none;" id='pre-contribution-document-controls'>
      <a href='javascript:IMATA.document.clear()' class='incomplete button brown'>
        <div class="button-text brown">CLEAR</div>
      </a>
      <a href='javascript:IMATA.dialogs.copy()' class='button'>
        <div class="button-text">COPY</div>
      </a>
      <a href='javascript:IMATA.document.save()' class='incomplete button orange'>
        <div class="button-text orange">SAVE</div>
      </a>
      <a href='javascript:IMATA.document.contribute()' class='button orange'>
        <div class="button-text orange">SAVE &amp; CONTRIBUTE</div>
      </a>
    </div>
    <div style="float: left; position: relative; display: none;" id='post-contribution-document-controls'>
      ${h.button("COPY", onclick="IMATA.dialogs.copy()")|n}
    </div>
  </div>
</div>
