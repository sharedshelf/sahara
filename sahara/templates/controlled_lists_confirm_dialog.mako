<div>
  <div class="dialog-header">
  </div>
  <div class="body">
    <h2>Are you sure you want to save this <span class="type">edit</span>?</h2>
    <p class="info">
      <span class="num-records">[xx,xxx]</span> SAHARA records will be updated. 
      Updates make take up to 24 hours to be reflected in the SAHARA Digital Library.
    </p>
  </div>
  <div class="footer">
    <button type="button" class="yes">Yes</button>
    <button type="button" class="no">No</button>
  </div>
</div>