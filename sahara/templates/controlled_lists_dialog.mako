<div>
  <div class="dialog-header">
    <img src="/images/dialog-icon.png">
    Administer Controlled Lists
    <a href="#" class="close-trigger">Close [x]</a>
  </div>
  <div>
    <h2>Title of controlled list</h2>
    <p class="status-message">&nbsp;</p>
    <div class="table-wrapper">
      <table>
        <thead>
          <tr>
            <th>Value</th>
            <th class="actions-header">Actions</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
          <tr>
            <td>Controlled list value</td>
            <td><a href="#" title="" class="edit">Edit</a></td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="footer">
      All controlled list are ordered alphabetically from A to Z.
      <button type="button" class="add-new">Add new</button>
    </div>
  </div>
</div>