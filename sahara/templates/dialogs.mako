<div class="jqmWindow" id="dialog"></div>
<div class="jqmWindow" id="transform-dialog"></div>
<div id="asset-edit-dialog"></div>
<div class="info-dialog" id="alert-dialog">
  <div class="dialog-header">
    <img src="images/dialog-icon.png" alt="icon">
  </div>
  <div class="dialog-centered-content">
      <div id="info-major"></div>
      <div id="info-minor"></div>
    </div>
  <div class="dialog-footer">
    ${h.button("CLOSE", id="close_info")|n}
  </div>
</div>
<div id="confirm-dialog">
  <div class="dialog-header">
	<div class="close-button" onclick="IMATA.dialogs.close('confirm-dialog')">
	  <a href="javascript:IMATA.dialogs.close('confirm-dialog');" class="button">
	    <span class="close"></span>
	  </a>
	  CLOSE
	</div>
	<img src="images/dialog-icon.png" alt="icon">&#160;<span id="confirm-dialog-title"></span>
  </div>
  <div class="message">&nbsp;</div>
  <div class="dialog-footer">
    ${h.button("YES", id="confirm-dialog-yes")|n}
    ${h.button("NO", id="confirm-dialog-no")|n}
  </div>
</div>
<div class="text-dialog" id="text-dialog">
  <div class="dialog-header">
	<div class="close-button" onclick="IMATA.dialogs.close('text-dialog')">
	  <a href="javascript:IMATA.dialogs.close('text-dialog');" class="button">
	    <span class="close"></span>
	  </a>
	  CLOSE
	</div>
	<img src="images/dialog-icon.png" alt="icon">&#160;<span id="text-dialog-title"></span>
  </div>
  <div class="text-content" id="text-content"></div>
</div>
