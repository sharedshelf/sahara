<div>
  <div class="dialog-header">
    <div class="close-button">
      ${h.button("", onclick="IMATA.document.multicontribute.close()")|n}
      CLOSE
    </div>
    <img src="images/dialog-icon.png"/>&#160;CONTRIBUTE MULTIPLE ASSETS
  </div>
  <div class="dialog-content">
    <h2>1: Select images to contribute</h2>
    <p style="margin-left: 2em;">These images meet the SAHARA Editorial Guidelines and may be contributed.</p>
    <%include file="../../asset_sort_controls.mako"
	      args="notify='IMATA.document.multicontribute.resort'"/>
    <div class="assets">
      %if len(c.works) > 0 :
      %for work in c.works:
      <div class="asset">
	<div class="thumbnail-size0">
	  <div id="mc_${work.uuid}" onclick="toggle_selection(this)" class="img-wrapper valid">
	    <img class="valid-img" src="images/valid.png"/>
	    <img src="${work.thumbnail(0)}" alt="thumbnail"></img>
	  </div>
	  <p class="caption">${work.brief_filename()}</p>
	</div>
      </div>
      %endfor
      %endif
    </div>
  </div>
  <div class="dialog-footer">
    ${h.button("NEXT", onclick="IMATA.document.multicontribute.next()", classes="float-right")|n}
    ${h.button("SELECT ALL", onclick="IMATA.document.multicontribute.select_all_works()")|n}
    ${h.button("CLEAR", onclick="IMATA.document.multicontribute.deselect_all_works()", classes="brown")|n}
  </div>
</div>

