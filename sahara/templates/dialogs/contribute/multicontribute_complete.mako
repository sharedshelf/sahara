<div>
  <div class="dialog-header">
    <div class="close-button">
      ${h.button("", onclick="IMATA.document.multicontribute.close()")|n}
      CLOSE
    </div>
    <img src="images/dialog-icon.png"/>&#160;CONTRIBUTE MULTIPLE ASSETS
  </div>
  <div class="dialog-content">
    <h2>>3: Images were successfully contributed</h2>
    <div id="dialog-assets">
      <div class="dialog-centered-success">
	<div class="multiaction-success"><br/>
	  Contributed images will appear in the<br/>
	  SAHARA collection within 30 minutes.<br/><br/><br/>
	  <a href="${g.COLLECTION_URL}"
	     OnClick="javascript:IMATA.multicontribute.close()"
	     target="blank" class="button">
	    <div class="button-text">VEIW COLLECTION</div>
	  </a><br/>&#160;
	</div>
      </div>
    </div>
  </div>
  <div class="dialog-footer">
    ${h.button("CLOSE", onclick="IMATA.document.multicontribute.next()", classes="float-right")|n}
  </div>
</div>

