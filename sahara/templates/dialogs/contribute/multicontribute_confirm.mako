<div>
  <div class='dialog-header'>
    <div class="close-button">
      ${h.button("", onclick="IMATA.document.multicontribute.close()")|n}
      CLOSE
    </div>
    <img src='images/dialog-icon.png'/>&#160;CONTRIBUTE MULTIPLE ASSETS
  </div>
  <div class='dialog-content'>
    <h2>2: Review selections</h2>
    <h0>All Contributions are final and cannot be edited, removed or replaced.</h0>
    <div class='assets'>
      %if len(c.works) > 0 :
      %for work in c.works:
      <div class='asset'>
        <div class='thumbnail-size0'>
          <div id='${work.uuid}' class='img-wrapper valid'>
            <img class="valid-img" src="images/valid.png"/>
            <img src='${work.thumbnail(0)}' alt='thumbnail'></img>
          </div>
          <p class='caption'>
            ${work.brief_filename()}
          </p>
        </div>
      </div>
      %endfor
      %endif
    </div>
  </div>
  <div class='dialog-footer'>
    ${h.button("CONTRIBUTE", onclick="IMATA.document.multicontribute.next()", classes="float-right orange")|n}
    ${h.button("BACK", onclick="IMATA.document.multicontribute.prev()", classes="float-right brown")|n}
  </div>
</div>



