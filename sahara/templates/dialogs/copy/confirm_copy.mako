<div>
  <div class='dialog-header'>
	<div class="close-button">
	  ${h.button("", onclick="IMATA.document.multicopy.close()")|n}
	  CLOSE
	</div>
	<img src='images/dialog-icon.png'/>&#160;COPY FIELDS TO OTHER IMAGE RECORDS
  </div>
  <div class='dialog-content'>
	<h2>3: Check your work</h2>
	&nbsp;&nbsp;The fields at the left will be copied to the images at the right.
	<div id='dialog-half-fields'>
	  %if 'title' in c.fields:
	  ALL Name/Title fields<br/>
		<span class="description">(Including Name/Title, View Type, Image View, Broad Classification, Narrow Classification, and more)</span><br/><br/>
	  %endif
	  %if 'creator' in c.fields:
		ALL Creator fields<br/>
		<span class="description">(Including Creator Name, Role, and more)</span><br/><br/>
	  %endif
	  %if 'location' in c.fields:
		ALL Location fields<br/>
		<span class="description">(Including City/County, State/Province, Country, Repository, and more)</span><br/><br/>
	  %endif
	  %if 'chronology' in c.fields:
		ALL Chronology fields<br/>
		<span class="description">(Including Date Earliest Date, Latest Date, and more)</span><br/><br/>
	  %endif
	  %if 'physical' in c.fields:
		ALL Physical Information fields<br/>
		<span class="description">(Including Description and more)</span><br/><br/>
	  %endif
	  %if 'subject' in c.fields:
		ALL Subject/Tags fields<br/>
		<span class="description">(Including Keywords)</span><br/><br/>
	  %endif
	  %if 'source' in c.fields:
		ALL Source fields<br/>
		<span class="description">(Including Photographer, Contributor, Image Date, Image Date Earliest, Image Date Latest, and more)</span><br/><br/>
	  %endif
	  %if 'rights' in c.fields:
		ALL Rights fields<br/>
		<span class="description">(Including Copyright of Photograph/Image, Usage Rights and more)</span><br/><br/>
	  %endif
    </div>
	<div id='dialog-half-assets'>
	  %if len(c.works) > 0 :
	  %for work in c.works:
	  <div class='asset'>
		<div class='thumbnail-size0'>
		  %if work.status.valid is True:
		  <div id='${work.uuid}' class='img-wrapper valid'>
			<img class="valid-img" src="images/valid.png"/>
		    <img src='${work.thumbnail(0)}' alt='thumbnail'></img>
		  </div>
		  %else:
		  <div id='${work.uuid}' class='img-wrapper'>
		    <img src='${work.thumbnail(0)}' alt='thumbnail'></img>
		  </div>
		  %endif
		  <p class='caption'>
		    ${work.brief_filename()}
		  </p>
		</div>
	  </div>
      %endfor
	  %endif
    </div>
  </div>
  <div class='dialog-footer'>
    ${h.button("COPY", onclick="IMATA.document.multicopy.next()", classes="float-right orange")|n}
    ${h.button("BACK", onclick="IMATA.document.multicopy.prev()", classes="float-right brown")|n}
  </div>
</div>
