<%page args="success"/>

<div>
  <div class="dialog-header">
	<div class="close-button">
	  ${h.button("", onclick="IMATA.document.multicopy.close()")|n}
	  CLOSE
	</div>
	<img src="images/dialog-icon.png">&#160;COPY FIELDS TO OTHER IMAGE RECORDS
  </div>
  <div class="dialog-content">
	<h2>4: Fields were successfully copied</h2>
	<div id="dialog-assets">
		<div class="dialog-centered-success">
			<div class="multiaction-success">
			<br>
      Once fields have been copied, you can always go
			<br>
		  back and edit individual records.
		  <br>&#160;
			</div>
		</div>
    </div>
  </div>
  <div class="dialog-footer">
    ${h.button("CLOSE", onclick="IMATA.document.multicopy.next()", classes="float-right")|n}
  </div>
</div>
