<div>
  <div class='dialog-header'>
	<div class="close-button">
	  ${h.button("", onclick="IMATA.document.multicopy.close()")|n}
	  CLOSE
	</div>
	<img src='images/dialog-icon.png'/>&#160;COPY FIELDS TO OTHER IMAGE RECORDS
  </div>
  <div class='dialog-content'>
	<h2>1: Select fields to copy</h2>
	<div class='assets'>
      <form>
  		<input name="title" value="title" type="checkbox"/>ALL Name/Title fields<br/>
		<span class="description">(Including Name/Title, View Type, Image View, Broad Classification, Narrow Classification, and more)</span><br/><br/>
		<input name="creator" value="creator" type="checkbox"/>ALL Creator fields<br/>
		<span class="description">(Including Creator Name, Role, and more)</span><br/><br/>
		<input name="location" value="location" type="checkbox"/>ALL Location fields<br/>
		<span class="description">(Including City/County, State/Province, Country, Repository, and more)</span><br/><br/>
		<input name="chronology" value="chronology" type="checkbox"/>ALL Chronology fields<br/>
		<span class="description">(Including Date Earliest Date, Latest Date, and more)</span><br/><br/>
		<input name="physical" value="physical" type="checkbox">ALL Physical Information fields<br/>
		<span class="description">(Including Description and more)</span><br/><br/>
		<input name="subject" value="subject" type="checkbox">ALL Subject/Tags fields<br/>
		<span class="description">(Including Keywords)</span><br/><br/>
		<input name="source" value="source" type="checkbox">ALL Source fields<br/>
		<span class="description">(Including Photographer, Contributor, Image Date, Image Date Earliest, Image Date Latest, and more)</span><br/><br/>
		<input name="rights" value="rights" type="checkbox">ALL Rights fields<br/>
		<span class="description">(Including Copyright of Photograph/Image, Usage Rights and more)</span><br/><br/>
	  </form>
	</div>
  </div>
  <div class='dialog-footer'>
    ${h.button("NEXT", onclick="IMATA.document.multicopy.next()", classes="float-right")|n}
    ${h.button("SELECT ALL", onclick="IMATA.document.multicopy.select_all_fields()")|n}
    ${h.button("CLEAR", onclick="IMATA.document.multicopy.deselect_all_fields()", classes="brown")|n}
  </div>
</div>
  
