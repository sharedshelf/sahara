<div>
  <div class="dialog-header">
    <div class="close-button">
      ${h.button("", onclick="IMATA.document.multicopy.close()")|n}
      CLOSE
    </div>
    <img src="images/dialog-icon.png"/>&#160;COPY FIELDS TO OTHER IMAGE RECORDS
  </div>
  <div class="dialog-content">
    <h2>2: Select images</h2>
    %if c.priveleged is False:
    <%include file="../../asset_sort_controls.mako" args="notify='IMATA.document.multicopy.resort'"/>
    %endif
    <div class="assets">
      %if len(c.works) > 0 :
      %for work in c.works :
      <div class="asset">
        <div class="thumbnail-size0">
          %if work.status.valid:
            <div id="cm_${work.uuid}" onclick="toggle_selection(this)" class="img-wrapper valid">
          %elif not work.is_edited():
            <div id="cm_${work.uuid}" onclick="toggle_selection(this)" class="img-wrapper new">
          %else:
            <div id="cm_${work.uuid}" onclick="toggle_selection(this)" class="img-wrapper">
                %endif
                <img class="valid-img" src="images/valid.png">
                <img src="${work.thumbnail(0)}" alt="thumbnail">
            </div>
          <p class="caption">
            ${work.brief_filename()}
          </p>
        </div>
      </div>
      %endfor
      %else:
	    There are no images available. Please 'lock' the images you would like to copy to on the Edit page.
      %endif
    </div>
  </div>
  <div class="dialog-footer">
    ${h.button("NEXT", onclick="IMATA.document.multicopy.next()", classes="float-right")|n}
    ${h.button("BACK", onclick="IMATA.document.multicopy.prev()", classes="float-right brown")|n}
    ${h.button("SELECT ALL", onclick="IMATA.document.multicopy.select_all_images()")|n}
    ${h.button("CLEAR", onclick="IMATA.document.multicopy.deselect_all_images()", classes="brown")|n}
  </div>
</div>

