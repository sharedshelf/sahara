<div>
  <div class='dialog-header'>
    <div class="close-button">
      ${h.button("", onclick="IMATA.assets.remove_multiple.close()")|n}
      CLOSE
    </div>
    <img src='images/dialog-icon.png'/>&#160;DELETE MULTIPLE ASSETS
  </div>
  <div class='dialog-content'>
    <h2>1: Select assets for deletion:</h2>
    <div class='assets'>
      %if len(c.works) > 0 :
      %for work in c.works:
      <div class='asset'>
        <div class='thumbnail-size0'>
          %if work.status.valid is True:
          <div id='da_${work.uuid}' onclick='toggle_selection(this)'
               class='img-wrapper valid'>
            <img class="valid-img" src="images/valid.png"/>
            <!-- javascript depends on da_ prefix -->
            <img src='${work.thumbnail(0)}' alt='thumbnail'></img>
          </div>
          %else:
          <div id='da_${work.uuid}' onclick='toggle_selection(this)'
               class='img-wrapper'>
            <!-- javascript depends on da_ prefix -->
            <img src='${work.thumbnail(0)}' alt='thumbnail'></img>
          </div>
          %endif
          <p class='caption'>
            ${work.brief_filename()}
          </p>
        </div>
      </div>
      %endfor
      %endif
    </div>
  </div>
  <div class='dialog-footer'>
    ${h.button("NEXT", onclick="IMATA.assets.remove_multiple.next()", classes="float-right")|n}
    ${h.button("CANCEL", onclick="IMATA.assets.remove_multiple.close()", classes="float-right brown")|n}
    ${h.button("SELECT ALL", onclick="IMATA.assets.remove_multiple.select_all()")|n}
    ${h.button("CLEAR", onclick="IMATA.assets.remove_multiple.deselect_all()", classes="brown")|n}
  </div>
</div>
