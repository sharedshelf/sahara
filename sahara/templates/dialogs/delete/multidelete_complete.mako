<div>
  <div class="dialog-header">
    <div class="close-button">
      ${h.button("", onclick="IMATA.assets.remove_multiple.close()")|n}
      CLOSE
    </div>
    <img src="images/dialog-icon.png"/>&#160;DELETE MULTIPLE ASSETS
  </div>
  <div class="dialog-content">
    <h2>3: Assets were successfully deleted</h2>
    <div id="dialog-assets">
      <div class="dialog-centered-success">
        <div class="multiaction-success"><br/>
          The request assets have be removed from the system.<br/>
          <br/><br/><br/>&#160;
        </div>
      </div>
    </div>
  </div>
  <div class="dialog-footer">
    ${h.button("CLOSE", onclick="IMATA.assets.remove_multiple.next()", classes="float-right")|n}
  </div>
</div>

