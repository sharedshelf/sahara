<div>
  <div class='dialog-header'>
    <div class="close-button">
      ${h.button("", onclick="IMATA.assets.remove_multiple.close()")|n}
      CLOSE
    </div>
    <img src='images/dialog-icon.png'/>&#160;DELETE MULTIPLE ASSETS
  </div>
  <div class='dialog-content'>
    <h2>2: Review selections</h2>
    <h0 style='color: red'>All selected assets will be permanently removed from the system.</h0>
    <div class='assets'>
      %if len(c.works) > 0 :
      %for work in c.works:
      <div class='asset'>
        <div class='thumbnail-size0'>
          <div id='da_${work.uuid}' class='img-wrapper'>
            <img class="valid-img" src="images/valid.png"/>
            <img src='${work.thumbnail(0)}' alt='thumbnail'></img>
          </div>
          <p class='caption'>
            ${work.brief_filename()}
          </p>
        </div>
      </div>
      %endfor
      %endif
    </div>
  </div>
  <div class='dialog-footer'>
    ${h.button("DELETE", onclick="IMATA.assets.remove_multiple.next()", classes="float-right orange")|n}
    ${h.button("BACK", onclick="IMATA.assets.remove_multiple.prev()", classes="float-right brown")|n}
  </div>
</div>

