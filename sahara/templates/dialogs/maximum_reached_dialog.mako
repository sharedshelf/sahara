<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet"
		  href="/stylesheets/dialog.css"
		  type="text/css"
		  media="screen"
		  charset="utf-8" />
    <link rel="stylesheet"
		  href="/stylesheets/imata.css"
		  type="text/css"
		  media="screen"
		  charset="utf-8" />
	<script
	   type="text/javascript"
	   src="/javascript/jquery-1.2.6.min.js">
	</script>
	<script
	   type="text/javascript"
	   src="/javascript/ui/ui.core.js">
	</script>
  </head>
  <body>
	<div id="content">
	  <div class='dialog-header'>
	    <div class="close-button">
	      ${h.button("", onclick="closeDialog()")|n}
	      CLOSE
	    </div>
	    <img src='images/dialog-icon.png'/>&nbsp;ADD IMAGES TO WORKSPACE
	  </div>
	  <div class='dialog-content'>
		<h2>No more space in processing queue.</h2>
		<p>All available space in the processing queue has been used.  Assets must either be contributed, or removed before more assets can be added.</p>
	  </div>
	  <div class='dialog-footer'>
	    ${h.button("CLOSE", onclick="closeDialog()" classes="brown")|n}
	  </div>
	</div>
  </body>
</html>
