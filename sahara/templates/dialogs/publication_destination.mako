<div class="publish-dialog">
  <div class="dialog-header">
	  <div class="close-button">
	    ${h.button("", onclick="$('#dialog').jqmHide()")|n}
	    CLOSE
	  </div>
	  <img src="images/dialog-icon.png" alt="">
  </div>
  <div class="dialog-content">
	  <h2>Where does this record belong?</h2>
  	<form id="publication_destination_form">
  	  %if c.current_repository == int(g.MEMBERS_COLLECTION):
    	  <p>
    	    <input name="collection" value="${g.EDITORS_COLLECTION}" type="radio" id="editors-collection">
      	  <label for="editors-collection">
      	    Send to Editors' Choice Collection
      	    <img src="images/editors_choice_icon.png" alt="" width="32">
      	  </label>
    	  </p>
    	  <p>
    	    <input name="collection" value="${g.MEMBERS_COLLECTION}" checked type="radio" id="members-collection">
      	  <label for="members-collection">Keep in Members' Collection</label>
    	  </p>
  	  %else:
    	  <p>
    	    <input name="collection" value="${g.EDITORS_COLLECTION}" checked type="radio" id="editors-collection">
      	  <label for="editors-collection">
      	    Keep in Editors' Choice Collection
      	    <img src="images/editors_choice_icon.png" alt="" width="32">
      	  </label>
    	  </p>
        <p>
          <input name="collection" value="${g.MEMBERS_COLLECTION}" type="radio" id="members-collection">
      	  <label for="members-collection">Send to Members' Collection</label>
        </p>
  	  %endif
  	</form>
  </div>
  <div class="dialog-footer">
    <span style="font-size:0.95em">Any changes will be made available to the SAHARA community.</span>
    ${h.button("NEXT &raquo;", onclick="determine_destination('"+c.uuid+"')", classes="next float-right")|n}
    ${h.button("CANCEL", onclick="return $('#dialog').jqmHide();", classes="brown cancel float-right")|n}
  </div>
</div>
