<div>
  <div class='dialog-header'>
    <div class="close-button">
      ${h.button("", onclick="IMATA.dialogs.close('transform-dialog')")|n}
      CLOSE
    </div>
    <img src='images/dialog-icon.png'/>&#160;${c.work.media_filename}
  </div>
  <ul id='image-manipulation-tabs'>
    <li><a href="#asset-information-pane"><span>Image Information</span></a>
    </li>
    <li><a href="#asset-transform-pane"><span>Rotate and Flip</span></a>
    </li>
    <li><a href="#asset-replacement-pane"><span>Replace</span></a></li>
  </ul>
  <div id='asset-information-pane'>
    <div class='dialog-image-area'>
      <span></span>
      <img class='centered-image' id='information-panel-img'
           src='${c.work.thumbnail(2)}' alt='thumbnail'> </img>
    </div>
    <br/><br/>
    <div class='dialog-section'>
      <h3>Source Image Information</h3>
    </div>
    <p><span class='label'>Name:</span>${c.work.media_filename}<br/>
      <span class='label'>Dimensions:</span>Width:  ${c.work.media_width}px;
      Height: ${c.work.media_height}px;<br/>
    </p>
  </div>
  <div id='asset-transform-pane'>
    <div style="width:100%;margin-left:1em"><img src="images/exclamation.png"/>&nbsp;Note: The quality of JPEGs
      degrades with each transformation.
    </div>
    <div class='dialog-image-area'>
      <span></span>
      <img class='centered-image' id='flip-img'
           src='${c.work.thumbnail(2)}'
           alt='thumbnail'> </img>
    </div>
    <span class='error'>&nbsp;</span>
    <div class="dialog-footer">
      ${h.button("Rotate", onclick="IMATA.stor.flip('right', '"+c.work.uuid+"')", classes="flip-cw")|n}
      ${h.button("Rotate", onclick="IMATA.stor.flip('left', '"+c.work.uuid+"')", classes="flip-ccw")|n}
      ${h.button("Flip Vertically", onclick="IMATA.stor.flip('flip', '"+c.work.uuid+"')", classes="flip-vertical")|n}
      ${h.button("Flip Horizontally", onclick="IMATA.stor.flip('mirror', '"+c.work.uuid+"')", classes="flip-horizontally")|n}
      ${h.button("SAVE", onclick="IMATA.stor.save('"+c.work.uuid+"')", classes="orange float-right")|n}
      <div class="wait" style="display: none;"><img src="images/loading.gif"> Saving...</div>
    </div>
  </div>
  <div id='asset-replacement-pane'>
    <h2>Replace image</h2><br/>
    <p>Supported formats include: JPEG, GIF, PNG, TIFF</p><br/>
    <form class='replace-asset' action="${h.url_for('replace', uuid=c.work.uuid)}"
          method='post' enctype='multipart/form-data'>
      <input type='file' size="45" name='asset-file'>
    </form>
    <span class='error'>&nbsp;</span><br/>
    <div class="dialog-footer">
      ${h.button("REPLACE", onclick="replace_asset('"+c.work.uuid+"')", classes="orange float-right")|n}
      ${h.button("CLEAR", onclick="IMATA.stor.replace_reset()", classes="brown float-right")|n}
      <div class="wait" style="display: none;"><img src="images/loading.gif"> Replacing...</div>
    </div>
  </div>
</div>
