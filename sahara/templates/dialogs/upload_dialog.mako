<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
	  "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en-US">
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    
    <link rel="stylesheet" href="stylesheets/dialog.css" type="text/css">
    <link rel="stylesheet" href="stylesheets/imata.css" type="text/css">
    
    <script type="text/javascript" src="javascript/jquery-1.2.6.min.js"></script>
    <script type="text/javascript" src="javascript/ui/ui.core.js"></script>
    <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
    <script type="text/javascript" src="javascript/jquery.scrollTo-min.js"></script>
    <script type="text/javascript" src="javascript/yui/yahoo-dom-event.js"></script>
    <script type="text/javascript" src="javascript/yui/element-min.js"></script>
    <script type="text/javascript" src="javascript/yui/uploader-min.js"></script>
    
  </head>
  <body>
    <div>
      <div class='dialog-header'>
	<div class="close-button">
	  ${h.button("", onclick="closeDialog()")|n}
	  CLOSE
	</div>
	<img src='images/dialog-icon.png'>
	&nbsp;ADD IMAGES TO WORKSPACE
      </div>
      <div class='dialog-content'>
	<h2>Add images to workspace</h2>
	<br>
	<img src="images/exclamation.png">
	&nbsp;Use the control and shift keys to select multiple files.
	<div id="upload-totals">
	  <img src="images/exclamation.png">
	  &nbsp;Number of files selected:<span class="file-count">0</span> |
	  Size of batch: <span class="file-size">0 bytes</span>
	</div>
	<div id="upload-stats">
	  &nbsp;
	</div>
	<div id="file-list-container" class="file-list-empty"></div>
	<div id="complete-wrapper">
	  <input type="checkbox" checked id="close-on-complete">
	  Close window when finished
	</div>
      </div>
      
      <div class='dialog-footer'>  
	<div id="select-files" style="margin-top: 5px; width: 93px; height: 23px;"></div>
	${h.button("CLEAR", id="clear-button", onclick="handleClearFiles()", classes="brown")|n}
        ${h.button('ADD', onclick='upload()', id='add-button', classes='orange')|n}
        ${h.button('CLOSE', onclick='cancelUpload()', id='cancel-button', classes='brown')|n}
      </div>
    </div>
    
    <script type="text/javascript">
      (function(){
      
      function closeDialog() {
      window.close();
      }
      window.closeDialog = closeDialog;

      YAHOO.widget.Uploader.SWFURL = "uploader.swf";

      var uploader = new YAHOO.widget.Uploader("select-files", "/images/add-button-src.png");
      var fileList = null;
      var uploads = {}
      var errors = false;
      var upload_speed = 0;
      var upload_start_time;
      var upload_bytes_remaining = 0;
      var total_upload_size = 0;

      // Add event listeners to various events on the uploader.
      // Methods on the uploader should only be called once the 
      // contentReady event has fired.

      uploader.addListener('contentReady', handleContentReady);
      uploader.addListener('fileSelect', onFileSelect);
      uploader.addListener('uploadProgress', onUploadProgress);
      uploader.addListener('uploadCancel', onUploadCancel);
      uploader.addListener('uploadComplete', onUploadComplete);
      uploader.addListener('uploadCompleteData', onUploadResponse);
      uploader.addListener('uploadError', onUploadError);

      function cancelUpload() {
      uploader.cancel();
      window.close();
      }
      window.cancelUpload = cancelUpload;

      function handleClearFiles() {
      uploader.clearFileList();
      fileList = null;
      uploads = {};
      uploader.enable();
      jQuery('#file-list-container').html("");
      jQuery('#file-list-container').addClass("file-list-empty");
      jQuery('#upload-stats span.file-count').text('0');
      jQuery('#upload-stats span.file-size').text('0 bytes');
      jQuery('#clear-button').hide();
      jQuery('#select-files').css({'bottom': '15px'});
      jQuery('#upload-totals').hide();
      }
      window.handleClearFiles = handleClearFiles;

      // When contentReady event is fired, you can call methods on the uploader.
      function handleContentReady () {
      uploader.setAllowMultipleFiles(true);
      // New set of file filters.
      var ff = new Array({
      description:"Images",
      extensions:"*.jpg;*.png;*.gif;*.tif;*.JPG;*.PNG;*.GIF;*.TIF;*.tiff;*.TIFF"
      },{
      description:"Videos",
      extensions:"*.mov;*.swf;*.MOV;*.SWF"
      },{
      description:"Audio",
      extensions:"*.mp3;*.MP3"
      });

      // Apply new set of file filters to the uploader.
      uploader.setFileFilters(ff);
      }

      function upload() {
        if (fileList != null) {
          jQuery('#upload-stats').html("Starting upload");
          disable_controls();
          upload_start_time = new Date().getTime();
          uploader.disable();
          uploader.setSimUploadLimit(3);
          var session_cookie = jQuery.cookie('sahara');
          uploader.uploadAll('${h.url_for('flash_upload')}', "POST", {
            cookie: 'sahara=' + session_cookie
          }, "asset_file");
        } else {
          alert("Click browse to select files for upload.");
        }
      }
      window.upload = upload;

      // Fired when the user selects files in the "Browse" dialog
      // and clicks "Ok".
      function onFileSelect(event) {
      var files = 0;
      if(YAHOO.lang.hasOwnProperty(event, 'fileList') && event.fileList != null) {
      fileList = event.fileList;
      for(var i in fileList) {
      var trackingDiv = "<div class='file' id='"+fileList[i]['id']+"'>" +
        "<div class='file-status'>0%</div>" +
        "<div class='filename'>" + fileList[i]['name'] + "</div>" +
      	"<div class='progress'></div>" +
        "<div class='error'>&nbsp;</div></div>";
      total_upload_size += fileList[i]['size'];
      files++;
      jQuery('#file-list-container').prepend(trackingDiv);
      }
      jQuery('#upload-totals span.file-count').text(files);
      jQuery('#upload-totals span.file-size').text(human_readable_bytes(total_upload_size));
      upload_bytes_remaining = total_upload_size;
      jQuery('#clear-button').show();
      jQuery('#select-files').css({'bottom': '-115px'});
      jQuery('#add-button').css({'bottom': '16px'});
      jQuery('#upload-totals').show();
      jQuery('#file-list-container').removeClass("file-list-empty");
      }
      }

      // Do something on each file's upload progress event.
      function onUploadProgress(event) {
      var prog = Math.round(370*(event["bytesLoaded"]/event["bytesTotal"]));
      progbar = "<div style=\"background-color: #9c9673; height: 7px; width: " + prog + "px\"/>";
      jQuery('#'+event['id']+' div.progress').html(progbar);
      jQuery('#'+event['id']+' div.file-status').html(Math.round((event["bytesLoaded"]/event["bytesTotal"])*100)+'%');
      }

      // Do something when each file's upload is complete.
      function onUploadComplete(event) {
      }

      function close_dialog_and_reload_palette() {
        if($('#close-on-complete').attr('checked')) {
          closeDialog();
        } else {
          $('#cancel-button div').html('CLOSE');
        }
      }

      // Do something if a file upload throws an error.
      // (When uploadAll() is used, the Uploader will
      // attempt to continue uploading.
      function onUploadError(event) {
      fileList[event['id']].done = true;
      jQuery('#'+event['id']+' div.error').html("<span style='color: red;'>" +
        "Error processing upload.</span>");
      errors = true;
      }

      // Do something if an upload is cancelled.
      function onUploadCancel(event) {

      }

      // Do something when data is received back from the server.
      function onUploadResponse(event) {
        if (event.data.indexOf('Error') > -1) {
          jQuery('#'+event['id']+' div.error').html("<span style='color: red;'>" + event.data + "</span>");
        $('#close-on-complete').removeAttr('checked');
        errors = true;
      }
      upload_bytes_remaining = upload_bytes_remaining - fileList[event['id']]['size'];
      update_upload_speed(upload_start_time, upload_bytes_remaining, total_upload_size);
      fileList[event['id']].done = true;
      progbar = "<div style=\"background-color: #9c9673; height: 7px; width: 370px\"/>";
      jQuery('#'+event['id']+' div.progress').html(progbar);
      jQuery('#'+event['id']+' div.file-status').html('100%');

      var alldone = true;
      for (var i in fileList) {
      if (!('done' in fileList[i])) { alldone = false; break; }
      
      }
      if (true == alldone) {
      $('#upload-stats').html('Upload complete.  (' +
        human_readable_bytes(upload_speed) + ' per second.)');
        window.opener.IMATA.upload.complete(errors);
        close_dialog_and_reload_palette();
        }
      }

      function update_upload_speed(start, bytes_remaining, total_bytes) {
      var elapsed = (new Date().getTime() - start) / 1000;
      var bps = (total_bytes - bytes_remaining)/elapsed;
      if(0 == upload_speed) { upload_speed = bps; }
      else { upload_speed = (upload_speed + bps) / 2; }

      var up_speed = human_readable_bytes(upload_speed);
      var remaining = human_readable_time(Math.round(bytes_remaining / upload_speed));
      jQuery('#upload-stats').html('Estimated time remaining: ' +
      remaining + ' ('+up_speed+' per second)');
      }

      function human_readable_bytes(bytes) {
      if(bytes < 1000) { return bytes + ' bytes'; }
		 else if(bytes < 1000000) { return Math.round(bytes/1000) + ' kilobytes'; }
				 else if(bytes < 1000000000) { return Math.round(bytes/1000000) + ' megabytes'; }
						 else { return Math.round((bytes/1000000000)*10)/10 + ' gigabytes'; }
						 }

						 function human_readable_time(seconds) {
						 if(seconds < 60) { return seconds + ' seconds'; }
							      else if(seconds < 3600) { return Math.round(seconds/60) + ' minutes'; }
										else { return Math.round((seconds/3600)*100)/100 + ' hours'; }
										}

										function disable_controls() {
										uploader.disable();
										jQuery('#clear-button').hide();
										jQuery('#add-button').hide();
										}
										})();
										</script>
  </body>
</html>
