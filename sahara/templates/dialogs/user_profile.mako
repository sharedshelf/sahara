<div>
  <div class='dialog-header'>
    <div class="close-button">
      ${h.button("", onclick="closeTextDialog()")|n}
      CLOSE
    </div>
    <img src='/images/dialog-icon.png'/>&#160;MY PROFILE
  </div>
  <ul id='user-profile-tabs'>
    <li><a href="#profile-tab"><span>My Profile</span></a>
    </li>
    <li><a href="#password-tab"><span>Change Password</span></a>
    </li>
  </ul>
  <div id='profile-tab'>

    <form action="${h.url_for('profile_update')}" method="POST">

      <div class="field">
        <div class="error-message">&nbsp;</div>
      </div>

      <div class="field">

        <div style="float: left; width: 300px; margin-right: 2em;">
          <label>First name</label>
          <input name="firstName" type="text" size="25" value="${profile['firstname']}" maxlength="50">
          <div class="hint">(50 character max)</div>
        </div>

        <div style="float: left; width: 100px;">
          <label>Middle initial</label>
          <input name="middleName" type="text" size="1" value="${profile['middleName']}" maxlength="1" class="short">
          <div class="hint">&nbsp;</div>
        </div>

      </div>
      <div class="field">
        <label>Last name</label>
        <input name="lastName" type="text" size="25" value="${profile['lastname']}" maxlength="50">
        <div class="hint">(50 character max)</div>
      </div>
      <p style="clear: both;"/>
      <div class="field">
        <label>Email</label>
        <input name="email" type="text" size="25" maxlength="255" value="${profile['email']}">
      </div>
      <div class="save-button">
        ${h.button("SAVE", onclick="updateProfile()" classes="orange")|n}
      </div>
    </form>
  </div>
  <div id='password-tab'>
    <div class="error-message">&nbsp;</div>
    <form action="${h.url_for('password')}" method="POST">
      <div class="field">
        <label for="old-password">Old password</label>
        <input type="password" size="25" maxlength="50" name="oldPassword">
        <div class="note">Note: Please do not use special characters in passwords.  (e.g. ! _ * / &amp; ^ # ')</div>
      </div>
      <div class="field">
        <label for="new-password">New password</label>
        <input type="password" size="25" maxlength="50" name="password">
        <div class="note">(7 character min.)</div>
      </div>
      <div class="field">
        <label for="confirm-password">Confirm new password</label>
        <input type="password" size="25" maxlength="50" name="passwordConfirm">
      </div>
      <div class="save-button">
        ${h.button("SAVE", onclick="updatePassword()" classes="orange")|n}
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  function updateProfile() {
    $('#profile-tab form').ajaxSubmit({target: '#profile-tab div.error-message'});
  }
  function updatePassword() {
    $('#password-tab form').ajaxSubmit({target: '#password-tab div.error-message'});
  }
</script>

