<div class="section-heading">
  <div class="controls">
    <label>
      List of fields:
    </label>
    <div class="settable-button">
      <a href="javascript:IMATA.document.set_type('brief')" class="button" id="ffields-brief">
        <span class="button-text">BRIEF</span>
      </a>
    </div>
    <div class="current-setting settable-button">
      <a href="javascript:IMATA.document.set_type('full')" class="button" id="ffields-full">
        <span class="button-text">FULL</span>
      </a>
    </div>
  </div>
  <img src="images/describe_the_image.png" class="name" alt="Describe the Image">
  <div id="document-wait" class="hidden">
    <img src="images/loading.gif" alt="Loading">
    <span id="document-wait-message">&nbsp;</span>
  </div>
  <hr class="section-splitter">
  <div class="required">
    <img src="images/required.png" alt="required">
    Required fields: Must be populated to contribute.
  </div>
</div>
<div id="document"></div>

<div class='column-footer'>
  <div style="float: left; position: relative; display: none;" id="pre-contribution-document-controls">
    ${h.button("CLEAR", onclick="IMATA.document.reset()", classes="brown incomplete")|n}
    ${h.button("COPY", onclick="IMATA.document.multicopy.start('#dialog', IMATA.assets.selected)")|n}
    ${h.button("SAVE", onclick="save_document()", classes="incomplete orange")|n}
    ${h.button("CONTRIBUTE", onclick="contribute_document()", classes="orange")|n}
  </div>
  <div style="float: left; position: relative; display: none;" id="post-contribution-document-controls">
    ${h.button("COPY", onclick="IMATA.document.multicopy.start('#dialog', IMATA.assets.selected)")|n}
  </div>
  <div id='contribute-multiple-footer' style="position: absolute; top: 3em; right: 0.75em">
    <a href="javascript:IMATA.document.multicontribute.start('#dialog')">
      Contribute Multiple Images
    </a>
  </div>
</div>

