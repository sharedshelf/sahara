<%inherit file="base.mako"/>
<% results_per_page = [25, 50, 100, 200, 300] %>
<% sort_class = dict(contribution_date='', contributor='', title='') %>

%if c.sort.startswith('contribution'):
  <% sort_class['contribution_date'] = 'asc' if c.sort[-3:] == 'asc' else 'desc' %>
%endif
%if c.sort.startswith('contributor'):
  <% sort_class['contributor'] = 'asc' if c.sort[-3:] == 'asc' else 'desc' %>
%endif
%if c.sort.startswith('title'):
  <% sort_class['title'] = 'asc' if c.sort[-3:] == 'asc' else 'desc' %>
%endif

%if (not c.view or c.view == 'thumb'):
  <% thumb_table_class = '' %>
  <% thumb_table_trigger_class = 'selected' %>
  <% list_table_class = 'hidden' %>
  <% list_table_trigger_class = '' %>
%elif (c.view == 'list'):
  <% thumb_table_class = 'hidden' %>
  <% thumb_table_trigger_class = '' %>
  <% list_table_class = '' %>
  <% list_table_trigger_class = 'selected' %>
%endif

<div>
  <div class="search-results-hd">
    % if c.locked is True:
    <h2>Edit ${ search_result.count } locked record(s)</h2>
    %else:
    <h2>Edit ${ search_result.count } record(s)</h2>
    %endif
    <p>Lock images below that you would like to edit. Locked images can only be edited by you.</p>
    
    <div class="new-search-wrapper">
      <a href="${h.url_for('bucket')}" title="" class="new-search" style="visibility:hidden;">New search</a>
      
      ${h.bucket_link("List view", page=c.page, view="list", classname=list_table_trigger_class)|n} | 
      ${h.bucket_link("Thumbnail view", page=c.page, view="thumb", classname=thumb_table_trigger_class)|n}
      
    </div>
  </div>

  ${control_bar()}

  %if c.error is True:
      &nbsp;&nbsp;There was an error processing your request, the administrator has been notified.
  %elif search_result.count is 0 and c.locked is False:
      &nbsp;&nbsp;No records are available for editing.
  %elif search_result.count is 0 and c.locked is True:
      &nbsp;&nbsp;You do not have any records locked.
  %else:
  

  %if not hasattr(c, 'view') or c.view == "thumb":
  <table class="thumb-view-table ${thumb_table_class}">
    <tbody>
      %for result in c.results:
      <tr>
        <td class="thumb-wrapper">
	        <a href="#" onclick="IMATA.dialogs.admin_edit('${result[u'uuid']}'); return false;">
            <img src="${g.ASSET_URL + result['media_uuid'] + '_size1'}" alt="thumbnail">
            %if result['suppress'] is True:
              <img src="/images/small_suppress.png" alt="" class="suppress-icon">
            %else:
              <img src="/images/small_suppress.png" alt="" class="suppress-icon" style="display:none">
            %endif
      	  </a>
        </td>
        <td class="info-wrapper">
          <dl>
            <dt>Filename</dt>
              <dd class="uuid">${result[u'uuid']|n}</dd>
            <dt>Creator name</dt>
              <dd>${", ".join(h.get_list_with_default(result, 'creator_name'))|n}</dd>
            <dt>Title/Name of work</dt>
              <dd>${", ".join(h.get_list_with_default(result, 'title'))|n}</dd>
            <dt>City</dt>
              <dd>${", ".join(h.get_list_with_default(result, 'city_county'))|n}</dd>
            <dt>Country</dt>
              <dd>${", ".join(h.get_list_with_default(result, 'country'))|n}</dd>
            <dt>Date</dt>
              <dd>${", ".join(h.get_list_with_default(result, 'display_date'))|n}</dd>
            <dt>Contributor</dt>
              <dd>${h.get_list_with_default(result, 'contributor')[0]|n}</dd>
            <dt>Date Contributed</dt>
              <dd>${result['contribution_date'].strftime("%m/%d/%Y")}</dd>
          </dl>
      	  <div style="width: 10em; padding-top: 1em; padding-bottom: 1em;">
      	    ${h.button("Full Record", onclick="IMATA.dialogs.admin_edit('"+result[u'uuid']+"')")|n}
      	  </div>
        </td>
        <td class="status-wrapper">
          ${ h.search_result_access_icon(result) |n}
        </td>
      </tr>
      %endfor
    </tbody>
  </table>
  %elif c.view == "list":
  <table class="list-view-table ${list_table_class}">
    <thead>
      ${table_header()}
    </thead>
    <tfoot>
      ${table_header()}
    </tfoot>
    <tbody>
    %for result in c.results:
      %if h.work_is_locked(result):
        <% rowclassname = 'locked' %>
      %else:
        <% rowclassname = '' %>
      %endif
      <tr class="${rowclassname}">
        <td>
          <div class="wrapper">
            ${h.search_result_access_icon_small(result)|n}
            <a href="#" title="" onclick="IMATA.dialogs.admin_edit('${result[u'uuid']}'); return false;">
              <span class="uuid">${result[u'uuid']}</span></a>
            <div class="tooltip" style="display:none;">
	      ${ h.search_result_access_icon(result) |n}
              <img src="${g.ASSET_URL + result['media_uuid'] + '_size1'}" alt="">
            </div>
          </div>
        </td>
        <td>${h.get_search_result_value(result, 'creator_name')|n}</td>
        <td>${h.get_search_result_value(result, 'title')|n}</td>
        <td>${h.get_search_result_value(result, 'city_county')|n}</td>
        <td>${h.get_search_result_value(result, 'country')|n}</td>
        <td>${h.get_search_result_value(result, 'display_date')|n}</td>
        <td>${h.get_search_result_value(result, 'contributor')|n}</td>
        <td>${result['contribution_date'].strftime("%m/%d/%Y")}</td>
      </tr>
    %endfor
    </tbody>
  </table>
  %endif
  
 <!--  {control_bar()} -->
  
  %endif
</div>

<%include file="refer_dialogs.mako" />

<%def name="control_bar()">
  <table class="control-bar">
    <tr>
      <td class="control-bar-section">
        <span style="margin-left: 0.5em;">Records per page:</span>
        <form action="${h.url_for('bucket')}" method="get">
  	      
  	      <input type="hidden" value="${c.page}" name="page">
  	      <input type="hidden" value="${c.sort}" name="sort">
  	      <input type="hidden" value="${c.view}" name="view">
  	      
          <select name="rows" onChange="form.submit()">
  	        %for result_size in results_per_page:
  	          %if result_size == c.rows:
                <option selected="true">${result_size}</option>
              %else:
                <option>${result_size}</option>
              %endif
            %endfor
          </select>
        </form>
        <!-- <span class="separator">&nbsp;</span> -->
      </td>
      <!-- </div>
      <div class="control-bar-section"> -->
      <td class="control-bar-section">
        <a href="/edit?view=${c.view}">Show all</a> | <a href="/edit?locked=true&view=${c.view}">Show locked</a>
      </td>
      <!--   <span class="separator">&nbsp;</span>
      </div>
      <div class="control-bar-section"> -->
      <td class="control-bar-section">
        <a href="#" class="lock-all">Lock all</a> | 
        <a href="#" class="unlock-all">Unlock all</a>
      </td>
        <!-- <span class="separator">&nbsp;</span>
      </div>
      <div class="control-bar-section sort-by-section"> -->
      <td class="control-bar-section sort-by-section">  
        <span><b>Sort by:</b></span>
        <span class="sort by-date-contributed ${sort_class['contribution_date']}">Date contributed</span>
        <span class="sort by-contributor ${sort_class['contributor']}">Contributor</span>
        <span class="sort by-title ${sort_class['title']}">Title/Name of work</span>
        <!-- <span class="separator" style="padding-left:0">&nbsp;</span> -->
      </td>
      <!-- </div>
      <div class="control-bar-section nav-control-section"> -->
      <td class="control-bar-section nav-control-section">
        %if c.page > 1:
  	      ${h.bucket_link('&laquo;', page=1)|n}
  	      ${h.bucket_link('&lsaquo;', page=c.page-1)|n}
        %else:	  
          <a href="#" class="nav-control">&laquo;</a>
        	<a href="#" class="nav-control">&lsaquo;</a>
        %endif
        Page <input type="text" name="" value="${c.page}" title="" class="text"> of ${search_result.num_pages}
        %if c.page < search_result.num_pages:
  	      ${h.bucket_link('&rsaquo;', page=c.page+1)|n}
  	      ${h.bucket_link('&raquo;', page=search_result.num_pages)|n}
        %else:
          <a href="#" class="nav-control">&rsaquo;</a>
          <a href="#" class="nav-control">&raquo;</a>
        %endif
      </td>
    </tr>
  </table>
  <input type="hidden" value="${search_result.num_pages}" id="last-page-num">
</%def>

<%def name="table_header()">
  <tr>
    <th>Filename</th>
    <th>Creator name</th>
    <th>Title/name of work</th>
    <th>City</th>
    <th>Country</th>
    <th>Date</th>
    <th>Contributor</th>
    <th>Contribution date</th>
  </tr>
</%def>

<%def name="css()">
  <link rel="stylesheet" href="stylesheets/docit.css" type="text/css">
  <link rel="stylesheet" href="stylesheets/dialog.css" type="text/css">
  <link rel="stylesheet" href="stylesheets/ui.tabs.css" type="text/css">
  <link rel="stylesheet" href="stylesheets/imata.css" type="text/css">
  <link rel="stylesheet" href="stylesheets/autocomplete.css" type="text/css">
</%def>

<%def name="javascript()">
  <script type="text/javascript" src="javascript/yui/yahoo-dom-event.js"></script>
  <script type="text/javascript" src="javascript/yui/connection-min.js"></script>
  <script type="text/javascript" src="javascript/yui/animation-min.js"></script>
  <script type="text/javascript" src="javascript/yui/datasource-min.js"></script>
  <script type="text/javascript" src="javascript/yui/autocomplete-min.js"></script>
  <script type="text/javascript" src="javascript/ui/ui.core.js"></script>
  <script type="text/javascript" src="javascript/ui/ui.tabs.js"></script>
  <script type="text/javascript" src="javascript/jquery.form.js"></script>
  <script type="text/javascript" src="javascript/jquery.cookie.js"></script>
  <script type="text/javascript" src="javascript/splitter.js"></script>
  <script type="text/javascript" src="javascript/jquery.scrollTo-min.js"></script>
  <script type="text/javascript" src="javascript/jquery.selectable.js"></script>
  <script type="text/javascript" src="javascript/jquery.positionBy.js"></script>
  <script type="text/javascript" src="javascript/jquery.dimensions.js"></script>
  <script type="text/javascript" src="javascript/work_edit_form.js"></script>
  <script type="text/javascript" src="javascript/jtip.js"></script>
  <script type="text/javascript" src="javascript/jquery.bt.js"></script>
  <script type="text/javascript" src="javascript/docit.js"></script>
  <script type="text/javascript" src="javascript/app/edit_tab.js"></script>
</%def>

<%def name="navigation()">
  <%include file="navigation.mako" />
</%def>
