<%inherit file="base.mako" />

<div class="sah-2" id="home-wrapper">
  <div id="left-sidebar">
    <div class="inner-wrapper">
      <h2>Search</h2>
      <p class="errors hidden">Please enter your search criteria</p>
      <form id="search-form" action="/search">
        <div>
          <input type="text" name="phrase" title="Search" class="text">
          <input type="submit" value="GO" class="button">
        </div>
      </form>
      <h2 class="editorial-links-hd">Editorial Links</h2>
      <div class="editorial-links-box">
        <div class="inner-wrapper">
          <span class="count">
            ${c.waiting}
          </span>
          <a href="/edit" title="">Images awaiting editorial review</a>
        </div>
      </div>
      <div class="editorial-links-box">
        <div class="inner-wrapper">
          <span class="count">
            ${c.locked}
          </span>
          <a href="/edit?locked=true" title="">Images that you are currently editing</a>
        </div>
      </div>
    </div>
  </div>
  <div id="right-sidebar">
    <div class="add-images-box">
      <h2>Add your images to the SAHARA Collection</h2>
      <span>
        <a href="/contribute" title="">Contribute</a>
      </span>
    </div>
    <h2 class="stats-hd">SAHARA Stats</h2>

    %if c.user.is_admin:
      <a href="http://stats.artstor.org/stats/sahara.asp" class="stats-site-link" target="_blank">Go to stats site</a>
    %endif
    
    <div class="stats-section">
      <span class="count">${c.num_of_contributors}</span>
      <p>Contributors to SAHARA</p>
    </div>
    
    <div class="stats-section">
      <span class="count">${c.editors_collection_total}</span>
      <p>Images in SAHARA Editors' Choice Collection</p>
    </div>
    
    <div class="stats-section">
      <span class="count">${c.members_collection_total}</span>
      <p>Images in SAHARA Members' Collection</p>
    </div>
    
    <div class="stats-section total">
      <span class="count">${c.editors_collection_total + c.members_collection_total}</span>
      <p>
        <span class="upper">Total</span> image in 
        <span class="upper">
          <a href="${g.COLLECTION_URL}" title="" id="sdl">SAHARA</a>
        </span>
      </p>
    </div>
  </div>
</div>

<%def name="css()"></%def>
<%def name="javascript()">
  <script src="javascript/app/home_tab.js" type="text/javascript"></script>
</%def>
<%def name="tools_menu()"></%def>
<%def name="navigation()">
  <%include file="navigation.mako"/>
</%def>
