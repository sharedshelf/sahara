<%page args="progressing, contributed" />

<div class="float-left" id="left-column">
  <div class="section-heading">
	<div class='controls'>Thumbnails: 
	  <a href="/image-panel/thumbnail/size/0" class="${h.size_selected(0)}"
		 id='thumbnail-small' onclick="updateThumbnails(0)">small</a> | 
	  <a href="/image-panel/thumbnail/size/1" class="${h.size_selected(1)}"
		 id='thumbnail-medium' onclick="updateThumbnails(1)">medium</a> | 
	  <a href="/image-panel/thumbnail/size/2" class="${h.size_selected(2)}"
		 id='thumbnail-large' onclick="updateThumbnails(2)">large</a>
	</div>
	<div class="header-number">1</div>Select an Image
  </div>
  <div id="folder-wrapper">
	<ul>
	  <li><a href="/works;in_process">
		  <span>In Progress
			<div id='progressing-count' class='inline'>
			  (${progressing})
			</div>
		  </span>
		</a>
	  </li>
	  <li><a href="/works;contributed">
		  <span>Contributed
			<div id='contributed-count' class='inline'>
			  (${contributed})
			</div>
		  </span>
		</a>
	  </li>
	</ul>
	<br/>
	<br/>
  </div>
</div>
