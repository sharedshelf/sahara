<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">
  
<html lang="en-us">
  <head>
    <title>IMATA Login</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    
    <link rel="stylesheet" href="stylesheets/login.css" type="text/css" media="screen">
    <link rel="stylesheet" href="stylesheets/imata.css" type="text/css" media="screen">
    
    <script type="text/javascript" src="javascript/jquery.js"></script>
    <script type="text/javascript" src="javascript/login.js"></script>
  </head>
  <body>
    <div id="header">
      <img src="images/icon.gif" alt="">
      Welcome to the SAH Architecture Resources Archive.
    </div>
    <div id="content">
      <div id="fail">
        <div class="center">
          <h2>
            SAH ARA requires Javascript to be enabled.
          </h2>
          Javascript does not appear to be enabled, please enable Javascript and try again.
        </div>
      </div>
      <div class="center">
        <p class="error">
          &nbsp;${c.error}
        </p>
        <h2>Log in</h2>
        <form action="${h.url_for(controller='login', action='authenticate')}" method="post" id="login-form">
          <div>
            <label for="first">Email address:</label>
            <input type="text" id="first" name="username">
          </div>
          <div>
            <label for="password">Password:</label>
            <input type="password" name="password" id="password">
          </div>
          <div>
            <a href="#">Forgot your password?</a>
          </div>
          <input type="submit" style="display:none">
          <div class="button-container">
            <a href="#" id="login-btn" class="button">
              <span class="button-text">
                LOG IN
              </span>
            </a>
          </div>
        </form>
      </div>
      <div class="center">
        <p>
          Not sure if you're eligible to contribute to the SAH ARA collection?
        </p>
        <ul>
          <li>
            If you are an SAH member, contact the <a href="mailto:membership@sah.org">SAH ARA Manager of Membership Services</a> to inquire about registering.
          </li>
          <li>
            If you are not an SAH member, go to the 
            <a href="http://sah.org/index.php?src=gendocs&amp;ref=AVRN%20Press%20Release&amp;category=AVRN" target="new">SAH Website</a> 
            to learn more about the benefits of membership.
          </li>
        </ul>
      </div>
      <div class="center">
        <p>
          Learn more about the SAH ARA Project.
        </p>
        <ul>
          <li>
            <a href="http://sah.org/index.php?src=gendocs&amp;ref=AVRN">SAH ARA project information</a>
          </li>
        </ul>
      </div>
    </div>
  </body>
</html>
