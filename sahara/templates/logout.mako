<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">
  <head>
    <title>IMATA</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="/stylesheets/login.css" type="text/css" media="screen" charset="utf-8" />
    <link rel="stylesheet" href="/stylesheets/imata.css" type="text/css" media="screen" charset="utf-8" />
  </head>
  <body>
	<div id="content">
	  <div id="header">
		<img src="/images/icon.gif"/>&nbsp;&nbsp;SAH Architecture Resources Archive.
	  </div>

      <div class="center">
        <h2>You've been logged out.</h2><br/><br/>
		<a href="${g.ARTSTOR_LOGIN_URL}">Log in</a> to re-enter.
      </div>
      <div class="center">
        <p>Learn more about the SAH ARA Project.</p>
        <ul>
          <li><a href="http://sah.org/index.php?src=gendocs&
					   ref=AVRN">SAH ARA project information</a></li>
        </ul>
      </div>
	</div>
  </body>
</html>
