<ul id="tools-menu">
    <li>
        <a href="http://www.sah.org/index.php?src=gendocs&ref=AVRN&category=AVRN" target="blank">About SAHARA Project</a>
    </li>
    <li>
        <a href="http://www.sah.org/index.php?src=gendocs&ref=AVRN&category=AVRN" target="blank">How to Contribute</a>
    </li>
    <li>
        <a href="${g.COLLECTION_URL}" target="blank">View SAHARA Collection</a>
    </li>
    <li>
        <a href="#" title="Help" id="help-btn">Help</a>
    </li>
</ul>
