%if c.user.is_privileged:
  <style type="text/css" media="screen">
    #content { top: 85px; }
  </style>
  <ul>
    <li>
      <a href="/home" title="Home">Home</a>
    </li>
    <li>
      <a href="/contribute" title="Contribute">Contribute</a>
    </li>
    <li>
      <a href="/search" title="Search">Search</a>
    </li>
    %if c.user.is_admin:
      <li>
        <a href="/edit" title="Edit">Edit</a>
      </li>
      <li class="last">
        <a href="/administer" title="Administer">Administer</a>
      </li>
    %else:
      <li class="last">
        <a href="/edit" title="Edit">Edit</a>
      </li>
    %endif
  </ul>
%endif
