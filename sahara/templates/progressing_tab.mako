<div id="progressing">
  %if len(c.works) > 0:
  <%include file="asset_sort_controls.mako" args="notify='IMATA.assets.resort'"/>
  %endif
  <div class="assets">
    %if len(c.works) > 0:
      <p>
        Maximum ${g.MAX_SLOTS} images in this palette at a time.<br>
        Single click to select.  Double click for image options.<br>
      </p>

      %for work in c.works:
      <div class="thumbnail-size${c.size} asset">
        <div>
          %if work.status.valid:
          <img src="images/valid.png" class="valid-img" alt="valid-flag">
          <div id="${work.uuid}" class="img-wrapper valid">
          %elif not work.is_edited():
          <img src="images/valid.png" class="valid-img" alt="valid-flag" style="display: none;">
          <div id="${work.uuid}" class="img-wrapper new">
          %else:
          <img src="images/valid.png" class="valid-img" alt="valid-flag" style="display: none;">
          <div id="${work.uuid}" class="img-wrapper">
          %endif
            <img src="${work.thumbnail(c.size)}" alt="thumbnail"
                 onclick="IMATA.assets.select_single('${work.uuid}', this)"
                 ondblclick="expatiate('${work.uuid}', '${work.media_category}',
                             '${work.media_format}', '${work.media_width}', '${work.media_height}');">
          </div>
          <p>
            <a href="${h.url_for('tooltip', uuid=work.uuid)}" class="jTip"
               id="caption-${work.uuid}"
               name="Asset Information">
              ${work.brief_filename(c.size)}
            </a>
          </p>
        </div>
      </div>
      %endfor
    %else :
      <div style="margin:0 auto; width:100%; text-align: center; margin-top: 1em; height: 10000px;">
        <img src="images/upload_bg.png" alt="" style="display: block; margin-bottom: 1em;">
        The "In progress" tab is currently empty.
        <br>
        To get started
        <a href="javascript:IMATA.upload.prompt();" class="button" style="width:10em; margin: 1em auto 0 auto; margin-top: 1em; background-color: #b64404">
          <span class="button-text" style="background-color: #b64404">ADD FILES</span>
        </a>
      </div>
    %endif
  </div>
  <div class="column-footer">
    ${h.button('ADD', onclick="IMATA.upload.prompt()")|n}
    ${h.button('REMOVE', onclick="IMATA.assets.remove_selected()")|n}
    ${h.button('REMOVE MULTIPLE', onclick="IMATA.assets.remove_multiple.start('#dialog')")|n}
    
    <p class="footer-links-wrapper">
      <a href="#" onclick="IMATA.dialogs.show_text('/fullterms.html', 'TERMS AND CONDITIONS OF USE')">SAHARA Terms of Use</a>
      <a href="#" onclick="IMATA.dialogs.show_text('/privacy.html', 'PRIVACY POLICY')">SAHara Privacy Policy</a>
    </p>
  </div>
</div>
