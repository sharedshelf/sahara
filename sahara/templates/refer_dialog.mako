<div>
  <div class="dialog-header">
    <img src="/images/dialog-icon.png">
    <span class="filename">&nbsp;</span>
    <div class="close-button">
      ${h.button("", onclick="$('#refer-dialog').jqmHide()")|n}
      CLOSE
    </div>
  </div>
  <div>
    <div class="body">
      <div style="overflow:hidden;">
        <img src="" alt="" class="thumb">
        <h2>Refer this record</h2>
        <p>Once a record is referred you may not be able to edit it. Select a user or users below.</p>
      </div>
      <p class="status-message">&nbsp;</p>
      <ul class="roles">
        <li class="role">
          <h3>
            <input type="radio" name="role" value="" id="role-administrator">
            <label for="role-administrator">Administrator</label>
          </h3>
          <ul class="any">
            <li>
              <input type="checkbox" id="any-administrator">
              <label for="any-administrator">Any administrator</label>
            </li>
          </ul>
          <ul class="administrators list">
            <li>
              &nbsp;
            </li>
          </ul>
        </li>
        <li class="role">
          <h3>
            <input type="radio" name="role" value="" id="role-content-editor">
            <label for="role-content-editor">Content Editor</label>
          </h3>
          <ul class="any">
            <li>
              <input type="checkbox" id="any-content-editor">
              <label for="any-content-editor">Any content editor</label>
            </li>
          </ul>
          <ul class="content-editors list">
            <li>&nbsp;</li>
          </ul>
        </li>
        <li class="role">
          <h3>
            <input type="radio" name="role" value="" id="role-librarian">
            <label for="role-librarian">Librarian</label>
          </h3>
          <ul class="any">
            <li>
              <input type="checkbox" id="any-librarian">
              <label for="any-librarian">Any librarian</label>
            </li>
          </ul>
          <ul class="librarians list">
            <li>&nbsp;</li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="footer">
      <input type="checkbox" name="remember-selection" id="remember-selection">
      <label for="remember-selection">Remember my selection(s)</label>

      ${h.button("NEXT &raquo;", onclick="", classes="next float-right")|n}
      ${h.button("CANCEL", onclick="", classes="brown cancel float-right")|n}
      <!--
      <button type="button" class="next">Next &raquo;</button>
      <a href="#" title="" class="cancel">Cancel</a> -->
    </div>
  </div>
</div>
