<div>
  <div class="dialog-header">
    <img src="/images/dialog-icon.png">
    <!-- <a href="#" class="close-trigger">Close [x]</a> -->
  </div>
  <div>
    <div class="body">
      <p class="large">Are you sure you want to cancel or close this window?</p>
      <p>This record will not be referred</p>
    </div>
    <div class="footer">
      ${h.button("NO", onclick="", classes="no float-right")|n}
      ${h.button("YES", onclick="", classes="brown yes float-right")|n}
    </div>
  </div>
</div>
