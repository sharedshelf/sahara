<div>
  <div class="dialog-header">
    <img src="/images/dialog-icon.png">
    <span class="filename">&nbsp;</span>
  </div>
  <div>
    <div class="body">
      <p class="large">Are you sure that you want to refer this record</p>
      <div class="refer-list">
        <img src="" alt="" class="floated">
        <span class="floated">to</span>
        <ul class="floated" style="height: 128px; width: 100px; overflow-y: auto; overflow-x: hidden;">
          <li>&nbsp;</li>
        </ul>
        <span class="floated">?</span>
      </div>
    </div>
    <div class="footer">
      <span style="font-weight:bold;">Click "Back" to edit selections.</span>
      ${h.button("REFER", onclick="", classes="orange refer float-right")|n}
      ${h.button("BACK", onclick="", classes="back  float-right")|n}
      ${h.button("CANCEL", onclick="", classes="cancel brown float-right")|n}
    </div>
  </div>
</div>
