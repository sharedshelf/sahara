<%page args="src"/>

<div class="section">
  <div class='document-section-heading editorial-notes'>
    <span>Editorial Information</span>
  </div>
  
  <div class="large editorial-section">
    %if c.work.updated_on > c.work.status.contributed_on:
      <p class="updates-info">
      </p>
    %endif
    <div class="contributor-info">
      <div class="floated left">
        <label>Contributor:</label>
        <a href="mailto:${h.original_contributor(c.work, c.editors)}">
          ${h.original_contributor(c.work, c.editors)[:25]}</a>
      </div>
      <div class="floated right">
        <label>Date of contribution:</label>
        <span>${ c.work.status.contributed_on.strftime('%m/%d/%Y %H:%M:%S') }</span>
      </div>
    </div>
    <p style="margin-bottom: 0.5em;">
      <label>Notes</label>
    </p>
    <textarea class="full"
              name="editorial_note_body"
              onfocus="IMATA.document.highlight_section('editorial-notes')"
              onblur="IMATA.document.clear_section_highlights()"
              tabindex="5"
              rows="5"
              cols="57"
              style="margin-bottom:0.5em;">${src.body}</textarea>
    <p>(Share notes with other Editors)</p>
  </div>
</div>
