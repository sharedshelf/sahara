
<div id="admin-asset-edit-dialog">
  <div class="dialog-header">
    <div class="close-button">
      <a href="#" class="button edit-dialog-close-button"><span></span></a>
      CLOSE
    </div>
    <img src="../images/dialog-icon.png" alt="icon">
    &#160;${c.work.media_filename}
    
    <input type="hidden" value="${ c.work.uuid }" id="work-id">
    <input type="hidden" value="${ c.work.thumbnail(1) }" id="work-image-url">
    <input type="hidden" value="${ c.work.media_filename }" id="work-filename">
    <input type="hidden" value="${ h.is_locked(c.work) }" id="is-work-locked">
    
    %if h.is_admin():
      ${h.button("UN/SUPPRESS", id="toggle_suppression", onclick="toggle_suppression('"+c.work.uuid+"')", classes="brown")|n}
    %endif
    
  </div>
  
  <ul id="admin-asset-manipulation-tabs">
    %if h.is_readonly(c.work):
      <li><a href="#admin-asset-information-pane"><span>Edit descriptive data</span></a></li>
      <li style="display:none;"><a href="#admin-asset-transform-pane"><span>Rotate and flip images</span></a></li>
      <li style="display:none;"><a href="#admin-asset-replacement-pane"><span>Replace images</span></a></li>
    %else:
      <li><a href="#admin-asset-information-pane"><span>Edit descriptive data</span></a></li>
      <li><a href="#admin-asset-transform-pane"><span>Rotate and flip images</span></a></li>
      <li><a href="#admin-asset-replacement-pane"><span>Replace images</span></a></li>
    %endif
  </ul>
  <div id="admin-asset-information-pane">
    %if c.work.status.suppress is True:
      <div class="status suppress"></div>
    %else:
      <div class="status"></div>
    %endif
    <div class="asset-data">
      <div class="asset-image" onclick="window.open('${c.imageviewer_url}')" style="background-image: url(${c.work.thumbnail(1)})"></div>
      <p class="click-invitation">${c.work.media_width}x${c.work.media_height}</p>
      <div class="change-history">
      ${h.button("PREVIEW", onclick="preview('"+c.work.uuid+"')", classes="float-left")|n}
        <h3 style="margin-top:0.5em;margin-bottom:0.5em;">
          Change log
          <span style="font-size: 1em; font-weight: normal; margin-left: 0.5em;">
            (<a href="#" id="changelog-toggler">Show</a>)
          </span>
        </h3>
        <div id="changelog">
          <div class="latest">
            <p class="title">${c.work.updated_on.strftime("%m/%d/%Y %I:%M%p")}</p>
            ${h.editors_email(c.work.updated_by)}
  	        <p>
  	          <a href="#" onclick="rollback('${c.work.uuid}', 'current'); return false;">view</a>
  	        </p>
          </div>
          %for hist in c.work.history[0:4]:
          <div class="entry">
            <p class="title">${hist.updated_on.strftime("%m/%d/%Y %I:%M%p")}</p>
            ${h.editors_email(hist.updated_by)}
  	        <p>
  	          <a href="#" onclick="rollback('${c.work.uuid}', '${hist.id}'); return false;">view</a>
  	        </p>
          </div>
          %endfor
        </div>
      </div>
    </div>
    <div class="admin-document-edit-form">
      <div class="replacable_form">
        <div id="edit-form-status-box"></div>
        <div id="document-messaging"></div>
      	<%include file="editorial_notes_section.mako" args="src=h.get_editorial_notes(c.work)"/>
        <%include file="../work_form.mako"/>
      </div>
    </div>
    <div class="dialog-footer">
      %if h.is_readonly(c.work):
        ${h.button("COPY", onclick="IMATA.document.multicopy.start('#dialog', '"+c.work.uuid+"', true)", classes="orange float-right")|n}
      %else:
        %if c.work.status.suppress is False:
          %if c.cataloger is True:
             ${h.button("PUBLISH", id="publish-trigger", onclick="IMATA.document.publish('"+c.work.uuid+"', '"+str(c.current_collection)+"')", classes="orange float-right")|n}
          %else:
             ${h.button("PUBLISH", id="publish-trigger", onclick="select_destination_for_publishing('"+c.work.uuid+"')", classes="orange float-right")|n}
          %endif
        %endif
        ${h.button("REFER", id="refer-trigger", classes="orange float-right")|n}
        ${h.button("SAVE", id="save-trigger", classes="orange float-right")|n}
        ${h.button("COPY", onclick="IMATA.document.multicopy.start('#dialog', '"+c.work.uuid+"', true)", classes="orange float-right")|n}
      %endif
    </div>
  </div>
  <div id="admin-asset-transform-pane">
    <div style="width:100%;margin-left:1em">
      <img src="images/exclamation.png" alt="">&nbsp;
      Note: The quality of JPEGs degrades with each transformation.
    </div>
    <div id="flip-assets">
      <div class="dialog-image-area">
        <img class="centered-image" id="flip-img" src="${c.work.thumbnail(2)}" alt="thumbnail">
      </div>
    </div>
    <span class="error">&nbsp;</span>
    <div class="dialog-footer">
      ${h.button("Rotate", onclick="IMATA.stor.flip('right', '"+c.work.uuid+"')", classes="flip-cw")|n}
      ${h.button("Rotate", onclick="IMATA.stor.flip('left', '"+c.work.uuid+"')", classes="flip-ccw")|n}
      ${h.button("Flip Vertically", onclick="IMATA.stor.flip('flip', '"+c.work.uuid+"')", classes="flip-vertical")|n}
      ${h.button("Flip Horizontally", onclick="IMATA.stor.flip('mirror', '"+c.work.uuid+"')", classes="flip-horizontally")|n}
      ${h.button("SAVE", onclick="IMATA.stor.admin_save('"+c.work.uuid+"')", classes="orange float-right")|n}
      <div class="wait" style="display: none;"><img src="images/loading.gif" alt=""> Saving...</div>
    </div>
  </div>
  <div id="admin-asset-replacement-pane">
    <h2>Replace image</h2>
    <p>Supported formats include: JPEG, GIF, PNG, TIFF</p>
    <form class="replace-asset" action="${h.url_for('replace', uuid=c.work.uuid)}"
          method="post" enctype="multipart/form-data">
      <input type="file" size="45" name="asset-file">
    </form>
    <span class="error">&nbsp;</span>
    <div class="dialog-footer">
      ${h.button("REPLACE", onclick="admin_replace_asset('"+c.work.uuid+"')", classes="orange float-right")|n}
      ${h.button("CLEAR", onclick="IMATA.stor.replace_reset()", classes="brown float-right")|n}
      <div class="wait" style="display: none;"><img src="images/loading.gif" alt=""> Replacing...</div>
    </div>
  </div>
</div>

<div id="edit-save-confirm-dialog" style="display:none;" class="dialog">
  <div class="dialog-header">&nbsp;</div>
  <div>
    <h2>Do you want to save your edits?</h2>
  </div>
  <div class="dialog-footer">
    ${h.button("NO", onclick="", classes="no float-right")|n}
    ${h.button("YES", onclick="", classes="brown yes float-right")|n}
  </div>
</div>

<script type="text/javascript" src="javascript/app/refer_dialog.js"></script>
