<%inherit file="../base.mako"/>
<% result_sizes = [25, 50, 100, 200, 300] %>
<img id="thumbnail_suppression_flag" src="images/small_suppress.png">
<div id="search_thumbnail"></div>
<div id="search-results">
  <div class="header">
    <div class="new-search">
      <a href="${h.url_for('search')}" class="button orange">
        <span class="button-text orange">New Search</span>
      </a>
    </div>
    <span class="heading">Results</span>&nbsp;&nbsp;(<span class="phrase">${phrase}</span>)
  </div>
  %if error is True:
    <div class="scrolling">
      %if paginated_response is not None and paginated_response.count is 0:
        &nbsp;&nbsp;Your search returned 0 results.
      %else:
        &nbsp;&nbsp;There was an error processing your search, the administator has been notified.
      %endif
    </div>
  %else:  
    <table>
      <tr>
        <th class="count">
          ${paginated_response.count} Record(s)
        </th>
        <th class="perpage">
          Records per page
          <form action="${h.url_for('search')}" method="get">
            <input name="phrase" type="hidden" value="${phrase}">
            <input name="tabular" type="hidden" value="${tabular}">
            <select name="rows" onChange="form.submit()">
              %for result_size in result_sizes:
                %if result_size == rows:
                  <option selected="true">${result_size}</option>
                %else:
                  <option>${result_size}</option>
                %endif
              %endfor
            </select>
          </form>
        </th>
        <th>
          <form id="result-view" action="${h.url_for('search')}" method="get">
            <input name="phrase" type="hidden" value="${phrase}">
            <input name="tabular" type="hidden" value="${tabular}">
            <input name="rows" type="hidden" value="${rows}">
            <input name="page" type="hidden" value="${results.number}">
          </form>
          %if tabular is False:
            ${h.search_link(phrase, 'List View', page=results.number, rows=rows, tabular=True)|n} | Thumbnail View
          %else:
            List View | ${h.search_link(phrase, 'Thumbnail View', page=results.number, rows=rows, tabular=False)|n}
          %endif
        </th>
        <th>&nbsp;</th>
        <th class="pagination">
          %if results.has_other_pages() is True:
            %if results.has_previous():
              ${h.search_link(phrase, '&lt;&lt;', page=1, tabular=tabular, rows=rows)|n}
              &nbsp;&nbsp;
              ${h.search_link(phrase, '&lt;', page=results.number-1, tabular=tabular, rows=rows)|n}
              &nbsp;&nbsp;
            %else:
              &lt;&lt;&nbsp;&nbsp;&lt;&nbsp;&nbsp;
            %endif
            Page ${results.number} of ${paginated_response.num_pages}&nbsp;&nbsp;
            %if results.has_next():
              ${h.search_link(phrase, '&gt;', page=results.number+1, tabular=tabular, rows=rows)|n}
              &nbsp;&nbsp;
              ${h.search_link(phrase, '&gt;&gt;', page=paginated_response.num_pages, tabular=tabular, rows=rows)|n}
            %else:
               &gt;&nbsp;&nbsp;&gt;&gt;
            %endif
          %else:
            &lt;&lt;&nbsp;&nbsp;&lt;&nbsp;&nbsp;&nbsp;Page 1 of 1&nbsp;&nbsp;&nbsp;&gt;&nbsp;&nbsp;&gt;&gt;
          %endif
        </th>
      </tr>
    </table>
  %endif
  
  ${next.body()}

  <div id="terms-footer">
    <a href="#" onclick="IMATA.dialogs.show_text('${h.url_for('fullterms')}', 'TERMS AND CONDITIONS OF USE'); return false;">
      SAHARA Terms of Use
    </a>
    |
    <a href="#" onclick="IMATA.dialogs.show_text('${h.url_for('privacy')}', 'PRIVACY POLICY'); return false;">
      SAHARA Privacy Policy
    </a>
  </div>
</div>

<%def name="css()">
<link rel="stylesheet" href="stylesheets/docit.css" type="text/css" media="screen">
<link rel="stylesheet" href="stylesheets/dialog.css" type="text/css" media="screen">
<link rel="stylesheet" href="stylesheets/ui.tabs.css" type="text/css" media="screen">
<link rel="stylesheet" href="stylesheets/imata.css" type="text/css" media="screen">
<link rel="stylesheet" href="stylesheets/autocomplete.css" type="text/css" media="screen">
<!--[if IE]><link rel="stylesheet" href="stylesheets/ie.css" type="text/css"><![endif]-->
</%def>

<%def name="javascript()">
<script type="text/javascript" src="javascript/yui/yahoo-dom-event.js">
</script>
<script type="text/javascript" src="javascript/yui/connection-min.js">
</script>
<script type="text/javascript" src="javascript/yui/animation-min.js">
</script>
<script type="text/javascript" src="javascript/yui/datasource-min.js">
</script>
<script type="text/javascript" src="javascript/yui/autocomplete-min.js">
</script>
<script type="text/javascript" src="javascript/jquery-1.2.6.min.js">
</script>
<script type="text/javascript" src="javascript/ui/ui.core.js">
</script>
<script type="text/javascript" src="javascript/ui/ui.tabs.js">
</script>
<script type="text/javascript" src="javascript/jqModal.js">
</script>
<script type="text/javascript" src="javascript/jquery.form.js">
</script>
<script type="text/javascript" src="javascript/jquery.cookie.js">
</script>
<script type="text/javascript" src="javascript/splitter.js">
</script>
<script type="text/javascript" src="javascript/jquery.scrollTo-min.js">
</script>
<script type="text/javascript" src="javascript/jquery.selectable.js">
</script>
<script type="text/javascript" src="javascript/jquery.positionBy.js">
</script>
<script type="text/javascript" src="javascript/jquery.dimensions.js">
</script>
<script type="text/javascript" src="javascript/work_edit_form.js">
</script>
<script type="text/javascript" src="javascript/application.js">
</script>
<script type="text/javascript" src="javascript/docit.js">
</script>
<script type="text/javascript" src="javascript/jtip.js">
</script>
<script type="text/javascript" src="javascript/jquery.bt.js">
</script>
</%def>

<%def name="tools_menu()">
<ul id="tools-menu">
  <li>
    <a href="http://www.sah.org/index.php?src=gendocs&amp;ref=AVRN&amp;category=AVRN" target="blank">About SAHARA Project</a>
  </li>
  <li>
    <a href="http://www.sah.org/index.php?src=gendocs&amp;ref=AVRN&amp;category=AVRN" target="blank">How to Contribute</a>
  </li>
  <li>
    <a href="${h.url_for('home')}">Cataloging Workspace</a>
  </li>
  <li>
    <a href="${g.COLLECTION_URL}" target="blank">View SAHARA Collection</a>
  </li>
  <li>
    <a href="#" title="Help" id="help-btn">Help</a>
  </li>
</ul>
</%def>
