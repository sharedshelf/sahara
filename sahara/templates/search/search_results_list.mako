<%inherit file="result_base.mako"/>
%if error is not True:
  <div class="scrolling">
    %for result in results.result:
      <div class="result_row" id="${result[u'uuid']}">
        %if result[u'suppress'] is True:
          <div class="status suppress"></div>
        %else:
          <div class="status"></div>
        %endif
        <div class="image">
          <a href="#" onclick="IMATA.dialogs.admin_edit('${result[u'uuid']}'); return false;">
            <img src="${g.ASSET_URL + result['media_uuid'] + '_size1'}" alt="thumbnail image">
          </a>
        </div>
        <div class="data">
          <div class="line uuid">
            <span class="label">Identifier</span>${result[u'uuid']}
          </div>
          <div class="line even creator">
            <span class="label">Creator name</span>${", ".join(h.get_list_with_default(result, 'creator_name'))}<br>
          </div>
          <div class="line title">
            <span class="label">Title/Name of work</span>${", ".join(h.get_list_with_default(result, 'title'))}<br>
          </div>
          <div class="line even city">
            <span class="label">City</span>${", ".join(h.get_list_with_default(result, 'city_county'))}<br>
          </div>
          <div class="line country">
            <span class="label">Country</span>${", ".join(h.get_list_with_default(result, 'country'))}<br>
          </div>
          <div class="line even date">
            <span class="label">Date</span>${", ".join(h.get_list_with_default(result, 'display_date'))}<br>
          </div>
          <div class="line contributor">
            <span class="label">Contributor</span>${h.get_list_with_default(result, 'contributor')[0]}<br>
          </div>
          <div class="search-control">
	          ${h.button("Edit Full Record", onclick="IMATA.dialogs.admin_edit('"+result[u'uuid']+"')")|n}
          </div>
        </div>
      </div>
    %endfor
  </div>
%endif

