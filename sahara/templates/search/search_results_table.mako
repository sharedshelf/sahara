<%inherit file="result_base.mako"/>
<div class="header-spacer">
  <table class="results-header">
    <tr class="header">
      <th class='id-column'>
        Id
      </th>
      <th>
        Creator name
      </th>
      <th>
        Title/Name of work
      </th>
      <th>
        City
      </th>
      <th>
        Country
      </th>
      <th>
        Date
      </th>
      <th>
        Contributor
      </th>
    </tr>
  </table>
</div>
<div class="table-container">
  <table class="tabular-results">
    %for result in results.result:
      <tr id="${result[u'uuid']}">
	<td class='id-column'>
	  <a onmouseover="show_thumbnail(this, '${g.ASSET_URL + result['media_uuid'] + '_size1'}', '${result['suppress']}')"
	     onmouseout="hide_thumbnail()"
	     onclick="IMATA.dialogs.admin_edit('${result[u'uuid']}')">${result['uuid']}</a></td>
	<td class="creator">${h.get_search_result_value(result, 'creator_name')|n}</td>
	<td class="title">${h.get_search_result_value(result, 'title')|n}</td>
	<td class="city">${h.get_search_result_value(result, 'city_county')|n}</td>
	<td class="country">${h.get_search_result_value(result, 'country')|n}</td>
	<td class="date">${h.get_search_result_value(result, 'display_date')|n}</td>
	<td class="contributor">${h.get_search_result_value(result, 'contributor')|n}</td>
      </tr>
    %endfor
  </table>
</div>
