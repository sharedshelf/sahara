<%inherit file="../base.mako"/>

<div id="simple-search">
  <h2>Search</h2>
  <span class="error-message">&nbsp;</span>
  <form action="${h.url_for('search')}" onsubmit="return search_validate()" method="GET">
    <input class="large" onkeypress="search_clear_error()" type="text" name="phrase">
    <input type="hidden" name="rows" value="25">
    <input type="submit" value="Go">
  </form>
  <br>
  <br>
  <span class="bold">How can I find an image quickly?</bold></span>
  <br>
  <br>
    Search by filename. You can find the image filename in the "File properties" tab of the
    Descriptive data window in the SAHARA Digital Library.
    (<a href="#" onclick="toggle_search_example(); return false;" id="search_example_toggle">Show example</a>)
</div>

<img id="search_example"
     src="images/search_screenshot.jpg"
     alt="example screenshot">

<%def name="css()">
  <link rel="stylesheet" href="stylesheets/docit.css" type="text/css" media="screen">
  <link rel="stylesheet" href="stylesheets/dialog.css" type="text/css" media="screen">
  <link rel="stylesheet" href="stylesheets/ui.tabs.css" type="text/css" media="screen">
  <link rel="stylesheet" href="stylesheets/imata.css" type="text/css" media="screen">
  <link rel="stylesheet" href="stylesheets/autocomplete.css" type="text/css" media="screen">
  <!--[if IE]><link rel="stylesheet" href="stylesheets/ie.css" type="text/css"><![endif]-->
</%def>


<%def name="javascript()">
<script type="text/javascript" src="javascript/yui/yahoo-dom-event.js">
</script>
<script type="text/javascript" src="javascript/yui/connection-min.js">
</script>
<script type="text/javascript" src="javascript/yui/animation-min.js">
</script>
<script type="text/javascript" src="javascript/yui/datasource-min.js">
</script>
<script type="text/javascript" src="javascript/yui/autocomplete-min.js">
</script>
<script type="text/javascript" src="javascript/jquery-1.2.6.min.js">
</script>
<script type="text/javascript" src="javascript/ui/ui.core.js">
</script>
<script type="text/javascript" src="javascript/ui/ui.tabs.js">
</script>
<script type="text/javascript" src="javascript/jqModal.js">
</script>
<script type="text/javascript" src="javascript/jquery.form.js">
</script>
<script type="text/javascript" src="javascript/jquery.cookie.js">
</script>
<script type="text/javascript" src="javascript/splitter.js">
</script>
<script type="text/javascript" src="javascript/jquery.scrollTo-min.js">
</script>
<script type="text/javascript" src="javascript/jquery.selectable.js">
</script>
<script type="text/javascript" src="javascript/jquery.positionBy.js">
</script>
<script type="text/javascript" src="javascript/jquery.dimensions.js">
</script>
<script type="text/javascript" src="javascript/work_edit_form.js">
</script>
<script type="text/javascript" src="javascript/application.js">
</script>
<script type="text/javascript" src="javascript/docit.js">
</script>
<script type="text/javascript" src="javascript/jtip.js">
</script>
<script type="text/javascript" src="javascript/jquery.bt.js">
</script>
</%def>

<%def name="tools_menu()">
<ul id="tools-menu">
  <li>
    <a href="http://www.sah.org/index.php?src=gendocs&ref=AVRN&category=AVRN" target="blank">About SAHARA Project</a>
  </li>
  <li>
    <a href="http://www.sah.org/index.php?src=gendocs&ref=AVRN&category=AVRN" target="blank">How to Contribute</a>
  </li>
  <li>
    <a href="${h.url_for('home')}">Cataloging Workspace</a>
  </li>
  <li>
    <a href="${g.COLLECTION_URL}" target="blank">View SAHARA Collection</a>
  </li>
  <li>
    <a href="#" title="Help" id="help-btn">Help</a>
  </li>
</ul>
</%def>
