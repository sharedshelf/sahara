<%inherit file="base.mako"/>

<div style="width:1000px;margin:0 auto;">
  <div id="simple-search">
    <h2>Search</h2>
    <p class="errors hidden">Please enter your search criteria</p>
    <form id="search-form" action="${h.url_for('search')}" method="GET">
      <div>
        <input type="text" name="phrase" title="Search" class="text" autofocus>
	      <input type="hidden" name="rows" value="25">
        <input type="submit" value="GO" class="button">
      </div>
    </form>
    <h2 class="how-to-find">How can I find an image quickly?</h2>
    <p class="search-info">
      Search by filename. You can find the image filename in the "File properties" tab 
      of the Descriptive data window in the SAHARA Digital Library.
      Pleaser remove the ".fpx" extension.
      (<a href="#" title="" class="show-example">Show example</a>)
    </p>
    <img src="images/screenshot.jpg" alt="" class="search-screenshot hidden">
  </div>
</div>

<%def name="css()"></%def>
<%def name="javascript()"></%def>
<%def name="tools_menu()"></%def>
<%def name="navigation()">
  <%include file="navigation.mako"/>
</%def>
