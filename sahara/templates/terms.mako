<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <title>SAHARA Terms</title>
    <link rel="stylesheet" href="/stylesheets/login.css" type="text/css">
    <link rel="stylesheet" href="/stylesheets/dialog.css" type="text/css">
    <link rel="stylesheet" href="/stylesheets/imata.css" type="text/css">
    <script type="text/javascript" src="/javascript/jquery.js"></script>
    <script type="text/javascript" src="/javascript/jqModal.js"></script>
    <script type="text/javascript" src="/javascript/application.js"></script>
    <script type="text/javascript" src="/javascript/app/nav_bar.js"></script>
    <script type="text/javascript" >
      function OnDecline() {
        $('#info-dialog').jqm({modal:true, overlay:10})
          .jqmAddClose($('#close_info')).jqmShow();
      }
      function closeTextDialog() {
        $('#text-dialog').jqmHide();
      }
  </script>
  </head>
  <body>
    <div id="header">
  	  <img src="images/icon.gif" alt="">
          <span id="welcome">Welcome, ${c.username}.</span>
          <a href="#" class="logout" title="">Log Out</a>
    </div>
  <div id="content">
    <h3 class="legal-summary">SUMMARY OF SAHARA TERMS OF USE</h3>
    <div class="legal-container">
      <p>
        Thank you for your interest in using SAHARA and/or contributing images to 
        SAHARA.
      </p>
      <p>
        Your use of SAHARA, and sharing images through SAHARA, is governed by the 
        SAHARA Terms of Use.  This summary highlights some of the most important 
        provisions of those terms of use.  To view the full set of applicable terms, 
        see the <a href="javascript:showText('${h.url_for('fullterms')}', 
  			 'TERMS AND CONDITIONS OF USE')">SAHARA Terms of Use</a>. Every
        time you use SAHARA or share images through SAHARA, you are agreeing to the
        terms of the SAHARA Terms of Use.
      </p>
      <p>
        By clicking on the ACCEPT button, you agree that you have read, understand 
        and agree to the <a href="javascript:showText('${h.url_for('fullterms')}', 
  			 'TERMS AND CONDITIONS OF USE')">SAHARA Terms of Use</a>.
      </p>
      <p>
        <b>Description of SAHARA.</b> SAHARA is a collection of images of visual 
        materials along with metadata, cataloging and texts related to such 
        materials (collectively &quot;SAHARA Images"), that depict the built environment.  
        SAHARA is being developed by the Society of Architectural Historians 
        ("SAH"), through contributions of SAH members, for educational and scholarly 
        uses.  SAHARA is made available on the software platform created by ARTstor 
        Inc. ("ARTstor Software Platform"), using tools developed by ARTstor Inc. 
        ("ARTstor") and in some instances developed by both ARTstor and SAH 
        (collectively "Access Software").
      </p>
      <p>
        SAH makes some SAHARA Images available only through SAHARA, and shares other 
        SAHARA Images through both SAHARA and the ARTstor Digital Library, an image 
        library in the arts and humanities made available by ARTstor on the ARTstor 
        Software Platform for noncommercial educational and scholarly purposes.  In 
        addition, should a contributor of a SAHARA Image so opt, a contributor's 
        contributed SAHARA Images may be shared through the Images for Academic 
        Publishing site (a site on the ARTstor Software Platform that makes images 
        available for academic publications), or on other parts of the ARTstor 
        Software Platform, or on other sites, for broader uses.]
      </p>
      <p>
        <b>Use of SAHARA Images.</b>  Use of SAHARA Images is only permitted for 
        teaching, research and study, unless SAHARA Images appear in Images for 
        Academic Publishing (in which case, they may be used for academic 
        publications) or unless such SAHARA Images are expressly accompanied by a 
        license that allows for broader uses, such as a Creative Commons license.
      </p>
      <p>
        <b>Sharing Your Images through SAHARA.</b>   By uploading your images to 
        SAHARA, you agree to make your images (which may include photographs, QTVRs, 
        born-digital visual materials, films and other visual media files) available 
        through SAHARA and through the ARTstor Digtial Library; and, should you so 
        opt, through IAP or other sites, to allow these resources and their users 
        worldwide to use your images, free of charge and royalty-free, for the life 
        of the copyright(s) in such images, in accordance with the permitted uses 
        you specify below.
      </p>
      <p>
        <b>What Rights Do You Have in Your Images?</b>  You may only contribute 
        images that you have created (or if you are contributing them on behalf of 
        someone else, you must have obtained the express permissions from the 
        creator of those images to share them through SAHARA).
      </p>
      <p>
        In sharing images through SAHARA, you may only share those images that to 
        the best of your knowledge do not violate any third party copyrights 
        (because any underlying works depicted in the images are not subject to 
        copyright, are no longer under copyright, or are being shared to the best of 
        your knowledge in a manner consistent with applicable copyright law, 
        including the US fair use doctrine).
      </p>
      <p>
        <b>How Can Your Images be Used?</b> You will be asked below to select 
        different uses that you permit users to make of your images, such as for 
        teaching, study, and research, or for use in academic publications, as well 
        as for teaching, study, and research. (Academic Publications are defined as 
        those publications, print or electronic, whose primary purpose is 
        educational or scholarly, and not commercial.  For examples of the types of 
        publications included in this definition, see the
  	  <a href="javascript:showText('${h.url_for('fullterms')}', 
  			 'TERMS AND CONDITIONS OF USE')">SAHARA Terms of Use</a>). You may also 
        select a <a href="http://creativecommons.org/about/license/">Creative 
  		Commons</a> license to govern the use of your images.
      </p>
      <p>
        Notwithstanding the permissions you have granted, your images may be shared 
        in more limited ways than the permissions you have granted, or may not be 
        shared at all, for legal or other reasons.
      </p>
      <p>
        <b>What about the Cataloging and Other Data Accompanying Your Images?</b> 
        SAHARA is intended to promote scholarship around architectural works. 
        Cataloging and other data you supply in connection with your images may be 
        modified or supplemented by those authorized by SAH to do so, except that 
        your copyright attribution in your images will not be changed.
      </p>
      <p>
        <b>SAHARA is intended as a community service,</b> and therefore you agree 
        not to hold SAH, ARTstor or other resources through which your images may be 
        shared liable for the use, display, performance, reproduction or 
        distribution of your images.
      </p>
    </div>
  </div>
  <div class="legal-button-container">
    ${h.button("Accept", onclick="window.location='%s'" % h.url_for('welcome'))|n}
    ${h.button("Decline", onclick="OnDecline()")|n}
  </div>

  <div class="column-footer">
    <div class="terms-footer">
          <a href="#" onclick="IMATA.dialogs.show_text('${h.url_for('fullterms')}',
                 'TERMS AND CONDITIONS OF USE');return false;"> SAHARA Terms of Use</a> |
          <a href="#" onclick="IMATA.dialogs.show_text('${h.url_for('privacy')}',
                 'PRIVACY POLICY'); return false;"> SAHARA Privacy Policy</a>
  	</div>
  </div>
  <%include file="dialogs.mako"/>
  </body>
</html>
