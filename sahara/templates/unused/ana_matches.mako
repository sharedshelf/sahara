<div class='dialog-header'>
	<a href="#" class='close'>CLOSE [X]</a>
	<img src='images/logo.gif'/>&nbsp;&nbsp;ANA Service
</div>
<div class='dialog-content'>
	<h2>Click on an actor:</h2>
	<div class='ana-matches'>
		%if len(matches) > 0 :
			%for match in matches :
				<a href="${match['label']}">${match['value']}</a>
			%endfor
		%else:
			No matches found
		%endif
	</div>
</div>
<div class='dialog-footer'>
	<a href='#' class='cancel button'>Cancel</a>
</div>

