<%def name="year_select(name, value)">
<input name="${name}" value="${value}" type="hidden"/>
%if value is not None:
<input name="widget-ys-${name}-abs" value="${abs(int(value))"
	   class="date-number" onchange="updateDate(${name})" type="text"/>
%else:
<input name="widget-ys-${name}-abs" type="text" class="date-number"
	   onchange="updateDate(${name})"/>
%endif
<select class="date-type" name="widget-ys-${name}-sel"
		onchange="updateDate(${name})">
  %if value is None or int(value) >= 0:
  <option selected="true">CE(AD)</option>
  <option>BCE(BC)</option>
  %else:
  <option>CE(AD)</option>
  <option selected="true">BCE(CE)</option>
</select>
</%def>
