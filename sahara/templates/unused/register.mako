<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en-us">
  <head>
    <title>SAHARA Registration</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" href="/stylesheets/login.css" type="text/css" media="screen" charset="utf-8">
    <link rel="stylesheet" href="/stylesheets/dialog.css" type="text/css" media="screen" charset="utf-8">
  	<script type="text/javascript" src="/javascript/jquery-1.2.6.min.js"></script>
    <script type="text/javascript" src="/javascript/jqModal.js"></script>
 	  <script type="text/javascript" src="/javascript/register.js"></script>
 </head>
  <body>
    <div id="fail">
		<div id="header">
			<div id='logo'>&nbsp;</div>
			Welcome to the SAH Architecture Resources Archive.
		</div>
		<div class="center">
		    <h2>SAH ARA requires Javascript to be enabled.</h2>
		    Javascript does not appear to be enabled, please enable Javascript
		    and try again.
		</div>
	</div>
	<div id="content">
		<div id="header">
			<div id='logo'>&nbsp;</div>Welcome to the SAH Architecture Resources 
      Archive. </div>
        <div class="center">
            <h2>Register for SAHARA</h2>
            <p id="error">&nbsp;${c.error}</p>

            <form name="registration" action="${h.url_for('registered')}" method="post">
              <label>Membership ID</label> 
              <input type="text" name="memberid"  size="38">&#160; 
              <a href="javascript:submit_if()" class="input-button"> SUBMIT</a>
              <br>
              <label>Email address</label>
              <input type="text" id="first"  name="email" size="38">&#160;
              <a href="javascript:clear_all()" class="input-button">
                &#160;CLEAR&#160;
              </a>
              <br>
              <label>Confirm email address</label> 
              <!-- do not remove autocomplete=off it _IS_ needed for firefox,etc. 
              to prevent autocomplete of this and next, password, field -->
              <input type="text"  name="remail" size="38" autocomplete="off">
              <br>
              <br>
              <label>Password</label> 
              <input type="password"  name="apassword" size="38">
              <br>
              <div class="subtext">(7 character min)
                <br>
              </div>
              <label>Confirm Password</label>
              <input type="password" name="repassword" size="38"><br><br>
              <input type="checkbox" id="checkbox" name="mailme" checked="checked">
              Yes, send me information about new SAHARA content &amp; tools
              <br>
              <input type="checkbox" id="checkbox" name="survey" checked="checked"> 
              Yes, send me email surveys
              <br>
            </form>
              <div class="disclaimer">
                 We will not give your personal information to any party. 
                <a  href="${h.url_for('privacy')}">See Privacy Policy.</a>
              </div>
        </div>
        <hr>
        <div class="center">
            <p>Already Registered?</p>
            <ul>
                <li> <a href="${h.url_for('login')}">Log in.</a></li>
            </ul>
        </div>
        <hr>
        <div class="center">
            <p>Learn more about the SAH ARA Project.</p>
            <ul>
                <li><a href="http://sah.org/index.php?src=gendocs&
                ref=AVRN">SAH ARA project information</a>
            </ul>
        </div>
	</div>
<div class="legal-footer">
  &gt; <a href="javascript:showText('${h.url_for('fullterms')}',
  'Terms and Conditions of Use')"> Terms and Conditions of Use</a> &gt;
  <a href="javascript:showText('${h.url_for('privacy')}', 'Privacy Policy')">
  Privacy Policy</a>
</div>
<div class="text-dialog" id="text-dialog">
  <div class='dialog-header'>
		<img src='/images/icon.gif'><ht id='title'></ht> <a
    href="javascript:closeDialog()" class='close'>CLOSE [X]</a>
		</div>
  <div class='text-content' id='text-content'></div>
</div>

	<script type="text/javascript">
    $('#content').show();
    $('#fail').hide();
	  $('#first').focus();
	</script> 
    </body>
</html>