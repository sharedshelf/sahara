<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-us" lang="en-us">
<head>
  <title>Thanks</title> 
  <meta http-equiv="content-type" content="text/html;
  charset=utf-8" /> <link rel="stylesheet" href="/stylesheets/login.css"
  type="text/css" media="screen" charset="utf-8" /> 
  <link rel="stylesheet" href="/stylesheets/dialog.css" type="text/css"
  media="screen" charset="utf-8" /> 
  <link rel="stylesheet" href="/stylesheets/imata.css" type="text/css"
  media="screen" charset="utf-8" />
</head>
<body>
<script type="text/javascript" src="/javascript/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="/javascript/jqModal.js"></script>
<script type="text/javascript" >
    function showText(content, title) {
        $('#text-dialog').jqm({modal:true, overlay:10}).find('#title').html(title);
         $.ajax({
         type: 'GET',
         url: content,
         success: function(data)
           {
           $('#text-dialog').find('div.text-content').html(data);
           scrollDocumentToTop();
           },
         error: function(data)
           {
             $('#text-dialog').html("Error retrieving document");
           }
         });
         $('#text-dialog').jqmAddClose($('#close')).jqmShow();
    }
    function closeDialog() {
      $('#text-dialog').jqmHide();
    }
</script>
<div id="header">
  <div class='logo'>
    SAHARA&nbsp;
    <div class='button'>
      V
    </div>
  </div>
  <img src="/images/icon.gif"/>&nbsp;&nbsp;Welcome, ${username}. &nbsp;&nbsp;
  <a href="#">My Profile</a>&nbsp;&nbsp; <a href="${h.url_for('logout')}">
  Log Out</a>
</div>
  <h1>Thank you for registering</h1><br/>
  <div class="message"> A confirmation email has been sent to your account. 
    </div> <br/> 
  <div class="right-center"> 
    <a class='larg-button float-right'   href="${h.url_for('terms')}"> CONTINUE
    SET UP</a>
  </div>

<div class="info-dialog" id="info-dialog">
  <div class='dialog-header'><img src='images/icon.gif'/></div>
  <div class='dialog-centered-content'>
    <div id='info-major'>
    You must agree to the Terms and Conditions of Use to participate in the 
    SAHARA project. </div>
       <div id='info-minor'></div>
      </div>
  <div class='dialog-footer'>
    <a href='#' id='close_info' class='button'>Close</a>
  </div>
</div>
<div class="text-dialog" id="text-dialog">
  <div class='dialog-header'>
		<img src='/images/icon.gif'/><ht id='title'></ht> <a
    href="javascript:closeDialog()" class='close'>CLOSE [X]</a>
		</div>
  <div class='text-content' id='text-content'></div>
</div>

<div class="legal-footer">
  &gt; <a href="javascript:showText('${h.url_for('fullterms')}',
  'Terms and Conditions of Use')"> Terms and Conditions of Use</a> &gt; 
  <a href="javascript:showText('${h.url_for('privacy')}', 'Privacy Policy')">
  Privacy Policy</a>
</div>
</body>
</html>

