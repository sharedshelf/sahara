<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
  "http://www.w3.org/TR/html4/strict.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>SAHARA Terms</title>
    
    <link rel="stylesheet" href="stylesheets/login.css" type="text/css">
    <link rel="stylesheet" href="stylesheets/dialog.css" type="text/css">
    <link rel="stylesheet" href="stylesheets/imata.css" type="text/css">
    
    <script type="text/javascript" src="/javascript/jquery.js"></script>
    <script type="text/javascript" src="/javascript/jqModal.js"></script>
    <script type="text/javascript" src="/javascript/application.js"></script>
    <script type="text/javascript" src="/javascript/app/nav_bar.js"></script>
    
    <script type="text/javascript">
      function closeTextDialog() {
        $('#text-dialog').jqmHide();
      }
    </script>
  </head>
  <body>
    <div id="wrap">
      <div id="main" class="clearfix">
	<div id="header">
	  <img src="/images/icon.gif">
	  <span id="welcome">Welcome, ${c.username}.</span>
          <a href="#" class="logout" title="">Log Out</a>
	</div>
	<div id="content">
	  <h3 class="legal-summary">Thank you. Now you can...</h3>
	  <div class="welcome-container">
            <p>Add up to 200 images, QTVRs, or videos to your IMATA workspace at a time.</p>
            <br>
            <p>Describe a single image or multiple images.</p>
            <br>
            <p>Contribute your images to the SAHARA Members Collection.</p>
            <br>
            <p>View your contributions, as well as those from other SAH members on the SAH website, www.sah.org.</p>
            <br>
	  </div>
	</div>
	<div class="legal-button-container">
	  ${h.button("GET STARTED", onclick="window.location='%s'" % h.url_for('home'))|n}
	</div>
	<div class="column-footer">
	  <div class="terms-footer">
            <a href="#" onclick="IMATA.dialogs.show_text('${h.url_for('fullterms')}',
				 'TERMS AND CONDITIONS OF USE');return false;"> SAHARA Terms of Use</a> |
            <a href="#" onclick="IMATA.dialogs.show_text('${h.url_for('privacy')}',
				 'PRIVACY POLICY'); return false;"> SAHARA Privacy Policy</a>
    	  </div>
	</div>
	<%include file="dialogs.mako"/>
      </div>
    </div>
  </body>
</html>
