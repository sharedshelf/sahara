<span id="document-messaging"></span>
<form id="work-document"
      class="document"
      onchange="IMATA.document.register_change()"
      method="post"
      action="${h.url_for('update', uuid=c.work.uuid)}">
  <%include file="work_form/title_section.mako" args="src=c.work.name, full=c.full"/>
  %for index, creator in enumerate(h.get_creators(c.work)):
  <%include file="work_form/creator_section.mako"
            args="index=index, creator=creator, full=c.full"/><br>
  %endfor
  <%include file="work_form/location_section.mako"
            args="src=c.work.location, full=c.full"/><br>
  <%include file="work_form/chronology_section.mako"
            args="src=c.work.chronology, full=c.full"/><br>
  <%include file="work_form/physical_section.mako"
            args="src=c.work.physical_description, full=c.full"/><br>
  %if c.full is True:
  <div class="section">
    <div class="document-section-heading subject">
      Subject/Tags
    </div>
    <div>
      <label>Keywords</label>
      <br>
      <input name="keywords"
             onfocus="IMATA.document.highlight_section('subject')"
             onblur="IMATA.document.clear_section_highlights()"
             size="64"
             maxlength="255"
             tabindex="5"
             type="text"
             class="full"
             value="${','.join([k.value for k in c.work.keywords])}">
      <br>
      <div class="hint">e.g., curtain wall, cathedral</div>
    </div>
  </div>
  <div class="clear"></div>
  %endif
  <%include file="work_form/source_section.mako" args="src=c.work.source, full=c.full"/>
  <br>
  <%include file="work_form/rights_section.mako" args="src=c.work.rights, full=c.full"/>
  <br>
  <br>
  <input type="submit" style="display:none">
</form>
