<%namespace file="year_selection_widget.mako" import="*"/>
<%page args="src, full"/>

<div class="section">
  <div class='document-section-heading chronology'>
    Chronology
  </div>
  <div>
    <label class="required">Date</label><br>
    <input name="display_date"
           onfocus="IMATA.document.highlight_section('chronology')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           size="64"
           maxlength="255"
           class="full"
           value="${src.display_date}"
           type="text"><br>
    <div class="hint">e.g., 18th century; or built 1565, restored 1787; or
      constructed ca. 1750</div>
  </div>
  <div class="tiny">
    <label>Earliest Date</label><br>
    ${year_select("earliest_date", src.earliest_date, "chronology")}
    <div class="hint">e.g., 1950; numbers only</div>
  </div>
  <div class="tiny">
    <label>Latest Date</label><br>
    ${year_select("latest_date", src.latest_date, "chronology")}
    <div class="hint">e.g., 1956; numbers only</div>
  </div>
  %if full is True:
  %for index, period in enumerate(h.get_periods(src)):
  <%include file="period_dynasty_field.mako"
            args="period_dynasty=period, index=index"/>
  %endfor
  %endif
</div>
