<%page args="title, index"/>

<div id="complex-title-${index}" class="numbered-form-field-complex-title">
  %if 0 == index:
  <div class="field-controls">
    <a href="javascript:IMATA.document.add_complex_title()"
       title="Click to duplicate this field"
       class="button">
      <span class="field-add-control"></span>
    </a>
    <div class="label" style="float:right">Add an Alternate Title/Name</div>

  </div>
  <label>Title/Name of Complex</label>
  %else:
  <div class="field-controls">
    <a href="javascript:IMATA.document.remove_field('complex-title-${index}')"
       title="Click to remove this field"
       class="button">
      <span class="field-remove-control"></span>
    </a>
    <a href="javascript:IMATA.document.add_complex_title()"
       title="Click to duplicate this field"
       class="button">
      <span class="field-add-control"></span>
    </a>
  </div>
  <label>Title/Name of Complex</label>
  %endif
  <br>
  <input name="complex_title" 
         id="input-complex-title-${index}"
         onfocus="IMATA.document.highlight_section('title')"
         onblur="IMATA.document.clear_section_highlights()"
         tabindex="5"
         maxlength="255"
         class="autocomplete full"
         data-url="lookup/complex_title"
         value="${title.value}" 
         type="text">

  <br/>
  %if 0 == index:
  <div class="hint">e.g., Alhambra</div>
  %else:
  <div class="hint">&nbsp;</div>
  %endif
</div>