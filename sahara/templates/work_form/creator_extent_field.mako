<%page args="extent, creator_index, index"/>

<div id="creator-${creator_index}_extent-${index}"
     class="numbered-form-field-creator-${creator_index}_extent">
  %if 0 == index:
  <div class="field-controls" style="width:20em;">

    <a class="button"
       title="Click to duplicate this field"
       href="javascript:IMATA.document.add_creator_extent('creator-${creator_index}')"
       style="float:right">
      <span class="field-add-control"></span>
    </a>
    <div class="label" style="float:right">Add another Extent</div>
  </div>
  %else:
  <div class="field-controls">
    <a class="button"
       title="Click to remove this field"
       href="javascript:IMATA.document.remove_field('creator-${creator_index}_extent-${index}')">
      <span class="field-remove-control"></span>
    </a>
    <a class="button"
       title="Click to duplicate this field"
       href="javascript:IMATA.document.add_creator_extent('creator-${creator_index}')">
      <span class="field-add-control"></span>
    </a>
  </div>
  %endif
  <label>${h.indexed_label('Extent', index)}</label><br/>
  <input name="creator-${creator_index}_extent"
         onfocus="IMATA.document.highlight_section('creator')"
         onblur="IMATA.document.clear_section_highlights()"
         tabindex="5"
         size="64"
         value="${extent.value}"
         class="full"
         type="text"><br>
  %if 0 == index:
  <div class="hint">e.g., interior design</div>
  %else:
  <div class="hint">&nbsp;</div>
  %endif
</div>
