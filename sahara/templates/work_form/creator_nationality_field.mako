<%page args="nationality, creator_index, index, readonly=False, full=True"/>

%if full is True:
<div id="creator-${creator_index}_nationality-${index}"
     class="numbered-form-field-creator-${creator_index}_nationality">
  %if 0 == index:
  <div
     %if readonly == True:
     style="display:none;width:20em;"
     %endif
     class="field-controls" style="width:20em;">

    <a class="button"
       title="Click to duplicate this field"
       href="javascript:IMATA.document.add_creator_nationality('creator-${creator_index}')"
       style="float:right;">
      <span class="field-add-control"></span>
    </a>
    <div class="label" style="float:right;">Add another Nationality</div>
  </div>
  %else:
  <div
     %if readonly == True:
     style="display:none;"
     %endif
     class="field-controls">
    <a class="button"
       title="Click to remove this field"
       href="javascript:IMATA.document.remove_field('creator-${creator_index}_nationality-${index}')">
      <span class="field-remove-control"></span>
    </a>
    <a class="button"
       title="Click to duplicate this field"
       href="javascript:IMATA.document.add_creator_nationality('creator-${creator_index}')"
       <span class="field-add-control"></span>
</a>
</div>
%endif
%if readonly:
<label class="locked">${h.indexed_label('Nationality', index)}</label><br/>
%else:
<label>${h.indexed_label('Nationality', index)}</label><br/>
%endif
<input name="creator-${creator_index}_nationality"
       size="64"
       maxlength="255"
       tabindex="5"
       %if readonly:
       readonly="readonly"
       %endif
       value="${nationality.value}"
       class="full"
       type="text"><br>
%if 0 == index:
<div class="hint">e.g., Spanish</div>
%else:
<div class="hint">&nbsp;</div>
%endif
</div>
%else:
<div id="creator-${creator_index}_nationality-${index}"
     class="hidden"
     style="display: none;">
  <input name="creator-${creator_index}_nationality"
         value="${nationality.value}"
         type="hidden">
</div>
%endif
