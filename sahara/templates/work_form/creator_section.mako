<%namespace file="year_selection_widget.mako" import="*"/>
<%page args="index, creator, full"/>

<% readonly = h.is_creator_locked(creator) %>

<div id="creator-${index}" class="section">
  <div class='document-section-heading numbered-section-creator creator'>
    <div class='document-control' style="width:20em;display:inline;right:-10px">
      %if 0 == index:
      <a class="button"
         title="Click to duplicate all creator fields"
         href="javascript:IMATA.document.add_creator()" style="float:right">
        <span class="field-add-control"></span>
      </a>
      <div class="label" style="float:right">Add another Creator</div>
      %else:
      <a class="button"
         title="Click to remove all fields related to this creator"
         href="javascript:IMATA.document.remove_field('creator-${index}')">
        <span class="field-remove-control"></span>
      </a>
      <a class="button"
         title="Click to duplicate all creator fields"
         href="javascript:IMATA.document.add_creator()">
        <span class="field-add-control"></span>
      </a>
      %endif
    </div>
    <span class='label'>Creator</span>
  </div>
  <div id='creator-${index}-name'>
    <div id="creator-${index}-unlink"
         %if not readonly:
         style="display: none;"
         %endif
         class="field-controls">
      <a class="clear-creator-lock" href="#"
         onclick="IMATA.document.unlink_creator('creator-${index}'); return false;"></a>
      <div class="label">Clear locked fields</div>
    </div>
    %if readonly:
      <label class="locked">Creator Name</label><br>
    %else:
      <label>Creator Name</label><br>
    %endif

    <input name="creator-${index}_name"
           tabindex="5"
           onfocus="IMATA.document.highlight_section('creator')"
           onblur="IMATA.document.clear_section_highlights()"
           size="64"
           %if readonly is True:
           readonly="readonly"
           %endif
           id="input-creator-${index}"
           maxlength="255"
           class="ana-lookup full"
           data-url="ana/creator"
           value="${creator.name}"
           type="text">
    <br/>
    <input name="creator-${index}_ana_id" class="hidden medium" value="${creator.ana_id}" type="hidden">
    <div class="hint">e.g., Calatrava Valls, Santiago</div>
  </div>


  %for i, nationality in enumerate(h.get_creator_nationalities(creator)):
  <%include file="creator_nationality_field.mako"
            args="nationality=nationality,
                  creator_index=index,
                  index=i,
                  readonly=readonly,
                  full=full"/>
  %endfor
  %if full is True:
    %for i, extent in enumerate(h.get_creator_extents(creator)):
      <%include file="creator_extent_field.mako"
		args="extent=extent, creator_index=index, index=i"/>
    %endfor
    <div>
      <label>Attribution</label><br>
      <select name="creator-${index}_attribution"
              tabindex="5"
              onfocus="IMATA.document.highlight_section('creator')"
              onblur="IMATA.document.clear_section_highlights()"
              class="half">
        <option></option>
        %for a in h.attributions():
          %if creator.attribution == a:
            <option value="${a.id}"selected>${a.value}</option>
          %else:
            <option value="${a.id}">${a.value}</option>
          %endif
        %endfor
      </select><br>
      <div class="hint">&nbsp;</div>
    </div>
  %else:
    %for extent in h.get_creator_extents(creator):
      <input type="hidden" name="creator-${index}_extent" value="${extent.value}"/>
    %endfor
    %if creator.attribution is not None:
      <input type="hidden" name="creator-${index}_attribution" value="${creator.attribution.id}"/>
    %endif
  %endif
  <div>
    <label>Role</label><br>
    <!-- This hidden field is a placeholder for classifications so
         POSTs always contain at least a blank classification -->
    <input type="hidden" name="creator-${index}_role"/>
    <div class="scrolling-checkbox-selector">
      %for role in h.roles():
        <div onclick="$(this).find(':input').click()">
          <input type="checkbox"
		 tabindex="5"
		 onfocus="IMATA.document.highlight_section('creator')"
		 onblur="IMATA.document.clear_section_highlights()"
		 name="creator-${index}_role"
		 %if role in creator.roles:
		   checked="true"
		 %endif
		 class="small"
		 value="${role.id}"/>
	  ${role.value}
	</div>
      %endfor
    </div>
  </div>
</div>
<div class="clear"></div>


