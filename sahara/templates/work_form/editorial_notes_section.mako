<%page args="src"/>

<div class="section">
  <div class='document-section-heading editorial-notes'>
    Editorial Information
  </div>
  
  <div class="large">
    <label>Notes</label><br>
    <textarea name="editorial_note_body"
              onfocus="IMATA.document.highlight_section('editorial-notes')"
              onblur="IMATA.document.clear_section_highlights()"
              tabindex="5"
              rows="5"
	      class="full"
              cols="57">${src.body}</textarea><br>
  </div>
</div>