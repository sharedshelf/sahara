<%page args="country, index"/>

<div id="country-${index}" class="small numbered-form-field-location_country">
  %if 0 == index:
  <div class="field-controls">
    <a class="button"
       title="Click to duplicate this field"
       href="javascript:IMATA.document.add_location_country()">
      <span class="field-add-control"></span>
    </a>
    <div class="label">Add another Country</div>
  </div>
  <label class='required'>Country</label>
  %else:
  <div class="field-controls">
    <a class="button"
       title="Click to remove this field"
       href="javascript:IMATA.document.remove_field('country-${index}')">
      <span class="field-remove-control"></span>
    </a>
    <a class="button"
       title="Click to duplicate this field"
       href="javascript:IMATA.document.add_location_country()">
      <span class="field-add-control"></span>
    </a>
  </div>
  <label>Country</label>
  %endif
  <br/>
  <select onfocus="IMATA.document.highlight_section('location')"
          onblur="IMATA.document.clear_section_highlights()"
          tabindex="5"
          class="full"
          name="country">
    <option></option>
    %for option in h.countries():
    %if option == country:
    <option value="${option.id}" selected="true">${option.value}</option>
    %else:
    <option value="${option.id}">${option.value}</option>
    %endif
    %endfor
  </select>
</div>
