<%page args="src, full"/>

<div id="location" class="section">
  <div class='document-section-heading location'>
    Location
  </div>
  %if full is True:
  <div>
    <label>Street Address</label>
    <br>
    <input type="text"
           size="64"
           maxlength="255"
           tabindex="5"
           onfocus="IMATA.document.highlight_section('location')"
           onblur="IMATA.document.clear_section_highlights()"
           value="${src.street_address}"
	   class="full"
           name="street_address"/>
    <br>
    <div class="hint">e.g., 1200 Getty Center Drive</div>
  </div>
  %endif
  <div>
    <label class="required">City/County</label>
    <br>
    <input size="28"
           id="input-city-county"
           maxlength="100"
           tabindex="5"
           onfocus="IMATA.document.highlight_section('location')"
           onblur="IMATA.document.clear_section_highlights()"
           type="text"
           class="autocomplete half"
           data-url="lookup/city_county"
           value="${src.city_county}"
           name="city_county">
    <br>
    <div class="hint">e.g., Los Angeles</div>
  </div>
  <div>
    <label>State/Province</label>
    <br>
    <input size="28"
           id="input-state-province"
           maxlength="100"
           tabindex="5"
           onfocus="IMATA.document.highlight_section('location')"
           onblur="IMATA.document.clear_section_highlights()"
           type="text"
	   class="autocomplete half"
	   data-url="lookup/state_province"
           value="${src.state_province}"
           name="state_province">
    <br>

    <div class="hint">e.g., California</div>
  </div>
    %for i, country in enumerate(h.get_countries(src)):
  <%include file="location_country_field.mako"
            args="country=country, index=i"/>
  %endfor
  %if full is True:
  <br/>
  <div>
    <label>Latitude</label>
    <br/>
    <input size="28"
           maxlength="100"
           tabindex="5"
           onfocus="IMATA.document.highlight_section('location')"
           onblur="IMATA.document.clear_section_highlights()"
           type="text"
           value="${src.latitude}"
	   class="half"
           name="latitude"/>
    <br/>
    <div class="hint">e.g., +34.077563</div>
  </div>
  <div>
    <label>Longitude</label>
    <br/>
    <input size="28"
           maxlength="100"
           tabindex="5"
           onfocus="IMATA.document.highlight_section('location')"
           onblur="IMATA.document.clear_section_highlights()"
           type="text"
           value="${src.longitude}"
	   class="half"
           name="longitude">
    <br>
    <div class="hint">e.g., -118.474629</div>
  </div>
  %endif
  <div>
    <label>Repository</label>
    <br>
    <input size="28"
           maxlength="100"
           id="input-repository"
           tabindex="5"
           onfocus="IMATA.document.highlight_section('location')"
           onblur="IMATA.document.clear_section_highlights()"
           type="text"
	   class="autocomplete half"
	   data-url="lookup/repository"
           value="${src.repository}"
           name="repository">
    <br/>
    <div class="hint">e.g., J. Paul Getty Museum</div>
  </div>
  %if full is True:
  <div>
    <label>Repository ID Number</label>
    <br>
    <input size="28"
           maxlength="100"
           tabindex="5"
           type="text"
           onfocus="IMATA.document.highlight_section('location')"
           onblur="IMATA.document.clear_section_highlights()"
           value="${src.repository_id}"
	   class="half"
           name="repository_id">
    <br>
    <div class="hint">e.g., 71.PB.29</div>
  </div>
  %endif
  <div class="clear"></div>
</div>

