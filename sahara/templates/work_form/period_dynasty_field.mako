<%page args="period_dynasty, index"/>

<div id="period_dynasty-${index}" class="autocomplete">
  %if 0 == index:
  <div class="field-controls">
    <a class="button"
       title="Click to duplicate this field"
       href="javascript:IMATA.document.add_period_dynasty()">
      <span class="field-add-control"></span>
    </a>
    <div class="label">Add another Period/Dynasty</div>
  </div>
  <label>Period/Dynasty</label>
  %else:
  <div class="field-controls">
    <a class="button"
       title="Click to remove this field"
       href="javascript:IMATA.document.remove_field('period_dynasty-${index}')">
      <span class="field-remove-control"></span>
    </a>
    <a class="button"
       title="Click to duplicate this field"
       href="javascript:IMATA.document.add_period_dynasty()">
      <span class="field-add-control"></span>
    </a>
  </div>
  <label>Period/Dynasty</label>
  %endif
  <br>
  <input name="period_dynasty"
         id="input-period-dynasty-${index}"
         tabindex="5"
         size="64"
         maxlength="255"
         type="text"
         onfocus="IMATA.document.highlight_section('chronology')"
         onblur="IMATA.document.clear_section_highlights()"
	 class="autocomplete full"
	 data-url="lookup/period_dynasty"
         value="${period_dynasty.value}"><br>
  %if 0 == index:
  <div class="hint">e.g., Ming, Late Bronze Age</div>
  %else:
  <div class="hint">&nbsp;</div>
  %endif
</div>

<!--
<script type="text/javascript">
  YAHOO.example.BasicRemote = function() {
  // Use an XHRDataSource
  var oDS = new YAHOO.util.XHRDataSource("/lookup/period_dynasty");
  // Set the responseType
  oDS.responseType = YAHOO.util.XHRDataSource.TYPE_TEXT;
  // Define the schema of the delimited results
  oDS.responseSchema = {
  recordDelim: "\n",
  fieldDelim: "\t"
  };
  // Enable caching
  oDS.maxCacheEntries = 10;

  // Instantiate the AutoComplete
  var oAC = new YAHOO.widget.AutoComplete("input-period-dynasty-${index}",
  "completer-period-dynasty-${index}", oDS);

  oAC.autoHighlight = false;
  oAC.animVert = false;

  oAC.containerExpandEvent.subscribe(onContainerExpand);
  oAC.containerCollapseEvent.subscribe(onContainerCollapse);

  return {
  oDS: oDS,
  oAC: oAC
  };
  }();
</script>
-->
