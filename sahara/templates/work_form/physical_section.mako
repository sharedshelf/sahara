<%page args="src, full"/>

<div class="section">
  <div class='document-section-heading physical'>
    Physical Description
  </div>
  %if full is True:
  <div>
    <label>Material/Technique</label><br>
    <input name="material_technique"
           onfocus="IMATA.document.highlight_section('physical')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           size="64"
           maxlength="255"
           type="text"
	   class="full"
           value="${src.material_technique}"><br>
    <div class="hint">e.g., stone, reinforced concrete, wood, stained glass</div>
  </div>
  <div>
    <label>Measurements</label><br>
    <input name="measurements"
           onfocus="IMATA.document.highlight_section('physical')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           size="64"
           maxlength="255"
	   class="full"
           value="${src.measurements}"
           type="text"><br>
    <div class="hint">e.g., square feet, square meters, 30 stories</div>
  </div>
  %endif
  <div class="large">
    <label>Description</label><br>
    <textarea name="description"
              onfocus="IMATA.document.highlight_section('physical')"
              onblur="IMATA.document.clear_section_highlights()"
              tabindex="5"
              rows="5"
	      class="full"
              cols="57">${src.description}</textarea><br>
  </div>
  <div class="large">
    <label>Commentary</label><br>
    <textarea name="commentary"
              onfocus="IMATA.document.highlight_section('physical')"
              onblur="IMATA.document.clear_section_highlights()"
              tabindex="5"
              rows="5"
	      class="full"
              cols="57">${src.commentary}</textarea><br>
  </div>
  <div class="medium">
    <label>Style</label><br>
    <input name="style"
           onfocus="IMATA.document.highlight_section('physical')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           size="64"
           maxlength="255"
	   class="full"
           value="${src.style}"
           type="text"><br>
    <div class="hint">e.g., Modernist, Baroque, Gothic Revival,
      Arts and Crafts</div>
  </div>
  <div class="clear"></div>
</div>
