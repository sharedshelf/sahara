<%page args="src, full"/>

<div class="section rights-section">
  <div class='document-section-heading rights'>
    Rights
  </div>
  <div class="medium">
    <label class="required">Copyright of Photograph/Image</label><br>
    <input name="image_copyright"
           onfocus="IMATA.document.highlight_section('rights')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           size="64"
           maxlength="255"
           class="required full"
           value="${src.image_copyright}"
           type="text"><br>
  </div>
  <div class="large">
    <label class="required">Do you have the rights to this image?</label>
    <p class="description">Have you created the images that you are
      sharing, or have you received express authorization from the
      creator of those images to share them here?  (Images include
      photographs, QTVRs, born-digital visual materials, films and
      other visual media files).</p>
    <input name="image_ownership"
           onfocus="IMATA.document.highlight_section('rights')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           value="True"
           %if src.image_ownership == True:
           checked="True"
           %endif
           type="radio">Yes<br>
    <input name="image_ownership"
           onfocus="IMATA.document.highlight_section('rights')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           %if src.image_ownership == False:
           checked="True"
           %endif
           value="False"
           type="radio">No
  </div>
  <div class="large"><br><br>By sharing this image through SAHARA,
    you are making it available for teaching, research, and study
    (in SAHARA and in resources like the ARTstor Digital Library).
    <br><br>How else may this image be used?<br><br>
  </div>
  <div class="large indent-left">
    <input name="academic_publishing"
           onfocus="IMATA.document.highlight_section('rights')"
           onblur="IMATA.document.clear_section_highlights()"
           onfocus="highlightSection('rights')"
           onblur="clearHighlighting()"
           tabindex="5"
           %if src.academic_publishing is True:
           checked="True"
           %endif
           value="True"
           type="checkbox"/>
    In academic publications<p class="description">(Academic
      publications are defined as those publications, print or
      electronic, whose primary purpose is educational or scholarly,
      and not commerical. For examples of the types of publications
      included in this definition, see the
      <a href="#" onclick="IMATA.dialogs.show_text('/fullterms.html', 'TERMS AND CONDITIONS OF USE')">SAHARA Terms of Use</a>)</p>
    <br><br>
  </div>
  <div class="clear"></div>
</div>
