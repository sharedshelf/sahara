<%page args="src, full"/>

<div class="section">
  <div class='document-section-heading source'>Source</div>
  %if full is True:
  <div>
    <label>Information Source</label>
    <br>
    <input name="information_source"
           onfocus="IMATA.document.highlight_section('source')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           size="64"
           class="autocomplete full"
           data-url="lookup/information_source"
           maxlength="255"
           id="input-information-source"
           value="${src.information_source}"
           type="text">
    <div class="hint">e.g., Grove Art Online, Art and Architecture Thesaurus</div>
  </div>
  %endif
  <div>
    <label class="required">Photographer</label>
    <br>
    <input name="photographer"
           onfocus="IMATA.document.highlight_section('source')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           id="input-photographer"
           size="64"
           data-url="lookup/photographer"
           maxlength="255"
           class="required autocomplete full"
           value="${src.photographer}"
           type="text">
    <br>
    <div class="hint">e.g., Klee, Jeffrey E.</div>
  </div>
  <div>
    <label class="required">Contributor</label>
    <br>
    <input name="contributor"
           onfocus="IMATA.document.highlight_section('source')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           id="input-contributor"
           size="64"
           maxlength="255"
           class="required autocomplete full"
           data-url="lookup/contributor"
           value="${src.contributor}"
           type="text">
    <br>
    <div class="hint">e.g., Whiteside, Ann</div>
  </div>
  %if full is True:
  <div>
    <label>Institutional Contributor</label><br>
    <input name="inst_contributor"
           onfocus="IMATA.document.highlight_section('source')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           id="input-inst-contributor"
           size="64"
           maxlength="255"
           class="autocomplete full"
           data-url="lookup/inst_contributor"
           value="${src.inst_contributor}"
           type="text"><br>
    <div class="hint">e.g., Massachusetts Institute of Technology</div>
  </div>
  %endif
  <div>
    <label>Image Date</label><br>
    <input name="image_date"
           onfocus="IMATA.document.highlight_section('source')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           class="full"
           size="64"
           maxlength="255"
           value="${src.image_date}"
           type="text"><br>
    <div class="hint">e.g., Winter 2007/2008</div>
  </div>
  <div class="tiny">
    <label>Image Earliest Date</label><br>
    <input name="image_earliest_date"
           onfocus="IMATA.document.highlight_section('source')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           size="8"
           maxlength="8"
           value="${src.image_earliest_date}"
           type="text"><br>
    <div class="hint">e.g., 2007; numbers only</div>
  </div>
  <div class="tiny">
    <label>Image Latest Date</label><br>
    <input name="image_latest_date"
           onfocus="IMATA.document.highlight_section('source')"
           onblur="IMATA.document.clear_section_highlights()"
           tabindex="5"
           size="8"
           maxlength="8"
           value="${src.image_latest_date}"
           type="text"><br>
    <div class="hint">e.g., 2008; numbers only</div>
  </div>
  <div class="clear"></div>
</div>

