<html>
<script type="text/javascript">
  %if work.status.complete is True:
    $('#work-document').remove();
    $('#document-container div.column-header').html('');
    $('#document-messaging').html('${message}');
    $('#${work.uuid}').parent().parent().remove();
    if (0 == $('#assets img').length) {
      $('#folder-wrapper > ul').tabs('load', 0);
    }
    IMATA.assets.update_count();
  %elif work.status.valid is True:
    $('#document-messaging').html('${message}')
    $('#${work.uuid}').addClass('valid')
      .before('<img class="valid-img" src="images/valid.png">');
    
    IMATA.document.scrollToTop();
    IMATA.document.enable();
  %else:
    $('#document-messaging').html('${message|n}')
    $('#${work.uuid}').removeClass('valid').prev().remove();
    
    IMATA.document.scrollToTop();
    IMATA.document.enable();
  %endif
</script>
</html>
