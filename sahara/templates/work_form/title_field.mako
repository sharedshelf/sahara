<%page args="title, index"/>

<div id="title-${index}" class="numbered-form-field-title">
  %if 0 == index:
  <div class="field-controls">
    <a href="javascript:IMATA.document.add_title()"
       title="Click to duplicate this field"
       class="button">
      <span class="field-add-control"></span>
    </a>
    <div class="label" style="float:right">Add an Alternate Title/Name</div>

  </div>
  <label class='required'>Title/Name of Work</label>
  %else:
  <div class="field-controls">
    <a href="javascript:IMATA.document.remove_field('title-${index}')"
       title="Click to remove this field"
       class="button">
      <span class="field-remove-control"></span>
    </a>
    <a href="javascript:IMATA.document.add_title()"
       title="Click to duplicate this field"
       class="button">
      <span class="field-add-control"></span>
    </a>
  </div>
  <label>Title/Name of Work</label>
  %endif
  <br>
  <input name="title" id="input-title-${index}"
         tabindex="5"
         maxlength="255"
         onfocus="IMATA.document.highlight_section('title')"
         onblur="IMATA.document.clear_section_highlights()"
         class="autocomplete full"
         data-url="lookup/title"
         value="${title.value}" type="text">

  <br/>
  %if 0 == index:
  <div class="hint">e.g., Palace of Charles V</div>
  %else:
  <div class="hint">&nbsp;</div>
  %endif
</div>

