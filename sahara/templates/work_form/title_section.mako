<%page args="src, full"/>

<div class="section">
  <div id="completer-complex-title" class="autocomplete-result yui-skin-sam" style="display: none;"></div>
  <div class='document-section-heading title'>
    Title/Name
  </div>
  %for index, title in enumerate(h.get_complex_titles(src)):
  <%include file="complex_title_field.mako" args="title=title, index=index"/>
  %endfor

  %for index, title in enumerate(h.get_titles(src)):
  <%include file="title_field.mako" args="title=title, index=index"/>
  %endfor
  <div id="view_type">
    <label class='required'>View Type</label><br>
    <select name="view_type"
            tabindex="5"
            onfocus="IMATA.document.highlight_section('title')"
            onblur="IMATA.document.clear_section_highlights()"
            class="half">
      <option></option>
      %for viewtype in h.viewtypes():
      %if src.view_type == viewtype:
      <option value="${viewtype.id}" selected>${viewtype.value}</option>
      %else:
      <option value="${viewtype.id}">${viewtype.value}</option>
      %endif
      %endfor
    </select>
  </div>
  <div id="image_view">
    <label>Image View</label><br>
    <input name="image_view"
           tabindex="5"
           onfocus="IMATA.document.highlight_section('title')"
           onblur="IMATA.document.clear_section_highlights()"
           size="28"
	   class="half"
           maxlength="100"
           value="${src.image_view}"
           type="text"><br/>
    <div class="hint">e.g., View of patio</div>
  </div>
  <div id="classifications">
    <label class='required'>Broad Classification</label><br>
    <!-- This hidden field is a placeholder for classifications so
         POSTs always contain at least a blank classification -->
    <input type="hidden" name="classifications"/>
    <div class="scrolling-checkbox-selector">
      %for c in h.classifications():
      <div onclick="$(this).find(':input').click();">
        <input type="checkbox"
               tabindex="5"
               onfocus="IMATA.document.highlight_section('title')"
               onblur="IMATA.document.clear_section_highlights()"
               onclick="updateNarrowClassification();"
               name="classifications"
               %if c in src.classifications:
               checked="true"
               %endif
               value="${c.value}">
        ${c.value}
      </div>
      %endfor
    </div>
  </div>
  <div id="narrow_classification">
    <label>Narrow Classification</label><br>
    <select disabled="true"
            tabindex="5"
            name="narrow_classification"
            onfocus="IMATA.document.highlight_section('title')"
            onblur="IMATA.document.clear_section_highlights()"
            class="full">
      <option></option>
      %for narrow in h.narrow_classifications():
      %if narrow == src.narrow_classification:
      <option value="${narrow.id}" selected>${narrow.value}</option>
      %else:
      <option value="${narrow.id}">${narrow.value}</option>
      %endif
      %endfor
    </select>
  </div>
  <div class="clear"></div>
</div>
