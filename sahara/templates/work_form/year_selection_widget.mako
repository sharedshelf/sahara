<%def name="year_select(name, value, section, readonly=False)">

<% readonly_str = "readonly='readonly'" if readonly else "" %>

<%doc>
readonly_str is used here to cut down on the amount
of branching.
</%doc>

<input name="${name}" value="${value}" type="hidden" class="small"/>

<input name="widget-ys-${name}-abs"
	   size="8"
	   maxlength="8"
	   tabindex="5"
	   type="text"
	   onfocus="IMATA.document.highlight_section('${section}')"
	   onblur="IMATA.document.clear_section_highlights()"
	   onchange="updateDate('${name}')"
	   %if value is not None:
	   value="${abs(int(value))}"
	   %endif
	   %if readonly:
	   readonly="readonly"
	   %endif
	   />

<%doc>
%if value is not None:
<input name="widget-ys-${name}-abs" value="${abs(int(value))}" ${readonly_str}
	   class="tiny" onchange="updateDate('${name}')" type="text"/>
%else:
<input name="widget-ys-${name}-abs" type="text" class="date-number"
	   onchange="updateDate('${name}')" ${readonly_str}/>
%endif
</%doc>
<select class="tiny"
		name="widget-ys-${name}-sel"
		tabindex="5"
		onfocus="IMATA.document.highlight_section('${section}')"
		onblur="IMATA.document.clear_section_highlights()"
		onchange="updateDate('${name}')"
		%if readonly:
		disabled='disabled'
		%endif
		>
  %if value is None or int(value) >= 0:
  <option selected="true">CE(AD)</option>
  <option>BCE(BC)</option>
  %else:
  <option>CE(AD)</option>
  <option selected="true">BCE(CE)</option>
  %endif
</select>
</%def>
