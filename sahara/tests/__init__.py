"""Pylons application test package

This package assumes the Pylons environment is already loaded, such as
when this script is imported from the `nosetests --with-pylons=test.ini`
command.

This module initializes the application via ``websetup`` (`paster
setup-app`) and provides the base testing objects.
"""
import base64
from unittest import TestCase

from paste.deploy import loadapp
from paste.fixture import TestApp
from paste.script.appinstall import SetupCommand
from pylons import config, url
from routes.util import URLGenerator
from routes import url_for

from sahara import model
from sahara.model import meta
from sahara.model.user import User
from pylons import config
from sahara.lib.app_globals import Globals
from beaker.session import CookieSession

import pylons.test

__all__ = ['environ', 'url', 'url_for', 'TestController',
           'TestCase', 'Privileges']

# Invoke websetup with the current config file
SetupCommand('setup-app').run([config['__file__']])

environ = {}

class Privileges(object):
    ADMIN = 1
    REVIEWER = 2
    CATALOGER = 3

class TestController(TestCase):

    def __init__(self, *args, **kwargs):
        if pylons.test.pylonsapp:
            wsgiapp = pylons.test.pylonsapp
        else:
            wsgiapp = loadapp('config:%s' % config['__file__'])
        self.app = TestApp(wsgiapp)
        url._push_object(URLGenerator(config['routes.map'], environ))
        TestCase.__init__(self, *args, **kwargs)
        self.solr_url = config['SOLR_URL']

    def authorize_session(self, privilege=None):
        """
        Use beaker to generate a fully encrypted fake session cookie, and
        put a fake user id into it.  This allows the request to bypass
        authentication

        """
        mock_request = dict()

        if privilege == Privileges.ADMIN:
            user_id = config['pylons.app_globals'].ADMINS[0]
        elif privilege == Privileges.REVIEWER:
            user_id = config['pylons.app_globals'].REVIEWERS[0]
        elif privilege == Privileges.CATALOGER:
            user_id = config['pylons.app_globals'].CATALOGERS[0]
        else:
            user_id = 1000
        cookie = base64.b64encode('Test User:::::%d' % user_id)
        self.app.cookies[config['ARTSTOR_COOKIE']] = cookie
            
        cs = CookieSession(mock_request, config['beaker.session.key'],
                           encrypt_key=config['beaker.session.encrypt_key'],
                           validate_key=config['beaker.session.validate_key'],
                           timeout=config['beaker.session.timeout']);
        cs['user'] = User(dict(id=user_id, username='testuser'))
        cs['seen_terms'] = True
        cs.save()
        parts = mock_request['cookie_out'].split('=')
        self.app.cookies[parts[0].strip()] = "=".join(parts[1:])
        
            
