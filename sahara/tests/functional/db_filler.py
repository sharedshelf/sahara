"""Load data for testing, uses the standard interfaces to do so"""
from datetime import datetime

from paste.util.multidict import MultiDict
from routes import url_for

from sahara.model import *

class DbFiller(object):

    BOGUS_RECORD = '100'
    DANGEROUS_RECORD = '200'
    DANGEROUS_COMPLETE_RECORD = '300'
    MINIMUM_VALID_RECORD = '400'

    VALID_RECORDS = [MINIMUM_VALID_RECORD, '401', '402', DANGEROUS_RECORD]
    INVALID_RECORDS = ['201', '202', BOGUS_RECORD]
    COMPLETE_RECORDS = ['301', '302', DANGEROUS_COMPLETE_RECORD]

    def __init__(self, app, solr_url):
        self.app = app
        self.solr_url = solr_url
        
    def generate(self):
        self._create_work(self.BOGUS_RECORD, self.bogus_record())
        self._create_work(self.DANGEROUS_RECORD, self.dangerous_record())
        self._create_work(self.DANGEROUS_COMPLETE_RECORD,
                          self.dangerous_record(),
                          complete=True)
        self._create_work(self.MINIMUM_VALID_RECORD,
                          self.minimum_valid_record())
        for uid in self.VALID_RECORDS:
            self._create_work(uid, self.valid_fully_cataloged_record())
        for uid in self.INVALID_RECORDS:
            self._create_work(uid, self.invalid_record())
        for uid in self.COMPLETE_RECORDS:
            self._create_work(uid, self.valid_fully_cataloged_record(),
                              complete=True)
        # Contribute the complete records
        contribute = MultiDict()
        for uuid in self.COMPLETE_RECORDS:
            contribute.add('uuid', uuid)
        response = self.app.post(url=url_for('multicontribute_execute'),
                                 params=contribute,
                                 status=200)

    def clear(self):
        works = Session.query(Work).all()
        for work in works:
            Session.delete(work)
        Session.commit()
        SearchEngine(self.solr_url).delete_all()

    def _populate_data(self, work, serialization):
        response = self.app.post(url=url_for('update', uuid=work.uuid),
                                 params=serialization,
                                 status=200)

    def _create_work(self, uuid, serialization, complete=False):
        w = Work.get_by(uuid=uuid)
        if w is not None:
            return w
        w = Work()
        w.media_uuid = uuid+':URL' # used for rendering search results
        w.created_by = 1000
        w.updated_by = 1000
        w.uuid = uuid
        Session.add(w)
        Session.commit()
        self._populate_data(w, serialization)
        return w

    def invalid_record(self):
        data = dict()
        data['complex_title'] = 'Invalid Complex Title'
        data['image_view'] = 'Invalid Image View'
        data['creator-0_extent'] = 'Invalid Extent One'
        data['creator-0_extent'] = 'Invalid Extent Two'
        data['creator-0_attribution'] = '1'
        data['street_address'] = 'Invalid 123 Main Street'
        data['city_county'] = 'Invalid New York'
        data['state_province'] = 'Invalid New York'
        data['latitude'] = '34.077563'
        data['longitude'] = '-118.474629'
        data['measurements'] = 'Invalid Measurements'
        data['display_date'] = 'Invalid Display Date'
        data['period_dynasty'] = ['Invalid Period Dynasty One', 'Invalid Period Dynasty Two']
        data['style'] = 'Invalid Very styling indeed'
        data['information_source'] = 'Invalid Information Source'
        data['photographer'] = 'Invalid Photographer'
        data['contributor'] = 'Invalid Contributor'
        data['image_date'] = 'Invalid 10/10/2009'
        data['image_copyright'] = 'Invalid Image Copyright'
        data['image_ownership'] = 'false'
        return data

    def bogus_record(self):
        data = dict()
        data['title'] = 'Bogus Title'
        data['creator-0_name'] = 'Bogus Creator'
        data['creator-0_nationality'] = 'Bogus Nationality'
        data['repository'] = 'Bogus repository'
        data['street_address'] = 'Bogus 123 Main Street'
        data['city_county'] = 'Bogus City'
        data['measurements'] = 'Bogus Measurements'
        data['display_date'] = 'Bogus 18th Century'
        data['earliest_date'] = '1000'
        data['description'] = 'Bogus description'
        data['image_ownership'] = 'true'
        data['photographer'] = 'Bogus Photographer'
        data['contributor'] = 'Bogus Contributor'
        data['image_date'] = 'Bogus 10/10/2009'
        data['image_copyright'] = 'Bogus Image Copyright'
        data['image_ownership'] = 'false'
        return data


    def minimum_valid_record(self):
        data = dict()
        data['view_type'] = '1'
        data['title'] = 'Valid Title One'
        data['classifications'] = 'Drawings and Watercolors'
        data['creator-0_name'] = 'Valid'
        data['city_county'] = 'Valid Suffolk County'
        data['country'] = '1'
        data['display_date'] = 'Valid 18th Century'
        data['photographer'] = 'Valid Photographer'
        data['contributor'] = 'Valid Contributor'
        data['image_copyright'] = 'Valid Image Copyright'
        data['image_ownership'] = 'true'
        return data

    def dangerous_record(self):
        data = dict()
        data['complex_title'] = "Dangerous <script>alert('evil')</script>"
        data['title'] = "Dangerous <script>alert('evil')</script>"
        data['view_type'] = '1'
        data['classifications'] = 'Drawings and Watercolors'
        data['creator-0_name'] = "Dangerous <script>alert('evil')</script>"
        data['street_address'] = "Dangerous <script>alert('evil')</script>"
        data['city_county'] = "Dangerous <script>alert('evil')</script>"
        data['country'] = '1'
        data['measurements'] = 'Dangerous Measurements'
        data['display_date'] = 'Dangerous 18th Century'
        data['photographer'] = "Dangerous <script>alert('evil')</script>"
        data['contributor'] = "Dangerous <script>alert('evil')</script>"
        data['image_copyright'] = "Dangerous <script>alert('evil')</script>"
        data['image_ownership'] = 'true'
        return data

    def valid_fully_cataloged_record(self):
        data = MultiDict()
        data.add('complex_title', 'Valid Complex Title')
        data.add('title', 'Valid Title One')
        data.add('title', 'Valid Title Two')
        data.add('title', 'Valid Title Three')
        data.add('view_type', '4')
        data.add('classifications', 'Architecture and City Planning')
        data.add('classifications', 'Drawings and Watercolors')
        data.add('narrow_classification', '4')
        data.add('creator-0_name', 'Valid Name, Creator One')
        data.add('creator-0_nationality', 'Valid Nationality, Creator One')
        data.add('creator-0_extent', 'Valid Extent One, Creator One')
        data.add('creator-0_extent', 'Valid Extent Two, Creator One')
        data.add('creator-0_extent', 'Valid Extent Three, Creator One')
        data.add('creator-0_extent', 'Valid Extent Four, Creator One')
        data.add('creator-0_attribution', '4')
        data.add('creator-0_role', '1')
        data.add('creator-0_role', '3')
        data.add('creator-0_role', '5')
        data.add('creator-1_name', 'Valid Name, Creator Two')
        data.add('creator-1_nationality', 'Valid Nationality One, Creator Two')
        data.add('creator-1_nationality', 'Valid Nationality Two, Creator Two')
        data.add('creator-1_nationality', 'Valid Nationality Three, Creator Two')
        data.add('creator-1_extent', 'Valid Extent One, Creator Two')
        data.add('creator-1_extent', 'Valid Extent Two, Creator Two')
        data.add('creator-1_extent', 'Valid Extent Three, Creator Two')
        data.add('creator-1_extent', 'Valid Extent Four, Creator Two')
        data.add('creator-1_attribution', '4')
        data.add('creator-1_role', '2')
        data.add('creator-1_role', '4')
        data.add('creator-1_role', '6')
        data.add('street_address', 'Valid 20 East Main Street')
        data.add('city_county', 'Valid Tampa')
        data.add('state_province', 'Valid Florida')
        data.add('country', '1')
        data.add('country', '5')
        data.add('country', '14')
        data.add('latitude', '+88.077563')
        data.add('longitude', '-18.474629')
        data.add('repository', 'Valid Repository')
        data.add('repository_id', 'Valid Rep.ID')
        data.add('display_date', 'Valid Display Date')
        data.add('earliest_date', '-900')
        data.add('latest_date', '900')
        data.add('period_dynasty', 'Valid Period Dynasty One')
        data.add('period_dynasty', 'Valid Period Dynasty Two')
        data.add('material_technique', 'Valid Material Technique')
        data.add('measurements', 'Valid Measurements')
        data.add('description', 'Valid This is a description')
        data.add('commentary', 'Valid This is a commentary')
        data.add('style', 'Valid Very styling indeed')
        data.add('keywords', 'big,bold,black,gate')
        data.add('information_source', 'Valid Information Source')
        data.add('photographer', 'Valid Photographer')
        data.add('contributor', 'Valid Contributor')
        data.add('inst_contributor', 'Valid Institutional Contributor')
        data.add('image_date', 'Valid 10/10/2009')
        data.add('image_earliest_date', '2700')
        data.add('image_latest_date', '2800')
        data.add('image_copyright', 'Valid Image Copyright')
        data.add('image_ownership', 'True')
        data.add('academic_publishing', 'True')
        return data
