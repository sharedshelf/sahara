import simplejson

from sahara.tests import *

class TestAnaController(TestController):

    def __init__(self, *args):
        super(TestAnaController, self).__init__(*args)

    def setUp(self):
        self.app.reset()
        self.authorize_session()

    def test_index(self):
        response = self.app.get(url('ana'), params=dict(query='chi'), status=200)
        data = simplejson.loads(response.body)
        assert(type(data) == dict)
