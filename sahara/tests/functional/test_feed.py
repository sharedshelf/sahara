import simplejson

from paste.util.multidict import MultiDict
from sahara.tests import *
from sahara.model import *
from sahara.tests.functional.db_filler import DbFiller

class TestFeedController(TestController):

    def __init__(self, *args):
        super(TestFeedController, self).__init__(*args)
        self.filler = DbFiller(self.app, self.solr_url)

    def setUp(self):
        self.app.reset()
        self.authorize_session()
        self.filler.generate()

    def tearDown(self):
        self.filler.clear()

    def test_full_feed(self):
        response = self.app.get(url=url_for('feed', date='01-01-1971-00-00-00'), status=200)
        resp_data = simplejson.loads(response.body)
        self.assertEqual(3, len(resp_data['uuids']))
        
        
