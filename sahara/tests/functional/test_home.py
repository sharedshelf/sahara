import simplejson

from sahara.tests import *

class TestHomeController(TestController):

    def __init__(self, *args):
        super(TestHomeController, self).__init__(*args)

    def setUp(self):
        self.app.reset()
        self.authorize_session()

    def test_collections_totals(self):
        response = self.app.post(url=url_for('totals'),
                                 status=200)
        data = simplejson.loads(response.body)
        assert('members_objects' in data.keys())
        assert('editors_objects' in data.keys())
        assert(data['members_objects'] > 0)
        assert(data['editors_objects'] > 0)

