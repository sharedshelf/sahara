import simplejson
from lxml import html

from paste.util.multidict import MultiDict
from sahara.tests import *
from sahara.model import *
from sahara.tests.functional.db_filler import DbFiller

class TestSearchController(TestController):

    def __init__(self, *args):
        super(TestSearchController, self).__init__(*args)
        self.filler = DbFiller(self.app, self.solr_url)

    def setUp(self):
        self.app.reset()
        self.authorize_session()
        self.filler.generate()

    def tearDown(self):
        self.filler.clear()

    def test_simple_search(self):
        params = dict(phrase='Valid')
        response = self.app.post(url=url_for('search'),
                                 params=params,
                                 status=200)
        doc = html.document_fromstring(response.body)
        results = doc.xpath("//td[@class='info-wrapper']")
        self.assertEqual(2, len(results))

    def test_no_results_search(self):
        params = dict(phrase='missing')
        response = self.app.post(url=url_for('search'),
                                 params=params,
                                 status=200)
        self.assert_("0 results" in response.body)

    def test_fielded_search(self):
        params = dict(phrase='title:bogus complete:false')
        response = self.app.post(url=url_for('search'),
                                 params=params,
                                 status=200)
        doc = html.document_fromstring(response.body)
        results = doc.xpath("//td[@class='info-wrapper']")
        self.assertEqual(1, len(results))
        
    def test_unicode_search(self):
        w = Work.get_by(uuid=self.filler.DANGEROUS_COMPLETE_RECORD)
        w.complex_title = '\xe6\xb3\xb0\xe5\x9b\xbd'.decode('utf-8')
        Session.add(w)
        Session.commit()
        params = dict(phrase='complex_title: ' + w.complex_title.encode('utf-8'))
        response = self.app.post(url=url_for('search'),
                                 params=params,
                                 status=200)
        doc = html.document_fromstring(response.body)
        results = doc.xpath("//td[@class='info-wrapper']")
        self.assertEqual(1, len(results))

    def test_dangerous_search_list_view(self):
        params = dict(phrase='dangerous')
        response = self.app.post(url=url_for('search'),
                                 params=params,
                                 status=200)
        doc = html.document_fromstring(response.body)
        results = doc.xpath("//td[@class='info-wrapper']")
        self.assertEqual(1, len(results))
        self.assert_("<script>alert" not in response.body)

    def test_dangerous_search_tabular_view(self):
        params = dict(phrase='dangerous', tabular=True)
        response = self.app.post(url=url_for('search'),
                                 params=params,
                                 status=200)
        doc = html.document_fromstring(response.body)
        results = doc.xpath("//td[@class='info-wrapper']")
        self.assertEqual(1, len(results))
        self.assert_("<script>alert" not in response.body)
        
        

    def _create_work(self, uuid):
        w = Work.get_by(uuid=uuid)
        if w is not None:
            Session.delete(w)
            Session.commit()
        w = Work()
        w.created_by = ARTSTOR_USER_ID
        w.updated_by = ARTSTOR_USER_ID
        w.uuid = uuid
        Session.add(w)
        Session.commit()
        return w

