import simplejson

from paste.util.multidict import MultiDict
from sahara.tests import *
from sahara.model import *
from sahara.tests.functional.db_filler import DbFiller

class TestUserController(TestController):

    def __init__(self, *args):
        super(TestUserController, self).__init__(*args)

    def setUp(self):
        self.app.reset()
        self.authorize_session()

    def test_getting_privileged_users(self):
        response = self.app.get(url=url_for('priveleged'), status=200)
        data = simplejson.loads(response.body)
        assert("reviewers" in data.keys())
        assert("catalogers" in data.keys())
        assert("admins" in data.keys())



