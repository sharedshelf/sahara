import simplejson
from lxml import etree

from paste.util.multidict import MultiDict
from sahara.tests import *
from sahara.model import *
from sahara.tests.functional.db_filler import DbFiller

class TestWorkController(TestController):

    def __init__(self, *args):
        super(TestWorkController, self).__init__(*args)
        self.filler = DbFiller(self.app, self.solr_url)

    def setUp(self):
        self.app.reset()
        self.authorize_session()
        self.filler.generate()

    def tearDown(self):
        self.filler.clear()

    def test_redirect_to_login(self):
        self.app.reset()
        response = self.app.get(url=url_for('in_process'), status=302)

    def test_reserves_work(self):
        self.authorize_session(privilege=Privileges.ADMIN)
        work_id = self.filler.VALID_RECORDS[0]
        response = self.app.post(
            url=url_for('reserve', uuid=work_id),
            status=200)
        resp_data = simplejson.loads(response.body)
        assert resp_data['complete'] is True
        
    def test_reserve_returns_404_on_invalid_id(self):
        response = self.app.post(
            url=url_for('reserve', uuid=''),
            status=404)
    
    def test_unreserves_work(self):
        self.authorize_session(privilege=Privileges.ADMIN)
        work_id = self.filler.VALID_RECORDS[0]
        response = self.app.post(
            url=url_for('unreserve', uuid=work_id),
            status=200)
        resp_data = simplejson.loads(response.body)
        assert resp_data['complete'] is True
    
    def test_unreserve_returns_404_on_invalid_id(self):
        response = self.app.post(
            url=url_for('unreserve', uuid=''),
            status=404)

    def test_marks_work_reviewed(self):
        self.authorize_session(privilege=Privileges.ADMIN)
        work_id = self.filler.VALID_RECORDS[0]
        response = self.app.post(
            url=url_for('mark_reviewed', uuid=work_id),
            status=200)
        resp_data = simplejson.loads(response.body)
        assert resp_data['complete'] is True

    def test_mark_reviewed_returns_404_on_invalid_id(self):
        response = self.app.post(
            url=url_for('reserve', uuid=''),
            status=404)

    def test_assigns_work(self):
        self.authorize_session(privilege=Privileges.ADMIN)
        work_id = self.filler.VALID_RECORDS[0]
        response = self.app.post(
            url=url_for('assign', uuid=work_id) + "?userid=1&userid=2&userid=3",
            status=200)
        resp_data = simplejson.loads(response.body)
        assert resp_data['complete'] is True

    def test_contribute_returns_404_on_invalid_id(self):
        response = self.app.post(
            url=url_for('contribute', uuid=''),
            params=dict(
                title='Hello world',
                created_by=1,
                updated_by=1
                ),
            status=404)

    def test_contribute_returns_errors_on_invalid_record(self):
        response = self.app.post(
            url=url_for('contribute', uuid=self.filler.INVALID_RECORDS[0]),
            params=dict(
                title_complex='complex'
                ),
            status=200)
        resp_data = simplejson.loads(response.body)
        assert resp_data['complete'] is False

    def test_saving_partial_record_and_filling_in_later(self):
        uuid = '12345678900987654321'
        self._create_work(uuid)
        response = self.app.post(url=url_for('update', uuid=uuid),
                                 params=self.filler.invalid_record(),
                                 status=200)
        assert 'saved' in response, "Full record not saved successfully"
        w = Work.get_by(uuid=uuid)
        assert w.status.valid is False, "Invalid work flagged as valid"
        response = self.app.post(url=url_for('update', uuid=uuid),
                                 params=self.filler.valid_fully_cataloged_record(),
                                 status=200)
        assert 'saved' in response, "Partial record not saved successfully"
        w = Work.get_by(uuid=uuid)
        self.assertEqual(u'Valid Complex Title', w.name.complex_title)
        assert u'Valid Title One' in [t.value for t in w.name.titles], 'Record not updated correctly on multiple edits'
        self.assert_('Valid' in w.location.city_county)
        self.assertTrue(w.is_contributable())
        self.assertTrue(w.status.valid)

    def test_minimum_valid_record_marked_as_valid(self):
        w = Work.get_by(uuid=self.filler.MINIMUM_VALID_RECORD)
        self.assertEqual(True, w.is_contributable()[0]) # The validation method on Work
        self.assertEqual(True, w.status.valid) # The stored validation value

    def test_posting_fully_cataloged_record(self):
        uuid = '12345678900987654321'
        w = self._create_work(uuid)
        response = self.app.post(url=url_for('update', uuid=uuid),
                                 params=self.filler.valid_fully_cataloged_record(),
                                 status=200)
        resp_data = simplejson.loads(response.body)
        assert resp_data['valid'] is True, "Full cataloged record not saved successfully"
        w = Work.get_by(uuid=uuid)
        self.assertTrue(w.is_contributable()[0]) # The validation method on Work
        self.assertTrue(w.status.valid) # The stored validation value


    def test_posting_invalid_record_over_valid_one_updates_valid_flag_to_false(self):
        uuid = self.filler.VALID_RECORDS[0]
        w = Work.get_by(uuid=uuid)
        self.assertTrue(w.is_contributable()[0])
        response = self.app.post(url=url_for('update', uuid=uuid),
                                 params=self.filler.invalid_record(),
                                 status=200)
        assert 'saved' in response, "Partial record not saved successfully"
        w = Work.get_by(uuid=uuid)
        self.assertEqual(False, w.is_contributable()[0]) # The validation method on Work
        self.assertEqual(False, w.status.valid) # The stored validation value

    def test_xss_vunerability(self):
        response = self.app.get(url=url_for('edit', uuid=self.filler.DANGEROUS_RECORD), status=200)
        assert "<script>alert('evil')</script>" not in response

    def test_copy_valid_record_to_invalid_records(self):
        valid = self.filler.VALID_RECORDS[1]
        invalids = self.filler.INVALID_RECORDS
        params = MultiDict()
        for invalid in invalids:
            params.add('works', invalid)
        params.add('fields', 'title')
        params.add('fields', 'creator')
        params.add('fields', 'location')
        params.add('fields', 'chronology')
        params.add('fields', 'physical')
        params.add('fields', 'subject')
        params.add('fields', 'source')
        params.add('fields', 'rights')
        response = self.app.post(url=url_for('copy_execute', uuid=valid),
                                 params=params,
                                 status=200)
        for invalid in invalids:
            w = Work.get_by(uuid=invalid)
            assert 'Valid Complex' in w.name.complex_title #name copied
            assert 'Valid Title One' == w.name.titles[0].value # title copied
            assert 3 == len(w.name.titles)
            assert 2 == len(w.creators) #creator 
            assert 'Valid' in w.location.street_address #location copied
            assert 'Valid' in w.chronology.display_date #chrono copied
            assert 'Valid' in w.physical_description.measurements #phsyical copied
            assert 'Valid' in w.source.photographer # source copied
            assert 'Valid' in w.rights.image_copyright # rights copied

    def test_copy_partial_record_to_full_record(self):
        invalid = self.filler.BOGUS_RECORD
        valids = self.filler.VALID_RECORDS
        params = MultiDict()
        for valid in valids:
            params.add('works', valid)
        params.add('fields', 'title')
        params.add('fields', 'creator')
        params.add('fields', 'location')
        params.add('fields', 'chronology')
        params.add('fields', 'physical')
        params.add('fields', 'subject')
        params.add('fields', 'source')
        params.add('fields', 'rights')
        response = self.app.post(url=url_for('copy_execute', uuid=invalid),
                                 params=params,
                                 status=200)
        for valid in valids:
            w = Work.get_by(uuid=valid)
            self.assert_('Bogus' in w.name.titles[0].value) # title copied
            assert 1 == len(w.name.titles)
            assert 1 == len(w.creators) #creator 
            self.assert_('Bogus' in w.location.street_address) #location not copied
            self.assert_('Bogus' in w.chronology.display_date) #chrono not copied
            self.assert_('Bogus' in w.physical_description.measurements) #phsyical not copied
            self.assert_('Bogus' in w.source.photographer) # source not copied
            self.assert_('Bogus' in w.rights.image_copyright) # rights not copied

    def test_copy_only_title_section_of_full_record_to_partial_records(self):
        valid = self.filler.VALID_RECORDS[1]
        invalids = self.filler.INVALID_RECORDS
        params = MultiDict()
        for invalid in invalids:
            params.add('works', invalid)
        params.add('fields', 'title')
        response = self.app.post(url=url_for('copy_execute', uuid=valid),
                                 params=params,
                                 status=200)
        for invalid in invalids:
            w = Work.get_by(uuid=invalid)
            self.assert_('Complex' in w.name.complex_title) #name copied
            self.assertEqual('Valid Title One', w.name.titles[0].value) # title copied
            assert 3 == len(w.name.titles)
            assert 1 == len(w.creators) #creator not copied 
            self.assert_('Valid' not in w.location.street_address) #location not copied
            self.assert_('Valid' not in w.chronology.display_date) #chrono not copied
            self.assert_('Valid' not in w.physical_description.measurements) #phsyical not copied
            self.assert_('Valid' not in w.source.photographer) # source not copied
            self.assert_('Valid' not in w.rights.image_copyright) # rights not copied

    def test_multicontribute_valid_records(self):
        valids = self.filler.VALID_RECORDS
        params = MultiDict()
        for valid in valids:
            params.add('uuid', valid)
        response = self.app.post(url=url_for('multicontribute_execute'),
                                 params=params,
                                 status=200)
        for valid in valids:
            w = Work.get_by(uuid=valid)
            self.assertTrue(w.status.complete)

    def test_multicontribute_invalid_records(self):
        recs = self.filler.INVALID_RECORDS
        params = MultiDict()
        for rec in recs:
            params.add('uuid', rec)
        response = self.app.post(url=url_for('multicontribute_execute'),
                                 params=params,
                                 status=200)
        for rec in recs:
            w = Work.get_by(uuid=rec)
            self.assertEqual(False, w.status.complete)

    def test_multicontribute_mixed_records(self):
        recs = [self.filler.BOGUS_RECORD, self.filler.MINIMUM_VALID_RECORD]
        params = MultiDict()
        for rec in recs:
            params.add('uuid', rec)
        response = self.app.post(url=url_for('multicontribute_execute'),
                                 params=params,
                                 status=200)
        w = Work.get_by(uuid=recs[0])
        self.assertEqual(False, w.status.complete)
        w = Work.get_by(uuid=recs[1])
        self.assertEqual(True, w.status.complete)
        
    def test_multidelete_invalid_records(self):
        invalids = self.filler.INVALID_RECORDS
        params = MultiDict()
        for invalid in invalids:
            params.add('uuid', invalid)
        response = self.app.post(url=url_for('multidelete_execute'),
                                 params=params,
                                 status=200)
        for invalid in invalids:
            w = Work.get_by(uuid=invalid)
            self.assertEqual(None, w)

    def test_cannot_multidelete_contributed_records(self):
        recs = self.filler.COMPLETE_RECORDS
        params = MultiDict()
        for rec in recs:
            params.add('uuid', rec)
        response = self.app.post(url=url_for('multidelete_execute'),
                                 params=params,
                                 status=200)
        for rec in recs:
            w = Work.get_by(uuid=rec)
            self.assert_(w is not None)

    def test_getting_work_xml(self):
        response = self.app.get(url=url_for('show',
                                            uuid=self.filler.COMPLETE_RECORDS[0],
                                            format='xml'), status=200)
        doc = etree.fromstring(response.body)
        self.assertEqual('sahara', doc.tag)

    def test_suppress_and_unsupress(self):
        self.authorize_session(privilege=Privileges.ADMIN)
        uuid = self.filler.COMPLETE_RECORDS[0]
        response = self.app.post(url=url_for('suppress', uuid=uuid), status=200)
        w = Work.get_by(uuid=uuid)
        self.assertTrue(w.status.suppress)
        response = self.app.post(url=url_for('unsuppress', uuid=uuid), status=200)
        w = Work.get_by(uuid=uuid)
        self.assertFalse(w.status.suppress)

    def test_can_update_narrow_classification_with_null(self):
        uuid = '1000'
        data = dict(classifications='Garden and Landscape',
                    narrow_classification=2)
        self._create_work(uuid)
        response = self.app.post(url=url_for('update', uuid=uuid),
                                 params=data,
                                 status=200)
        w = Work.get_by(uuid=uuid)
        self.assertEqual(2, w.name.narrow_classification.id)
        data = dict(classifications='Garden and Landscape',
                    narrow_classification=None)
        response = self.app.post(url=url_for('update', uuid=uuid),
                                 params=data,
                                 status=200)
        w = Work.get_by(uuid=uuid)
        self.assertEqual(None, w.name.narrow_classification)
        
    def test_cant_update_narrow_classification_without_required_classification(self):
        uuid = '1000'
        data = dict(classifications='Drawings and Watercolors',
                    narrow_classification=2)
        self._create_work(uuid)
        response = self.app.post(url=url_for('update', uuid=uuid),
                                 params=data,
                                 status=200)
        w = Work.get_by(uuid=uuid)
        self.assertEqual(None, w.name.narrow_classification)


    def _create_work(self, uuid):
        w = Work.get_by(uuid=uuid)
        if w is not None:
            Session.delete(w)
            Session.commit()
        w = Work()
        w.created_by = 1000
        w.updated_by = 1000
        w.uuid = uuid
        Session.add(w)
        Session.commit()
        return w

