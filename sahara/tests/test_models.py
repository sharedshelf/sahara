from datetime import datetime

from lxml import etree
from paste.util.multidict import MultiDict

from sahara.tests import *
from sahara.model import *
from sahara.lib import form_parser
from sqlalchemy.exceptions import *


class TestWorkModel(TestCase):

    def setUp(self):
        Session.remove()
    
    def tearDown(self):
        Session.remove()
        
    def test_valid_simple_work(self):
        work = Work()
        work.created_by = 1
        work.updated_by = 1
        Session.add(work)
        Session.commit()

    def test_work_empty_created_by(self):
        try:
            work = Work()
            work.updated_by = 1
            Session.add(work)
            Session.commit()
            fail('Work should not allow an empty created_by')
        except IntegrityError:
            pass
        
    def test_work_empty_updated_by(self):
        try:
            work = Work()
            work.created_by = 1
            Session.add(work)
            Session.commit()
            fail('Work should not allow an empty updated_by')
        except IntegrityError:
            pass

    def test_work_brief_filename(self):
        work = Work()
        work.media_filename = "One_really_really_really_really_long_filename.jpg"
        self.assertEqual(15, len(work.brief_filename(size=0)))
        self.assertEqual(30, len(work.brief_filename(size=1)))
        self.assertEqual(45, len(work.brief_filename(size=2)))
        self.assertRaises(AssertionError, work.brief_filename, size=-1)
        self.assertRaises(AssertionError, work.brief_filename, size=3)

    def test_work_is_edited(self):
        work = Work()
        self.assertEqual(False, work.is_edited())
        work.updated_on = datetime.now()
        self.assertEqual(True, work.is_edited())

    def test_work_validation_fails_on_empty_work(self):
        work = self._create_valid_work()
        self.assertEqual(False, work.validate()[0])
    
    def test_work_reservation(self):
        work = self._create_valid_work()
        self.assert_(not work.reserved_by())
        work.reserve(1)
        self.assertEqual(1, work.reserved_by())
        
    def test_work_reviewed(self):
        work = self._create_valid_work()
        self.assert_(not work.reviewed())
        work.mark_reviewed()
        self.assert_(work.reviewed())

    def test_work_assignment(self):
        work = self._create_valid_work()
        self.assertEqual(0, len(work.assignments))
        work.assign_to([1,2,3])
        self.assertEqual(3, len(work.assignments))
        self.assertEqual([1,2,3], [a.user_id for a in work.assignments])
        work.assign_to([4,5])
        self.assertEqual(2, len(work.assignments))
        self.assertEqual([4,5], [a.user_id for a in work.assignments])

    def test_work_rollback(self):
        # create some history
        work = self._completed_work_record()
        work.contribute()
        Session.add(work)
        Session.commit()
        orig_xml = work.to_xml()
        form_parser.parse(work, self._completed_work_post_encoded(), userid=1)
        Session.commit()
        new_xml = work.to_xml()
        work.rollback(work.history[0].id, userid=1)
        # Can't test string equality because time stamp was updated.
        self.assertEqual(len(orig_xml), len(work.to_xml()))
        orig_doc = etree.XML(orig_xml)
        rolled_back_doc = etree.XML(work.to_xml())
        self.assertEqual(etree.tostring(orig_doc.find('./data')),
                         etree.tostring(rolled_back_doc.find('./data')))
        Session.commit()
        work.rollback(work.history[0].id, userid=1)
        self.assertEqual(len(new_xml), len(work.to_xml()))
        new_doc = etree.XML(new_xml)
        rolled_back_doc = etree.XML(work.to_xml())
        self.assertEqual(etree.tostring(new_doc.find('./data')),
                         etree.tostring(rolled_back_doc.find('./data')))
        
    # Rights model tests
    def test_rights_validation_fails_on_empty_rights(self):
        rights = Rights()
        self.assertEqual(False, rights.validate()[0])
    
    def test_rights_validation_fails_on_image_ownership(self):
        rights = Rights()
        rights.image_copyright = "Copyright 2009"
        self.assertEqual(False, rights.validate()[0])
        assert 'You must have the rights to this image in order to contribute it.' in \
            rights.validate()[1]

    def test_rights_validation_fails_on_image_copyright(self):
        rights = Rights()
        rights.image_ownership = True
        self.assertEqual(False, rights.validate()[0])

    def test_rights_validation_passes_for_valid_record(self):
        rights = Rights()
        rights.image_copyright = "Copyright 2009"
        rights.image_ownership = True
        self.assertEqual(True, rights.validate()[0])


    # Name model tests
    def test_name_validation_fails_on_empty_record(self):
        name = Name()
        self.assertEqual(False, name.validate()[0])
    
    def test_name_validation_fails_on_empty_view_type(self):
        name = Name()
        name.classifications = [Classification.get(1)]
        name.titles = [Title()]
        self.assertEqual(False, name.validate()[0])

    def test_name_validation_fails_on_empty_classifications(self):
        name = Name()
        name.view_type = ViewType()
        name.titles = [Title()]
        self.assertEqual(False, name.validate()[0])

    def test_name_validation_fails_on_empty_titles(self):
        name = Name()
        name.view_type = ViewType()
        name.titles = []
        name.classifications = [Classification.get(1)]
        self.assertEqual(False, name.validate()[0])

    def test_name_validation_fails_on_narrow_classification(self):
        name = Name()
        name.view_type = ViewType()
        # When classification includes 'Architecture and City Planning' or
        # 'Garden and Landscape' narrow_classification is required.
        name.classifications = [Classification.get(1)] # Architecture and City Planning 
        name.titles = [Title()]
        self.assertEqual(False, name.validate()[0])

    def test_name_validation_passes_for_valid_record(self):
        name = Name()
        name.view_type = ViewType()
        name.classifications = [Classification.get(2)]
        name.titles = [Title()]
        self.assertEqual(True, name.validate()[0])

    # Location model tests
    def test_location_validation_fails_on_empty_record(self):
        loc = Location()
        self.assertEqual(False, loc.validate()[0])
    
    def test_location_validation_fails_on_empty_city_county(self):
        loc = Location()
        loc.countries = [Country.get(1)]
        self.assertEqual(False, loc.validate()[0])

    def test_location_validation_fails_on_empty_countries(self):
        loc = Location()
        loc.city_county = "New York"
        self.assertEqual(False, loc.validate()[0])

    def test_locatoin_validation_passes_for_valid_record(self):
        loc = Location()
        loc.countries = [Country.get(1)]
        loc.city_county = "New York"
        self.assertEqual(True, loc.validate()[0])

    # Chronolgy model tests
    def test_chronology_validation_fails_on_empty_record(self):
        c = Chronology()
        self.assertEqual(False, c.validate()[0])
    
    def test_chronology_validation_passes_for_valid_record(self):
        c = Chronology()
        c.display_date = "Date"
        self.assertEqual(True, c.validate()[0])

    # PhysicalDescription model tests
    def test_physical_description_validation_passes_for_valid_record(self):
        p = PhysicalDescription()
        self.assertEqual(True, p.validate()[0])

    # Source model tests
    def test_source_validation_fails_on_empty_record(self):
        s = Source()
        self.assertEqual(False, s.validate()[0])
    
    def test_source_validation_fails_on_empty_contributor(self):
        s = Source()
        s.photographer = "Photographer"
        self.assertEqual(False, s.validate()[0])

    def test_source_validation_fails_on_empty_photographer(self):
        s = Source()
        s.contributor = "Contributor"
        self.assertEqual(False, s.validate()[0])

    def test_source_validation_passes_for_valid_record(self):
        s = Source()
        s.photographer = "Photographer"
        s.contributor = "Contributor"
        self.assertEqual(True, s.validate()[0])

    # Rights model tests
    def test_rights_validation_fails_on_empty_record(self):
        r = Rights()
        self.assertEqual(False, r.validate()[0])
    
    def test_rights_validation_fails_on_empty_image_copyright(self):
        r = Rights()
        r.image_ownership = True
        self.assertEqual(False, r.validate()[0])

    def test_rights_validation_fails_on_empty_image_ownership(self):
        r = Rights()
        r.image_copyright = "Image Copyright"
        self.assertEqual(False, r.validate()[0])

    def test_rights_validation_fails_on_false_image_ownership(self):
        r = Rights()
        r.image_copyright = "Image Copyright"
        r.image_ownership = False
        self.assertEqual(False, r.validate()[0])

    def test_rights_validation_passes_for_valid_record(self):
        r = Rights()
        r.image_copyright = "Image Copyright"
        r.image_ownership = True
        self.assertEqual(True, r.validate()[0])

    def test_xml_generation(self):
        from xml.etree import ElementTree as ET
        w = self._completed_work_record()
        doc = ET.fromstring(w.to_xml())
        self.assertEqual('Title One',
                         doc.findtext('data/TitleInformation/Title'))
        assert 3 == len(doc.findall('data/TitleInformation/Title'))
        assert 'Architecture' in \
            doc.findtext('data/TitleInformation/Classification')

        assert 2 == len(doc.findall('data/Creator'))
        assert 4 == len(doc.find('data/Creator').findall('Extent'))
        
        
    def _create_valid_work(self):
        work = Work()
        work.created_by = 1
        work.updated_by = 1
        return work

    def _completed_work_record(self):
        data = MultiDict()
        data['complex_title'] = u'Complex Title'
        data['title'] = u'Title One'
        data.add('title', u'Title Two')
        data.add('title', u'Title Three')
        data['view_type'] = '4'
        data['classifications'] = u'Architecture and City Planning'
        data.add('classifications', u'Drawings and Watercolors')
        data['narrow_classification'] = '4'
        data['creator-0_name'] = u'Name, Creator One'
        data['creator-0_nationality'] = u'Nationality, Creator One'
        data['creator-0_extent'] = u'Extent One, Creator One'
        data.add('creator-0_extent', u'Extent Two, Creator One')
        data.add('creator-0_extent', u'Extent Three, Creator One')
        data.add('creator-0_extent',u'Extent Four, Creator One')
        data['creator-0_attribution'] = '4'
        data['creator-0_role'] = '1'
        data.add('creator-0_role', '3')
        data.add('creator-0_role', '5')
        data['creator-1_name'] = u'Name, Creator Two'
        data['creator-1_nationality'] = u'Nationality One, Creator Two'
        data.add('creator-1_nationality', u'Nationality Two, Creator Two')
        data.add('creator-1_nationality', u'Nationality Three, Creator Two')
        data['creator-1_extent'] = u'Extent One, Creator Two'
        data.add('creator-1_extent', u'Extent Two, Creator Two')
        data.add('creator-1_extent', u'Extent Three, Creator Two')
        data.add('creator-1_extent', u'Extent Four, Creator Two')
        data['creator-1_attribution'] = '4'
        data['creator-1_role'] = '2'
        data.add('creator-1_role', '4')
        data.add('creator-1_role', '6')
        data['street_address'] = u'123 Main Street'
        data['city_county'] = u'New York'
        data['state_province'] = u'New York'
        data['country'] = '1'
        data.add('country', '5')
        data.add('country', '14')
        data['latitude'] = '+34.077563'
        data['longitude'] = '-118.474629'
        data['repository'] = u'Repository'
        data['repository_id'] = u'Rep.ID'
        data['display_date'] = u'Display Date'
        data['earliest_date'] = '-100'
        data['latest_date'] = '100'
        data['period_dynasty'] = u'Period Dynasty One'
        data.add('period_dynasty', u'Period Dynasty Two')
        data['material_technique'] = u'Material Technique'
        data['measurements'] = u'Measurements'
        data['description'] = u'This is a description'
        data['commentary'] = u'This is a commentary'
        data['style'] = u'Very styling indeed'
        data['keywords'] = u'big,bold,black,gate'
        data['information_source'] = u'Information Source'
        data['photographer'] = u'Photographer'
        data['contributor'] = u'Contributor'
        data['inst_contributor'] = u'Institutional Contributor'
        data['image_date'] = u'10/10/2009'
        data['image_earliest_date'] = '2007'
        data['image_latest_date'] = '2008'
        data['image_copyright'] = u'Image Copyright'
        data['image_ownership'] = 'True'
        data['academic_publishing'] = 'True'
        data['other_contexts'] = 'CCANL'
        work = Work()
        work.created_by = 1
        work.updated_by = 1
        form_parser.parse(work, data, userid=1)
        return work

    def _completed_work_post_encoded(self):
        data = MultiDict()
        data['complex_title'] = u'ct1'
        data.add('complex_title', u'ct2')
        data['title'] = u't1'
        data.add('title', u't2')
        data['view_type'] = '1'
        data['classifications'] = u'Paintings'
        data['creator-0_name'] = u'c1'
        data['creator-0_nationality'] = u'c1n1'
        data['creator-0_extent'] = u'c1e1'
        data.add('creator-0_extent', u'c1e2')
        data['creator-0_attribution'] = '2'
        data['creator-0_role'] = '5'
        data['street_address'] = u'sa1'
        data['city_county'] = u'cc1'
        data['state_province'] = u'sp1'
        data['country'] = '50'
        data['latitude'] = '+55.077563'
        data['longitude'] = '-138.474629'
        data['repository'] = u'r1'
        data['repository_id'] = u'rid1'
        data['display_date'] = u'dd1'
        data['earliest_date'] = '200'
        data['latest_date'] = '400'
        data['period_dynasty'] = u'pd1'
        data['material_technique'] = u'mt1'
        data['measurements'] = u'm1'
        data['description'] = u'd1'
        data['commentary'] = u'c1'
        data['style'] = u's1'
        data['keywords'] = u'k1,k2'
        data['information_source'] = u'is1'
        data['photographer'] = u'p1'
        data['contributor'] = u'c1'
        data['inst_contributor'] = u'ic1'
        data['image_date'] = u'id1'
        data['image_earliest_date'] = '2000'
        data['image_latest_date'] = '2001'
        data['image_copyright'] = u'ic1'
        data['image_ownership'] = 'True'
        data['academic_publishing'] = 'True'
        data['other_contexts'] = 'CCANL'
        return data

