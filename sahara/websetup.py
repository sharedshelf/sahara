"""Setup the sahara application"""
import os
import logging

import yaml
from sahara.config.environment import load_environment

log = logging.getLogger(__name__)

def setup_app(command, conf, vars):
    """Place any commands to setup sahara here"""
    load_environment(conf.global_conf, conf.local_conf)

    # Load the models
    from sahara import model
    from sahara.model import *

    # For testing start clean every time
    filename = os.path.split(conf.filename)[-1]
    if filename.find('test') > -1:
        log.info('Dropping and recreating all tables')
        metadata.drop_all(checkfirst=True)
        
    # Create the tables if they aren't there already
    metadata.create_all(checkfirst=True)

    # Load data from fixtures
    path = os.path.split(__file__)[0]
    try:
        f = open("%s/model/data.yaml" % path)
        data = yaml.load(f)
        for m in data.keys():
            # Gets the model class from the model module
            klass = model.__getattribute__(m)
            # Check for existing data
            if Session.query(klass).count() == 0:
                print "Populating %s" % klass.__table__
                for map_ in data[m]:
                    instance = klass()
                    for key in map_.keys():
                        instance.__setattr__(key, unicode(map_[key]))
                    Session.add(instance)
                Session.commit()
    except:
        print "Failed to load data"
        raise
    finally:
        f.close()


